#include <stdlib.h>
#include <unistd.h>
#include "vt_platform.h"

#ifndef __MICROBLAZE__
#if ACCESS_INTERFACE == P2040_LB
const char* const get_lb_dev_name() 
{
	if(access("/dev/amc516_fpga_lb", F_OK) != -1) 
	{
		return "/dev/amc516_fpga_lb";
	}
	else if(access("/dev/amc517_fpga_lb", F_OK) != -1) 
	{
		return "/dev/amc517_fpga_lb";
	}
	else if(access("/dev/amc525_fpga_lb", F_OK) != -1) 
	{
		return "/dev/amc525_fpga_lb";
	}
	else if(access("/dev/amc583_fpga_lb", F_OK) != -1) 
	{
		return "/dev/amc583_fpga_lb";
	}
	else if(access("/dev/amc593_fpga_lb", F_OK) != -1) 
	{
		return "/dev/amc593_fpga_lb";
	}
	else 
		return NULL;
}
#elif ACCESS_INTERFACE == IMX6_LB
const char* const get_lb_dev_name() 
{
	if(access("/dev/vt510_fpga", F_OK) != -1) 
	{
		return "/dev/vt510_fpga";
	}	
	else 
		return NULL;
}
#endif
#endif /* #ifndef __MICROBLAZE__ */

int write_reg32(int fd, void* base, const unsigned int addr, const unsigned int write_data)
{
	volatile unsigned int* reg;

	if( fd > 0) {
#ifndef __MICROBLAZE__
#if ACCESS_INTERFACE == PCIE_DEV_DRV
		reg_t config;
		config.addr = addr;
		config.data = write_data;
		if(ioctl(fd, IOC_SET_REG32, &config))
		{
			perror("Error in calling IOC_SET_REG32 in function write_reg32");
			return EXIT_FAILURE;
		}
#elif ACCESS_INTERFACE == P2040_LB || ACCESS_INTERFACE == IMX6_LB
		lb_reg_t reg16;
		
		/* lower 2-byte write */
		reg16.offset = addr;
		reg16.value = (unsigned short) (write_data & 0xffff);
		
		if( ioctl( fd, LB_IOC_SET_REG, &reg16 ) ) {
			return EXIT_FAILURE;
		}
	
		/* higher 2-byte write */
		reg16.offset = addr + 2;
		reg16.value = (unsigned short) ((write_data >> 16) & 0xffff);
		
		if( ioctl( fd, LB_IOC_SET_REG, &reg16 ) ) {
			return EXIT_FAILURE;
		}
		
		/* optional additional delay if AXI write speed is too low */
#endif
#endif /* #ifndef __MICROBLAZE__ */
	}
	else {
		reg = (unsigned int*)(base + addr);
		*reg = write_data;
	}
	
	return EXIT_SUCCESS; 
}

int read_reg32(int fd, void* base, const unsigned int addr, unsigned int* read_data)
{
	volatile unsigned int* reg;
	
	if( fd > 0) {
#ifndef __MICROBLAZE__
#if ACCESS_INTERFACE == PCIE_DEV_DRV
		reg_t config;
		config.addr = addr;
		if(ioctl(fd, IOC_GET_REG32, &config))
		{
			perror("Error in calling IOC_GET_REG32 in function read_reg32");
			return EXIT_FAILURE;
		}
		*read_data = config.data;				

#elif ACCESS_INTERFACE == P2040_LB  || ACCESS_INTERFACE == IMX6_LB
		lb_reg_t reg16;
		
		/* lower 2-byte read */
		reg16.offset = addr;
		
		if( ioctl( fd, LB_IOC_GET_REG, &reg16 ) ) {
			return EXIT_FAILURE;
		}
		
		/* optional additional delay */
		
		/* READ_ONCE must be enabled in FPGA moduel */
		/* lower 2-byte read again in case the axi read didn't finish in the 1st read operation */
		/* read value is the latched axi read data from the 1st lower 2-byte read */
		reg16.offset = addr;
		
		if( ioctl( fd, LB_IOC_GET_REG, &reg16 ) ) {
			return EXIT_FAILURE;
		}
		
		*read_data = reg16.value;
		
		/* higher 2-byte read */
		/* read value is the latched axi read data from the 1st lower 2-byte read */
		reg16.offset = addr + 2;
		
		if( ioctl( fd, LB_IOC_GET_REG, &reg16 ) ) {
			return EXIT_FAILURE;
		}
		
		*read_data += (reg16.value << 16);
#endif
#endif /* #ifndef __MICROBLAZE__ */
	}
	else {
		reg = (unsigned int*)(base + addr);
		*read_data = *reg;
	}
	
	return EXIT_SUCCESS; 
}

void vt_delay(unsigned long micro_seconds)
{
#ifdef __MICROBLAZE__
	usleep(micro_seconds);
#else
	struct timespec delay = { 0, micro_seconds*1000 };
	struct timespec rem;
	nanosleep(&delay, &rem);
#endif	
}
