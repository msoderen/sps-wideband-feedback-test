#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "devices.h"

int dev_init(dev_with_regs_t* dev )
{
	const reg_with_name_t* reg;
	
	if(dev->regs_init == NULL) 
	{
		return EXIT_SUCCESS;		
	}
	
	for(reg = dev->regs_init; reg->name; reg++)
	{
		if((reg->access_type & 0x80) == 0x80)
		{
			vt_delay(reg->data);
			continue;
		}
		
		if((reg->access_type & 2) == 0)
		{
			continue;
		}				
		dev->write(dev, reg->addr, reg->data);		
	}
	
	return EXIT_SUCCESS;
}


