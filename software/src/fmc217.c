/*
 * fmc217.c
 *
 * Copyright 2016 VadaTech Incorporated.  All rights reserved.
 * Author: Maxwell S.

 * When this application is compiled under Xilinx SDK as part of the FPGA reference design,
 *  it uses the UARTLITE port as the stdin/stdout console to provide
 *  a menu interface to test the board.
 *
 * When this application is compiled under Linux, it uses PCIe interface to test the board.
 *
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>

#include "fmc217.h"

#ifdef __MICROBLAZE__
	#define PROG			""
	#define FILE_OPT		"      "
	#define ADC_10SAMPLES		52
	#include "xstatus.h"
	#include "platform.h"
	#include "xparameters.h"
	#include "cli.h"
#else
	#define PROG			"fmc217"
	#define FILE_OPT		"[file]"
	#define ADC_10SAMPLES		8192
#endif

#ifndef __MICROBLAZE__
#if ACCESS_INTERFACE == PCIE_MMAP
	typedef struct
	{
		struct pci_access*	pci;		// PCILIB access structure
		struct pci_dev*		dev;		// PCILIB device structure
		FILE*			mem_file;	// handle to /dev/mem
		void*			base;		// mmapped base address of BAR0
	} pci_card_context_t;

	#include "vt_pcie_mmap.h"
	#define DESIRED_SUB_DEVICE	0xF225
	#define DESIRED_BAR		0
	#include <errno.h>
	#include <pci/pci.h>
	#include <sys/mman.h>
	#include <linux/pci.h>
#endif

#if ACCESS_INTERFACE == ZYNQ
	#include <errno.h>
	#include <sys/mman.h>
#endif
#endif

#ifndef __MICROBLAZE__
#if ACCESS_INTERFACE == P2040_LB || ACCESS_INTERFACE == PCIE_DEV_DRV || ACCESS_INTERFACE == IMX6_LB
	#include <fcntl.h>
#endif
#endif

#include "tool_common.h"
#include "spi_sdio.h"
#include "jesd204.h"
#include "hmc7043.h"
#include "lmk04828.h"
#include "lmx2592.h"
#include "ad9164.h"
#include "adc12dj3200.h"

#define DAC_SRAM_TOTAL_BYTE	256*1024
#define DAC_SAMPLES_PER_SRAMB	16
#define DAC_SRAMB_LEN		DAC_SRAM_TOTAL_BYTE/2/DAC_SAMPLES_PER_SRAMB
#define DAC_TOTAL_CHANNELS	2
#define DAC_SAMPLING_FREQ_MHZ	2800

#define BOARD			"AMC Carrier"
#define APPLICATION		"FMC217 on AMC Carrier"

int			fd = 0;		// device driver
void*			base = 0;	// memory mapped base address
char *const 		prompt = ">>";

/**
 * @brief buses definitions
 */

static sdio_config_t sdio_lmk04828 = {
	.name			= "lMK04828",
	.base_addr		= SDIO_LMK_BASE,
	.rb_shift		= 0,
	.rbpol			= 0,
	.cpol			= 0,
	.cpha			= 0,
	.rw_bit_read_pol	= 1,
	.rw_bit_pos		= 24,
	.data_pos		= 8,
	.data_width		= 8,
	.tran_width		= 24,
	.write_reg32		= write_reg32,
	.read_reg32		= read_reg32
};

static sdio_config_t sdio_hmc7043 = {
	.name			= "HMC7043",
	.base_addr		= SDIO_HMC_BASE,
	.channels		= 1,
	.rb_shift		= HMC7043_SDIO_RB_SHIFT,
	.rbpol			= HMC7043_SDIO_RBPOL,
	.cpol			= HMC7043_SDIO_CPOL,
	.cpha			= HMC7043_SDIO_CPHA,
	.rw_bit_read_pol	= HMC7043_SDIO_RW_BIT_READ_POL,
	.rw_bit_pos		= HMC7043_SDIO_RW_BIT_POS,
	.data_pos		= HMC7043_SDIO_DATA_POS,
	.data_width		= HMC7043_SDIO_DATA_WIDTH,
	.tran_width		= HMC7043_SDIO_TRAN_WIDTH,
	.write_reg32		= write_reg32,
	.read_reg32		= read_reg32
};

static sdio_config_t sdio_lmx2592_adc = {
	.name			= "lMX2592 for ADC",
	.base_addr		= SDIO_LMXA_BASE,
	.channels		= 1,
	.rb_shift		= LMX2592_SDIO_RB_SHIFT,
	.rbpol			= LMX2592_SDIO_RBPOL,
	.cpol			= LMX2592_SDIO_CPOL,
	.cpha			= LMX2592_SDIO_CPHA,
	.rw_bit_read_pol	= LMX2592_SDIO_RW_BIT_READ_POL,
	.rw_bit_pos		= LMX2592_SDIO_RW_BIT_POS,
	.data_pos		= LMX2592_SDIO_DATA_POS,
	.data_width		= LMX2592_SDIO_DATA_WIDTH,
	.tran_width		= LMX2592_SDIO_TRAN_WIDTH,
	.write_reg32		= write_reg32,
	.read_reg32		= read_reg32
};

static sdio_config_t sdio_lmx2592_dac = {
	.name			= "lMX2592 for DAC",
	.base_addr		= SDIO_LMXD_BASE,
	.channels		= 1,
	.rb_shift		= LMX2592_SDIO_RB_SHIFT,
	.rbpol			= LMX2592_SDIO_RBPOL,
	.cpol			= LMX2592_SDIO_CPOL,
	.cpha			= LMX2592_SDIO_CPHA,
	.rw_bit_read_pol	= LMX2592_SDIO_RW_BIT_READ_POL,
	.rw_bit_pos		= LMX2592_SDIO_RW_BIT_POS,
	.data_pos		= LMX2592_SDIO_DATA_POS,
	.data_width		= LMX2592_SDIO_DATA_WIDTH,
	.tran_width		= LMX2592_SDIO_TRAN_WIDTH,
	.write_reg32		= write_reg32,
	.read_reg32		= read_reg32
};

static sdio_config_t sdio_adc = {
	.name			= "ADC12DJ3200",
	.base_addr		= SDIO_ADC_BASE,
	.channels		= 1,
	.rb_shift		= 0,
	.rbpol			= 0,
	.cpol			= 0,
	.cpha			= 0,
	.rw_bit_read_pol	= 1,
	.rw_bit_pos		= 24,
	.data_pos		= 8,
	.data_width		= 8,
	.tran_width		= 24,
	.write_reg32		= write_reg32,
	.read_reg32		= read_reg32
};


static sdio_config_t sdio_dac = {
	.name			= "AD9162",
	.base_addr		= SDIO_DAC_BASE,
	.channels		= 1,
	.rb_shift		= 0,
	.rbpol			= 0,
	.cpol			= 0,
	.cpha			= 0,
	.rw_bit_read_pol	= 1,
	.rw_bit_pos		= 24,
	.data_pos		= 8,
	.data_width		= 8,
	.tran_width		= 24,
	.write_reg32		= write_reg32,
	.read_reg32		= read_reg32
};


/**
 * @brief serial devices definitions
 */
dev_with_regs_t dev_lmk =
{
	.fd			= 0,
	.base			= 0,
	.name			= "l",
	.full_name		= "LMK04828",
	.regs_dump		= lmk04828_regs,
	.regs_init		= lmk04828_regs,
	.bus_type		= SDIO,
	.pBus			= &sdio_lmk04828,
	.private_data[0]	= 0,
	.init			= dev_init,
	.write			= spi_write,
	.read			= spi_read,
	.dump			= spi_dump
};

dev_with_regs_t dev_hmc =
{
	.fd			= 0,
	.base			= 0,
	.name			= "h",
	.full_name		= "HMC7043",
	.regs_dump		= hmc7043_regs,
	.regs_init		= hmc7043_regs,
	.bus_type		= SDIO,
	.pBus			= &sdio_hmc7043,
	.private_data[0]	= HMC7043_SDIO_PRIVATE_DATA0,
	.init			= spi_init,
	.write			= spi_write,
	.read			= spi_read,
	.dump			= spi_dump
};

dev_with_regs_t dev_lmxa =
{
	.fd			= 0,
	.base			= 0,
	.name			= "xa",
	.full_name		= "LMX2592 for ADC",
	.regs_dump		= lmx2592_adc_regs,
	.regs_init		= lmx2592_adc_regs,
	.bus_type		= SDIO,
	.pBus			= &sdio_lmx2592_adc,
	.private_data[0]	= LMX2592_SDIO_PRIVATE_DATA0,
	.curr_channel		= 0,
	.init			= spi_init,
	.write			= spi_write,
	.read			= spi_read,
	.dump			= spi_dump
};

dev_with_regs_t dev_lmxd =
{
	.fd			= 0,
	.base			= 0,
	.name			= "xd",
	.full_name		= "LMX2592 for DAC",
	.regs_dump		= lmx2592_dac_regs,
	.regs_init		= lmx2592_dac_regs,
	.bus_type		= SDIO,
	.pBus			= &sdio_lmx2592_dac,
	.private_data[0]	= LMX2592_SDIO_PRIVATE_DATA0,
	.curr_channel		= 0,
	.init			= spi_init,
	.write			= spi_write,
	.read			= spi_read,
	.dump			= spi_dump
};

dev_with_regs_t dev_dac =
{
	.fd			= 0,
	.base			= 0,
	.name			= "d",
	.full_name		= "AD9162",
	.regs_dump		= ad9164_regs,
	.regs_init		= ad9164_init_regs,
	.bus_type		= SDIO,
	.pBus			= &sdio_dac,
	.private_data[0]	= 0,
	.curr_channel		= 0,
	.init			= spi_init,
	.write			= spi_write,
	.read			= spi_read,
	.dump			= spi_dump
};


dev_with_regs_t dev_adc =
{
	.fd			= 0,
	.base			= 0,
	.name			= "a",
	.full_name		= "ADC12DJ3200",
	.regs_dump		= adc12dj3200_regs,
	.regs_init		= adc12dj3200_regs,
	.bus_type		= SDIO,
	.pBus			= &sdio_adc,
	.private_data[0]	= 0,
	.curr_channel		= 0,
	.init			= spi_init,
	.write			= spi_write,
	.read			= spi_read,
	.dump			= spi_dump
};

dev_with_regs_t* p_devices[] = {
	&dev_lmk,
	&dev_hmc,
	&dev_lmxa,
	&dev_lmxd,
	&dev_dac,
	&dev_adc,
	NULL
};

static void update_dev_base(int fd, void* base)
{
	int i = 0;
	while(p_devices[i])
	{
		p_devices[i]->fd = fd;
		p_devices[i]->base = base;
		i++;
	}
}

#ifndef __MICROBLAZE__
#if ACCESS_INTERFACE == PCIE_MMAP
static int map_bar( FILE **mem_file, void** base, struct pci_dev *dev )
{
	printf( PROG ": BAR%d start 0x%08llX size 0x%08llX\n",
			DESIRED_BAR,
			(long long unsigned)dev->base_addr[DESIRED_BAR], (long long unsigned)dev->size[DESIRED_BAR] );

	*mem_file = fopen( "/dev/mem", "r+" );

	if( !*mem_file )
	{
		perror( PROG ": Unable to open /dev/mem" );
		return EXIT_FAILURE;
	}

	*base = mmap( NULL, dev->size[DESIRED_BAR],
				  PROT_READ|PROT_WRITE, MAP_SHARED,
				  fileno(*mem_file),
				  (off_t)dev->base_addr[DESIRED_BAR] );

	if( *base == MAP_FAILED )
	{
		fprintf( stderr, "*** ERRNO %d ***\n", errno );
		perror( PROG ": Unable to map BAR" );
		fclose( *mem_file );
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

static int unmap_bar( FILE *mem_file, void* base, struct pci_dev *dev )
{
	if( munmap( base, dev->size[DESIRED_BAR] ) < 0 )
	{
		return EXIT_FAILURE;
	}

	fclose( mem_file );

	return EXIT_SUCCESS;
}

static int pci_setup_card( pci_card_context_t* context )
{
	context->pci = pci_alloc();

	if( !context->pci )
	{
		fprintf( stderr, PROG ": Unable to allocate PCI structure\n" );
		goto out;
	}

	pci_init( context->pci );
	pci_scan_bus( context->pci );

	/* walk the list of devices and see how many belong to us */
	for( context->dev = context->pci->devices; context->dev; context->dev = context->dev->next )
	{
		pci_fill_info( context->dev, PCI_FILL_IDENT|
					   PCI_FILL_IRQ|
					   PCI_FILL_BASES|
					   PCI_FILL_ROM_BASE|
					   PCI_FILL_SIZES|
					   PCI_FILL_CLASS|
					   PCI_FILL_RESCAN );

		unsigned short sub_device_id = pci_read_word(context->dev, PCI_SUBSYSTEM_ID_POS);
		if( vt_carrier_bkpn_pcie_id_chk(context->dev->vendor_id, context->dev->device_id) &&
			sub_device_id == DESIRED_SUB_DEVICE)
		{
			/* this is our board */
			break;
		}
		if( context->dev->vendor_id == DESIRED_VENDOR) {
			printf("found Vadatech board\n");
		}

	}

	if( !context->dev )
	{
		printf( PROG ": Unable to find " BOARD " board\n" );
		goto cleanup;
	}


	printf( PROG ": Found " BOARD " board at %d:%d.%d\n", context->dev->bus, context->dev->dev, context->dev->func );

	if( map_bar( &context->mem_file, &context->base, context->dev ) != EXIT_SUCCESS )
	{
		context->dev = NULL;
		goto cleanup;
	}

	goto out;

cleanup:
	pci_cleanup( context->pci );
	return EXIT_FAILURE;

out:
	return EXIT_SUCCESS;
}

static void pci_cleanup_card( pci_card_context_t* context )
{
	unmap_bar( context->mem_file, context->base, context->dev );

	pci_cleanup( context->pci );
}
#endif
#endif

static int check_jesd204_link(int fd, void* base, unsigned int jesd_base_addr, unsigned int num_ch)
{
	unsigned int read_data;
	int j;

	read_reg32(fd, base, jesd_base_addr + JESD204_RESET, &read_data);
	if((read_data & 1) == 1) {
		printf("reset\n");
		return EXIT_FAILURE;
	}

	read_reg32(fd, base, jesd_base_addr + JESD204_SYNC_STATUS, &read_data);
	if((read_data & 1) == 0) {
		printf("sync_status\n");
		return EXIT_FAILURE;
	}

	for(j = 0; j < num_ch; j++) {
		read_reg32(fd, base, jesd_base_addr + JESD204_LANE0_LINK_ERROR_CNT + JESD204_LANE_SPAN * j, &read_data);
		if(read_data > 1) {
			printf("error count\n");
			return EXIT_FAILURE;
		}
	}

	return EXIT_SUCCESS;
}

static int cmd_jesd_rx_error( int argc, char* argv[] )
{
	unsigned int read_data = 0;
	int i, j;

	if(argc != 0)
	{
		return EXIT_FAILURE;
	}

	unsigned int addr[2] = {JESD_A0_BASE, JESD_A1_BASE};

	for(j = 0; j < 2; j++)
	{
		printf("ADC Link %d\n", j);
		read_reg32(fd, base, addr[j] + JESD204_LINK_ERROR_STATUS, &read_data);
		printf("\tLink error status: %08X\n", read_data);
		read_reg32(fd, base, addr[j] + JESD204_LINK_DEBUG_STATUS, &read_data);
		printf("\tLink debug status: %08X\n", read_data);

		for( i = 0; i < 4; i++)
		{
			read_reg32(fd, base, addr[j] + JESD204_LANE0_LINK_ERROR_CNT + i * JESD204_LANE_SPAN, &read_data);
			printf("\tLane %d error cnt: %08X\n", i, read_data);
		}
	}

	return EXIT_SUCCESS;
}

static int sync_clk_gen(int fd, void* base)
{
	int i;

	const unsigned int addr[6] = {0x139, 0x143, 0x144, 0x144, 0x143, 0x139 };
	const unsigned int data[6] = {  0x0,  0x11,   0x0,  0x9F,  0x10,   0x3 };

	for(i=0; i<6; i++)
	{

		// Toggle SYNC pin
		if(i == 3)
		{
			// Driving the SYNC pin high will reset BUFR divider and JESD204 cores inside the FPGA too

			set_reg_bits(fd, base, GPIO_BASE + GPIO_LMK_IO, GPIO_LMK04828_SYNC_MASK);
			vt_delay(10);
			reset_reg_bits(fd, base, GPIO_BASE + GPIO_LMK_IO, GPIO_LMK04828_SYNC_MASK);
			vt_delay(10);
		}

		dev_lmk.write(&dev_lmk, addr[i], data[i]);

	}

	return EXIT_SUCCESS;
}

static int write_pe43713( int reg_addr, unsigned int pe43713_addr, unsigned int attn_data )
{
	int 		i;
	unsigned int 	addr;
	unsigned int 	value;

	value = attn_data | (pe43713_addr << 8);

	reset_reg_bits(fd, base, reg_addr, GPIO_PE43713_LE_MASK);

	for(i = 0; i < 16; i++)
	{
		if(value & 0x1)
		{
			set_reg_bits(fd, base, reg_addr, GPIO_PE43713_SI_MASK);
		}
		else
		{
			reset_reg_bits(fd, base, reg_addr, GPIO_PE43713_SI_MASK);
		}
		vt_delay(1);
		set_reg_bits(fd, base, reg_addr, GPIO_PE43713_CLK_MASK);
		vt_delay(1);
		reset_reg_bits(fd, base, reg_addr, GPIO_PE43713_CLK_MASK);
		vt_delay(1);
		value >>= 1;
	}

	set_reg_bits(fd, base, reg_addr, GPIO_PE43713_LE_MASK);
	vt_delay(1);
	reset_reg_bits(fd, base, reg_addr, GPIO_PE43713_LE_MASK);

	return EXIT_SUCCESS;
}

static unsigned int get_version(void)
{
	unsigned int read_data;
	read_reg32(fd, base, GPIO_BASE + GPIO_VER, &read_data);
	return (read_data & 0xff000000 ) >> 24;
}

static int usage_exit( void )
{
	fprintf( stderr, "\n" APPLICATION " Tool v%u\n"
		 "Usage: " PROG " <cmd> [<opts>]\n"
		"CMDs:\n"
		"    i <b0|b1|b2> [s|l]           - Initialize the card to default values\n"
		"                                     b0: ADC12DJ3200 - ordering option B=0\n"
		"                                     b1: ADC12DJ2700 - ordering option B=1\n"
		"                                     b2: ADC12DJ1600 - ordering option B=2\n"
		"                                     l: DAC same sampling freq. as ADC. ADC to DAC\n"
		"                                          loop back for GTH transceivers only\n"
		"                                        For GTX transceivers, no need this option\n"
		"                                        !Loopback for b0, dual-channel only!\n"
		"                                     s: ADC is configured as single-channel\n"
		"                                     default: ADC is configured as dual-channel\n"
		"                                     ADC sampling freq: 2.8GSPS for FPGA GTH transceiver\n"
		"                                     ADC sampling freq: 2.5GSPS for FPGA GTX transceiver\n"
		"                                     DAC sampling freq: 4GSPS default\n"
		"    c <0|1> " FILE_OPT "                - Capture ADC channel data. Use channel 0\n"
		"                                     if the ADC is initialized with single-channel\n"
		"    d <b|g|j0|j1|j2>              - Dump FPGA register contents\n"
		"    w <addr> <val>                - Write value at FPGA address\n"
		"    r <addr>                      - Read back at FPGA address\n"
		"    dd <a|d|h|l|xa|xd>            - Dump device's register contents\n"
		"    wd <a|d|h|l|xa|xd> <addr> <val>\n"
		"                                  - Write value to the device's address\n"
		"    rd <a|d|h|l|xa|xd> <addr>     - Read value from the device's address\n"
		"    rs <ob|fp>                    - Select 10MHz ref clock input for lmk04828\n"
		"                                     ob = on board; fp = front panel CLK IN\n"
		"    dm <freq> <dBFs> [d]          - Generate DAC tone (MHz) to DAC SRAM\n"
		"                                     d: output generated data to screen too\n"
		"    j                             - ADC JESD204 RX error cnt\n"
		"    sa <a0|a1|a2|a3|d> <0~31.75>  - Set PE43713 attenuators in dB\n"
		,get_version() );

	fprintf( stderr, "Device Key:\n"
		 "\t b    = DAC SRAM waveform content\n"
		 "\t j0   = JESD204 RX for ADC Link 0\n"
		 "\t j1   = JESD204 RX for ADC Link 1\n"
		 "\t j2   = JESD204 TX for DAC\n"
		 "\t g    = GPIO\n"
		 "\t a    = ADC12DJxx00 ADC\n"
		 "\t d    = AD9162/4 DAC\n"
		 "\t h    = HMC7043\n"
		 "\t l    = LMK04828\n"
		 "\t xa   = LMX2592 for ADC\n"
		 "\t xd   = LMX2592 for DAC\n"
		 "\t a0   = 1st attn. PE43712 for ADC channel 0. for option -01 only\n"
		 "\t a1   = 2nd attn. PE43712 for ADC channel 0. for option -01 only\n"
		 "\t a2   = 1st attn. PE43712 for ADC channel 1. for option -01 only\n"
		 "\t a3   = 2nd attn. PE43712 for ADC channel 1. for option -01 only\n"
		 );

#ifndef __MICROBLAZE__
	exit( EXIT_FAILURE );
#else
	return EXIT_FAILURE;
#endif
}

static int cmd_init( int argc, char* argv[] )
{
	const reg_with_name_t* reg;
	int j;
	unsigned int read_data;
	unsigned int adc_dac_loop = 0;
	unsigned int ordering_option_b = 0;
	unsigned int adc_single_ch = 0;
	unsigned int gtxe2 = 0;		// 1 = Xilin carrier board with GTXE2 transceiver such as AMC518

	if(argc != 1 && argc != 2)
	{
		return EXIT_FAILURE;
	}

	/* currently only support ADC12DJ3200 (B0) and ADC12DJ2700 (B1) */
	if(strcmp( argv[0], "b0") == 0)
	{
		ordering_option_b = 0;
	}
	else if(strcmp( argv[0], "b1") == 0)
	{
		ordering_option_b = 1;
	}
	else
	{
		return EXIT_FAILURE;
	}

	if(argc == 2 && ordering_option_b == 0 && strcmp( argv[1], "l") == 0 )
	{
		adc_dac_loop = 1;
	}
	else if(argc == 2 && strcmp( argv[1], "s") == 0 )
	{
		adc_single_ch = 1;
	}
	else if(argc == 2)
	{
		return EXIT_FAILURE;
	}

	/* Set DAC to the same sampling frequency as the ADC to loop back ADC inputs */
	if(adc_dac_loop)
	{
		dev_lmxd.regs_init = lmx2592_adc_regs;
	}
	else
	{
		dev_lmxd.regs_init = lmx2592_dac_regs;
	}


	read_reg32(fd, base, GPIO_BASE + GPIO_SIG, &read_data);
	// if ultrascale AMC583 or ZYNQ amc518, enalbe GPIO_RX_LPM_EN
	if((read_data & 0xFFFF) == 0xA583 || (read_data & 0xFFFF) == 0xA518)
	{
		write_reg32(fd, base, GPIO_BASE + GPIO_RX_LPM_EN, 1);
	}

	/* If AMC518, the speed grade 2 GTXE2 transceiver doesn't support 11.2Gbps.
	 *   ADC is set to 2.5Gbps, 10Gbps.
	 *   No ADC to DAC loop back.
	 */
	if((read_data & 0xFFFF) == 0xA518)
	{
		if(adc_dac_loop)
		{
			printf("Xilinx carrier boards with GTXE2 transceiver don't support ADC to DAC loop back\n");
			return EXIT_FAILURE;
		}
		gtxe2 = 1;
	}

	// ------------- Disable JESD204 clock input and reset the core -----------
	write_reg32(fd, base, GPIO_BASE + GPIO_JESD_CTRL, 0xff);

	// --------------- LMK04828 initialization
	printf("Initializing LMK04828\n");
	// reset the chip
	set_reg_bits(fd, base, GPIO_BASE + GPIO_LMK_IO, GPIO_LMK04828_RESET_MASK);
	vt_delay(10);
	reset_reg_bits(fd, base, GPIO_BASE + GPIO_LMK_IO, GPIO_LMK04828_RESET_MASK);
	vt_delay(10);

	dev_lmk.init(&dev_lmk);

	/* LMK default for DAC 4GSPS, line rate 10Gbps
	 * LMK04828 PLL2 VCO0 = 2.5GHz
	 * LMK04828 DCLKOUT2 = DAC JESD204 core ref = 2.5G/10 = 250MHz
	 * LMK04828 SDCLKOUT1 & SDCLKOUT3 = DAC LMFC = 2.5G/80 = 31.25MHz

	 * Set DAC to 2.8GSPS to loop back ADC inputs for DAC line rate 7Gbps
	 * LMK04828 PLL2 VCO0 = 2.45GHz
	 * LMK04828 DCLKOUT2 = DAC JESD204 core ref = 2.45G/14 = 175MHz
	 * LMK04828 SDCLKOUT1 & SDCLKOUT3 = DAC LMFC = 2.45G/112 = 21.875MHz
	 */
	if(adc_dac_loop)
	{
		// disable DCLKOUT2, SDCLKOUT1, and SDCLKOUT3 outputs
		dev_lmk.write(&dev_lmk, 0x107, 0x00);
		dev_lmk.write(&dev_lmk, 0x10F, 0x00);
		// change PLL2 PDF to 25MHz
		dev_lmk.write(&dev_lmk, 0x161, 0x04);
		// change PLL2 N to 49 (25*2*49=2450)
		dev_lmk.write(&dev_lmk, 0x168, 0x31);
		// change sysref divider to 112=0x70
		dev_lmk.write(&dev_lmk, 0x13B, 0x70);
		// change DCLKOUT2 divider to 14
		dev_lmk.write(&dev_lmk, 0x108, 0x0E);
		// enable DCLKOUT2, SDCLKOUT1, and SDCLKOUT3 outputs
		dev_lmk.write(&dev_lmk, 0x107, 0x50);
		dev_lmk.write(&dev_lmk, 0x10F, 0x15);
	}

	// check if LMK04828 locked
	printf("Checking if the LMK04828 is locked ... ");
	for(j=0; j<50; j++)
	{
		vt_delay(500);

		int pll_locked = 0;
		dev_lmk.read(&dev_lmk, 0x182, &read_data);
		pll_locked = read_data & 2;

		dev_lmk.read(&dev_lmk, 0x183, &read_data);
		pll_locked &= read_data & 2;

		if(pll_locked == 2)
		{
			break;
		}
	}

	if(j == 50)
	{
		printf("not locked after 50 ms!!!\n");
	}
	else
	{
		printf("locked.\n");
	}

	/*------------------ LMX2592 for ADC initialization --------- */
	printf("Initializing LMX2592 PLL for ADC\n");
	// Reset the chip
	reset_reg_bits(fd, base, GPIO_BASE + GPIO_LMXA_IO, GPIO_LMX2592_CE_MASK);
	vt_delay(10);
	set_reg_bits(fd, base, GPIO_BASE + GPIO_LMXA_IO, GPIO_LMX2592_CE_MASK);

	dev_lmxa.init(&dev_lmxa);

	/* LMX2592 for ADC outputs 2.8GHz default for ADC12DJ3200
	 *                         2.7GHz for ADC12DJ2700
	 * for Xilinx carrier board such as AMC518 with GTXE2 transceiver, set to 2.5GHz for both ADC12DJ3200 and ADC12DJ2700
	 */
	if(gtxe2)
	{
		/* Disable OSC_2X */
		dev_lmxa.write(&dev_lmxa, 0x9, 0x302);
		/* Change PLL_N to 25 */
		dev_lmxa.write(&dev_lmxa, 0x26, 0x32);
		/* Calibrate */
		dev_lmxa.write(&dev_lmxa, 0, 0x231C);
	}
	else if(ordering_option_b == 1)
	{
		/* Disable OSC_2X */
		dev_lmxa.write(&dev_lmxa, 0x9, 0x302);
		/* Change PLL_N to 27 */
		dev_lmxa.write(&dev_lmxa, 0x26, 0x36);
		/* Calibrate */
		dev_lmxa.write(&dev_lmxa, 0, 0x231C);
	}

	vt_delay(100000);

	// check if the LMX is locked
	printf("\tChecking if LMX2592 for ADC is locked ... ");
	// drive lock detect to MUXOUT pin
	dev_lmxa.write(&dev_lmxa, 0, 0x2314);
	for(j=0; j<50; j++)
	{
		vt_delay(500);

		read_data = get_reg_bits(fd, base, GPIO_BASE + GPIO_LMXA_IO, GPIO_LMX2592_LD_MASK);

		if(read_data)
		{
			break;
		}
	}

	if(j == 50)
	{
		printf("not locked after 50 ms!!!\n");
	}
	else
	{
		printf("locked.\n");
	}

	/*------------------ LMX2592 for DAC initialization --------- */
	printf("Initializing LMX2592 PLL for DAC\n");
	// Reset the chip
	reset_reg_bits(fd, base, GPIO_BASE + GPIO_LMXD_IO, GPIO_LMX2592_CE_MASK);
	vt_delay(10);
	set_reg_bits(fd, base, GPIO_BASE + GPIO_LMXD_IO, GPIO_LMX2592_CE_MASK);

	dev_lmxd.init(&dev_lmxd);

	// disable RFOUTB
	if(adc_dac_loop)
	{
		dev_lmxd.write(&dev_lmxd, 0x2E, 0x3F90);
	}

	vt_delay(100000);

	// check if the LMX is locked
	printf("\tChecking if LMX2592 for DAC is locked ... ");
	// drive lock detect to MUXOUT pin
	dev_lmxd.write(&dev_lmxd, 0, 0x2314);
	for(j=0; j<50; j++)
	{
		vt_delay(500);

		read_data = get_reg_bits(fd, base, GPIO_BASE + GPIO_LMXD_IO, GPIO_LMX2592_LD_MASK);

		if(read_data)
		{
			break;
		}
	}

	if(j == 50)
	{
		printf("not locked after 50 ms!!!\n");
	}
	else
	{
		printf("locked.\n");
	}

	//------------------ HMC initialization
	printf("Initializing HMC7043\n");
	// reset the chip
	set_reg_bits(fd, base, GPIO_BASE + GPIO_HMC_IO, GPIO_HMC7043_RESET_MASK);
	vt_delay(10);
	reset_reg_bits(fd, base, GPIO_BASE + GPIO_HMC_IO, GPIO_HMC7043_RESET_MASK);
	vt_delay(10);
	dev_hmc.init(&dev_hmc);

	// software restart
	dev_hmc.write(&dev_hmc, 0x1, 0x42);
	dev_hmc.write(&dev_hmc, 0x1, 0x40);

	// sync using the RFSYNCIN pins
	set_reg_bits(fd, base, GPIO_BASE + GPIO_HMC_IO, GPIO_HMC7043_SYNC_MASK);
	reset_reg_bits(fd, base, GPIO_BASE + GPIO_HMC_IO, GPIO_HMC7043_SYNC_MASK);

	// sync using register
//	 dev_hmc.write(&dev_hmc, 0x1, 0xC0);
//	 dev_hmc.write(&dev_hmc, 0x1, 0x40);

	//--------------- ADC12DJ3200 initialization
	if(adc_single_ch)
	{
		dev_adc.regs_init = adc12dj3200_regs_single_ch;
	}
	else
	{
		dev_adc.regs_init = adc12dj3200_regs;
	}

	printf("Initializing ADC12DJ3200 ADC\n");
	set_reg_bits(fd, base, GPIO_BASE + GPIO_ADC_IO, GPIO_ADC_PD_MASK);
	vt_delay(100);
	reset_reg_bits(fd, base, GPIO_BASE + GPIO_ADC_IO, GPIO_ADC_PD_MASK);
	vt_delay(10000);

	dev_adc.init(&dev_adc);
	vt_delay(10000);

	// wait for ADC calibration done
	printf("Delay 300ms\n");
	vt_delay(300*1000);

	// ------------- JESD204 Core initialization
	printf("Initializing JESD204 TX Core\n");

	for(reg = xilinx_jesd204_tx_regs; reg->name; reg++)
	{
		if((reg->access_type & 2) == 2)
		{
			write_reg32(fd, base, JESD_D0_BASE + reg->addr, reg->data);
		}
	}

	printf("Initializing JESD204 RX Cores\n");

	for(reg = xilinx_jesd204_rx_regs; reg->name; reg++)
	{
		if((reg->access_type & 2) == 2)
		{
			write_reg32(fd, base, JESD_A0_BASE + reg->addr, reg->data);
			write_reg32(fd, base, JESD_A1_BASE + reg->addr, reg->data);
		}
	}

	sync_clk_gen(fd, base);

	// Enable JESD204 clocks
	printf("\tEnabling JESD204 clocks\n");
	write_reg32(fd, base, GPIO_BASE + GPIO_JESD_CTRL, 0x0);

	vt_delay(20000);

	// Check JESD204 links
	printf("\tChecking ADC JESD204 links ... ");
	for(j=0; j<50; j++)
	{
		vt_delay(500);

		if(check_jesd204_link(fd, base, JESD_A0_BASE, 4) == EXIT_SUCCESS)
		{
			if(check_jesd204_link(fd, base, JESD_A1_BASE, 4) == EXIT_SUCCESS)
			{
				break;
			}
		}
	}

	if(j == 50)
	{
		printf("failed after 250ms!!!\n");
	}
	else
	{
		printf("linked\n");
	}

	//--------------- AD9162 initialization
	printf("Initializing AD9162 DAC\n");
	reset_reg_bits(fd, base, GPIO_BASE + GPIO_DAC_IO, GPIO_DAC_nRST_MASK);
	vt_delay(100);
	set_reg_bits(fd, base, GPIO_BASE + GPIO_DAC_IO, GPIO_DAC_nRST_MASK);

	dev_dac.init(&dev_dac);

	vt_delay(1000);

	// check SERDES PLL
	dev_dac.read(&dev_dac, 0x281, &read_data);	// 0: lock 3: calibrated; 4:lower end 5:high end

	// enable tx
	set_reg_bits(fd, base, GPIO_BASE + GPIO_DAC_IO, GPIO_DAC_TX_EN_MASK);

	return EXIT_SUCCESS;
}

static int cmd_write( int argc, char* argv[] )
{
	unsigned int		write_addr;
	unsigned int		write_val;

	if( argc != 2 )
	{
		return EXIT_FAILURE;
	}

	write_addr = strtoul( argv[0], NULL, 0 );
	write_val  = strtoul( argv[1], NULL, 0 );

	printf( "Writing 0x%08X @ 0x%04X\n", write_val, write_addr );
	fflush( stdout );

	write_reg32(fd, base, write_addr, write_val);

	return EXIT_SUCCESS;
}

static int cmd_read( int argc, char* argv[] )
{
	unsigned int		read_addr;
	unsigned int		read_val;

	if( argc != 1 )
	{
		return EXIT_FAILURE;
	}

	read_addr = strtoul( argv[0], NULL, 0 );

	read_reg32(fd, base, read_addr, &read_val);

	printf( "Read value = 0x%08X @ register address 0x%04X\n", read_val, read_addr );
	fflush( stdout );

	return EXIT_SUCCESS;
}

static int cmd_write_dev( int argc, char* argv[] )
{
	int 			i;

	unsigned int addr;
	unsigned int write_data;
	unsigned int channel = 0;


	if(argc != 3 && argc != 4)
	{
		return EXIT_FAILURE;
	}

	addr = strtoul( argv[1], NULL, 0 );
	write_data = strtoul( argv[2], NULL, 0 );

	if(argc == 4)
	{
		channel = strtoul( argv[3], NULL, 0);
	}

	i = 0;
	while (p_devices[i])
	{
		if(strcmp( argv[0], p_devices[i]->name ) == 0 )
		{
			if(argc == 4) {
				p_devices[i]->curr_channel = channel;
			}
			p_devices[i]->write(p_devices[i], addr, write_data);
			printf( "Wrote 0x%X @ 0x%X to device %s\n", write_data, addr, p_devices[i]->full_name );
			fflush( stdout );
			return EXIT_SUCCESS;
		}
		i++;
	}

	fflush( stdout );
	return EXIT_FAILURE;
}

static int cmd_read_dev( int argc, char* argv[] )
{
	int			i;

	unsigned int addr;
	unsigned int read_data;
	unsigned int channel = 0;


	if(argc != 2 && argc != 3)
	{
		return EXIT_FAILURE;
	}

	addr = strtoul( argv[1], NULL, 0 );

	if(argc == 3)
	{
		channel = strtoul( argv[2], NULL, 0);
	}

	i = 0;
	while(p_devices[i])
	{
		if(strcmp( argv[0], p_devices[i]->name ) == 0 )
		{
			if(argc == 3)
			{
				p_devices[i]->curr_channel = channel;
			}

			/* if LMX2592, drive readback to MUXOUT pin during read */
			if((strcmp("xa", p_devices[i]->name ) == 0) || (strcmp("xd", p_devices[i]->name ) == 0))
			{
				p_devices[i]->write(p_devices[i], 0, 0x2310);
				p_devices[i]->read(p_devices[i], addr, &read_data);
				p_devices[i]->write(p_devices[i], 0, 0x2314);
			}
			else
			{
				p_devices[i]->read(p_devices[i], addr, &read_data);
			}

			printf( "Read value = 0x%X @ register address 0x%X from device %s\n", read_data, addr, p_devices[i]->full_name );
			fflush( stdout );
			return EXIT_SUCCESS;
		}
		i++;
	}

	fflush( stdout );
	return EXIT_FAILURE;
}

static int cmd_dump( int argc, char* argv[] )
{
	int dumped = 0;

	if(argc != 1 && argc != 2)
	{
		return EXIT_FAILURE;
	}

	/* FPGA registers dump */
	/* FPGA registers dump */
	if( (strcmp( argv[0], "all" ) == 0 ) || (strcmp( argv[0], "b" ) == 0 ))
	{
		int i;
		unsigned int read_data;
		unsigned int read_q;
		unsigned int dump_num;

		read_reg32(fd, base, GPIO_BASE + GPIO_DAC_MEM_LEN, &read_data);

		if(read_data == 0)
		{
			printf("\nPlease download waveform to DAC buffer first\n\n");
			return EXIT_SUCCESS;
		}

		if(argc == 2)
		{
			dump_num = strtoul( argv[1], NULL, 0);
		}

		if(dump_num == 0 || dump_num > read_data)
		{
			dump_num = read_data * 8;
		}


		printf("//---- Dumping %s Registers ---------\n", "DAC waveform buffer");
		printf("//---- Base Address = 0x%X ---------\n",  BRAM_D0_BASE);
		printf("Sample#\tD\n");

		for( i = 0; i < dump_num; i+=2)
		{
			read_reg32(fd, base, BRAM_D0_BASE + i * 2, &read_data);
			printf("%d\t%04X\n", i, read_data&0xFFFF);
			printf("%d\t%04X\n", i+1, (read_data>>16)&0xFFFF);
		}
		dumped = 1;
	}

	if( (strcmp( argv[0], "all" ) == 0 ) || (strcmp( argv[0], "g" ) == 0 ))
	{
		reg_dump(fd, base, GPIO_BASE, "VT GPIO Core", gpio_regs);
		dumped = 1;
	}

	if( (strcmp( argv[0], "all" ) == 0 ) || (strcmp( argv[0], "j0" ) == 0 ))
	{
		reg_dump(fd, base, JESD_A0_BASE, "Xilinx JESD204 Core for ADC Link 0", xilinx_jesd204_rx_regs);
		dumped = 1;
	}

	if( (strcmp( argv[0], "all" ) == 0 ) || (strcmp( argv[0], "j1" ) == 0 ))
	{
		reg_dump(fd, base, JESD_A1_BASE, "Xilinx JESD204 Core for ADC Link 1", xilinx_jesd204_rx_regs);
		dumped = 1;
	}

	if( (strcmp( argv[0], "all" ) == 0 ) || (strcmp( argv[0], "j2" ) == 0 ))
	{
		reg_dump(fd, base, JESD_D0_BASE, "Xilinx JESD204 Core for DAC", xilinx_jesd204_tx_regs);
		dumped = 1;
	}

	if(!dumped)
	{
		printf("Nothing to dump.... \n");
		return EXIT_FAILURE;
	}

	return EXIT_SUCCESS;
}

static int cmd_dump_dev( int argc, char* argv[] )
{
	int i;
	int dumped = 0;
	unsigned int channel = 0;

	if(argc != 1 && argc != 2)
	{
		return EXIT_FAILURE;
	}

	if(argc == 2)
	{
		channel = strtoul( argv[1], NULL, 0);
	}

	/* Devices registers dump */
	i = 0;
	while(p_devices[i])
	{
		if(strcmp( argv[0], p_devices[i]->name ) == 0 )
		{
			if(p_devices[i]->regs_dump != NULL)
			{
				printf("found device name = %s\n", p_devices[i]->full_name);
				if(argc == 2)
				{
					p_devices[i]->curr_channel = channel;
				}

				/* if LMX2592, drive readback to MUXOUT pin during dump */
				if((strcmp("xa", p_devices[i]->name ) == 0) || (strcmp("xd", p_devices[i]->name ) == 0))
				{
					p_devices[i]->write(p_devices[i], 0, 0x2310);
					p_devices[i]->dump(p_devices[i]);
					p_devices[i]->write(p_devices[i], 0, 0x2314);
				}
				else
				{
					p_devices[i]->dump(p_devices[i]);
				}
			}
			return EXIT_SUCCESS;
		}
		i++;
	}

	printf("Nothing to dump.... \n");
	return EXIT_FAILURE;
}

static int cmd_set_att( int argc, char* argv[] )
{
	int 			i;

	unsigned int reg_addr;
	unsigned int pe43713_addr;
	float attn;
	unsigned int write_data;
	unsigned int channel = 0;


	if(argc != 2)
	{
		return EXIT_FAILURE;
	}

	if(strcmp( argv[0], "d" ) == 0 || strcmp( argv[0], "D" ) == 0 )
	{
		reg_addr = GPIO_PE43713_DAC_IO;
		pe43713_addr = 0;
	}
	else if(strcmp( argv[0], "a0" ) == 0 || strcmp( argv[0], "A0" ) == 0 )
	{
		reg_addr = GPIO_PE43713_ADC_IO;
		pe43713_addr = 0;
	}
	else if(strcmp( argv[0], "a1" ) == 0 || strcmp( argv[0], "A1" ) == 0 )
	{
		reg_addr = GPIO_PE43713_ADC_IO;
		pe43713_addr = 1;
	}
	else if(strcmp( argv[0], "a2" ) == 0 || strcmp( argv[0], "A2" ) == 0 )
	{
		reg_addr = GPIO_PE43713_ADC_IO;
		pe43713_addr = 2;
	}
	else if(strcmp( argv[0], "a3" ) == 0 || strcmp( argv[0], "A3" ) == 0 )
	{
		reg_addr = GPIO_PE43713_ADC_IO;
		pe43713_addr = 3;
	}
	else
	{
		return EXIT_FAILURE;
	}

	attn = strtof( argv[1], NULL);

	if(attn > 31.75 || attn < 0)
	{
		return EXIT_FAILURE;
	}

	write_pe43713(GPIO_BASE + reg_addr, pe43713_addr, (unsigned int)(attn * 4));

	printf( "Set %s attenuation to %2.2f dB\n", argv[0], attn);

	return EXIT_SUCCESS;
}

static int cmd_ref_clk_sel( int argc, char* argv[] )
{
	unsigned int write_data = 0;

	if(argc != 1)
	{
		return EXIT_FAILURE;
	}

	if(strcmp( argv[0], "ob" ) == 0 )
	{
		write_data = 0xA;
	}
	else if(strcmp( argv[0], "fp" ) == 0 )
	{
		write_data = 0x1A;
	}
	else
	{
		return EXIT_FAILURE;
	}

	dev_lmk.write(&dev_lmk, 0x147, write_data);

	fflush( stdout );

	return EXIT_SUCCESS;
}

static unsigned short get_lsb_16bit(unsigned int* data)
{
#if defined(__MICROBLAZE__) || ACCESS_INTERFACE == PCIE_MMAP || ACCESS_INTERFACE == IMX6_LB || ACCESS_INTERFACE == ZYNQ
	/* Little endian, Microblaze is set to little endian. Assuming host cpu using PCIE_MMAP is little endian */
	return *((unsigned short*) (data) + 1);
#else
	/* big endian: P2040_LB is big endian. Assuming host cpu using PCIE_DEV_DRV is big endian */
	return *((unsigned short*) data);
#endif
}

static unsigned short get_msb_16bit(unsigned int* data)
{
#if defined(__MICROBLAZE__) || ACCESS_INTERFACE == PCIE_MMAP || ACCESS_INTERFACE == IMX6_LB || ACCESS_INTERFACE == ZYNQ
	/* Little endian, Microblaze is set to little endian. Assuming host cpu using PCIE_MMAP is little endian */
	return *((unsigned short*) data);
#else
	/* big endian: P2040_LB is big endian. Assuming host cpu using PCIE_DEV_DRV is big endian */
	return *((unsigned short*) (data) + 1);
#endif
}

static int cmd_capture( int argc, char* argv[], int lotsof_adc_data )
{
	unsigned int read_data;
	unsigned int read_data1;
	unsigned int adc_single_ch = 0;
	unsigned int ch = 0;
	int i;
	int j;

#ifdef __MICROBLAZE__
	if(argc != 1)
#else
	if(argc != 2 &&  argc != 1)
#endif
	{
		return EXIT_FAILURE;
	}

	/* check if the ADC is initialized to single channel */
	dev_adc.read(&dev_adc, 0x201, &read_data);
	if(read_data == 0)
	/* JMODE = 0; single channel */
	{
		adc_single_ch = 1;
	}

	if(strlen(argv[0]) == 1 && argv[0][0] == '0')
	{
		ch = 0;
	}
	else if(strlen(argv[0]) == 1 && argv[0][0] == '1' && adc_single_ch == 0)
	{
		ch = 1;
	}
	else if(strlen(argv[0]) == 1 && argv[0][0] == '1' && adc_single_ch == 1)
	{
		printf("The ADC is initialized to single channel mode. Channel 0 must be used for input.\n");
		return EXIT_FAILURE;
	}
	else
	{
		return EXIT_FAILURE;
	}

	// Select channel
	if(ch)
	{
		set_reg_bits(fd, base, GPIO_BASE + GPIO_ADC_CH_SEL, GPIO_ADC_ACQ_CH_SEL_MASK);
	}
	else
	{
		reset_reg_bits(fd, base, GPIO_BASE + GPIO_ADC_CH_SEL, GPIO_ADC_ACQ_CH_SEL_MASK);
	}

	// Disable fifo write and reset fifo
	reset_reg_bits(fd, base, GPIO_BASE + GPIO_ADC_ACQ_CTRL, GPIO_ADC_ACQ_FIFO_WR_EN_MASK);
	set_reg_bits(fd, base, GPIO_BASE + GPIO_ADC_ACQ_CTRL, GPIO_ADC_ACQ_FIFO_RST_MASK);
	vt_delay(1000);

	// Release fifo reset
	reset_reg_bits(fd, base, GPIO_BASE + GPIO_ADC_ACQ_CTRL, GPIO_ADC_ACQ_FIFO_RST_MASK);
	vt_delay(1000);

	// Enable fifo write
	set_reg_bits(fd, base, GPIO_BASE + GPIO_ADC_ACQ_CTRL, GPIO_ADC_ACQ_FIFO_WR_EN_MASK);
	vt_delay(1000);

	// Read the fifo status. the fifo should be full
	read_data = get_reg_bits(fd, base, GPIO_BASE + GPIO_ACQ_FIFO_STAT,
		(GPIO_CH0_FIFO0_FULL_MASK | GPIO_CH0_FIFO1_FULL_MASK |
		GPIO_CH1_FIFO0_FULL_MASK | GPIO_CH1_FIFO1_FULL_MASK ));
	if(read_data != (GPIO_CH0_FIFO0_FULL_MASK | GPIO_CH0_FIFO1_FULL_MASK |
		GPIO_CH1_FIFO0_FULL_MASK | GPIO_CH1_FIFO1_FULL_MASK ) )
	{
		printf("Not all ADC FIFOs are not full.\n");
		goto cleanup;
	}

	printf("start reading adc fifo \n");

	// Save data to the file
	char* filename = "<unknown>";
	FILE* outfile;

	if(argc == 2)
	{
		filename = argv[1];
		outfile = fopen(filename, "w" );
		if( !outfile )
		{
			perror( filename );
			goto cleanup;
		}
		printf( ">>> Recording to file '%s'\n", filename );
	}
	else
	{
		outfile = stdout;
	}

	// Now start read data.
	int data_i;
	int samples10 = ADC_10SAMPLES;
	char ss[4] = "%d\n";

	if(lotsof_adc_data)
	{
		samples10 = 8192;
		ss[2] = ' ';
	}


	for(i = 0; i < samples10; i++)
	{
		for(j = 0; j < 4; j++)
		{
			read_reg32(fd, base, A0_RD_BASE, &read_data);
			if(adc_single_ch)
			{
				set_reg_bits(fd, base, GPIO_BASE + GPIO_ADC_CH_SEL, GPIO_ADC_ACQ_CH_SEL_MASK);
				read_reg32(fd, base, A0_RD_BASE, &read_data1);
				reset_reg_bits(fd, base, GPIO_BASE + GPIO_ADC_CH_SEL, GPIO_ADC_ACQ_CH_SEL_MASK);
			}
			data_i = (int)get_lsb_16bit(&read_data);
			if(data_i > 2047) data_i -= 4096;
			fprintf(outfile, ss, data_i);

			if(adc_single_ch)
			{
				data_i = (int)get_lsb_16bit(&read_data1);
				if(data_i > 2047) data_i -= 4096;
				fprintf(outfile, ss, data_i);
			}

			data_i = (int)get_msb_16bit(&read_data);
			if(data_i > 2047) data_i -= 4096;
			fprintf(outfile, ss, data_i);

			if(adc_single_ch)
			{
				data_i = (int)get_msb_16bit(&read_data1);
				if(data_i > 2047) data_i -= 4096;
				fprintf(outfile, ss, data_i);
			}
		}

		for(j = 0; j < 1; j++)
		{
			read_reg32(fd, base, A1_RD_BASE, &read_data);
			if(adc_single_ch)
			{
				set_reg_bits(fd, base, GPIO_BASE + GPIO_ADC_CH_SEL, GPIO_ADC_ACQ_CH_SEL_MASK);
				read_reg32(fd, base, A1_RD_BASE, &read_data1);
				reset_reg_bits(fd, base, GPIO_BASE + GPIO_ADC_CH_SEL, GPIO_ADC_ACQ_CH_SEL_MASK);
			}

			data_i = (int)get_lsb_16bit(&read_data);
			if(data_i > 2047) data_i -= 4096;
			fprintf(outfile, ss, data_i);

			if(adc_single_ch)
			{
				data_i = (int)get_lsb_16bit(&read_data1);
				if(data_i > 2047) data_i -= 4096;
				fprintf(outfile, ss, data_i);
			}

			data_i = (int)get_msb_16bit(&read_data);
			if(data_i > 2047) data_i -= 4096;
			fprintf(outfile, ss, data_i);

			if(adc_single_ch)
			{
				data_i = (int)get_msb_16bit(&read_data1);
				if(data_i > 2047) data_i -= 4096;
				fprintf(outfile, ss, data_i);
			}
		}

		if(lotsof_adc_data == 0 && argc == 1 && i == 511/10)
		{
			break;
		}
	}

	if(lotsof_adc_data)
	{
		fprintf(outfile, "\n");
	}

cleanup:
	if(argc == 2 && outfile)
	{
		fclose(outfile);
	}
	return EXIT_SUCCESS;
}

static int cmd_capture0( int argc, char* argv[])
{
	return cmd_capture(argc, argv, 0);
}

static int cmd_capture1( int argc, char* argv[])
{
	return cmd_capture(argc, argv, 1);
}

static int cmd_gen_sine( int argc, char* argv[] )
{
	double out_freq;
	double sa_freq = DAC_SAMPLING_FREQ_MHZ;
	int i, j, k;
	unsigned int data_l, data_h;
	int dump = 0;

	if(argc != 2 && argc != 3)
	{
		return EXIT_FAILURE;
	}

	printf("Calculating DAC samples based on sampling frequency of %d MHz\n", (unsigned int)sa_freq);

	out_freq = strtof(argv[0], NULL) ;

	if(out_freq == 0)
	{
		return EXIT_FAILURE;
	}

	double dbc = strtof(argv[1], NULL);
	unsigned int amp = (unsigned int) lround(0x7fff * pow(10, -dbc/20));

	if(argc == 3)
	{
		if(strcmp(argv[2], "d") == 0 || strcmp(argv[2], "D") == 0)
		{
			dump = 1;
		}
	}

	int done = 0;
	double integral;
	for(i = 0; i < DAC_SRAMB_LEN; i++)
	{
		for(j = 0; j < DAC_SAMPLES_PER_SRAMB; j += 2)
		{
			k = i * DAC_SAMPLES_PER_SRAMB + j;

			done = 0;
			if(i > 0 && j == 0)
			{
				double f = fabs(modf(k * out_freq / sa_freq, &integral));
				if(f < 1E-10)
				{
					done = 1;
					break;
				}
			}

			data_l = (unsigned int) lround (amp * sin(k * out_freq / sa_freq * 2 * M_PI)) & 0xffff;
			data_h = (unsigned int) lround (amp * sin((k + 1) * out_freq / sa_freq * 2 * M_PI)) & 0xffff;

			if(dump)
			{
				int l = (int) (data_l >= 32768? data_l - 65536 : data_l);
				int h = (int) (data_h >= 32768? data_h - 65536 : data_h);
				printf("\t%d\t%d\n", l, l);
				printf("\t%d\t%d\n", h, h);
			}


			write_reg32(fd, base, BRAM_D0_BASE + (k << 1), (data_h << 16) | data_l);
		}

		if(done)
		{
			break;
		}
	}

	write_reg32(fd, base, GPIO_BASE + GPIO_DAC_MEM_LEN, i);
	if(done)
	{
		printf( "# Total number of blocks of %d samples downloaded to the DAC waveform buffer is %d\n", DAC_SAMPLES_PER_SRAMB, i);
	}
	else
	{
		printf( "# The tone at %s MHz is not convergent. Please use with caution\n", argv[0]);
	}

	return EXIT_SUCCESS;
}

#ifdef __MICROBLAZE__
int main()
#else
int main( int argc, char* argv[] )
#endif
{
	const cmd_t*		cmd;
	const cmd_t		cmds[] =
	{
		{ "i",			cmd_init },
		{ "c",			cmd_capture0 },
		{ "d",			cmd_dump },
		{ "w",			cmd_write },
		{ "r",			cmd_read },
		{ "dd",			cmd_dump_dev },
		{ "wd",			cmd_write_dev },
		{ "rd",			cmd_read_dev },
		{ "rs",			cmd_ref_clk_sel },
		{ "dm",			cmd_gen_sine },
		{ "j",			cmd_jesd_rx_error },
		{ "sa",			cmd_set_att },
		{ NULL,			NULL }
	};

	int ret_val = EXIT_FAILURE;

#if defined __MICROBLAZE__
	char input[CONFIG_SYS_CBSIZE+1];
	char* argv[20];
	int argc;

	init_platform();
	setvbuf(stdin, NULL, _IONBF, 0);

	fprintf(stdout, "HELLO WORLD!");

	base = (void*)XPAR_M00_AXI_BASEADDR;
	update_dev_base(fd, base);

	cmd_init(0, NULL);
	usage_exit();

	while (1) {

		input[0] = '\0';
		// gets_xst(prompt, input);
		cli_readline_into_buffer(prompt, input);

		argc = get_cmd_args(input, argv, 20);
#elif ACCESS_INTERFACE == ZYNQ
	FILE* mem_file = fopen( "/dev/mem", "r+" );

	if( !mem_file )
	{
		perror( PROG ": Unable to open /dev/mem" );
		return EXIT_FAILURE;
	}

	base = (void *)mmap( NULL, ZYNQ_AXI_SIZE,
		      PROT_READ|PROT_WRITE, MAP_SHARED,
		      fileno(mem_file),
		      (off_t)ZYNQ_AXI_BASEADDR );

	if( base == MAP_FAILED )
	{
		fprintf( stderr, "*** ERRNO %d ***\n", errno );
		perror( PROG ": Unable to map BAR" );
		fclose( mem_file );
		return EXIT_FAILURE;
	}

#elif ACCESS_INTERFACE == PCIE_MMAP
	pci_card_context_t	context;
	int retval = pci_setup_card( &context );
	if( retval != EXIT_SUCCESS )
	{
		fprintf(stderr, "Failed to map PCIe address");
		return retval;
	}

	base = context.base;
#elif ACCESS_INTERFACE == PCIE_DEV_DRV

	fd = open( DEV_NAME, O_RDWR );

	if( fd < 0 )
	{
		perror( DEV_NAME );
		return EXIT_FAILURE;
	}

#elif ACCESS_INTERFACE == P2040_LB

	fd = open( get_lb_dev_name(), O_RDWR );

	if( fd < 0 )
	{
		perror( get_lb_dev_name() );
		return EXIT_FAILURE;
	}

#endif

	update_dev_base(fd, base);

	if( argc >= 2) {
		for( cmd = cmds; cmd->name; cmd++ )
		{
			if( strcmp( cmd->name, argv[1] ) == 0 )
			{
				ret_val = (cmd->func)( argc - 2, argv + 2 );
			}
		}
	}

	if(ret_val == EXIT_FAILURE) {
		usage_exit();
	}
	else {
		ret_val = EXIT_FAILURE;
	}

#if defined __MICROBLAZE__
	}

	cleanup_platform();
#elif ACCESS_INTERFACE == PCIE_MMAP
	pci_cleanup_card( &context );

#elif ACCESS_INTERFACE == ZYNQ
	if( munmap( base, ZYNQ_AXI_SIZE ) < 0 )
	{
		perror( PROG ": Unable to unmap memory" );
	}

	fclose( mem_file );

#else
	if(fd)
	{
		close(fd);
	}
#endif
	return EXIT_SUCCESS;
}
