#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <ctype.h>
#include "tool_common.h"
#include "devices.h"

int hex_dump( void* base, const char* tag, unsigned int offset, unsigned int len )
{
	int idx;

	for( idx = 0; idx < len; idx++ )
	{
		if( !(idx & 0x7) )
		{
			printf( "%s0x%04X [%s]: ", idx ? "\n" : "", offset + (idx << 2), tag );
		}
		printf( "0x%08X ", *(unsigned int*)(base + offset + (idx << 2)) );
	}

	printf( "\n" );

	return EXIT_SUCCESS;
}

int reg_dump(int fd, void* base, unsigned int module_base, const char* tag, const reg_with_name_t* regs )
{
	const reg_with_name_t* reg;
	unsigned int read_data;

	printf("//---- Dumping %s Registers ---------\n", tag);
	printf("//---- Base Address = 0x%X ---------\n",  module_base);
	printf("Addr      \tData      \tName\n");
	for( reg = regs; reg->name; reg++ )
	{
		read_reg32(fd, base, module_base + reg->addr, &read_data);

		printf( "0x%03X\t0x%08X\t%s\n", reg->addr, read_data, reg->name);
	}

	printf( "\n" );

	return EXIT_SUCCESS;
}

unsigned int get_reg_bits(int fd, void* base, unsigned int reg_addr, unsigned int bit_mask)
{
	unsigned int read_data;
	read_reg32(fd, base, reg_addr, &read_data);
	return read_data & bit_mask;	
}

void set_reg_bits(int fd, void* base, unsigned int reg_addr, unsigned int bit_mask)
{
	unsigned int read_data;
	read_reg32(fd, base, reg_addr, &read_data);
	write_reg32(fd, base, reg_addr, read_data | bit_mask);
}

void reset_reg_bits(int fd, void* base, unsigned int reg_addr, unsigned int bit_mask)
{
	unsigned int read_data;
	read_reg32(fd, base, reg_addr, &read_data);
	write_reg32(fd, base, reg_addr, read_data & (~bit_mask));
}

void toggle_reg_bits(int fd, void* base, unsigned int reg_addr, unsigned int bit_mask)
{
	unsigned int read_data;
	read_reg32(fd, base, reg_addr, &read_data);
	write_reg32(fd, base, reg_addr, read_data ^ bit_mask);
}

void write_reg_bits(int fd, void* base, unsigned int reg_addr, unsigned int bit_mask, unsigned int bit_data)
{
	unsigned int read_data;
	unsigned int shift_data = bit_mask;
	int i;
		
	for(i = 0; i < 31; i++)
	{
		if((shift_data & 1) == 0)
		{
			bit_data <<= 1;
			shift_data >>= 1;
		}
		else 
		{
			break;
		}
	}	

	read_reg32(fd, base, reg_addr, &read_data);
	write_reg32(fd, base, reg_addr, (read_data & (~bit_mask)) | bit_data);
}

unsigned int read_reg_bits(int fd, void* base, unsigned int reg_addr, unsigned int bit_mask)
{
	unsigned int read_data;
	int i;
	
	read_reg32(fd, base, reg_addr, &read_data);
	read_data &= bit_mask;	

	for(i = 0; i < 31; i++)
	{
		if((bit_mask & 1) == 0)
		{
			read_data >>= 1;
			bit_mask >>= 1;
		}
		else 
		{
			break;
		}
	}	
	
	return read_data;
}
