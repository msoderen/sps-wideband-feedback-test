#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "spi_sdio.h"

int spi_sdio_init(int fd, void* base, sdio_config_t* sdio_config)
{
	unsigned int ctrl_data = 0;
	unsigned int read_data;
	
	if(sdio_config == NULL) {
		return EXIT_SUCCESS;
	}
	
	ctrl_data = (sdio_config->rbpol << 27) | (sdio_config->cpol << 26) | (sdio_config->cpha << 25) | 
			(sdio_config->rw_bit_read_pol << 24) | (sdio_config->rw_bit_pos << 16) | 
			(sdio_config->data_pos << 8) | sdio_config->tran_width;
	
	if(sdio_config->write_reg32(fd, base, sdio_config->base_addr + SDIO_SPICR, ctrl_data) == EXIT_FAILURE)
	{
		perror("Failed to write SDIO register\n");
		return EXIT_FAILURE;
	}
	if(sdio_config->read_reg32(fd, base, sdio_config->base_addr + SDIO_SPICR, &read_data) == EXIT_FAILURE)
	{
		perror("Failed to read SDIO register\n");
		return EXIT_FAILURE;
	}
	
	if(ctrl_data == read_data)
	{
		return EXIT_SUCCESS;
	}
	else
	{
		fprintf(stderr, "Failed to initialize %s SDIO control register.\n", sdio_config->name);
		return EXIT_FAILURE;
	}		
}

int spi_sdio_write(int fd, void* base, sdio_config_t* sdio_config, unsigned int sdio_addr, unsigned int write_data)
{
	unsigned int tx_data;
	unsigned int tx_data1;
	unsigned int rx_data;
	int timeout;

	if(sdio_config->tran_width <= 32) { // not change old working code
		tx_data	= write_data<<(sdio_config->data_pos - sdio_config->data_width);
		if(sdio_config->rw_bit_pos > 0) 
		// has R/W bit. R/W bit position is 1 based
		{
			// add r/w write bit
			tx_data |= (sdio_config->rw_bit_read_pol? 0 : 1)<<(sdio_config->rw_bit_pos-1);
			
			if(sdio_config->rw_bit_pos == sdio_config->tran_width)
			{
				tx_data |= (sdio_config->data_pos == sdio_config->data_width)? 
						(sdio_addr<<sdio_config->data_width) : sdio_addr;
			}
			else if(sdio_config->rw_bit_pos == 1)
			{
				tx_data |= (sdio_config->data_pos == sdio_config->tran_width)? 
						sdio_addr<<1 : (sdio_addr<<(sdio_config->data_width+1));			
			}
			else
			{
				tx_data |= (sdio_config->data_pos == sdio_config->tran_width)? 
						sdio_addr : (sdio_addr<<(sdio_config->data_width+1));						
			}
		}
		else
		// no R/W bit
		{
			if(sdio_config->tran_width == sdio_config->data_pos)
			// data first
			{
				tx_data	|= sdio_addr;
			}
			else
			// address first
			{
				tx_data	|= sdio_addr<<sdio_config->data_width;		
			}
		}
		
	
		tx_data	<<= (32 - sdio_config->tran_width);
	} 
	else 
	{
		if(sdio_config->rw_bit_pos > 0) 
		// has R/W bit. R/W bit position is 1 based
		{
			// add r/w write bit
			if(sdio_config->tran_width - sdio_config->rw_bit_pos <= 31) {
				tx_data = (sdio_config->rw_bit_read_pol? 0 : 1)<<(31-(sdio_config->tran_width - sdio_config->rw_bit_pos));
				tx_data1 = 0;
			}
			else 
			{
				tx_data = 0;
				tx_data1 = (sdio_config->rw_bit_read_pol? 0 : 1)<<(sdio_config->tran_width - sdio_config->rw_bit_pos - 1);
			}
			
			if(sdio_config->rw_bit_pos == sdio_config->tran_width)
			// r/w bit is the first one (MSB)
			{
				if(sdio_config->data_pos == sdio_config->data_width) 
				// address before data
				{
					tx_data	|= sdio_addr<<(32 - (sdio_config->tran_width - sdio_config->data_width));
					tx_data |= write_data>>(sdio_config->tran_width - 32);
					tx_data1 |= write_data<<(64 - sdio_config->tran_width);
					printf("sdio_write tx_data = 0x%x; tx_data1=0x%x\n", tx_data, tx_data1);
				}
				else
				// data before address
				{
					if(sdio_config->data_width == 32) 
					{
						tx_data |= write_data >> 1;
						tx_data1 |= write_data << 31;
						tx_data1 |= sdio_addr<<(64 - sdio_config->tran_width); 								
					} 
					else 
					{
						tx_data	|= write_data<<(31 - sdio_config->data_width );
						tx_data |= sdio_addr>>(sdio_config->tran_width - 32);
						tx_data1 |= sdio_addr<<(64 - sdio_config->tran_width);
					}
				}
			}
			else if(sdio_config->rw_bit_pos == 1)
			{
			}
			else
			{
			}
		} 
		else 
		// no R/W bit
		{ 		
			if(sdio_config->tran_width == sdio_config->data_pos) 
			// data first
			{
				tx_data	= write_data<<(32 - sdio_config->data_width);
				tx_data |= sdio_addr>>(sdio_config->tran_width - 32);
				tx_data1 = sdio_addr<<(64 - sdio_config->tran_width);
			
			} 
			else 
			{
				tx_data	= sdio_addr<<(32 - (sdio_config->tran_width - sdio_config->data_width));
				tx_data |= write_data>>(sdio_config->tran_width - 32);
				tx_data1 = write_data<<(64 - sdio_config->tran_width);
			}
		}
		
		if(sdio_config->write_reg32(fd, base, sdio_config->base_addr + SDIO_SPIDTRE, tx_data1) == EXIT_FAILURE)
		{
			perror("Failed to write SDIO register\n");
			return EXIT_FAILURE;
		}
	}

	if(sdio_config->write_reg32(fd, base, sdio_config->base_addr + SDIO_SPIDTR, tx_data) == EXIT_FAILURE)
	{
		perror("Failed to write SDIO register\n");
		return EXIT_FAILURE;
	}
	
	// wait for transaction complete
	timeout = 1000;
	do
	{
		if(sdio_config->read_reg32(fd, base, sdio_config->base_addr + SDIO_SPISR, &rx_data) == EXIT_FAILURE)
		{
			perror("Failed to read SDIO register\n");
			return EXIT_FAILURE;
		}
		vt_delay(10);
	}
	while( (rx_data & 1) == 0 && --timeout );

	if(timeout == 0)
	{
		fprintf(stderr, "Time out in waiting %s SDIO write to finish.\n", sdio_config->name);
		return EXIT_FAILURE;
	}	
	
	return EXIT_SUCCESS;
}

/**
 * @brief Read back sdio data @ sdio_addr
 *
 * @param fd device driver file descriptor, used to access SDIO module registers using ioctl
 * @param base base address, used for memory mapped access to SDIO module registers.
 * @param sdio_config pointer to sdio_config_t struct holding sdio configuration
 * @param sdio_addr sdio address, this is not the SDIO module register address
 * @param read_data pointer to hold read back data
 * @param write_data optional write data when performing read 
 */
int spi_sdio_read(int fd, void* base, sdio_config_t* sdio_config, unsigned int sdio_addr,
	unsigned int* read_data, unsigned int write_data)
{
	unsigned int tx_data;
	unsigned int rx_data;
	int timeout;

	if(sdio_config->tran_width <= 32) 
	{ // not change old working code
		tx_data	= write_data<<(sdio_config->data_pos - sdio_config->data_width);
		if(sdio_config->rw_bit_pos > 0) 
		// has R/W bit. R/W bit position is 1 based
		{
			// add r/w read bit
			tx_data |= sdio_config->rw_bit_read_pol<<(sdio_config->rw_bit_pos-1);
			
			if(sdio_config->rw_bit_pos == sdio_config->tran_width)
			{
				tx_data |= (sdio_config->data_pos == sdio_config->data_width)? 
						(sdio_addr<<sdio_config->data_width) : sdio_addr;
			}
			else if(sdio_config->rw_bit_pos == 1)
			{
				tx_data |= (sdio_config->data_pos == sdio_config->tran_width)? 
						sdio_addr<<1 : (sdio_addr<<(sdio_config->data_width+1));			
			}
			else
			{
				tx_data |= (sdio_config->data_pos == sdio_config->tran_width)? 
					sdio_addr : (sdio_addr<<(sdio_config->data_width+1));						
			}
		}
		else
		// no R/W bit
		{
			if(sdio_config->tran_width == sdio_config->data_pos)
			// data first
			{
				tx_data	|= sdio_addr;
			}
			else
			// address first
			{
				tx_data	|= sdio_addr<<sdio_config->data_width;		
			}
		}
		
		tx_data	<<= (32 - sdio_config->tran_width);
	}
	else 
	// todo: generic support (code only for AD9915 case)
	{
		tx_data = sdio_config->rw_bit_read_pol<<(31-(sdio_config->tran_width - sdio_config->rw_bit_pos));
		tx_data	|= sdio_addr<<(32 - (sdio_config->tran_width - sdio_config->data_width));
		
		printf("sdio read tx_data = 0x%x\n", tx_data);
	}	

	if(sdio_config->write_reg32(fd, base, sdio_config->base_addr + SDIO_SPIDTR, tx_data) == EXIT_FAILURE)
	{
		perror("Failed to write SDIO register\n");
		return EXIT_FAILURE;
	}
	
	// wait for transaction complete
	timeout = 10000;
	do
	{
		if(sdio_config->read_reg32(fd, base, sdio_config->base_addr + SDIO_SPISR, &rx_data) == EXIT_FAILURE)
		{
			perror("Failed to read SDIO register\n");
			return EXIT_FAILURE;
		}
	}
	while( (rx_data & 1) == 0 && --timeout );

	if(timeout == 0)
	{
		fprintf(stderr, "Time out in waiting %s SDIO write to finish.\n", sdio_config->name);
		return EXIT_FAILURE;
	}	

	// read data
	if(sdio_config->read_reg32(fd, base, sdio_config->base_addr + SDIO_SPIDRR, &rx_data) == EXIT_FAILURE)
	{
		perror("Failed to read SDIO register\n");
		return EXIT_FAILURE;
	}	
	
	*read_data = rx_data << (32 - sdio_config->data_pos + sdio_config->rb_shift);
	*read_data >>= (32 - sdio_config->data_width);
	
	return EXIT_SUCCESS;
}

int sdio_sel_ch(int fd, void* base, sdio_config_t* sdio_config, unsigned int ch)
{
	if(sdio_config->write_reg32(fd, base, sdio_config->base_addr + SDIO_SPISSR, 1 << ch) == EXIT_FAILURE)
	{
		perror("Failed to write SDIO register\n");
		return EXIT_FAILURE;
	}
	
	return EXIT_SUCCESS;
}

// dev->private_data[1] format for SPI_SDIO device:
// 
// [15:8] write address extra shift
// [23:16] extra data to write to address field

int spi_write(dev_with_regs_t* dev, unsigned int reg_addr, unsigned int w_data)
{
	unsigned int write_reg;
	unsigned int channel = dev->curr_channel & 0x1f;

	sdio_config_t*  sdio_config = (sdio_config_t*) (dev->pBus);
	spi_sdio_init(dev->fd, dev->base, sdio_config);
	
	if(sdio_config->channels > 1) {
		channel = (channel >= sdio_config->channels)? 0 : channel;
		sdio_sel_ch(dev->fd, dev->base, sdio_config, channel);	
	}
	write_reg = reg_addr << ((dev->private_data[1] >> 8) & 0xff);
	write_reg |= (dev->private_data[1] >> 16) & 0xff;	
	return spi_sdio_write(dev->fd, dev->base, sdio_config, write_reg, w_data);		
}

int spi_init(dev_with_regs_t* dev )
{
	sdio_config_t*  sdio_config = (sdio_config_t*) (dev->pBus);	
	spi_sdio_init(dev->fd, dev->base, sdio_config);
	
	if(dev->regs_init == NULL) 
	{
		return EXIT_SUCCESS;		
	}
	
	const reg_with_name_t* reg;
		
	for(reg = dev->regs_init; reg->name; reg++)
	{
		if((reg->access_type & 0x80) == 0x80)
		{
			vt_delay(reg->data);
			continue;
		}
		
		if((reg->access_type & 2) == 0)
		{
			continue;
		}				
		dev->write(dev, reg->addr, reg->data);		
	}
	
	return EXIT_SUCCESS;
}

// dev->private_data[0] format for SPI_SDIO device:
// 
// [7:0] read register
// [15:8] read address extra shift
// [23:16] extra data to write to the read register 
// [31] use write then read
int spi_read(dev_with_regs_t* dev, unsigned int reg_addr, unsigned int* r_data)
{
	unsigned int channel = dev->curr_channel & 0x1f;
	unsigned int write_data;
	unsigned int write_reg;
	
	sdio_config_t*  sdio_config = (sdio_config_t*) (dev->pBus);
	spi_sdio_init(dev->fd, dev->base, sdio_config);
	
	if(sdio_config->channels > 1) {
		channel = (channel >= sdio_config->channels)? 0 : channel;
		sdio_sel_ch(dev->fd, dev->base, sdio_config, channel);	
	}
	
	if(dev->private_data[0] & 0x80000000) {
		write_reg = dev->private_data[0] & 0xff;
		write_data = reg_addr << ((dev->private_data[0] >> 8) & 0xff);
		write_data |= (dev->private_data[0] >> 16) & 0xff;
		
		spi_write(dev, write_reg, write_data);	
		spi_sdio_read(dev->fd, dev->base, sdio_config, write_reg, r_data, write_data);
	} else {
		spi_sdio_read(dev->fd, dev->base, sdio_config, reg_addr, r_data, 0);
	}

	return EXIT_SUCCESS;
}

int spi_dump(dev_with_regs_t* dev)
{
	unsigned int r_data;
	const reg_with_name_t* reg;
	char w[8];
	char f[100] = "0x%04X \t0x%0";
	unsigned int data_width = 8;
		
	if(dev->pBus != NULL && dev->bus_type == SDIO) 
	{
		sdio_config_t*  sdio_config = (sdio_config_t*) (dev->pBus);	
		data_width = sdio_config->data_width;	
	}
	
	int bits = data_width/4 + ((data_width%4)? 1:0);
	
	sprintf(w, "%d", bits);
	strcat(f, w);
	strcat(f, "X\t%s\n");
		
	printf("//---- Dumping %s Registers ---------\n", dev->full_name);
	printf("Addr	  \tData      \tName\n");
	
	for(reg = dev->regs_dump; reg->name; reg++)
	{
		if((reg->access_type & 1) == 0)
		{
			continue;
		}
				
		dev->read(dev, reg->addr, &r_data);
		
		printf( f, reg->addr, r_data, reg->name);
		
	}

	printf( "\n" );
	return EXIT_SUCCESS;
}