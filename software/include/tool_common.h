#ifndef __TOOL_COMMON_H_
#define __TOOL_COMMON_H_

#include "devices.h"

#ifdef __PCIE_MMAP__
#include <pci/pci.h>
typedef struct
{
	struct pci_access*	pci;		// PCILIB access structure
	struct pci_dev*		dev;		// PCILIB device structure
	FILE*			mem_file;	// handle to /dev/mem
	void*			base;		// mmapped base address of BAR0
} pci_card_context_t;
#endif

typedef int (*cmd_func_t)( int argc, char* argv[] );

typedef struct
{
	const char*		name;
	cmd_func_t		func;
} cmd_t;

int hex_dump( void* base, const char* tag, unsigned int offset, unsigned int len );
int reg_dump(int fd, void* base, unsigned int module_base, const char* tag, const reg_with_name_t* regs );
unsigned int get_reg_bits(int fd, void* base, unsigned int reg_addr, unsigned int bit_mask);
void set_reg_bits(int fd, void* base, unsigned int reg_addr, unsigned int bit_mask);
void reset_reg_bits(int fd, void* base, unsigned int reg_addr, unsigned int bit_mask);
void toggle_reg_bits(int fd, void* base, unsigned int reg_addr, unsigned int bit_mask);
void write_reg_bits(int fd, void* base, unsigned int reg_addr, unsigned int bit_mask, unsigned int bit_data);
unsigned int read_reg_bits(int fd, void* base, unsigned int reg_addr, unsigned int bit_mask);

#endif /* __TOOL_COMMON_H_ */
