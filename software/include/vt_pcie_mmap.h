#ifndef __VT_PCIE_MMAP_H_
#define __VT_PCIE_MMAP_H_

#include <ctype.h>

#define PCI_SUBSYSTEM_ID_POS	0x2E
#define DESIRED_VENDOR		0xabcd
#define PCI516_DEVICE		0x0516
#define AMC502_DEVICE		0x4502
#define AMC515_DEVICE		0x4515
#define AMC516_DEVICE		0x4516
#define AMC517_DEVICE		0x4517
#define AMC518_DEVICE		0x4518
#define AMC519_DEVICE		0x4519
#define AMC523_DEVICE		0x4523
#define AMC525_DEVICE		0x4525
#define AMC527_DEVICE		0x4527
#define AMC583_DEVICE		0x4583
#define AMC593_DEVICE		0x4593

int vt_carrier_bkpn_pcie_id_chk(unsigned short vendor_id, unsigned short device_id)
{
	if(vendor_id == DESIRED_VENDOR && (
	     device_id == PCI516_DEVICE || 
	     device_id == AMC502_DEVICE || 
	     device_id == AMC515_DEVICE ||
	     device_id == AMC516_DEVICE ||
	     device_id == AMC517_DEVICE ||
	     device_id == AMC518_DEVICE ||
	     device_id == AMC519_DEVICE ||
	     device_id == AMC523_DEVICE ||
	     device_id == AMC525_DEVICE ||
	     device_id == AMC527_DEVICE ||
	     device_id == AMC583_DEVICE ||
	     device_id == AMC593_DEVICE 
	     )) 
		return 1;
	else 
		return 0;
}

#endif /*__VT_PCIE_MMAP_H_*/
