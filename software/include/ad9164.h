#ifndef __AD9164_H_
#define __AD9164_H_

#include "devices.h"

/* 
ad916x api +
reg 0x30 0x301 0x30E 0x314 0x604 

datasheet rev. A +
0x80 0x132 0x133 0x134 0x135 0x2AC 0x2B3 0x31E 0x31F 0x320 0x321 0x322 0x323 0x32C 0x32D
0x32E 0x32F 0x480~0x497
*/

#define AD9164_REG_IF_CFG_A                       0x000
#define AD9164_REG_IF_CFG_B                       0x001
#define AD9164_REG_DEV_CFG                        0x002
#define AD9164_REG_CHIP_TYPE                      0x003
#define AD9164_REG_PROD_ID_LSB                    0x004
#define AD9164_REG_PROD_ID_MSB                    0x005
#define AD9164_REG_CHIP_GRADE                     0x006
#define AD9164_REG_IRQ_EN                         0x020
#define AD9164_REG_IRQ_STATUS                     0x024 
#define AD9164_REG_SYNC_LMFC_DELAY_FRAME          0x031
#define AD9164_REG_SYNC_LMFC_DELAY0               0x032
#define AD9164_REG_SYNC_LMFC_DELAY1               0x033
#define AD9164_REG_SYNC_LMFC_ST0                  0x034
#define AD9164_REG_SYNC_LMFC_ST1                  0x035
#define AD9164_REG_SYSREF_COUNT                   0x036
#define AD9164_REG_SYSREF_PHASE0                  0x037
#define AD9164_REG_SYSREF_PHASE1                  0x038
#define AD9164_REG_SYSREF_JITTER_WIN              0x039
#define AD9164_REG_SYNC_CTRL                      0x03A
#define AD9164_REG_TX_ENABLE                      0x03F
#define AD9164_REG_ANA_DAC_BIAS_PD                0x040
#define AD9164_REG_ANA_FSC0                       0x041
#define AD9164_REG_ANA_FSC1                       0x042
#define AD9164_REG_CLK_PHASE_TUNE                 0x07F
#define AD9164_REG_CLK_PD                         0x080
#define AD9164_REG_CLK_DUTY                       0x082
#define AD9164_REG_CLK_CRS_CTRL                   0x083
#define AD9164_REG_PLL_REF_CLK_PD                 0x084
#define AD9164_REG_SYSREF_CTRL0                   0x088
#define AD9164_REG_SYSREF_CTRL1                   0x089
#define AD9164_REG_DLL_PD                         0x090
#define AD9164_REG_DLL_CTRL                       0x091
#define AD9164_REG_DLL_STATUS                     0x092
#define AD9164_REG_DLL_GB                         0x093
#define AD9164_REG_DLL_COARSE                     0x094
#define AD9164_REG_DLL_FINE                       0x095
#define AD9164_REG_DLL_PHASE                      0x096
#define AD9164_REG_DLL_BW                         0x097
#define AD9164_REG_DLL_READ                       0x098
#define AD9164_REG_DLL_COARSE_RB                  0x099
#define AD9164_REG_DLL_FINE_RB                    0x09A
#define AD9164_REG_DLL_PHASE_RB                   0x09B
#define AD9164_REG_DIG_CLK_INVERT                 0x09D
#define AD9164_REG_DLL_CLK_DEBUG                  0x0A0
#define AD9164_REG_LANE_INTPL_MODE                0x110
#define AD9164_REG_DATAPATH_CFG                   0x111
#define AD9164_REG_FTW_UPDATE                     0x113
#define AD9164_REG_FTW0                           0x114
#define AD9164_REG_FTW1                           0x115
#define AD9164_REG_FTW2                           0x116
#define AD9164_REG_FTW3                           0x117
#define AD9164_REG_FTW4                           0x118
#define AD9164_REG_FTW5                           0x119
#define AD9164_REG_PHASE_OFFSET0                  0x11C
#define AD9164_REG_PHASE_OFFSET1                  0x11D
#define AD9164_REG_ACC_MOD0                       0x124
#define AD9164_REG_ACC_MOD1                       0x125
#define AD9164_REG_ACC_MOD2                       0x126
#define AD9164_REG_ACC_MOD3                       0x127
#define AD9164_REG_ACC_MOD4                       0x128
#define AD9164_REG_ACC_MOD5                       0x129
#define AD9164_REG_ACC_DELTA0                     0x12A
#define AD9164_REG_ACC_DELTA1                     0x12B
#define AD9164_REG_ACC_DELTA2                     0x12C
#define AD9164_REG_ACC_DELTA3                     0x12D
#define AD9164_REG_ACC_DELTA4                     0x12E
#define AD9164_REG_ACC_DELTA5                     0x12F
#define AD9164_REG_TEMP_SENS_LSB                  0x132
#define AD9164_REG_TEMP_SENS_MSB                  0x133
#define AD9164_REG_TEMP_SENS_UPDATE               0x134
#define AD9164_REG_TEMP_SENS_CTRL                 0x135
#define AD9164_REG_PRBS                           0x14B
#define AD9164_REG_PRBS_ERROR_I                   0x14C
#define AD9164_REG_PRBS_ERROR_Q                   0x14D
#define AD9164_REG_TEST_DC_DATA1                  0x14E
#define AD9164_REG_TEST_DC_DATA0                  0x14F
#define AD9164_REG_DIG_TEST                       0x150
#define AD9164_REG_DECODE_CTRL                    0x151
#define AD9164_REG_DECODE_MODE                    0x152
#define AD9164_REG_SPI_STRENGTH                   0x1DF
#define AD9164_REG_MASTER_PD                      0x200
#define AD9164_REG_PHY_PD                         0x201
#define AD9164_REG_GENERIC_PD                     0x203
#define AD9164_REG_CDR_RESET                      0x206
#define AD9164_REG_CDR_OP_MODE_0                  0x230
#define AD9164_REG_EQ_CONFIG_PHY_0_1              0x250
#define AD9164_REG_EQ_CONFIG_PHY_2_3              0x251
#define AD9164_REG_EQ_CONFIG_PHY_4_5              0x252
#define AD9164_REG_EQ_CONFIG_PHY_6_7              0x253
#define AD9164_REG_SYNTH_ENABLE_CTRL              0x280
#define AD9164_REG_PLL_STATUS                     0x281
#define AD9164_REG_REF_CLK_DIVIDER_LDO            0x289
#define AD9164_REG_TERM_BLK1_CTRL0                0x2A7
#define AD9164_REG_TERM_BLK1_CTRL1                0x2A8
#define AD9164_REG_TERM_BLK1_RD_REG0              0x2AC
#define AD9164_REG_TERM_BLK2_CTRL0                0x2AE
#define AD9164_REG_TERM_BLK2_CTRL1                0x2AF
#define AD9164_REG_TERM_BLK2_RD_REG0              0x2B3
#define AD9164_REG_TERM_OFFSET_0                  0x2BB
#define AD9164_REG_TERM_OFFSET_1                  0x2BC
#define AD9164_REG_TERM_OFFSET_2                  0x2BD
#define AD9164_REG_TERM_OFFSET_3                  0x2BE
#define AD9164_REG_TERM_OFFSET_4                  0x2BF
#define AD9164_REG_TERM_OFFSET_5                  0x2C0
#define AD9164_REG_TERM_OFFSET_6                  0x2C1
#define AD9164_REG_TERM_OFFSET_7                  0x2C2
#define AD9164_REG_GEN_JRX_CTRL_0                 0x300
#define AD9164_REG_DYN_LINK_LATENCY_0             0x302
#define AD9164_REG_LMFC_DELAY_0                   0x304
#define AD9164_REG_LMFC_VAR_0                     0x306
#define AD9164_REG_XBAR_LN_0_1                    0x308
#define AD9164_REG_XBAR_LN_2_3                    0x309
#define AD9164_REG_XBAR_LN_4_5                    0x30A
#define AD9164_REG_XBAR_LN_6_7                    0x30B
#define AD9164_REG_FIFO_STATUS_0                  0x30C
#define AD9164_REG_FIFO_STATUS_1                  0x30D
#define AD9164_REG_SYNCB_GEN_0                    0x311
#define AD9164_REG_SYNCB_GEN_1                    0x312
#define AD9164_REG_SYNCB_GEN_3                    0x313
#define AD9164_REG_PHY_PRBS_EN                    0x315
#define AD9164_REG_PHY_PRBS_CTRL                  0x316
#define AD9164_REG_PHY_PRBS_ERROR_THRESHOLD_LO    0x317
#define AD9164_REG_PHY_PRBS_ERROR_THRESHOLD_MID   0x318
#define AD9164_REG_PHY_PRBS_ERROR_THRESHOLD_HI    0x319
#define AD9164_REG_PHY_PRBS_ERRCNT_0              0x31A
#define AD9164_REG_PHY_PRBS_ERRCNT_1              0x31B
#define AD9164_REG_PHY_PRBS_ERRCNT_2              0x31C
#define AD9164_REG_PHY_PRBS_STAT                  0x31D
#define AD9164_REG_PHY_DATA_SNAPSHOT_CTRL         0x31E
#define AD9164_REG_PHY_SNAPSHOT_DATA_BYTE0        0x31F
#define AD9164_REG_PHY_SNAPSHOT_DATA_BYTE1        0x320
#define AD9164_REG_PHY_SNAPSHOT_DATA_BYTE2        0x321
#define AD9164_REG_PHY_SNAPSHOT_DATA_BYTE3        0x322
#define AD9164_REG_PHY_SNAPSHOT_DATA_BYTE4        0x323
#define AD9164_REG_PHY_SHORT_TPL_TEST_0           0x32C
#define AD9164_REG_PHY_SHORT_TPL_TEST_1           0x32D
#define AD9164_REG_PHY_SHORT_TPL_TEST_2           0x32E
#define AD9164_REG_PHY_SHORT_TPL_TEST_3           0x32F
#define AD9164_REG_JESD_BIT_INVERT_CTRL           0x334
#define AD9164_REG_DID                            0x400
#define AD9164_REG_BID                            0x401
#define AD9164_REG_LID_O                          0x402
#define AD9164_REG_SCR                            0x403
#define AD9164_REG_F                              0x404
#define AD9164_REG_K                              0x405
#define AD9164_REG_M                              0x406
#define AD9164_REG_CS_n                           0x407
#define AD9164_REG_NP                             0x408
#define AD9164_REG_S                              0x409
#define AD9164_REG_hd_CF                          0x40A
#define AD9164_REG_RES_1                          0x40B
#define AD9164_REG_RES_2                          0x40C
#define AD9164_REG_CHECKSUM_0                     0x40D
#define AD9164_REG_COMPSUM_0                      0x40E
#define AD9164_REG_LID_1                          0x412
#define AD9164_REG_CHECKSUM_1                     0x415
#define AD9164_REG_COMPSUM_1                      0x416
#define AD9164_REG_LID_2                          0x41A
#define AD9164_REG_CHECKSUM_2                     0x41D
#define AD9164_REG_COMPSUM_2                      0x41E
#define AD9164_REG_LID_3                          0x422
#define AD9164_REG_CHECKSUM_3                     0x425
#define AD9164_REG_COMPSUM_3                      0x426
#define AD9164_REG_LID_4                          0x42A
#define AD9164_REG_CHECKSUM_4                     0x42D
#define AD9164_REG_COMPSUM_4                      0x42E
#define AD9164_REG_LID_5                          0x432
#define AD9164_REG_CHECKSUM_5                     0x435
#define AD9164_REG_COMPSUM_5                      0x436
#define AD9164_REG_LID_6                          0x43A
#define AD9164_REG_CHECKSUM_6                     0x43D
#define AD9164_REG_COMPSUM_6                      0x43E
#define AD9164_REG_LID_7                          0x442
#define AD9164_REG_CHECKSUM_7                     0x445
#define AD9164_REG_COMPSUM_7                      0x446
#define AD9164_REG_ILS_DID                        0x450
#define AD9164_REG_ILS_BID                        0x451
#define AD9164_REG_ILS_LID0                       0x452
#define AD9164_REG_ILS_SCR_L                      0x453
#define AD9164_REG_ILS_F                          0x454
#define AD9164_REG_ILS_K                          0x455
#define AD9164_REG_ILS_M                          0x456
#define AD9164_REG_ILS_CS_N                       0x457
#define AD9164_REG_ILS_NP                         0x458
#define AD9164_REG_ILS_S                          0x459
#define AD9164_REG_ILS_HD_CF                      0x45A
#define AD9164_REG_ILS_RES_1                      0x45B
#define AD9164_REG_ILS_RES_2                      0x45C
#define AD9164_REG_ILS_CHECKSUM                   0x45D
#define AD9164_REG_LANE_DESKEW                    0x46C
#define AD9164_REG_BAD_DISPARITY                  0x46D
#define AD9164_REG_NOT_IN_TABLE                   0x46E
#define AD9164_REG_UNEXPECTED_KC                  0x46F
#define AD9164_REG_CODE_GRP_SYNC                  0x470
#define AD9164_REG_FRAME_SYNC                     0x471
#define AD9164_REG_GOOD_CHECKSUM                  0x472
#define AD9164_REG_INIT_LANE_SYNC                 0x473
#define AD9164_REG_JESD_CTRL0                     0x475
#define AD9164_REG_JESD_CTRL1                     0x476
#define AD9164_REG_JESD_CTRL2                     0x477
#define AD9164_REG_KVAL                           0x478
#define AD9164_REG_ERROR_THRES                    0x47C
#define AD9164_REG_SYNC_ASSERT_MASK               0x47D
#define AD9164_REG_ECNT_CTRL0                     0x480
#define AD9164_REG_ECNT_CTRL1                     0x481
#define AD9164_REG_ECNT_CTRL2                     0x482
#define AD9164_REG_ECNT_CTRL3                     0x483
#define AD9164_REG_ECNT_CTRL4                     0x484
#define AD9164_REG_ECNT_CTRL5                     0x485
#define AD9164_REG_ECNT_CTRL6                     0x486
#define AD9164_REG_ECNT_CTRL7                     0x487
#define AD9164_REG_ECNT_TCH0                      0x488
#define AD9164_REG_ECNT_TCH1                      0x489
#define AD9164_REG_ECNT_TCH2                      0x48A
#define AD9164_REG_ECNT_TCH3                      0x48B
#define AD9164_REG_ECNT_TCH4                      0x48C
#define AD9164_REG_ECNT_TCH5                      0x48D
#define AD9164_REG_ECNT_TCH6                      0x48E
#define AD9164_REG_ECNT_TCH7                      0x48F
#define AD9164_REG_ECNT_STAT0                     0x490
#define AD9164_REG_ECNT_STAT1                     0x491
#define AD9164_REG_ECNT_STAT2                     0x492
#define AD9164_REG_ECNT_STAT3                     0x493
#define AD9164_REG_ECNT_STAT4                     0x494
#define AD9164_REG_ECNT_STAT5                     0x495
#define AD9164_REG_ECNT_STAT6                     0x496
#define AD9164_REG_ECNT_STAT7                     0x497
#define AD9164_REG_LINK_STATUS0                   0x4B0
#define AD9164_REG_LINK_STATUS1                   0x4B1
#define AD9164_REG_LINK_STATUS2                   0x4B2
#define AD9164_REG_LINK_STATUS3                   0x4B3
#define AD9164_REG_LINK_STATUS4                   0x4B4
#define AD9164_REG_LINK_STATUS5                   0x4B5
#define AD9164_REG_LINK_STATUS6                   0x4B6
#define AD9164_REG_LINK_STATUS7                   0x4B7
#define AD9164_REG_JESD_IRQ_ENABLE_A              0x4B8
#define AD9164_REG_JESD_IRQ_ENABLE_B              0x4B9
#define AD9164_REG_JESD_IRQ_STATUS_A              0x4BA
#define AD9164_REG_JESD_IRQ_STATUS_B              0x4BB
#define AD9164_REG_HOPF_CTRL                      0x800
#define AD9164_REG_HOPF_FTW1_0                    0x806
#define AD9164_REG_HOPF_FTW1_1                    0x807
#define AD9164_REG_HOPF_FTW1_2                    0x808
#define AD9164_REG_HOPF_FTW1_3                    0x809
#define AD9164_REG_HOPF_FTW2_0                    0x80A
#define AD9164_REG_HOPF_FTW2_1                    0x80B
#define AD9164_REG_HOPF_FTW2_2                    0x80C
#define AD9164_REG_HOPF_FTW2_3                    0x80D
#define AD9164_REG_HOPF_FTW3_0                    0x80E
#define AD9164_REG_HOPF_FTW3_1                    0x80F
#define AD9164_REG_HOPF_FTW3_2                    0x810
#define AD9164_REG_HOPF_FTW3_3                    0x811
#define AD9164_REG_HOPF_FTW4_0                    0x812
#define AD9164_REG_HOPF_FTW4_1                    0x813
#define AD9164_REG_HOPF_FTW4_2                    0x814
#define AD9164_REG_HOPF_FTW4_3                    0x815
#define AD9164_REG_HOPF_FTW5_0                    0x816
#define AD9164_REG_HOPF_FTW5_1                    0x817
#define AD9164_REG_HOPF_FTW5_2                    0x818
#define AD9164_REG_HOPF_FTW5_3                    0x819
#define AD9164_REG_HOPF_FTW6_0                    0x81A
#define AD9164_REG_HOPF_FTW6_1                    0x81B
#define AD9164_REG_HOPF_FTW6_2                    0x81C
#define AD9164_REG_HOPF_FTW6_3                    0x81D
#define AD9164_REG_HOPF_FTW7_0                    0x81E
#define AD9164_REG_HOPF_FTW7_1                    0x81F
#define AD9164_REG_HOPF_FTW7_2                    0x820
#define AD9164_REG_HOPF_FTW7_3                    0x821
#define AD9164_REG_HOPF_FTW8_0                    0x822
#define AD9164_REG_HOPF_FTW8_1                    0x823
#define AD9164_REG_HOPF_FTW8_2                    0x824
#define AD9164_REG_HOPF_FTW8_3                    0x825
#define AD9164_REG_HOPF_FTW9_0                    0x826
#define AD9164_REG_HOPF_FTW9_1                    0x827
#define AD9164_REG_HOPF_FTW9_2                    0x828
#define AD9164_REG_HOPF_FTW9_3                    0x829
#define AD9164_REG_HOPF_FTW10_0                   0x82A
#define AD9164_REG_HOPF_FTW10_1                   0x82B
#define AD9164_REG_HOPF_FTW10_2                   0x82C
#define AD9164_REG_HOPF_FTW10_3                   0x82D
#define AD9164_REG_HOPF_FTW11_0                   0x82E
#define AD9164_REG_HOPF_FTW11_1                   0x82F
#define AD9164_REG_HOPF_FTW11_2                   0x830
#define AD9164_REG_HOPF_FTW11_3                   0x831
#define AD9164_REG_HOPF_FTW12_0                   0x832
#define AD9164_REG_HOPF_FTW12_1                   0x833
#define AD9164_REG_HOPF_FTW12_2                   0x834
#define AD9164_REG_HOPF_FTW12_3                   0x835
#define AD9164_REG_HOPF_FTW13_0                   0x836
#define AD9164_REG_HOPF_FTW13_1                   0x837
#define AD9164_REG_HOPF_FTW13_2                   0x838
#define AD9164_REG_HOPF_FTW13_3                   0x839
#define AD9164_REG_HOPF_FTW14_0                   0x83A
#define AD9164_REG_HOPF_FTW14_1                   0x83B
#define AD9164_REG_HOPF_FTW14_2                   0x83C
#define AD9164_REG_HOPF_FTW14_3                   0x83D
#define AD9164_REG_HOPF_FTW15_0                   0x83E
#define AD9164_REG_HOPF_FTW15_1                   0x83F
#define AD9164_REG_HOPF_FTW15_2                   0x840
#define AD9164_REG_HOPF_FTW15_3                   0x841
#define AD9164_REG_HOPF_FTW16_0                   0x842
#define AD9164_REG_HOPF_FTW16_1                   0x843
#define AD9164_REG_HOPF_FTW16_2                   0x844
#define AD9164_REG_HOPF_FTW16_3                   0x845
#define AD9164_REG_HOPF_FTW17_0                   0x846
#define AD9164_REG_HOPF_FTW17_1                   0x847
#define AD9164_REG_HOPF_FTW17_2                   0x848
#define AD9164_REG_HOPF_FTW17_3                   0x849
#define AD9164_REG_HOPF_FTW18_0                   0x84A
#define AD9164_REG_HOPF_FTW18_1                   0x84B
#define AD9164_REG_HOPF_FTW18_2                   0x84C
#define AD9164_REG_HOPF_FTW18_3                   0x84D
#define AD9164_REG_HOPF_FTW19_0                   0x84E
#define AD9164_REG_HOPF_FTW19_1                   0x84F
#define AD9164_REG_HOPF_FTW19_2                   0x850
#define AD9164_REG_HOPF_FTW19_3                   0x851
#define AD9164_REG_HOPF_FTW20_0                   0x852
#define AD9164_REG_HOPF_FTW20_1                   0x853
#define AD9164_REG_HOPF_FTW20_2                   0x854
#define AD9164_REG_HOPF_FTW20_3                   0x855
#define AD9164_REG_HOPF_FTW21_0                   0x856
#define AD9164_REG_HOPF_FTW21_1                   0x857
#define AD9164_REG_HOPF_FTW21_2                   0x858
#define AD9164_REG_HOPF_FTW21_3                   0x859
#define AD9164_REG_HOPF_FTW22_0                   0x85A
#define AD9164_REG_HOPF_FTW22_1                   0x85B
#define AD9164_REG_HOPF_FTW22_2                   0x85C
#define AD9164_REG_HOPF_FTW22_3                   0x85D
#define AD9164_REG_HOPF_FTW23_0                   0x85E
#define AD9164_REG_HOPF_FTW23_1                   0x85F
#define AD9164_REG_HOPF_FTW23_2                   0x860
#define AD9164_REG_HOPF_FTW23_3                   0x861
#define AD9164_REG_HOPF_FTW24_0                   0x862
#define AD9164_REG_HOPF_FTW24_1                   0x863
#define AD9164_REG_HOPF_FTW24_2                   0x864
#define AD9164_REG_HOPF_FTW24_3                   0x865
#define AD9164_REG_HOPF_FTW25_0                   0x866
#define AD9164_REG_HOPF_FTW25_1                   0x867
#define AD9164_REG_HOPF_FTW25_2                   0x868
#define AD9164_REG_HOPF_FTW25_3                   0x869
#define AD9164_REG_HOPF_FTW26_0                   0x86A
#define AD9164_REG_HOPF_FTW26_1                   0x86B
#define AD9164_REG_HOPF_FTW26_2                   0x86C
#define AD9164_REG_HOPF_FTW26_3                   0x86D
#define AD9164_REG_HOPF_FTW27_0                   0x86E
#define AD9164_REG_HOPF_FTW27_1                   0x86F
#define AD9164_REG_HOPF_FTW27_2                   0x870
#define AD9164_REG_HOPF_FTW27_3                   0x871
#define AD9164_REG_HOPF_FTW28_0                   0x872
#define AD9164_REG_HOPF_FTW28_1                   0x873
#define AD9164_REG_HOPF_FTW28_2                   0x874
#define AD9164_REG_HOPF_FTW28_3                   0x875
#define AD9164_REG_HOPF_FTW29_0                   0x876
#define AD9164_REG_HOPF_FTW29_1                   0x877
#define AD9164_REG_HOPF_FTW29_2                   0x878
#define AD9164_REG_HOPF_FTW29_3                   0x879
#define AD9164_REG_HOPF_FTW30_0                   0x87A
#define AD9164_REG_HOPF_FTW30_1                   0x87B
#define AD9164_REG_HOPF_FTW30_2                   0x87C
#define AD9164_REG_HOPF_FTW30_3                   0x87D
#define AD9164_REG_HOPF_FTW31_0                   0x87E
#define AD9164_REG_HOPF_FTW31_1                   0x87F
#define AD9164_REG_HOPF_FTW31_2                   0x880
#define AD9164_REG_HOPF_FTW31_3                   0x881

/* FMC217 AD9162/9164 settings datasheet Rev. A
 * Fixed JESD204B parameters: K=32 N=NP=16 CF=CS=0 HD=1
 * links=1 N=16 L=8 M=2 F=1 S=2 K=32  -- from Table 17 doesn't support in Table 28
             16   8   1   1   4   32  -- from Table 17 and Table 28
	 Table 24 and 0x459 register says S=1 or 2???
 *   sampling frequency fs = 4G
 *   serial link rate = 4Gsps*16bit*10/8(8b10b)/(8lanes) = 10Gbps
 *   LMFC = 10G/10/F/K = 10G/10/32 = 31.25MHz = 4GHz(Fs)/128
 */
const reg_with_name_t ad9164_init_regs[] =
{
	{0x000, 3, 0x18, 0x00, ""}, 	/* configure the device for 4-wire operation */
	{0x001, 3, 0x80, 0x00, ""}, 	/* DBG: single transfers */
	{0x0D2, 3, 0x52, 0x00, ""}, 	/*ADI INTERNAL:Internal Cal*/
	{0x0D2, 3, 0xD2, 0x00, ""}, 	/*ADI INTERNAL:Internal Cal*/
	{0x606, 3, 0x02, 0x00, ""}, 	/*ADI INTERNAL:Configure NVRAM*/
	{0x607, 3, 0x00, 0x00, ""}, 	/*ADI INTERNAL:Configure NVRAM*/
	{0x604, 3, 0x01, 0x00, ""},  	/*ADI INTERNAL:LOAD NVRAM FACTORY SETTINGS*/

	{0x000, 0x80, 2000, 0x00, ""},	/* delay 2ms */	
	{0x604, 1, 0x00, 0x00, ""}, 	/* read NVRAM_LOADER, will not execute, place holder here */

	{0x058, 3, 0x03, 0x00, ""}, /*Enable Bandgap Reference*/
	{0x090, 3, 0x1E, 0x00, ""}, /*Power up the DACCLK DLL*/
	{0x080, 3, 0x00, 0x00, ""}, /*Enable the Clock Receiver*/
	{0x040, 3, 0x00, 0x00, ""}, /*Enable the DAC BIAS circuits*/
	{0x09E, 3, 0x85, 0x00, ""}, /*ADI INTERNAL: Configure DACCLK DLL*/
	{0x091, 3, 0xE9, 0x00, ""}, /*Enable DACCLK DLL*/
	{0x0E8, 3, 0x20, 0x00, ""}, /*ADI INTERNAL: Enable Calibration Factors*/
	
	{0x152, 3, 0x00, 0x00, ""}, /*DECODE_MODE: NRZ*/

	{0x000, 0x80, 20000, 0x00, ""},	/* delay 20ms */	
	
	/* JESD204B start-up sequence */
	{0x300, 3, 0x00, 0x00, ""},
	{0x4B8, 3, 0xFF, 0x00, ""},
	{0x4B9, 3, 0x01, 0x00, ""},
	{0x480, 3, 0x38, 0x00, ""},
	{0x481, 3, 0x38, 0x00, ""},
	{0x482, 3, 0x38, 0x00, ""},
	{0x483, 3, 0x38, 0x00, ""},
	{0x484, 3, 0x38, 0x00, ""},
	{0x485, 3, 0x38, 0x00, ""},
	{0x486, 3, 0x38, 0x00, ""},
	{0x487, 3, 0x38, 0x00, ""},
	// {0x475, 3, 0x09, 0x00, ""},	/* held QBD: 0x475 bit 3 */
	// {0x110, 3, 0x81, 0x00, ""},
	{0x111, 3, 0x00, 0x00, ""},
	{0x230, 3, 0x28, 0x28, ""},	//
	{0x289, 3, 0x04, 0x04, ""},	//
	{0x084, 3, 0x00, 0x00, ""},
	{0x200, 3, 0x00, 0x00, ""},
	{0x475, 3, 0x09, 0x00, ""},
	{0x110, 3, 0x80, 0x00, ""},
	{0x453, 3, 0x87, 0x00, ""},	/* [7] scrambling enabled [4:0] L*/
	{0x456, 3, 0x00, 0x00, ""},	/* [7:0] M */
	{0x458, 3, 0x2F, 0x00, ""},	/* [7:5] subclass 001=1; [4:0] NP*/
	{0x459, 3, 0x23, 0x00, ""},	/* [7:5] JESD204B 001=B; [4:0] S */
	{0x45D, 3, 0x4b, 0x00, ""},	/* checksum 0x4b for M=0 S=4; 0x4a for M=1 S=2*/
	{0x475, 3, 0x01, 0x00, ""},
	{0x201, 3, 0x00, 0x00, ""},
	{0x2A7, 3, 0x01, 0x00, ""},
	{0x2AE, 3, 0x01, 0x00, ""},
	{0x29E, 3, 0x1F, 0x00, ""},
	// {0x206, 3, 0x00, 0x00, ""},
	// {0x206, 3, 0x01, 0x00, ""},
	{0x280, 3, 0x03, 0x00, ""},
	
	{0x268, 3, 0x22, 0x00, ""},	/* [7:6] 0=normal mode*/		//
	
	// {0x039, 3, 0x08, 0x00, ""}, 	/* SYSREF jitter window */
	// {0x03A, 3, 0x00, 0x00, ""}, 	/* clear one shot mode */
	// {0x03A, 3, 0x02, 0x00, ""}, 	/* one shot sync */
	// {0x000, 0x80, 2000, 0x00, ""},	/* delay 2ms */	
	
	{0x300, 3, 0x01, 0x00, ""},
	{0x024, 3, 0x1F, 0x00, ""},
	{0x4BA, 3, 0xFF, 0x00, ""},
	{0x4BB, 3, 0x01, 0x00, ""},

	{0x000, 0x80, 100000, 0x00, ""},	/* delay 100ms */	
	{0x300, 3, 0x00, 0x00, ""},		/* disable all links */
	{0x475, 3, 0x09, 0x00, ""},		/* soft reset DAC0 deframer */
	{0x110, 3, 0x80, 0x00, ""},		/* bypass mode */
	{0x456, 3, 0x00, 0x00, ""},		/* [7:0] M */
	{0x459, 3, 0x23, 0x00, ""},		/* [7:5] JESD204B 001=B; [4:0] S */
	{0x477, 3, 0x00, 0x00, ""},		/* disable ILS_MODE for DAC0 */
	{0x475, 3, 0x01, 0x00, ""},		/* bring DAC0 deframer out of reset */
	{0x300, 3, 0x01, 0x00, ""},		/* enable all links */

	
	
	{ 0, 0, 0, 0, NULL}
};

	
/* for dump only */
const reg_with_name_t ad9164_regs[] =
{
	{0x000, 1, 0x00, 0x00, "IF_CFG_A                    "},   
	{0x001, 1, 0x00, 0x00, "IF_CFG_B                    "},   
	{0x002, 1, 0x00, 0x00, "DEV_CFG                     "},   
	{0x003, 1, 0x00, 0x00, "CHIP_TYPE                   "},   
	{0x004, 1, 0x00, 0x00, "PROD_ID_LSB                 "},   
	{0x005, 1, 0x00, 0x00, "PROD_ID_MSB                 "},   
	{0x006, 1, 0x00, 0x00, "CHIP_GRADE                  "},   
	{0x020, 1, 0x00, 0x00, "IRQ_EN                      "},   
	{0x024, 1, 0x00, 0x00, "IRQ_STATUS                  "},   
	{0x031, 1, 0x00, 0x00, "SYNC_LMFC_DELAY_FRAME       "},   
	{0x032, 1, 0x00, 0x00, "SYNC_LMFC_DELAY0            "},   
	{0x033, 1, 0x00, 0x00, "SYNC_LMFC_DELAY1            "},   
	{0x034, 1, 0x00, 0x00, "SYNC_LMFC_ST0               "},   
	{0x035, 1, 0x00, 0x00, "SYNC_LMFC_ST1               "},   
	{0x036, 1, 0x00, 0x00, "SYSREF_COUNT                "},   
	{0x037, 1, 0x00, 0x00, "SYSREF_PHASE0               "},   
	{0x038, 1, 0x00, 0x00, "SYSREF_PHASE1               "},   
	{0x039, 1, 0x00, 0x00, "SYSREF_JITTER_WIN           "},   
	{0x03A, 1, 0x00, 0x00, "SYNC_CTRL                   "},   
	{0x03F, 1, 0x00, 0x00, "TX_ENABLE                   "},   
	{0x040, 1, 0x00, 0x00, "ANA_DAC_BIAS_PD             "},   
	{0x041, 1, 0x00, 0x00, "ANA_FSC0                    "},   
	{0x042, 1, 0x00, 0x00, "ANA_FSC1                    "},   
	{0x07F, 1, 0x00, 0x00, "CLK_PHASE_TUNE              "},   
	{0x080, 1, 0x00, 0x00, "CLK_PD                      "},   
	{0x082, 1, 0x00, 0x00, "CLK_DUTY                    "},   
	{0x083, 1, 0x00, 0x00, "CLK_CRS_CTRL                "},   
	{0x084, 1, 0x00, 0x00, "PLL_REF_CLK_PD              "},   
	{0x088, 1, 0x00, 0x00, "SYSREF_CTRL0                "},   
	{0x089, 1, 0x00, 0x00, "SYSREF_CTRL1                "},   
	{0x090, 1, 0x00, 0x00, "DLL_PD                      "},   
	{0x091, 1, 0x00, 0x00, "DLL_CTRL                    "},   
	{0x092, 1, 0x00, 0x00, "DLL_STATUS                  "},   
	{0x093, 1, 0x00, 0x00, "DLL_GB                      "},   
	{0x094, 1, 0x00, 0x00, "DLL_COARSE                  "},   
	{0x095, 1, 0x00, 0x00, "DLL_FINE                    "},   
	{0x096, 1, 0x00, 0x00, "DLL_PHASE                   "},   
	{0x097, 1, 0x00, 0x00, "DLL_BW                      "},   
	{0x098, 1, 0x00, 0x00, "DLL_READ                    "},   
	{0x099, 1, 0x00, 0x00, "DLL_COARSE_RB               "},   
	{0x09A, 1, 0x00, 0x00, "DLL_FINE_RB                 "},   
	{0x09B, 1, 0x00, 0x00, "DLL_PHASE_RB                "},   
	{0x09D, 1, 0x00, 0x00, "DIG_CLK_INVERT              "},   
	{0x0A0, 1, 0x00, 0x00, "DLL_CLK_DEBUG               "},   
	{0x110, 1, 0x00, 0x00, "LANE_INTPL_MODE             "},   
	{0x111, 1, 0x00, 0x00, "DATAPATH_CFG                "},   
	{0x113, 1, 0x00, 0x00, "FTW_UPDATE                  "},   
	{0x114, 1, 0x00, 0x00, "FTW0                        "},   
	{0x115, 1, 0x00, 0x00, "FTW1                        "},   
	{0x116, 1, 0x00, 0x00, "FTW2                        "},   
	{0x117, 1, 0x00, 0x00, "FTW3                        "},   
	{0x118, 1, 0x00, 0x00, "FTW4                        "},   
	{0x119, 1, 0x00, 0x00, "FTW5                        "},   
	{0x11C, 1, 0x00, 0x00, "PHASE_OFFSET0               "},   
	{0x11D, 1, 0x00, 0x00, "PHASE_OFFSET1               "},   
	{0x124, 1, 0x00, 0x00, "ACC_MOD0                    "},   
	{0x125, 1, 0x00, 0x00, "ACC_MOD1                    "},   
	{0x126, 1, 0x00, 0x00, "ACC_MOD2                    "},   
	{0x127, 1, 0x00, 0x00, "ACC_MOD3                    "},   
	{0x128, 1, 0x00, 0x00, "ACC_MOD4                    "},   
	{0x129, 1, 0x00, 0x00, "ACC_MOD5                    "},   
	{0x12A, 1, 0x00, 0x00, "ACC_DELTA0                  "},   
	{0x12B, 1, 0x00, 0x00, "ACC_DELTA1                  "},   
	{0x12C, 1, 0x00, 0x00, "ACC_DELTA2                  "},   
	{0x12D, 1, 0x00, 0x00, "ACC_DELTA3                  "},   
	{0x12E, 1, 0x00, 0x00, "ACC_DELTA4                  "},   
	{0x12F, 1, 0x00, 0x00, "ACC_DELTA5                  "},   
	{0x132, 1, 0x00, 0x00, "TEMP_SENS_LSB               "},   
	{0x133, 1, 0x00, 0x00, "TEMP_SENS_MSB               "},   
	{0x134, 1, 0x00, 0x00, "TEMP_SENS_UPDATE            "},   
	{0x135, 1, 0x00, 0x00, "TEMP_SENS_CTRL              "},   
	{0x14B, 1, 0x00, 0x00, "PRBS                        "},   
	{0x14C, 1, 0x00, 0x00, "PRBS_ERROR_I                "},   
	{0x14D, 1, 0x00, 0x00, "PRBS_ERROR_Q                "},   
	{0x14E, 1, 0x00, 0x00, "TEST_DC_DATA1               "},   
	{0x14F, 1, 0x00, 0x00, "TEST_DC_DATA0               "},   
	{0x150, 1, 0x00, 0x00, "DIG_TEST                    "},   
	{0x151, 1, 0x00, 0x00, "DECODE_CTRL                 "},   
	{0x152, 1, 0x00, 0x00, "DECODE_MODE                 "},   
	{0x1DF, 1, 0x00, 0x00, "SPI_STRENGTH                "},   
	{0x200, 1, 0x00, 0x00, "MASTER_PD                   "},   
	{0x201, 1, 0x00, 0x00, "PHY_PD                      "},   
	{0x203, 1, 0x00, 0x00, "GENERIC_PD                  "},   
	{0x206, 1, 0x00, 0x00, "CDR_RESET                   "},   
	{0x230, 1, 0x00, 0x00, "CDR_OP_MODE_0               "},   
	{0x250, 1, 0x00, 0x00, "EQ_CONFIG_PHY_0_1           "},   
	{0x251, 1, 0x00, 0x00, "EQ_CONFIG_PHY_2_3           "},   
	{0x252, 1, 0x00, 0x00, "EQ_CONFIG_PHY_4_5           "},   
	{0x253, 1, 0x00, 0x00, "EQ_CONFIG_PHY_6_7           "},   
	{0x268, 1, 0x00, 0x00, "EQ_BIAS_REG                 "},   
	{0x280, 1, 0x00, 0x00, "SYNTH_ENABLE_CTRL           "},   
	{0x281, 1, 0x00, 0x00, "PLL_STATUS                  "},   
	{0x289, 1, 0x00, 0x00, "REF_CLK_DIVIDER_LDO         "},   
	{0x2A7, 1, 0x00, 0x00, "TERM_BLK1_CTRL0             "},   
	{0x2A8, 1, 0x00, 0x00, "TERM_BLK1_CTRL1             "},   
	{0x2AC, 1, 0x00, 0x00, "TERM_BLK1_RD_REG0           "},   
	{0x2AE, 1, 0x00, 0x00, "TERM_BLK2_CTRL0             "},   
	{0x2AF, 1, 0x00, 0x00, "TERM_BLK2_CTRL1             "},   
	{0x2B3, 1, 0x00, 0x00, "TERM_BLK2_RD_REG0           "},   
	{0x2BB, 1, 0x00, 0x00, "TERM_OFFSET_0               "},   
	{0x2BC, 1, 0x00, 0x00, "TERM_OFFSET_1               "},   
	{0x2BD, 1, 0x00, 0x00, "TERM_OFFSET_2               "},   
	{0x2BE, 1, 0x00, 0x00, "TERM_OFFSET_3               "},   
	{0x2BF, 1, 0x00, 0x00, "TERM_OFFSET_4               "},   
	{0x2C0, 1, 0x00, 0x00, "TERM_OFFSET_5               "},   
	{0x2C1, 1, 0x00, 0x00, "TERM_OFFSET_6               "},   
	{0x2C2, 1, 0x00, 0x00, "TERM_OFFSET_7               "},   
	{0x300, 1, 0x00, 0x00, "GEN_JRX_CTRL_0              "},   
	{0x302, 1, 0x00, 0x00, "DYN_LINK_LATENCY_0          "},   
	{0x304, 1, 0x00, 0x00, "LMFC_DELAY_0                "},   
	{0x306, 1, 0x00, 0x00, "LMFC_VAR_0                  "},   
	{0x308, 1, 0x00, 0x00, "XBAR_LN_0_1                 "},   
	{0x309, 1, 0x00, 0x00, "XBAR_LN_2_3                 "},   
	{0x30A, 1, 0x00, 0x00, "XBAR_LN_4_5                 "},   
	{0x30B, 1, 0x00, 0x00, "XBAR_LN_6_7                 "},   
	{0x30C, 1, 0x00, 0x00, "FIFO_STATUS_0               "},   
	{0x30D, 1, 0x00, 0x00, "FIFO_STATUS_1               "},   
	{0x311, 1, 0x00, 0x00, "SYNCB_GEN_0                 "},   
	{0x312, 1, 0x00, 0x00, "SYNCB_GEN_1                 "},   
	{0x313, 1, 0x00, 0x00, "SYNCB_GEN_3                 "},   
	{0x315, 1, 0x00, 0x00, "PHY_PRBS_EN                 "},   
	{0x316, 1, 0x00, 0x00, "PHY_PRBS_CTRL               "},   
	{0x317, 1, 0x00, 0x00, "PHY_PRBS_ERROR_THRESHOLD_LO "},   
	{0x318, 1, 0x00, 0x00, "PHY_PRBS_ERROR_THRESHOLD_MID"},   
	{0x319, 1, 0x00, 0x00, "PHY_PRBS_ERROR_THRESHOLD_HI "},   
	{0x31A, 1, 0x00, 0x00, "PHY_PRBS_ERRCNT_0           "},   
	{0x31B, 1, 0x00, 0x00, "PHY_PRBS_ERRCNT_1           "},   
	{0x31C, 1, 0x00, 0x00, "PHY_PRBS_ERRCNT_2           "},   
	{0x31D, 1, 0x00, 0x00, "PHY_PRBS_STAT               "},   
	{0x31E, 1, 0x00, 0x00, "PHY_DATA_SNAPSHOT_CTRL      "},   
	{0x31F, 1, 0x00, 0x00, "PHY_SNAPSHOT_DATA_BYTE0     "},   
	{0x320, 1, 0x00, 0x00, "PHY_SNAPSHOT_DATA_BYTE1     "},   
	{0x321, 1, 0x00, 0x00, "PHY_SNAPSHOT_DATA_BYTE2     "},   
	{0x322, 1, 0x00, 0x00, "PHY_SNAPSHOT_DATA_BYTE3     "},   
	{0x323, 1, 0x00, 0x00, "PHY_SNAPSHOT_DATA_BYTE4     "},   
	{0x32C, 1, 0x00, 0x00, "PHY_SHORT_TPL_TEST_0        "},   
	{0x32D, 1, 0x00, 0x00, "PHY_SHORT_TPL_TEST_1        "},   
	{0x32E, 1, 0x00, 0x00, "PHY_SHORT_TPL_TEST_2        "},   
	{0x32F, 1, 0x00, 0x00, "PHY_SHORT_TPL_TEST_3        "},   
	{0x334, 1, 0x00, 0x00, "JESD_BIT_INVERT_CTRL        "},   
	{0x400, 1, 0x00, 0x00, "DID                         "},   
	{0x401, 1, 0x00, 0x00, "BID                         "},   
	{0x402, 1, 0x00, 0x00, "LID_O                       "},   
	{0x403, 1, 0x00, 0x00, "SCR                         "},   
	{0x404, 1, 0x00, 0x00, "F                           "},   
	{0x405, 1, 0x00, 0x00, "K                           "},   
	{0x406, 1, 0x00, 0x00, "M                           "},   
	{0x407, 1, 0x00, 0x00, "CS_n                        "},   
	{0x408, 1, 0x00, 0x00, "NP                          "},   
	{0x409, 1, 0x00, 0x00, "S                           "},   
	{0x40A, 1, 0x00, 0x00, "hd_CF                       "},   
	{0x40B, 1, 0x00, 0x00, "RES_1                       "},   
	{0x40C, 1, 0x00, 0x00, "RES_2                       "},   
	{0x40D, 1, 0x00, 0x00, "CHECKSUM_0                  "},   
	{0x40E, 1, 0x00, 0x00, "COMPSUM_0                   "},   
	{0x412, 1, 0x00, 0x00, "LID_1                       "},   
	{0x415, 1, 0x00, 0x00, "CHECKSUM_1                  "},   
	{0x416, 1, 0x00, 0x00, "COMPSUM_1                   "},   
	{0x41A, 1, 0x00, 0x00, "LID_2                       "},   
	{0x41D, 1, 0x00, 0x00, "CHECKSUM_2                  "},   
	{0x41E, 1, 0x00, 0x00, "COMPSUM_2                   "},   
	{0x422, 1, 0x00, 0x00, "LID_3                       "},   
	{0x425, 1, 0x00, 0x00, "CHECKSUM_3                  "},   
	{0x426, 1, 0x00, 0x00, "COMPSUM_3                   "},   
	{0x42A, 1, 0x00, 0x00, "LID_4                       "},   
	{0x42D, 1, 0x00, 0x00, "CHECKSUM_4                  "},   
	{0x42E, 1, 0x00, 0x00, "COMPSUM_4                   "},   
	{0x432, 1, 0x00, 0x00, "LID_5                       "},   
	{0x435, 1, 0x00, 0x00, "CHECKSUM_5                  "},   
	{0x436, 1, 0x00, 0x00, "COMPSUM_5                   "},   
	{0x43A, 1, 0x00, 0x00, "LID_6                       "},   
	{0x43D, 1, 0x00, 0x00, "CHECKSUM_6                  "},   
	{0x43E, 1, 0x00, 0x00, "COMPSUM_6                   "},   
	{0x442, 1, 0x00, 0x00, "LID_7                       "},   
	{0x445, 1, 0x00, 0x00, "CHECKSUM_7                  "},   
	{0x446, 1, 0x00, 0x00, "COMPSUM_7                   "},   
	{0x450, 1, 0x00, 0x00, "ILS_DID                     "},   
	{0x451, 1, 0x00, 0x00, "ILS_BID                     "},   
	{0x452, 1, 0x00, 0x00, "ILS_LID0                    "},   
	{0x453, 1, 0x00, 0x00, "ILS_SCR_L                   "},   
	{0x454, 1, 0x00, 0x00, "ILS_F                       "},   
	{0x455, 1, 0x00, 0x00, "ILS_K                       "},   
	{0x456, 1, 0x00, 0x00, "ILS_M                       "},   
	{0x457, 1, 0x00, 0x00, "ILS_CS_N                    "},   
	{0x458, 1, 0x00, 0x00, "ILS_NP                      "},   
	{0x459, 1, 0x00, 0x00, "ILS_S                       "},   
	{0x45A, 1, 0x00, 0x00, "ILS_HD_CF                   "},   
	{0x45B, 1, 0x00, 0x00, "ILS_RES_1                   "},   
	{0x45C, 1, 0x00, 0x00, "ILS_RES_2                   "},   
	{0x45D, 1, 0x00, 0x00, "ILS_CHECKSUM                "},   
	{0x46C, 1, 0x00, 0x00, "LANE_DESKEW                 "},   
	{0x46D, 1, 0x00, 0x00, "BAD_DISPARITY               "},   
	{0x46E, 1, 0x00, 0x00, "NOT_IN_TABLE                "},   
	{0x46F, 1, 0x00, 0x00, "UNEXPECTED_KC               "},   
	{0x470, 1, 0x00, 0x00, "CODE_GRP_SYNC               "},   
	{0x471, 1, 0x00, 0x00, "FRAME_SYNC                  "},   
	{0x472, 1, 0x00, 0x00, "GOOD_CHECKSUM               "},   
	{0x473, 1, 0x00, 0x00, "INIT_LANE_SYNC              "},   
	{0x475, 1, 0x00, 0x00, "JESD_CTRL0                  "},   
	{0x476, 1, 0x00, 0x00, "JESD_CTRL1                  "},   
	{0x477, 1, 0x00, 0x00, "JESD_CTRL2                  "},   
	{0x478, 1, 0x00, 0x00, "KVAL                        "},   
	{0x47C, 1, 0x00, 0x00, "ERROR_THRES                 "},   
	{0x47D, 1, 0x00, 0x00, "SYNC_ASSERT_MASK            "},   
	{0x480, 1, 0x00, 0x00, "ECNT_CTRL0                  "},   
	{0x481, 1, 0x00, 0x00, "ECNT_CTRL1                  "},   
	{0x482, 1, 0x00, 0x00, "ECNT_CTRL2                  "},   
	{0x483, 1, 0x00, 0x00, "ECNT_CTRL3                  "},   
	{0x484, 1, 0x00, 0x00, "ECNT_CTRL4                  "},   
	{0x485, 1, 0x00, 0x00, "ECNT_CTRL5                  "},   
	{0x486, 1, 0x00, 0x00, "ECNT_CTRL6                  "},   
	{0x487, 1, 0x00, 0x00, "ECNT_CTRL7                  "},   
	{0x488, 1, 0x00, 0x00, "ECNT_TCH0                   "},   
	{0x489, 1, 0x00, 0x00, "ECNT_TCH1                   "},   
	{0x48A, 1, 0x00, 0x00, "ECNT_TCH2                   "},   
	{0x48B, 1, 0x00, 0x00, "ECNT_TCH3                   "},   
	{0x48C, 1, 0x00, 0x00, "ECNT_TCH4                   "},   
	{0x48D, 1, 0x00, 0x00, "ECNT_TCH5                   "},   
	{0x48E, 1, 0x00, 0x00, "ECNT_TCH6                   "},   
	{0x48F, 1, 0x00, 0x00, "ECNT_TCH7                   "},   
	{0x490, 1, 0x00, 0x00, "ECNT_STAT0                  "},   
	{0x491, 1, 0x00, 0x00, "ECNT_STAT1                  "},   
	{0x492, 1, 0x00, 0x00, "ECNT_STAT2                  "},   
	{0x493, 1, 0x00, 0x00, "ECNT_STAT3                  "},   
	{0x494, 1, 0x00, 0x00, "ECNT_STAT4                  "},   
	{0x495, 1, 0x00, 0x00, "ECNT_STAT5                  "},   
	{0x496, 1, 0x00, 0x00, "ECNT_STAT6                  "},   
	{0x497, 1, 0x00, 0x00, "ECNT_STAT7                  "},   
	{0x4B0, 1, 0x00, 0x00, "LINK_STATUS0                "},   
	{0x4B1, 1, 0x00, 0x00, "LINK_STATUS1                "},   
	{0x4B2, 1, 0x00, 0x00, "LINK_STATUS2                "},   
	{0x4B3, 1, 0x00, 0x00, "LINK_STATUS3                "},   
	{0x4B4, 1, 0x00, 0x00, "LINK_STATUS4                "},   
	{0x4B5, 1, 0x00, 0x00, "LINK_STATUS5                "},   
	{0x4B6, 1, 0x00, 0x00, "LINK_STATUS6                "},   
	{0x4B7, 1, 0x00, 0x00, "LINK_STATUS7                "},   
	{0x4B8, 1, 0x00, 0x00, "JESD_IRQ_ENABLE_A           "},   
	{0x4B9, 1, 0x00, 0x00, "JESD_IRQ_ENABLE_B           "},   
	{0x4BA, 1, 0x00, 0x00, "JESD_IRQ_STATUS_A           "},   
	{0x4BB, 1, 0x00, 0x00, "JESD_IRQ_STATUS_B           "},   
	// {0x800, 1, 0x00, 0x00, "HOPF_CTRL                   "},   
	// {0x806, 1, 0x00, 0x00, "HOPF_FTW1_0                 "},   
	// {0x807, 1, 0x00, 0x00, "HOPF_FTW1_1                 "},   
	// {0x808, 1, 0x00, 0x00, "HOPF_FTW1_2                 "},   
	// {0x809, 1, 0x00, 0x00, "HOPF_FTW1_3                 "},   
	// {0x80A, 1, 0x00, 0x00, "HOPF_FTW2_0                 "},   
	// {0x80B, 1, 0x00, 0x00, "HOPF_FTW2_1                 "},   
	// {0x80C, 1, 0x00, 0x00, "HOPF_FTW2_2                 "},   
	// {0x80D, 1, 0x00, 0x00, "HOPF_FTW2_3                 "},   
	// {0x80E, 1, 0x00, 0x00, "HOPF_FTW3_0                 "},   
	// {0x80F, 1, 0x00, 0x00, "HOPF_FTW3_1                 "},   
	// {0x810, 1, 0x00, 0x00, "HOPF_FTW3_2                 "},   
	// {0x811, 1, 0x00, 0x00, "HOPF_FTW3_3                 "},   
	// {0x812, 1, 0x00, 0x00, "HOPF_FTW4_0                 "},   
	// {0x813, 1, 0x00, 0x00, "HOPF_FTW4_1                 "},   
	// {0x814, 1, 0x00, 0x00, "HOPF_FTW4_2                 "},   
	// {0x815, 1, 0x00, 0x00, "HOPF_FTW4_3                 "},   
	// {0x816, 1, 0x00, 0x00, "HOPF_FTW5_0                 "},   
	// {0x817, 1, 0x00, 0x00, "HOPF_FTW5_1                 "},   
	// {0x818, 1, 0x00, 0x00, "HOPF_FTW5_2                 "},   
	// {0x819, 1, 0x00, 0x00, "HOPF_FTW5_3                 "},   
	// {0x81A, 1, 0x00, 0x00, "HOPF_FTW6_0                 "},   
	// {0x81B, 1, 0x00, 0x00, "HOPF_FTW6_1                 "},   
	// {0x81C, 1, 0x00, 0x00, "HOPF_FTW6_2                 "},   
	// {0x81D, 1, 0x00, 0x00, "HOPF_FTW6_3                 "},   
	// {0x81E, 1, 0x00, 0x00, "HOPF_FTW7_0                 "},   
	// {0x81F, 1, 0x00, 0x00, "HOPF_FTW7_1                 "},   
	// {0x820, 1, 0x00, 0x00, "HOPF_FTW7_2                 "},   
	// {0x821, 1, 0x00, 0x00, "HOPF_FTW7_3                 "},   
	// {0x822, 1, 0x00, 0x00, "HOPF_FTW8_0                 "},   
	// {0x823, 1, 0x00, 0x00, "HOPF_FTW8_1                 "},   
	// {0x824, 1, 0x00, 0x00, "HOPF_FTW8_2                 "},   
	// {0x825, 1, 0x00, 0x00, "HOPF_FTW8_3                 "},   
	// {0x826, 1, 0x00, 0x00, "HOPF_FTW9_0                 "},   
	// {0x827, 1, 0x00, 0x00, "HOPF_FTW9_1                 "},   
	// {0x828, 1, 0x00, 0x00, "HOPF_FTW9_2                 "},   
	// {0x829, 1, 0x00, 0x00, "HOPF_FTW9_3                 "},   
	// {0x82A, 1, 0x00, 0x00, "HOPF_FTW10_0                "},   
	// {0x82B, 1, 0x00, 0x00, "HOPF_FTW10_1                "},   
	// {0x82C, 1, 0x00, 0x00, "HOPF_FTW10_2                "},   
	// {0x82D, 1, 0x00, 0x00, "HOPF_FTW10_3                "},   
	// {0x82E, 1, 0x00, 0x00, "HOPF_FTW11_0                "},   
	// {0x82F, 1, 0x00, 0x00, "HOPF_FTW11_1                "},   
	// {0x830, 1, 0x00, 0x00, "HOPF_FTW11_2                "},   
	// {0x831, 1, 0x00, 0x00, "HOPF_FTW11_3                "},   
	// {0x832, 1, 0x00, 0x00, "HOPF_FTW12_0                "},   
	// {0x833, 1, 0x00, 0x00, "HOPF_FTW12_1                "},   
	// {0x834, 1, 0x00, 0x00, "HOPF_FTW12_2                "},   
	// {0x835, 1, 0x00, 0x00, "HOPF_FTW12_3                "},   
	// {0x836, 1, 0x00, 0x00, "HOPF_FTW13_0                "},   
	// {0x837, 1, 0x00, 0x00, "HOPF_FTW13_1                "},   
	// {0x838, 1, 0x00, 0x00, "HOPF_FTW13_2                "},   
	// {0x839, 1, 0x00, 0x00, "HOPF_FTW13_3                "},   
	// {0x83A, 1, 0x00, 0x00, "HOPF_FTW14_0                "},   
	// {0x83B, 1, 0x00, 0x00, "HOPF_FTW14_1                "},   
	// {0x83C, 1, 0x00, 0x00, "HOPF_FTW14_2                "},   
	// {0x83D, 1, 0x00, 0x00, "HOPF_FTW14_3                "},   
	// {0x83E, 1, 0x00, 0x00, "HOPF_FTW15_0                "},   
	// {0x83F, 1, 0x00, 0x00, "HOPF_FTW15_1                "},   
	// {0x840, 1, 0x00, 0x00, "HOPF_FTW15_2                "},   
	// {0x841, 1, 0x00, 0x00, "HOPF_FTW15_3                "},   
	// {0x842, 1, 0x00, 0x00, "HOPF_FTW16_0                "},   
	// {0x843, 1, 0x00, 0x00, "HOPF_FTW16_1                "},   
	// {0x844, 1, 0x00, 0x00, "HOPF_FTW16_2                "},   
	// {0x845, 1, 0x00, 0x00, "HOPF_FTW16_3                "},   
	// {0x846, 1, 0x00, 0x00, "HOPF_FTW17_0                "},   
	// {0x847, 1, 0x00, 0x00, "HOPF_FTW17_1                "},   
	// {0x848, 1, 0x00, 0x00, "HOPF_FTW17_2                "},   
	// {0x849, 1, 0x00, 0x00, "HOPF_FTW17_3                "},   
	// {0x84A, 1, 0x00, 0x00, "HOPF_FTW18_0                "},   
	// {0x84B, 1, 0x00, 0x00, "HOPF_FTW18_1                "},   
	// {0x84C, 1, 0x00, 0x00, "HOPF_FTW18_2                "},   
	// {0x84D, 1, 0x00, 0x00, "HOPF_FTW18_3                "},   
	// {0x84E, 1, 0x00, 0x00, "HOPF_FTW19_0                "},   
	// {0x84F, 1, 0x00, 0x00, "HOPF_FTW19_1                "},   
	// {0x850, 1, 0x00, 0x00, "HOPF_FTW19_2                "},   
	// {0x851, 1, 0x00, 0x00, "HOPF_FTW19_3                "},   
	// {0x852, 1, 0x00, 0x00, "HOPF_FTW20_0                "},   
	// {0x853, 1, 0x00, 0x00, "HOPF_FTW20_1                "},   
	// {0x854, 1, 0x00, 0x00, "HOPF_FTW20_2                "},   
	// {0x855, 1, 0x00, 0x00, "HOPF_FTW20_3                "},   
	// {0x856, 1, 0x00, 0x00, "HOPF_FTW21_0                "},   
	// {0x857, 1, 0x00, 0x00, "HOPF_FTW21_1                "},   
	// {0x858, 1, 0x00, 0x00, "HOPF_FTW21_2                "},   
	// {0x859, 1, 0x00, 0x00, "HOPF_FTW21_3                "},   
	// {0x85A, 1, 0x00, 0x00, "HOPF_FTW22_0                "},   
	// {0x85B, 1, 0x00, 0x00, "HOPF_FTW22_1                "},   
	// {0x85C, 1, 0x00, 0x00, "HOPF_FTW22_2                "},   
	// {0x85D, 1, 0x00, 0x00, "HOPF_FTW22_3                "},   
	// {0x85E, 1, 0x00, 0x00, "HOPF_FTW23_0                "},   
	// {0x85F, 1, 0x00, 0x00, "HOPF_FTW23_1                "},   
	// {0x860, 1, 0x00, 0x00, "HOPF_FTW23_2                "},   
	// {0x861, 1, 0x00, 0x00, "HOPF_FTW23_3                "},   
	// {0x862, 1, 0x00, 0x00, "HOPF_FTW24_0                "},   
	// {0x863, 1, 0x00, 0x00, "HOPF_FTW24_1                "},   
	// {0x864, 1, 0x00, 0x00, "HOPF_FTW24_2                "},   
	// {0x865, 1, 0x00, 0x00, "HOPF_FTW24_3                "},   
	// {0x866, 1, 0x00, 0x00, "HOPF_FTW25_0                "},   
	// {0x867, 1, 0x00, 0x00, "HOPF_FTW25_1                "},   
	// {0x868, 1, 0x00, 0x00, "HOPF_FTW25_2                "},   
	// {0x869, 1, 0x00, 0x00, "HOPF_FTW25_3                "},   
	// {0x86A, 1, 0x00, 0x00, "HOPF_FTW26_0                "},   
	// {0x86B, 1, 0x00, 0x00, "HOPF_FTW26_1                "},   
	// {0x86C, 1, 0x00, 0x00, "HOPF_FTW26_2                "},   
	// {0x86D, 1, 0x00, 0x00, "HOPF_FTW26_3                "},   
	// {0x86E, 1, 0x00, 0x00, "HOPF_FTW27_0                "},   
	// {0x86F, 1, 0x00, 0x00, "HOPF_FTW27_1                "},   
	// {0x870, 1, 0x00, 0x00, "HOPF_FTW27_2                "},   
	// {0x871, 1, 0x00, 0x00, "HOPF_FTW27_3                "},   
	// {0x872, 1, 0x00, 0x00, "HOPF_FTW28_0                "},   
	// {0x873, 1, 0x00, 0x00, "HOPF_FTW28_1                "},   
	// {0x874, 1, 0x00, 0x00, "HOPF_FTW28_2                "},   
	// {0x875, 1, 0x00, 0x00, "HOPF_FTW28_3                "},   
	// {0x876, 1, 0x00, 0x00, "HOPF_FTW29_0                "},   
	// {0x877, 1, 0x00, 0x00, "HOPF_FTW29_1                "},   
	// {0x878, 1, 0x00, 0x00, "HOPF_FTW29_2                "},   
	// {0x879, 1, 0x00, 0x00, "HOPF_FTW29_3                "},   
	// {0x87A, 1, 0x00, 0x00, "HOPF_FTW30_0                "},   
	// {0x87B, 1, 0x00, 0x00, "HOPF_FTW30_1                "},   
	// {0x87C, 1, 0x00, 0x00, "HOPF_FTW30_2                "},   
	// {0x87D, 1, 0x00, 0x00, "HOPF_FTW30_3                "},   
	// {0x87E, 1, 0x00, 0x00, "HOPF_FTW31_0                "},   
	// {0x87F, 1, 0x00, 0x00, "HOPF_FTW31_1                "},   
	// {0x880, 1, 0x00, 0x00, "HOPF_FTW31_2                "},   
	// {0x881, 1, 0x00, 0x00, "HOPF_FTW31_3                "},   

	{ 0, 0, 0, 0, NULL}
};


#endif /* __AD9164_H_ */