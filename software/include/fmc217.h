#ifndef __FMC217_H_
#define __FMC217_H_

#include "devices.h"

#if ACCESS_INTERFACE == ZYNQ
	#define ZYNQ_AXI_BASEADDR	0xB0000000
	#define ZYNQ_AXI_SIZE		0x00100000
#endif

#define AMC_COMMON_BASE			0x00000
#define GPIO_BASE			0x01000
#define SDIO_LMK_BASE			0x02000
#define SDIO_LMXA_BASE			0x03000
#define SDIO_LMXD_BASE			0x04000
#define SDIO_HMC_BASE			0x05000
#define SDIO_ADC_BASE			0x06000
#define SDIO_DAC_BASE			0x07000
#define JESD_A0_BASE			0x08000
#define JESD_A1_BASE			0x09000
#define JESD_D0_BASE			0x0A000
#define A0_RD_BASE			0x0B000
#define A1_RD_BASE			0x0C000
#define BRAM_D0_BASE			0x40000
	
#define GPIO_OPTION			0x00000
#define GPIO_HMC_IO			0x00004
#define GPIO_ADC_ACQ_CTRL		0x00008
#define GPIO_ACQ_FIFO_STAT      	0x0000C
#define GPIO_DAC_IO  		     	0x00010
#define GPIO_DAC_TEST_PTN      		0x00014
#define GPIO_TRIG_OUT_SEL		0x00018
#define GPIO_LMK_IO			0x0001C
#define GPIO_LMXA_IO			0x00020
#define GPIO_LMXD_IO	        	0x00024
#define GPIO_ADC_IO	        	0x00028
#define GPIO_JESD_CTRL	        	0x0002C
#define GPIO_DAC_MEM_LEN		0x00030
#define GPIO_ADC_CH_SEL 		0x00034
#define GPIO_TX_POST_CURSOR		0x00038
#define GPIO_TX_PRE_CURSOR		0x0003C
#define GPIO_TX_DIFF_CTRL		0x00040
#define GPIO_RX_LPM_EN			0x00044
#define GPIO_PE43713_ADC_IO		0x00048
#define GPIO_PE43713_DAC_IO		0x0004C
#define GPIO_SCRATCH           		0x00FF4
#define GPIO_VER      			0x00FF8
#define GPIO_SIG		      	0x00FFC

//
// SHORT VERSION OF COMMON REGISTERS
//
#define REG_SIG				GPIO_BASE + GPIO_SIG
#define REG_VER				GPIO_BASE + GPIO_VER
#define REG_SCRATCH			GPIO_BASE + GPIO_SCRATCH

const reg_with_name_t gpio_regs[] =
{
	{ 0x00000, 1, 0, 0, "[1:0] Speed grade" },
	{ 0x00004, 3, 0, 0, "HMc7043 IO [1] RFSYNCIN [0] RESET" },	
	{ 0x00008, 3, 0, 0, "ADC_ACQ_FIFO_CTRL [3] wr_en [2] rst" },
	{ 0x0000C, 1, 0, 0, "ACQ_FIFO_STAT [7:4] ch1 (f1_empty f1_full f0_empty f0_full) [3:0] ch0" },
	{ 0x00010, 3, 0, 0, "DAC_IO [2] IRQn [1] TX_EN [0] RESETn" },
	{ 0x00014, 3, 0, 0, "DAC_TEST_PTN [3:0] 0=sine; 1=bram data; 2=adc data if applicable; others=0" },
	{ 0x00018, 3, 0, 0, "FRONT PANEL TRIG [5] trig in1 (opt A=1) [4] trig_in/trig_in0 [3:0] trig out sel (A=0)" },
	{ 0x0001C, 3, 0, 0, "LMK04828 IO [5:4] Status LD(2:1) [1] SYSREF_REQ [0] RESET" },
	{ 0x00020, 3, 0, 0, "LMX2592 for ADC IO [1] MUXOUT [0] CE" },
	{ 0x00024, 3, 0, 0, "LMX2592 for DAC IO [1] MUXOUT [0] CE" },
	{ 0x00028, 3, 0, 0, "ADC IO [11] CALSTAT [10:9] ORB [8:7] ORA [6] CALTRIG [5] TMSTP [4:3] NCOB [2:1] NCOA [0] PD" },
	{ 0x0002C, 3, 0, 0, "JESD204 ctrl [7:4] DAC (refclk_dis sysref_dis clk_dis usr_rst) [3:0] ADC" },
	{ 0x00030, 3, 0, 0, "[13:0] DAC memory data len" },
	{ 0x00034, 3, 0, 0, "[0] ADC channel sel for acquisition" },
	{ 0x00038, 3, 0, 0, "TX post cursor [4:0] " },
	{ 0x0003C, 3, 0, 0, "TX pre cursor [4:0] " },
	{ 0x00040, 3, 0, 0, "TX diff ctrl [3:0] " },
	{ 0x00044, 3, 0, 0, "RX LPM EN [0]" },
	{ 0x00048, 3, 0, 0, "PE43713 for ADC IO [2:0] (LE CLK SI)" },	
	{ 0x00048, 3, 0, 0, "PE43713 for DAC IO [2:0] (LE CLK SI)" },
	{ 0x00FF4, 3, 0, 0, "SCRATCH" },
	{ 0x00FF8, 1, 0, 0, "VER" },
	{ 0x00FFC, 1, 0, 0, "SIG" },
	{ 0, 0, 0, 0, NULL}
};

/*
 * GPIO bit definition
 */

#define GPIO_HMC7043_RESET_MASK		0x1
#define GPIO_HMC7043_SYNC_MASK		0x2

#define GPIO_ADC_ACQ_FIFO_RST_MASK	0x4
#define GPIO_ADC_ACQ_FIFO_WR_EN_MASK	0x8

#define GPIO_CH0_FIFO0_FULL_MASK	0x1
#define GPIO_CH0_FIFO0_EMPTY_MASK	0x2
#define GPIO_CH0_FIFO1_FULL_MASK	0x4
#define GPIO_CH0_FIFO1_EMPTY_MASK	0x8
#define GPIO_CH1_FIFO0_FULL_MASK	0x10
#define GPIO_CH1_FIFO0_EMPTY_MASK	0x20
#define GPIO_CH1_FIFO1_FULL_MASK	0x40
#define GPIO_CH1_FIFO1_EMPTY_MASK	0x80

#define GPIO_DAC_nRST_MASK		0x1
#define GPIO_DAC_TX_EN_MASK		0x2

#define GPIO_LMK04828_RESET_MASK	0x1
#define GPIO_LMK04828_SYNC_MASK		0x2

#define GPIO_LMX2592_CE_MASK		0x1
#define GPIO_LMX2592_LD_MASK		0x2

#define GPIO_ADC_PD_MASK		0x1

#define GPIO_JESD_ADC_RST_MASK		0x1
#define GPIO_JESD_ADC_CLK_DIS_MASK	0x2
#define GPIO_JESD_ADC_SYSREF_DIS_MASK	0x4
#define GPIO_JESD_ADC_REFCLK_DIS_MASK	0x8
#define GPIO_JESD_DAC_RST_MASK		0x10
#define GPIO_JESD_DAC_CLK_DIS_MASK	0x20
#define GPIO_JESD_DAC_SYSREF_DIS_MASK	0x40
#define GPIO_JESD_DAC_REFCLK_DIS_MASK	0x80

#define GPIO_ADC_ACQ_CH_SEL_MASK	0x1

#define GPIO_PE43713_SI_MASK		0x1
#define GPIO_PE43713_CLK_MASK		0x2
#define GPIO_PE43713_LE_MASK		0x4

#endif 
/* __FMC217_H_ */
