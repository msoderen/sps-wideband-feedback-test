#ifndef __HMC7043_H_
#define __HMC7043_H_

#include "devices.h"

#define HMC7043_SDIO_RB_SHIFT			0
#define HMC7043_SDIO_RBPOL			0
#define HMC7043_SDIO_CPOL			0
#define HMC7043_SDIO_CPHA			0
#define HMC7043_SDIO_RW_BIT_READ_POL		1
#define HMC7043_SDIO_RW_BIT_POS			24
#define HMC7043_SDIO_DATA_POS			8
#define HMC7043_SDIO_DATA_WIDTH			8
#define HMC7043_SDIO_TRAN_WIDTH			24
#define HMC7043_SDIO_PRIVATE_DATA0		0
#define HMC7043_SDIO_PRIVATE_DATA1		0

/* for FMC217 */

/*
 * Used to generate SYSREF and FPGA reference clock for ADC12DJ3200 only:
 *	ADC12DJ3200 sampling rate = 2.8GSPS
 * 	Serial link rate = 2.8Gsps*2channel*12.8(effective_bits)*10/8(8b10b)/8(lanes) = 11.2Gbps
 * 	LMFC = 11.2G/10/F/K = 11.2G/10/8/32 = 4.375MHz
 * 	
 * Settings:
 *	CLKIN=2.8GHz (Fs) from RFOUTB of the LMX2592 for ADC 
 *	SYSREF Timer:	4.375MHz/2 = 2.1875MHz = Fs/1280 = Fs/0x500
 *	SCLKOUT1:	To ADC SYSREF. LVPECL. 4.375MHz = Fs/640 = Fs/0x280
 *	CLKOUT2: 	To FMC GBTCLK0_M2C. LVPECL. 280MHz = Fs/10
 *	SCLKOUT3: 	To FMC LA0 for ADC JESD204B core sysref. LVDS. 4.375MHz = Fs/640 = Fs/0x280
 *
*/
const reg_with_name_t hmc7043_regs[] =
{
	// Bios settings
	{ 0x09F, 2,	0x4D,	0x55,	"Clk out driver low power setting (set to 0x4D instead of default value 0x55)" },
	{ 0x0A0, 2,	0xDF,	0x56,	"Clk out driver high power setting (set to 0xDF instead of default value 0x56)" },

	// Global control
	{ 0x000, 3,	0x00,	0x00,	"[0] Soft reset" },
	{ 0x001, 3,	0x40,	0x00,	"[7] Reseed [6] High perf. [3] Mute [2] Pulse gen req [1] restart FSM [0] Sleep mode" },
	{ 0x002, 3,	0x00,	0x00,	"[0] Multislip req" },
	{ 0x003, 3,	0x24,	0x34,	"[5] RF reseeder req [2] SYSREF timer en" },
	{ 0x004, 3,	0x03,	0x7F,	"[6:0] Seven pairs of 14-channel OE" },
	{ 0x006, 3,	0x00,	0x00,	"[0] Clear alarm" },

	// Input buffer
	{ 0x00A, 3,	0x01,	0x07,	"RFSYNCIN buffer [4] High-Z [3] LVPECL [2] AC coupling [1] 100ohm term. [0] En" },
	{ 0x00B, 3,	0x07,	0x07,	"CLKIN buffer [4] High-Z [3] LVPECL [2] AC coupling [1] 100ohm term. [0] En" },

	// GPIO/SDATA control
	{ 0x046, 3,	0x00,	0x00,	"GPI [3:1] sel [0] En" },
	{ 0x050, 3,	0x00,	0x37,	"GPO [6:2] sel [1] Mode [0] En" },
	{ 0x054, 3,	0x03,	0x03,	"SDATA [1] Mode 1:CMOS; 0:Open-drain [0] En" },

	// SYSREF/SYNC
	{ 0x05A, 3,	0x07,	0x00,	"[2:0] pulse gen. mode " },
	{ 0x05B, 3,	0x04,	0x04,	"SYNC ctrl [2] retime [0] invert polarity" },
	{ 0x05C, 3,	0x00,	0x00,	"SYSREF timer LSB[7:0]" },
	{ 0x05D, 3,	0x05,	0x01,	"[3:0] SYSREF timer MSB[11:8]" },

	// Clock distribution network
	{ 0x064, 3,	0x00,	0x00,	"Clock input ctrl [1] divide by 2 [0] low freq input. set <1GHz" },
	{ 0x065, 3,	0x00,	0x00,	"[0] analog delay low power mode" },

	// Alarm masks
	{ 0x071, 3,	0x10,	0x10,	"Alarm mask ctrl [4] sync req [2] clk out phase status [1] SYSREF sync status" },

	// Product ID
	{ 0x078, 1,	0x00,	0x00,	"Product ID [7:0]" },
	{ 0x079, 1,	0x00,	0x00,	"Product ID [15:8]" },
	{ 0x07A, 1,	0x00,	0x00,	"Product ID [23:16]" },

	// Alarm readback status
	{ 0x07B, 1,	0x00,	0x00,	"[0] Alarm" },
	{ 0x07D, 1,	0x00,	0x00,	"[4] Sync req status [2] clk out phase staus [1] SYSREF sync status" },

	// SYSREF status
	{ 0x091, 1,	0x00,	0x00,	"[4] channel outputs FSM busy [3:0] SYSREF FSM state" },

	// Clock distribution
	{ 0x0C8, 3,	0x10,	0xF3,	"ch 0 out ctrl. [7] high perf. [6] sync en [5] slip en [3:2] start-up mode [1] multislip en [0] en" },
	{ 0x0C9, 3,	0x04,	0x04,	"ch 0 divider. LSB[7:0]" },
	{ 0x0CA, 3,	0x00,	0x00,	"ch 0 divider. [3:0] MSB[11:8]" },
	{ 0x0CB, 3,	0x00,	0x00,	"ch 0 [4:0] fine analog delay" },
	{ 0x0CC, 3,	0x00,	0x00,	"ch 0 [4:0] coarse digital delay" },
	{ 0x0CD, 3,	0x00,	0x00,	"ch 0 multislip digital delay. LSB[7:0]" },
	{ 0x0CE, 3,	0x00,	0x00,	"ch 0 multislip digital delay. [3:0] MSB[11:8]" },
	{ 0x0CF, 3,	0x00,	0x00,	"ch 0 [1:0] output mux sel" },
	{ 0x0D0, 3,	0x09,	0x01,	"ch 0 [7:6] idle at zero [5] dynamic driver en [4:3] driver mode [1:0] CML impedance" }, // mode 0:CML; 1:LVPECL; 2:LVDS; 3:CMOS

	{ 0x0D2, 3,	0xD1,	0xFD,	"ch 1 out ctrl. [7] high perf. [6] sync en [5] slip en [3:2] start-up mode [1] multislip en [0] en" },
	{ 0x0D3, 3,	0x80,	0x00,	"ch 1 divider. LSB[7:0]" },
	{ 0x0D4, 3,	0x02,	0x01,	"ch 1 divider. [3:0] MSB[11:8]" },
	{ 0x0D5, 3,	0x00,	0x00,	"ch 1 [4:0] fine analog delay" },
	{ 0x0D6, 3,	0x00,	0x00,	"ch 1 [4:0] coarse digital delay" },
	{ 0x0D7, 3,	0x00,	0x00,	"ch 1 multislip digital delay. LSB[7:0]" },
	{ 0x0D8, 3,	0x00,	0x00,	"ch 1 multislip digital delay. [3:0] MSB[11:8]" },
	{ 0x0D9, 3,	0x00,	0x00,	"ch 1 [1:0] output mux sel" },
	{ 0x0DA, 3,	0x09,	0x30,	"ch 1 [7:6] idle at zero [5] dynamic driver en [4:3] driver mode [1:0] CML impedance" }, // mode 0:CML; 1:LVPECL; 2:LVDS; 3:CMOS

	{ 0x0DC, 3,	0xD1,	0xF3,	"ch 2 out ctrl. [7] high perf. [6] sync en [5] slip en [3:2] start-up mode [1] multislip en [0] en" },
	{ 0x0DD, 3,	0x0A,	0x08,	"ch 2 divider. LSB[7:0]" },
	{ 0x0DE, 3,	0x00,	0x00,	"ch 2 divider. [3:0] MSB[11:8]" },
	{ 0x0DF, 3,	0x00,	0x00,	"ch 2 [4:0] fine analog delay" },
	{ 0x0E0, 3,	0x00,	0x00,	"ch 2 [4:0] coarse digital delay" },
	{ 0x0E1, 3,	0x00,	0x00,	"ch 2 multislip digital delay. LSB[7:0]" },
	{ 0x0E2, 3,	0x00,	0x00,	"ch 2 multislip digital delay. [3:0] MSB[11:8]" },
	{ 0x0E3, 3,	0x00,	0x00,	"ch 2 [1:0] output mux sel" },
	{ 0x0E4, 3,	0x09,	0x01,	"ch 2 [7:6] idle at zero [5] dynamic driver en [4:3] driver mode [1:0] CML impedance" }, // mode 0:CML; 1:LVPECL; 2:LVDS; 3:CMOS

	{ 0x0E6, 3,	0xD1,	0xFD,	"ch 3 out ctrl. [7] high perf. [6] sync en [5] slip en [3:2] start-up mode [1] multislip en [0] en" },
	{ 0x0E7, 3,	0x80,	0x00,	"ch 3 divider. LSB[7:0]" },
	{ 0x0E8, 3,	0x02,	0x01,	"ch 3 divider. [3:0] MSB[11:8]" },
	{ 0x0E9, 3,	0x00,	0x00,	"ch 3 [4:0] fine analog delay" },
	{ 0x0EA, 3,	0x00,	0x00,	"ch 3 [4:0] coarse digital delay" },
	{ 0x0EB, 3,	0x00,	0x00,	"ch 3 multislip digital delay. LSB[7:0]" },
	{ 0x0EC, 3,	0x00,	0x00,	"ch 3 multislip digital delay. [3:0] MSB[11:8]" },
	{ 0x0ED, 3,	0x00,	0x00,	"ch 3 [1:0] output mux sel" },
	{ 0x0EE, 3,	0x11,	0x30,	"ch 3 [7:6] idle at zero [5] dynamic driver en [4:3] driver mode [1:0] CML impedance" }, // mode 0:CML; 1:LVPECL; 2:LVDS; 3:CMOS

	{ 0x0F0, 3,	0x10,	0xF3,	"ch 4 out ctrl. [7] high perf. [6] sync en [5] slip en [3:2] start-up mode [1] multislip en [0] en" },
	{ 0x0F1, 3,	0x04,	0x02,	"ch 4 divider. LSB[7:0]" },
	{ 0x0F2, 3,	0x00,	0x00,	"ch 4 divider. [3:0] MSB[11:8]" },
	{ 0x0F3, 3,	0x00,	0x00,	"ch 4 [4:0] fine analog delay" },
	{ 0x0F4, 3,	0x00,	0x00,	"ch 4 [4:0] coarse digital delay" },
	{ 0x0F5, 3,	0x00,	0x00,	"ch 4 multislip digital delay. LSB[7:0]" },
	{ 0x0F6, 3,	0x00,	0x00,	"ch 4 multislip digital delay. [3:0] MSB[11:8]" },
	{ 0x0F7, 3,	0x00,	0x00,	"ch 4 [1:0] output mux sel" },
	{ 0x0F8, 3,	0x09,	0x01,	"ch 4 [7:6] idle at zero [5] dynamic driver en [4:3] driver mode [1:0] CML impedance" }, // mode 0:CML; 1:LVPECL; 2:LVDS; 3:CMOS

	{ 0x0FA, 3,	0x10,	0xFD,	"ch 5 out ctrl. [7] high perf. [6] sync en [5] slip en [3:2] start-up mode [1] multislip en [0] en" },
	{ 0x0FB, 3,	0x40,	0x00,	"ch 5 divider. LSB[7:0]" },
	{ 0x0FC, 3,	0x00,	0x01,	"ch 5 divider. [3:0] MSB[11:8]" },
	{ 0x0FD, 3,	0x00,	0x00,	"ch 5 [4:0] fine analog delay" },
	{ 0x0FE, 3,	0x00,	0x00,	"ch 5 [4:0] coarse digital delay" },
	{ 0x0FF, 3,	0x00,	0x00,	"ch 5 multislip digital delay. LSB[7:0]" },
	{ 0x100, 3,	0x00,	0x00,	"ch 5 multislip digital delay. [3:0] MSB[11:8]" },
	{ 0x101, 3,	0x00,	0x00,	"ch 5 [1:0] output mux sel" },
	{ 0x102, 3,	0x09,	0x30,	"ch 5 [7:6] idle at zero [5] dynamic driver en [4:3] driver mode [1:0] CML impedance" }, // mode 0:CML; 1:LVPECL; 2:LVDS; 3:CMOS

	{ 0x104, 3,	0x10,	0xF3,	"ch 6 out ctrl. [7] high perf. [6] sync en [5] slip en [3:2] start-up mode [1] multislip en [0] en" },
	{ 0x105, 3,	0x08,	0x02,	"ch 6 divider. LSB[7:0]" },
	{ 0x106, 3,	0x00,	0x00,	"ch 6 divider. [3:0] MSB[11:8]" },
	{ 0x107, 3,	0x00,	0x00,	"ch 6 [4:0] fine analog delay" },
	{ 0x108, 3,	0x00,	0x00,	"ch 6 [4:0] coarse digital delay" },
	{ 0x109, 3,	0x00,	0x00,	"ch 6 multislip digital delay. LSB[7:0]" },
	{ 0x10A, 3,	0x00,	0x00,	"ch 6 multislip digital delay. [3:0] MSB[11:8]" },
	{ 0x10B, 3,	0x00,	0x00,	"ch 6 [1:0] output mux sel" },
	{ 0x10C, 3,	0x09,	0x01,	"ch 6 [7:6] idle at zero [5] dynamic driver en [4:3] driver mode [1:0] CML impedance" }, // mode 0:CML; 1:LVPECL; 2:LVDS; 3:CMOS

	{ 0x10E, 3,	0x10,	0xFD,	"ch 7 out ctrl. [7] high perf. [6] sync en [5] slip en [3:2] start-up mode [1] multislip en [0] en" },
	{ 0x10F, 3,	0x40,	0x00,	"ch 7 divider. LSB[7:0]" },
	{ 0x110, 3,	0x00,	0x01,	"ch 7 divider. [3:0] MSB[11:8]" },
	{ 0x111, 3,	0x00,	0x00,	"ch 7 [4:0] fine analog delay" },
	{ 0x112, 3,	0x00,	0x00,	"ch 7 [4:0] coarse digital delay" },
	{ 0x113, 3,	0x00,	0x00,	"ch 7 multislip digital delay. LSB[7:0]" },
	{ 0x114, 3,	0x00,	0x00,	"ch 7 multislip digital delay. [3:0] MSB[11:8]" },
	{ 0x115, 3,	0x00,	0x00,	"ch 7 [1:0] output mux sel" },
	{ 0x116, 3,	0x11,	0x30,	"ch 7 [7:6] idle at zero [5] dynamic driver en [4:3] driver mode [1:0] CML impedance" }, // mode 0:CML; 1:LVPECL; 2:LVDS; 3:CMOS

	{ 0x118, 3,	0x10,	0xF3,	"ch 8 out ctrl. [7] high perf. [6] sync en [5] slip en [3:2] start-up mode [1] multislip en [0] en" },
	{ 0x119, 3,	0x04,	0x02,	"ch 8 divider. LSB[7:0]" },
	{ 0x11A, 3,	0x00,	0x00,	"ch 8 divider. [3:0] MSB[11:8]" },
	{ 0x11B, 3,	0x00,	0x00,	"ch 8 [4:0] fine analog delay" },
	{ 0x11C, 3,	0x00,	0x00,	"ch 8 [4:0] coarse digital delay" },
	{ 0x11D, 3,	0x00,	0x00,	"ch 8 multislip digital delay. LSB[7:0]" },
	{ 0x11E, 3,	0x00,	0x00,	"ch 8 multislip digital delay. [3:0] MSB[11:8]" },
	{ 0x11F, 3,	0x00,	0x00,	"ch 8 [1:0] output mux sel" },
	{ 0x120, 3,	0x09,	0x01,	"ch 8 [7:6] idle at zero [5] dynamic driver en [4:3] driver mode [1:0] CML impedance" }, // mode 0:CML; 1:LVPECL; 2:LVDS; 3:CMOS

	{ 0x122, 3,	0x10,	0xFD,	"ch 9 out ctrl. [7] high perf. [6] sync en [5] slip en [3:2] start-up mode [1] multislip en [0] en" },
	{ 0x123, 3,	0x04,	0x00,	"ch 9 divider. LSB[7:0]" },
	{ 0x124, 3,	0x00,	0x01,	"ch 9 divider. [3:0] MSB[11:8]" },
	{ 0x125, 3,	0x00,	0x00,	"ch 9 [4:0] fine analog delay" },
	{ 0x126, 3,	0x00,	0x00,	"ch 9 [4:0] coarse digital delay" },
	{ 0x127, 3,	0x00,	0x00,	"ch 9 multislip digital delay. LSB[7:0]" },
	{ 0x128, 3,	0x00,	0x00,	"ch 9 multislip digital delay. [3:0] MSB[11:8]" },
	{ 0x129, 3,	0x00,	0x00,	"ch 9 [1:0] output mux sel" },
	{ 0x12A, 3,	0x09,	0x30,	"ch 9 [7:6] idle at zero [5] dynamic driver en [4:3] driver mode [1:0] CML impedance" }, // mode 0:CML; 1:LVPECL; 2:LVDS; 3:CMOS

	{ 0x12C, 3,	0x10,	0xF3,	"ch 10 out ctrl. [7] high perf. [6] sync en [5] slip en [3:2] start-up mode [1] multislip en [0] en" },
	{ 0x12D, 3,	0x04,	0x02,	"ch 10 divider. LSB[7:0]" },
	{ 0x12E, 3,	0x00,	0x00,	"ch 10 divider. [3:0] MSB[11:8]" },
	{ 0x12F, 3,	0x00,	0x00,	"ch 10 [4:0] fine analog delay" },
	{ 0x130, 3,	0x00,	0x00,	"ch 10 [4:0] coarse digital delay" },
	{ 0x131, 3,	0x00,	0x00,	"ch 10 multislip digital delay. LSB[7:0]" },
	{ 0x132, 3,	0x00,	0x00,	"ch 10 multislip digital delay. [3:0] MSB[11:8]" },
	{ 0x133, 3,	0x00,	0x00,	"ch 10 [1:0] output mux sel" },
	{ 0x134, 3,	0x09,	0x01,	"ch 10 [7:6] idle at zero [5] dynamic driver en [4:3] driver mode [1:0] CML impedance" }, // mode 0:CML; 1:LVPECL; 2:LVDS; 3:CMOS

	{ 0x136, 3,	0x10,	0xFD,	"ch 11 out ctrl. [7] high perf. [6] sync en [5] slip en [3:2] start-up mode [1] multislip en [0] en" },
	{ 0x137, 3,	0x04,	0x00,	"ch 11 divider. LSB[7:0]" },
	{ 0x138, 3,	0x00,	0x01,	"ch 11 divider. [3:0] MSB[11:8]" },
	{ 0x139, 3,	0x00,	0x00,	"ch 11 [4:0] fine analog delay" },
	{ 0x13A, 3,	0x00,	0x00,	"ch 11 [4:0] coarse digital delay" },
	{ 0x13B, 3,	0x00,	0x00,	"ch 11 multislip digital delay. LSB[7:0]" },
	{ 0x13C, 3,	0x00,	0x00,	"ch 11 multislip digital delay. [3:0] MSB[11:8]" },
	{ 0x13D, 3,	0x00,	0x00,	"ch 11 [1:0] output mux sel" },
	{ 0x13E, 3,	0x09,	0x30,	"ch 11 [7:6] idle at zero [5] dynamic driver en [4:3] driver mode [1:0] CML impedance" }, // mode 0:CML; 1:LVPECL; 2:LVDS; 3:CMOS

	{ 0x140, 3,	0x10,	0xF3,	"ch 12 out ctrl. [7] high perf. [6] sync en [5] slip en [3:2] start-up mode [1] multislip en [0] en" },
	{ 0x141, 3,	0x10,	0x10,	"ch 12 divider. LSB[7:0]" },
	{ 0x142, 3,	0x00,	0x00,	"ch 12 divider. [3:0] MSB[11:8]" },
	{ 0x143, 3,	0x00,	0x00,	"ch 12 [4:0] fine analog delay" },
	{ 0x144, 3,	0x00,	0x00,	"ch 12 [4:0] coarse digital delay" },
	{ 0x145, 3,	0x00,	0x00,	"ch 12 multislip digital delay. LSB[7:0]" },
	{ 0x146, 3,	0x00,	0x00,	"ch 12 multislip digital delay. [3:0] MSB[11:8]" },
	{ 0x147, 3,	0x00,	0x00,	"ch 12 [1:0] output mux sel" },
	{ 0x148, 3,	0x09,	0x01,	"ch 12 [7:6] idle at zero [5] dynamic driver en [4:3] driver mode [1:0] CML impedance" }, // mode 0:CML; 1:LVPECL; 2:LVDS; 3:CMOS

	{ 0x14A, 3,	0x10,	0xFD,	"ch 13 out ctrl. [7] high perf. [6] sync en [5] slip en [3:2] start-up mode [1] multislip en [0] en" },
	{ 0x14B, 3,	0x04,	0x00,	"ch 13 divider. LSB[7:0]" },
	{ 0x14C, 3,	0x00,	0x01,	"ch 13 divider. [3:0] MSB[11:8]" },
	{ 0x14D, 3,	0x00,	0x00,	"ch 13 [4:0] fine analog delay" },
	{ 0x14E, 3,	0x00,	0x00,	"ch 13 [4:0] coarse digital delay" },
	{ 0x14F, 3,	0x00,	0x00,	"ch 13 multislip digital delay. LSB[7:0]" },
	{ 0x150, 3,	0x00,	0x00,	"ch 13 multislip digital delay. [3:0] MSB[11:8]" },
	{ 0x151, 3,	0x00,	0x00,	"ch 13 [1:0] output mux sel" },
	{ 0x152, 3,	0x09,	0x30,	"ch 13 [7:6] idle at zero [5] dynamic driver en [4:3] driver mode [1:0] CML impedance" }, // mode 0:CML; 1:LVPECL; 2:LVDS; 3:CMOS

	{ 0x00, 0, 	0x0000, 0x0000, 	NULL}
};

#endif /* __HMC7043_H_ */
