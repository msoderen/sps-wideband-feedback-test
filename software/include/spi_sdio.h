#ifndef __SPI_SDIO_H_
#define __SPI_SDIO_H_

#include "devices.h"

#define SDIO_DGIER			0x0001C
#define SDIO_IPISR			0x00020
#define SDIO_IPIER			0x00028
#define SDIO_SRR			0x00040
#define SDIO_SPICR			0x00060
#define SDIO_SPISR			0x00064
#define SDIO_SPIDTR			0x00068
#define SDIO_SPIDRR			0x0006C
#define SDIO_SPISSR			0x00070
#define SDIO_SPIDTRE			0x00074
#define SDIO_SPIDRRE			0x00078

// SDIO driver
extern int write_reg32(int fd, void* base, const unsigned int addr, const unsigned int write_data);
extern int read_reg32(int fd, void* base, const unsigned int addr, unsigned int* read_data);

typedef struct
{
	char * name			;
	unsigned int base_addr		;
	unsigned int channels		: 6;
	unsigned int rb_shift		: 1; // read back shift bits
	unsigned int rbpol		: 1; // read back polarity
	unsigned int cpol		: 1; // sclk polarity when idle (SS is high)
	unsigned int cpha		: 1; // sclk phase
	unsigned int rw_bit_read_pol	: 1; // r/w bit read level (usually 1 for read, 0 for write)
	unsigned int rw_bit_pos		: 6; // r/w bit position
	unsigned int data_pos		: 6; // data start
	unsigned int data_width		: 6;
	unsigned int tran_width		: 6;
	int (*write_reg32)(int, void *, const unsigned int, const unsigned int);	// function to write 32-bit register
	int (*read_reg32)(int, void *, const unsigned int, unsigned int *);	// function to read 32-bit register
	
} sdio_config_t;

int spi_sdio_init(int fd, void* base, sdio_config_t* sdio_config);
int spi_sdio_write(int fd, void* base, sdio_config_t* sdio_config, unsigned int sdio_addr, unsigned int write_data);
int spi_sdio_read(int fd, void* base, sdio_config_t* sdio_config, unsigned int sdio_addr,
	unsigned int* read_data, unsigned int write_data);
int sdio_sel_ch(int fd, void* base, sdio_config_t* sdio_config, unsigned int ch);
int spi_write(dev_with_regs_t* dev, unsigned int reg_addr, unsigned int w_data);
int spi_init(dev_with_regs_t* dev );
int spi_read(dev_with_regs_t* dev, unsigned int reg_addr, unsigned int* r_data);
int spi_dump(dev_with_regs_t* dev );

#endif /* __SPI_SDIO_H_ */
