#ifndef __ADC12DJ3200_H_
#define __ADC12DJ3200_H_

#include "devices.h"

/* FMC217 ADC12DJ3200 settings
 * JMODE = 2. dual-channel; 12-bit; 8 lanes
 * links=2 N=12 N'=12 L=4 M=4 F=8 S=5 K=32
 * sampling frequency fs = 2.8G (limited by Xilinx speed grade 2 GTH2 with maximum data rate of 11.3Gbps)
 * serial link rate = 2.8Gsps*2channel*12.8(effective_bits)*10/8(8b10b)/8(lanes) = 11.2Gbps
 * LMFC = 11.2G/10/F/K = 11.2G/10/8/32 = 4.375MHz
 */

const reg_with_name_t adc12dj3200_regs[] =
{
	{ 0x0000, 3, 0x30, 0x30, "Cfg_A. [7] soft_rst [5] addr_asc [4] sdo_active"},
	{ 0x0002, 3, 0x00, 0x00, "Device configuration. [1:0] mode 0&1: normal operation 2&3:all powered down"},
	{ 0x0003, 1, 0x03, 0x03, "Chip type = 0x03"},
	{ 0x0004, 1, 0x20, 0x20, "Chip id[7:0] = 0x20"},
	{ 0x0005, 1, 0x00, 0x00, "Chip id[15:8] = 0x0"},
	{ 0x0006, 1, 0x05, 0x04, "Chip version = 0x05 as of testing on the first article"},
	{ 0x000C, 1, 0x51, 0x51, "Vender ID = 0x51"},
	{ 0x000D, 1, 0x04, 0x04, "Vender ID = 0x04"},

	{ 0x0010, 3, 0x00, 0x00, "SPI configuration"},

	/* Miscellaneous analog registers */
	{ 0x0029, 2, 0x20, 0x00, "clk_ctrl0. [6] sysref_proc_en [5] sysref_recv_en [4] sysref_zoom [3:0] sysref_sel"},	// set sysref_lvpecl_en first
	{ 0x0029, 3, 0x60, 0x00, "clk_ctrl0. [6] sysref_proc_en [5] sysref_recv_en [4] sysref_zoom [3:0] sysref_sel"},
	{ 0x002A, 3, 0x00, 0x00, "clk_ctrl1. [2] devclk_lvpecl_en [1] sysref_lvpecl_en [0] sysref_inverted"},
	{ 0x002C, 1, 0x00, 0x00, "SYSREF_POS[7:0]"},
	{ 0x002D, 1, 0x00, 0x00, "SYSREF_POS[15:8]"},
	{ 0x002E, 1, 0x00, 0x00, "SYSREF_POS[23:16]"},
	{ 0x0030, 3, 0xC4, 0xC4, "FS_RANGE_A[7:0]"},
	{ 0x0031, 3, 0xA4, 0xA4, "FS_RANGE_A[15:8]"},
	{ 0x0032, 3, 0xC4, 0xC4, "FS_RANGE_B[7:0]"},
	{ 0x0033, 3, 0xA4, 0xA4, "FS_RANGE_B[15:8]"},
	{ 0x0038, 3, 0x00, 0x00, "[0] BG_BYPASS"},
	{ 0x003B, 3, 0x00, 0x00, "TMSTP_CTRL [1] LVPECL_EN [0] RECV_EN"},

	/* Serializer Registers */
	{ 0x0048, 3, 0x00, 0x00, "[3:0] SER_PE"},

	/* Calibration Registers */
	{ 0x0060, 3, 0x01, 0x01, "INPUT_MUX. [4] dual_input [1:0] single_input"},
	{ 0x0061, 3, 0x01, 0x01, "[0] CAL_EN"},	// always set CAL_EN before setting JESD_EN ...
	{ 0x0062, 3, 0x01, 0x01, "CAL_CFG0. [4] cal_osfilt [3] cal_bgos [2] cal_os [1] cal_bg [0] cal_fg"},	// change while CAL_EN=0
	{ 0x0063, 1, 0x00, 0x00, "CAL_STATUS. [3:2] CAL_ERROR [1] cal_stopped [0] FG_DONE"},
	{ 0x006B, 3, 0x00, 0x00, "CAL_PIN_CFG. [2:1] CAL_STATUS_SEL [0] CAL_TRIG_EN"},
	{ 0x006C, 3, 0x01, 0x01, "[0] CAL_SOFT_TRIG"},
	{ 0x0070, 3, 0x01, 0x01, "[0] CAL_DATA_EN"},
	{ 0x0071, 0, 0x00, 0x00, "CAL_DATA"}, 	// read/write 673 times to get all the calibration data
	{ 0x007A, 1, 0x00, 0x00, "GAIN_TRIM_A"},
	{ 0x007B, 1, 0x00, 0x00, "GAIN_TRIM_B"},
	{ 0x007C, 1, 0x00, 0x00, "[3:0] BG_TRIM"},
	{ 0x007E, 1, 0x00, 0x00, "RTRIM_A"}, 
	{ 0x007F, 1, 0x00, 0x00, "RTRIM_B"}, 
	{ 0x0080, 1, 0x00, 0x00, "TADJ_A_FG90"}, 
	{ 0x0081, 1, 0x00, 0x00, "TADJ_B_FG90"}, 
	{ 0x0082, 1, 0x00, 0x00, "TADJ_A_BG90"}, 
	{ 0x0083, 1, 0x00, 0x00, "TADJ_C_BG0"}, 
	{ 0x0084, 1, 0x00, 0x00, "TADJ_C_BG90"}, 
	{ 0x0085, 1, 0x00, 0x00, "TADJ_B_BG0"}, 
	{ 0x0086, 1, 0x00, 0x00, "TADJ_A"}, 
	{ 0x0087, 1, 0x00, 0x00, "TADJ_CA"}, 
	{ 0x0088, 1, 0x00, 0x00, "TADJ_CB"}, 
	{ 0x0089, 1, 0x00, 0x00, "TADJ_B"}, 
	{ 0x008A, 1, 0x00, 0x00, "OADJ_A_INA[7:0]"}, 
	{ 0x008B, 1, 0x00, 0x00, "[3:0] OADJ_A_INA[11:8]"}, 
	{ 0x008C, 1, 0x00, 0x00, "OADJ_A_INB[7:0]"}, 
	{ 0x008D, 1, 0x00, 0x00, "[3:0] OADJ_A_INB[11:8]"}, 
	{ 0x008E, 1, 0x00, 0x00, "OADJ_C_INA[7:0]"}, 
	{ 0x008F, 1, 0x00, 0x00, "[3:0] OADJ_C_INA[11:8]"}, 
	{ 0x0090, 1, 0x00, 0x00, "OADJ_C_INB[7:0]"}, 
	{ 0x0091, 1, 0x00, 0x00, "[3:0] OADJ_C_INB[11:8]"},
	{ 0x0092, 1, 0x00, 0x00, "OADJ_B_INA[7:0]"}, 
	{ 0x0093, 1, 0x00, 0x00, "[3:0] OADJ_B_INA[11:8]"},	
	{ 0x0094, 1, 0x00, 0x00, "OADJ_B_INB[7:0]"}, 
	{ 0x0095, 1, 0x00, 0x00, "[3:0] OADJ_B_INB[11:8]"},	
	
	/* ADC bank registers */
	{ 0x0102, 1, 0x00, 0x00, "B0_TIME_0"}, 
	{ 0x0103, 1, 0x00, 0x00, "B0_TIME_90"}, 
	{ 0x0112, 1, 0x00, 0x00, "B1_TIME_0"}, 
	{ 0x0113, 1, 0x00, 0x00, "B1_TIME_90"}, 
	{ 0x0122, 1, 0x00, 0x00, "B2_TIME_0"}, 
	{ 0x0123, 1, 0x00, 0x00, "B2_TIME_90"}, 
	{ 0x0132, 1, 0x00, 0x00, "B3_TIME_0"}, 
	{ 0x0133, 1, 0x00, 0x00, "B3_TIME_90"}, 
	{ 0x0142, 1, 0x00, 0x00, "B4_TIME_0"}, 
	{ 0x0143, 1, 0x00, 0x00, "B4_TIME_90"}, 
	{ 0x0152, 1, 0x00, 0x00, "B5_TIME_0"}, 
	{ 0x0153, 1, 0x00, 0x00, "B5_TIME_90"}, 
	
	/* LSB control registers */
	{ 0x0160, 3, 0x00, 0x00, "ENC_LSB. [1] CAL_STATE_EN [0] TIMESTAMP_EN"}, 
	
	/* JESD204B registers */
	{ 0x0200, 3, 0x00, 0x01, "[0] JESD_EN"},	// clear JESD_EN before clear CAL_EN
	{ 0x0061, 2, 0x00, 0x01, "[0] CAL_EN"},		// clear CAL_EN 
	{ 0x0201, 3, 0x02, 0x02, "[4:0] JMODE"},	// change when JESD_EN=0 and CAL_EN=0
	{ 0x0202, 1, 0x1F, 0x1F, "[4:0] KM1"},
	{ 0x0203, 1, 0x01, 0x01, "[0] JSYNC_N"},
	{ 0x0204, 1, 0x02, 0x02, "JCTRL [3:2] SYNC_SEL 0: nSYNCSE; 1:TMSTP; 2:soft JSYNC_N [1] SFORMAT 1:2's [0] SCR scrambler "},
	{ 0x0205, 1, 0x00, 0x00, "[3:0] JTEST"},
	{ 0x0206, 1, 0x00, 0x00, "DID"},
	{ 0x0207, 1, 0x00, 0x00, "[1:0] FCHAR"},
	{ 0x0208, 1, 0x00, 0x00, "JESD_STATUS [6] link_up [5] sync_status [4] realigned '1' to clr [3] aligned '1' to clr [2] pll_locked"},
	{ 0x0209, 1, 0x00, 0x00, "PD_CH [1] PD_BCH [0] PD_ACH"},
	{ 0x0061, 2, 0x01, 0x01, "[0] CAL_EN"},		// set CAL_EN before setting JESD_EN
	{ 0x0000, 0x80, 100000, 0x0, ""},		// delay 100ms 
	{ 0x0200, 2, 0x01, 0x01, "[0] JESD_EN"},	// set JESD_EN 
		
	/* Digital down converter registers */
	{ 0x0210, 1, 0x00, 0x00, "DDC_CFG [3] D4_AP87 [2] D2_HIGH_PASS [1] INVERT_SPECTRUM [0] BOOST"},
	{ 0x0211, 1, 0xF2, 0xF2, "OVR_T0"},
	{ 0x0212, 1, 0xAB, 0xAB, "OVR_T1"},
	{ 0x0213, 1, 0x07, 0x07, "OVR_CFG [3] OVR_EN [2:0] OVR_N"},
	{ 0x0214, 3, 0x01, 0x00, "[1:0] CMODE 0: use csel; 1:use nco* pins; 2: use ncoa pins for both DDCs"},	// change
	{ 0x0215, 1, 0x00, 0x00, "CSEL [3:2] CSELB [1:0] CSELA"},
	{ 0x0216, 1, 0x02, 0x02, "DIG_BIND [1] DIG_BIND_B [0] DIG_BIND_A"},
	{ 0x0217, 1, 0x00, 0x00, "NCO_RDIV[7:0]"},
	{ 0x0218, 1, 0x00, 0x00, "NCO_RDIV[15:8]"},
	{ 0x0219, 1, 0x02, 0x02, "NCO_SYNC [1] NCO_SYNC_ILA [0] NCO_SYNC_NEXT"},
	
	/* comment out for short dumping */
	// { 0x0220, 3, 0x00, 0x00, "FREQA0[7:0]"},
	// { 0x0221, 3, 0x00, 0x00, "FREQA0[15:8]"},
	// { 0x0222, 3, 0x00, 0x00, "FREQA0[23:16]"},
	// { 0x0223, 3, 0x10, 0x00, "FREQA0[31:24]"},
	// { 0x0224, 3, 0x00, 0x00, "PHASEA0[7:0]"},
	// { 0x0225, 3, 0x00, 0x00, "PHASEA0[15:8]"},
	// { 0x0228, 3, 0x00, 0x00, "FREQA1[7:0]"},
	// { 0x0229, 3, 0x00, 0x00, "FREQA1[15:8]"},
	// { 0x022A, 3, 0x00, 0x00, "FREQA1[23:16]"},
	// { 0x022B, 3, 0x20, 0x00, "FREQA1[31:24]"},
	// { 0x022C, 3, 0x00, 0x00, "PHASEA1[7:0]"},
	// { 0x022D, 3, 0x00, 0x00, "PHASEA1[15:8]"},

	// { 0x0230, 3, 0x00, 0x00, "FREQA2[7:0]"},
	// { 0x0231, 3, 0x00, 0x00, "FREQA2[15:8]"},
	// { 0x0232, 3, 0x00, 0x00, "FREQA2[23:16]"},
	// { 0x0233, 3, 0x40, 0x00, "FREQA2[31:24]"},
	// { 0x0234, 3, 0x00, 0x00, "PHASEA2[7:0]"},
	// { 0x0235, 3, 0x00, 0x00, "PHASEA2[15:8]"},
	// { 0x0238, 3, 0x00, 0x00, "FREQA3[7:0]"},
	// { 0x0239, 3, 0x00, 0x00, "FREQA3[15:8]"},
	// { 0x023A, 3, 0x00, 0x00, "FREQA3[23:16]"},
	// { 0x023B, 3, 0x80, 0x00, "FREQA3[31:24]"},
	// { 0x023C, 3, 0x00, 0x00, "PHASEA3[7:0]"},
	// { 0x023D, 3, 0x00, 0x00, "PHASEA3[15:8]"},

	// { 0x0240, 3, 0x00, 0x00, "FREQB0[7:0]"},
	// { 0x0241, 3, 0x00, 0x00, "FREQB0[15:8]"},
	// { 0x0242, 3, 0x00, 0x00, "FREQB0[23:16]"},
	// { 0x0243, 3, 0x10, 0x00, "FREQB0[31:24]"},
	// { 0x0244, 3, 0x00, 0x00, "PHASEB0[7:0]"},
	// { 0x0245, 3, 0x00, 0x00, "PHASEB0[15:8]"},
	// { 0x0248, 3, 0x00, 0x00, "FREQB1[7:0]"},
	// { 0x0249, 3, 0x00, 0x00, "FREQB1[15:8]"},
	// { 0x024A, 3, 0x00, 0x00, "FREQB1[23:16]"},
	// { 0x024B, 3, 0x20, 0x00, "FREQB1[31:24]"},
	// { 0x024C, 3, 0x00, 0x00, "PHASEB1[7:0]"},
	// { 0x024D, 3, 0x00, 0x00, "PHASEB1[15:8]"},

	// { 0x0250, 3, 0x00, 0x00, "FREQB2[7:0]"},
	// { 0x0251, 3, 0x00, 0x00, "FREQB2[15:8]"},
	// { 0x0252, 3, 0x00, 0x00, "FREQB2[23:16]"},
	// { 0x0253, 3, 0x40, 0x00, "FREQB2[31:24]"},
	// { 0x0254, 3, 0x00, 0x00, "PHASEB2[7:0]"},
	// { 0x0255, 3, 0x00, 0x00, "PHASEB2[15:8]"},
	// { 0x0258, 3, 0x00, 0x00, "FREQB3[7:0]"},
	// { 0x0259, 3, 0x00, 0x00, "FREQB3[15:8]"},
	// { 0x025A, 3, 0x00, 0x00, "FREQB3[23:16]"},
	// { 0x025B, 3, 0x80, 0x00, "FREQB3[31:24]"},
	// { 0x025C, 3, 0x00, 0x00, "PHASEB3[7:0]"},
	// { 0x025D, 3, 0x00, 0x00, "PHASEB3[15:8]"},
	
	{ 0x0297, 1, 0x00, 0x00, "[4:0] SPIN_ID 0:ADC12DJ3200 "},
	
	/* SYSREF calibration registers */
	{ 0x02B0, 3, 0x00, 0x00, "[0] SRC_EN 1:sysref calibration enabled"},
	{ 0x02B1, 3, 0x05, 0x05, "SRC_CFG [3:2] SRC_AVG 0:4;1:16;2:64;3:256 [1:0] SRC_HDUR high duration accumulation"},
	{ 0x02B2, 1, 0x00, 0x00, "SRC_TAD[7:0]"},
	{ 0x02B3, 1, 0x00, 0x00, "SRC_TAD[15:8]"},
	{ 0x02B4, 1, 0x00, 0x00, "[1] SRC_DONE [0] SRC_TAD[16]"},
	{ 0x02B5, 3, 0x00, 0x00, "TAD_FINE DEVCLK aperture adjustment 25fs resolution"},
	{ 0x02B6, 3, 0x00, 0x00, "TAD_COARSE 1ps resolution"},
	{ 0x02B7, 3, 0x00, 0x00, "[0] TAD_INV"},
	
	/* Alarm registers */
	{ 0x02C0, 1, 0x00, 0x00, "[0] ALARM"},
	{ 0x02C1, 3, 0x1F, 0x1F, "ALM_STATUS write '1' to clear [4] pll_alm [3] link_alm [2] realigned_alm [1] nco_alm [0] clk_alm"},
	{ 0x02C2, 3, 0x00, 0x1F, "ALM_MASK [4] pll_alm [3] link_alm [2] realigned_alm [1] nco_alm [0] clk_alm"},	// change allow all alarms

	{ 0, 0, 0, 0, NULL}
};


/* FMC217 ADC12DJ3200 settings
 * JMODE = 0. single-channel; 12-bit; 8 lanes
 * links=2 N=12 N'=12 L=4 M=4 F=8 S=5 K=32
 * sampling frequency fs = 2.8G (limited by Xilinx speed grade 2 GTH2 with maximum data rate of 11.3Gbps)
 * serial link rate = 2.8Gsps*2channel*12.8(effective_bits)*10/8(8b10b)/8(lanes) = 11.2Gbps
 * LMFC = 11.2G/10/F/K = 11.2G/10/8/32 = 4.375MHz
 */

const reg_with_name_t adc12dj3200_regs_single_ch[] =
{
	{ 0x0000, 3, 0x30, 0x30, "Cfg_A. [7] soft_rst [5] addr_asc [4] sdo_active"},
	{ 0x0002, 3, 0x00, 0x00, "Device configuration. [1:0] mode 0&1: normal operation 2&3:all powered down"},
	{ 0x0003, 1, 0x03, 0x03, "Chip type = 0x03"},
	{ 0x0004, 1, 0x20, 0x20, "Chip id[7:0] = 0x20"},
	{ 0x0005, 1, 0x00, 0x00, "Chip id[15:8] = 0x0"},
	{ 0x0006, 1, 0x05, 0x04, "Chip version = 0x05 as of testing on the first article"},
	{ 0x000C, 1, 0x51, 0x51, "Vender ID = 0x51"},
	{ 0x000D, 1, 0x04, 0x04, "Vender ID = 0x04"},

	{ 0x0010, 3, 0x00, 0x00, "SPI configuration"},

	/* Miscellaneous analog registers */
	{ 0x0029, 2, 0x20, 0x00, "clk_ctrl0. [6] sysref_proc_en [5] sysref_recv_en [4] sysref_zoom [3:0] sysref_sel"},	// set sysref_lvpecl_en first
	{ 0x0029, 3, 0x60, 0x00, "clk_ctrl0. [6] sysref_proc_en [5] sysref_recv_en [4] sysref_zoom [3:0] sysref_sel"},
	{ 0x002A, 3, 0x00, 0x00, "clk_ctrl1. [2] devclk_lvpecl_en [1] sysref_lvpecl_en [0] sysref_inverted"},
	{ 0x002C, 1, 0x00, 0x00, "SYSREF_POS[7:0]"},
	{ 0x002D, 1, 0x00, 0x00, "SYSREF_POS[15:8]"},
	{ 0x002E, 1, 0x00, 0x00, "SYSREF_POS[23:16]"},
	{ 0x0030, 3, 0xC4, 0xC4, "FS_RANGE_A[7:0]"},
	{ 0x0031, 3, 0xA4, 0xA4, "FS_RANGE_A[15:8]"},
	{ 0x0032, 3, 0xC4, 0xC4, "FS_RANGE_B[7:0]"},
	{ 0x0033, 3, 0xA4, 0xA4, "FS_RANGE_B[15:8]"},
	{ 0x0038, 3, 0x00, 0x00, "[0] BG_BYPASS"},
	{ 0x003B, 3, 0x00, 0x00, "TMSTP_CTRL [1] LVPECL_EN [0] RECV_EN"},

	/* Serializer Registers */
	{ 0x0048, 3, 0x00, 0x00, "[3:0] SER_PE"},

	/* Calibration Registers */
	{ 0x0060, 3, 0x01, 0x01, "INPUT_MUX. [4] dual_input [1:0] single_input"},
	{ 0x0061, 3, 0x01, 0x01, "[0] CAL_EN"},	// always set CAL_EN before setting JESD_EN ...
	{ 0x0062, 3, 0x01, 0x01, "CAL_CFG0. [4] cal_osfilt [3] cal_bgos [2] cal_os [1] cal_bg [0] cal_fg"},	// change while CAL_EN=0
	{ 0x0063, 1, 0x00, 0x00, "CAL_STATUS. [3:2] CAL_ERROR [1] cal_stopped [0] FG_DONE"},
	{ 0x006B, 3, 0x00, 0x00, "CAL_PIN_CFG. [2:1] CAL_STATUS_SEL [0] CAL_TRIG_EN"},
	{ 0x006C, 3, 0x01, 0x01, "[0] CAL_SOFT_TRIG"},
	{ 0x0070, 3, 0x01, 0x01, "[0] CAL_DATA_EN"},
	{ 0x0071, 0, 0x00, 0x00, "CAL_DATA"}, 	// read/write 673 times to get all the calibration data
	{ 0x007A, 1, 0x00, 0x00, "GAIN_TRIM_A"},
	{ 0x007B, 1, 0x00, 0x00, "GAIN_TRIM_B"},
	{ 0x007C, 1, 0x00, 0x00, "[3:0] BG_TRIM"},
	{ 0x007E, 1, 0x00, 0x00, "RTRIM_A"}, 
	{ 0x007F, 1, 0x00, 0x00, "RTRIM_B"}, 
	{ 0x0080, 1, 0x00, 0x00, "TADJ_A_FG90"}, 
	{ 0x0081, 1, 0x00, 0x00, "TADJ_B_FG90"}, 
	{ 0x0082, 1, 0x00, 0x00, "TADJ_A_BG90"}, 
	{ 0x0083, 1, 0x00, 0x00, "TADJ_C_BG0"}, 
	{ 0x0084, 1, 0x00, 0x00, "TADJ_C_BG90"}, 
	{ 0x0085, 1, 0x00, 0x00, "TADJ_B_BG0"}, 
	{ 0x0086, 1, 0x00, 0x00, "TADJ_A"}, 
	{ 0x0087, 1, 0x00, 0x00, "TADJ_CA"}, 
	{ 0x0088, 1, 0x00, 0x00, "TADJ_CB"}, 
	{ 0x0089, 1, 0x00, 0x00, "TADJ_B"}, 
	{ 0x008A, 1, 0x00, 0x00, "OADJ_A_INA[7:0]"}, 
	{ 0x008B, 1, 0x00, 0x00, "[3:0] OADJ_A_INA[11:8]"}, 
	{ 0x008C, 1, 0x00, 0x00, "OADJ_A_INB[7:0]"}, 
	{ 0x008D, 1, 0x00, 0x00, "[3:0] OADJ_A_INB[11:8]"}, 
	{ 0x008E, 1, 0x00, 0x00, "OADJ_C_INA[7:0]"}, 
	{ 0x008F, 1, 0x00, 0x00, "[3:0] OADJ_C_INA[11:8]"}, 
	{ 0x0090, 1, 0x00, 0x00, "OADJ_C_INB[7:0]"}, 
	{ 0x0091, 1, 0x00, 0x00, "[3:0] OADJ_C_INB[11:8]"},
	{ 0x0092, 1, 0x00, 0x00, "OADJ_B_INA[7:0]"}, 
	{ 0x0093, 1, 0x00, 0x00, "[3:0] OADJ_B_INA[11:8]"},	
	{ 0x0094, 1, 0x00, 0x00, "OADJ_B_INB[7:0]"}, 
	{ 0x0095, 1, 0x00, 0x00, "[3:0] OADJ_B_INB[11:8]"},	
	
	/* ADC bank registers */
	{ 0x0102, 1, 0x00, 0x00, "B0_TIME_0"}, 
	{ 0x0103, 1, 0x00, 0x00, "B0_TIME_90"}, 
	{ 0x0112, 1, 0x00, 0x00, "B1_TIME_0"}, 
	{ 0x0113, 1, 0x00, 0x00, "B1_TIME_90"}, 
	{ 0x0122, 1, 0x00, 0x00, "B2_TIME_0"}, 
	{ 0x0123, 1, 0x00, 0x00, "B2_TIME_90"}, 
	{ 0x0132, 1, 0x00, 0x00, "B3_TIME_0"}, 
	{ 0x0133, 1, 0x00, 0x00, "B3_TIME_90"}, 
	{ 0x0142, 1, 0x00, 0x00, "B4_TIME_0"}, 
	{ 0x0143, 1, 0x00, 0x00, "B4_TIME_90"}, 
	{ 0x0152, 1, 0x00, 0x00, "B5_TIME_0"}, 
	{ 0x0153, 1, 0x00, 0x00, "B5_TIME_90"}, 
	
	/* LSB control registers */
	{ 0x0160, 3, 0x00, 0x00, "ENC_LSB. [1] CAL_STATE_EN [0] TIMESTAMP_EN"}, 
	
	/* JESD204B registers */
	{ 0x0200, 3, 0x00, 0x01, "[0] JESD_EN"},	// clear JESD_EN before clear CAL_EN
	{ 0x0061, 2, 0x00, 0x01, "[0] CAL_EN"},		// clear CAL_EN 
	{ 0x0201, 3, 0x00, 0x02, "[4:0] JMODE"},	// change when JESD_EN=0 and CAL_EN=0
	{ 0x0202, 1, 0x1F, 0x1F, "[4:0] KM1"},
	{ 0x0203, 1, 0x01, 0x01, "[0] JSYNC_N"},
	{ 0x0204, 1, 0x02, 0x02, "JCTRL [3:2] SYNC_SEL 0: nSYNCSE; 1:TMSTP; 2:soft JSYNC_N [1] SFORMAT 1:2's [0] SCR scrambler "},
	{ 0x0205, 1, 0x00, 0x00, "[3:0] JTEST"},
	{ 0x0206, 1, 0x00, 0x00, "DID"},
	{ 0x0207, 1, 0x00, 0x00, "[1:0] FCHAR"},
	{ 0x0208, 1, 0x00, 0x00, "JESD_STATUS [6] link_up [5] sync_status [4] realigned '1' to clr [3] aligned '1' to clr [2] pll_locked"},
	{ 0x0209, 1, 0x00, 0x00, "PD_CH [1] PD_BCH [0] PD_ACH"},
	{ 0x0061, 2, 0x01, 0x01, "[0] CAL_EN"},		// set CAL_EN before setting JESD_EN
	{ 0x0000, 0x80, 100000, 0x0, ""},		// delay 100ms 
	{ 0x0200, 2, 0x01, 0x01, "[0] JESD_EN"},	// set JESD_EN 
		
	/* Digital down converter registers */
	{ 0x0210, 1, 0x00, 0x00, "DDC_CFG [3] D4_AP87 [2] D2_HIGH_PASS [1] INVERT_SPECTRUM [0] BOOST"},
	{ 0x0211, 1, 0xF2, 0xF2, "OVR_T0"},
	{ 0x0212, 1, 0xAB, 0xAB, "OVR_T1"},
	{ 0x0213, 1, 0x07, 0x07, "OVR_CFG [3] OVR_EN [2:0] OVR_N"},
	{ 0x0214, 3, 0x01, 0x00, "[1:0] CMODE 0: use csel; 1:use nco* pins; 2: use ncoa pins for both DDCs"},	// change
	{ 0x0215, 1, 0x00, 0x00, "CSEL [3:2] CSELB [1:0] CSELA"},
	{ 0x0216, 1, 0x02, 0x02, "DIG_BIND [1] DIG_BIND_B [0] DIG_BIND_A"},
	{ 0x0217, 1, 0x00, 0x00, "NCO_RDIV[7:0]"},
	{ 0x0218, 1, 0x00, 0x00, "NCO_RDIV[15:8]"},
	{ 0x0219, 1, 0x02, 0x02, "NCO_SYNC [1] NCO_SYNC_ILA [0] NCO_SYNC_NEXT"},
	
	/* comment out for short dumping */
	// { 0x0220, 3, 0x00, 0x00, "FREQA0[7:0]"},
	// { 0x0221, 3, 0x00, 0x00, "FREQA0[15:8]"},
	// { 0x0222, 3, 0x00, 0x00, "FREQA0[23:16]"},
	// { 0x0223, 3, 0x10, 0x00, "FREQA0[31:24]"},
	// { 0x0224, 3, 0x00, 0x00, "PHASEA0[7:0]"},
	// { 0x0225, 3, 0x00, 0x00, "PHASEA0[15:8]"},
	// { 0x0228, 3, 0x00, 0x00, "FREQA1[7:0]"},
	// { 0x0229, 3, 0x00, 0x00, "FREQA1[15:8]"},
	// { 0x022A, 3, 0x00, 0x00, "FREQA1[23:16]"},
	// { 0x022B, 3, 0x20, 0x00, "FREQA1[31:24]"},
	// { 0x022C, 3, 0x00, 0x00, "PHASEA1[7:0]"},
	// { 0x022D, 3, 0x00, 0x00, "PHASEA1[15:8]"},

	// { 0x0230, 3, 0x00, 0x00, "FREQA2[7:0]"},
	// { 0x0231, 3, 0x00, 0x00, "FREQA2[15:8]"},
	// { 0x0232, 3, 0x00, 0x00, "FREQA2[23:16]"},
	// { 0x0233, 3, 0x40, 0x00, "FREQA2[31:24]"},
	// { 0x0234, 3, 0x00, 0x00, "PHASEA2[7:0]"},
	// { 0x0235, 3, 0x00, 0x00, "PHASEA2[15:8]"},
	// { 0x0238, 3, 0x00, 0x00, "FREQA3[7:0]"},
	// { 0x0239, 3, 0x00, 0x00, "FREQA3[15:8]"},
	// { 0x023A, 3, 0x00, 0x00, "FREQA3[23:16]"},
	// { 0x023B, 3, 0x80, 0x00, "FREQA3[31:24]"},
	// { 0x023C, 3, 0x00, 0x00, "PHASEA3[7:0]"},
	// { 0x023D, 3, 0x00, 0x00, "PHASEA3[15:8]"},

	// { 0x0240, 3, 0x00, 0x00, "FREQB0[7:0]"},
	// { 0x0241, 3, 0x00, 0x00, "FREQB0[15:8]"},
	// { 0x0242, 3, 0x00, 0x00, "FREQB0[23:16]"},
	// { 0x0243, 3, 0x10, 0x00, "FREQB0[31:24]"},
	// { 0x0244, 3, 0x00, 0x00, "PHASEB0[7:0]"},
	// { 0x0245, 3, 0x00, 0x00, "PHASEB0[15:8]"},
	// { 0x0248, 3, 0x00, 0x00, "FREQB1[7:0]"},
	// { 0x0249, 3, 0x00, 0x00, "FREQB1[15:8]"},
	// { 0x024A, 3, 0x00, 0x00, "FREQB1[23:16]"},
	// { 0x024B, 3, 0x20, 0x00, "FREQB1[31:24]"},
	// { 0x024C, 3, 0x00, 0x00, "PHASEB1[7:0]"},
	// { 0x024D, 3, 0x00, 0x00, "PHASEB1[15:8]"},

	// { 0x0250, 3, 0x00, 0x00, "FREQB2[7:0]"},
	// { 0x0251, 3, 0x00, 0x00, "FREQB2[15:8]"},
	// { 0x0252, 3, 0x00, 0x00, "FREQB2[23:16]"},
	// { 0x0253, 3, 0x40, 0x00, "FREQB2[31:24]"},
	// { 0x0254, 3, 0x00, 0x00, "PHASEB2[7:0]"},
	// { 0x0255, 3, 0x00, 0x00, "PHASEB2[15:8]"},
	// { 0x0258, 3, 0x00, 0x00, "FREQB3[7:0]"},
	// { 0x0259, 3, 0x00, 0x00, "FREQB3[15:8]"},
	// { 0x025A, 3, 0x00, 0x00, "FREQB3[23:16]"},
	// { 0x025B, 3, 0x80, 0x00, "FREQB3[31:24]"},
	// { 0x025C, 3, 0x00, 0x00, "PHASEB3[7:0]"},
	// { 0x025D, 3, 0x00, 0x00, "PHASEB3[15:8]"},
	
	{ 0x0297, 1, 0x00, 0x00, "[4:0] SPIN_ID 0:ADC12DJ3200 "},
	
	/* SYSREF calibration registers */
	{ 0x02B0, 3, 0x00, 0x00, "[0] SRC_EN 1:sysref calibration enabled"},
	{ 0x02B1, 3, 0x05, 0x05, "SRC_CFG [3:2] SRC_AVG 0:4;1:16;2:64;3:256 [1:0] SRC_HDUR high duration accumulation"},
	{ 0x02B2, 1, 0x00, 0x00, "SRC_TAD[7:0]"},
	{ 0x02B3, 1, 0x00, 0x00, "SRC_TAD[15:8]"},
	{ 0x02B4, 1, 0x00, 0x00, "[1] SRC_DONE [0] SRC_TAD[16]"},
	{ 0x02B5, 3, 0x00, 0x00, "TAD_FINE DEVCLK aperture adjustment 25fs resolution"},
	{ 0x02B6, 3, 0x00, 0x00, "TAD_COARSE 1ps resolution"},
	{ 0x02B7, 3, 0x00, 0x00, "[0] TAD_INV"},
	
	/* Alarm registers */
	{ 0x02C0, 1, 0x00, 0x00, "[0] ALARM"},
	{ 0x02C1, 3, 0x1F, 0x1F, "ALM_STATUS write '1' to clear [4] pll_alm [3] link_alm [2] realigned_alm [1] nco_alm [0] clk_alm"},
	{ 0x02C2, 3, 0x00, 0x1F, "ALM_MASK [4] pll_alm [3] link_alm [2] realigned_alm [1] nco_alm [0] clk_alm"},	// change allow all alarms

	{ 0, 0, 0, 0, NULL}
};

#endif /* __ADC12DJ3200_H_ */
