#ifndef __DEVICES_H_
#define __DEVICES_H_

#include "vt_platform.h"

typedef struct
{
	unsigned int addr;
	char access_type; // bit[0]=1: read; bit[1]=1: write; bit[7]=1: data is delay time in 1us. only applicable during init
	unsigned int data;
	unsigned int por_default;
	const char* name;
} reg_with_name_t;

enum dev_bus_type 
{
	I2C,
	SDIO,
	OTHER
};

typedef struct dev_with_regs_s dev_with_regs_t;

struct dev_with_regs_s
{
	const char* name;
	const char* full_name;
	int fd;
	void* base;
	const reg_with_name_t* regs_dump;
	const reg_with_name_t* regs_init;
	enum dev_bus_type bus_type;
	void* pBus;
	unsigned int private_data[2];
	unsigned int curr_channel;
	int (*init)(dev_with_regs_t*);
	int (*write)(dev_with_regs_t*, unsigned int, unsigned int );
	int (*read)(dev_with_regs_t*, unsigned int, unsigned int* );
	int (*dump)(dev_with_regs_t*);	
};

int dev_init(dev_with_regs_t* dev);
// int dev_dump(dev_with_regs_t* dev);

#endif /*__DEVICES_H_*/
