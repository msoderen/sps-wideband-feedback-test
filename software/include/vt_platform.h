#ifndef __VT_PLATFORM_H_
#define __VT_PLATFORM_H_

#ifdef __MICROBLAZE__
#include "microblaze_sleep.h"
#include "xstatus.h"
#else
#include <sys/time.h>
#include <time.h>
#include <sys/ioctl.h>
#endif


/***************************************************
 * Choose one of the interface options to run on a linux system 
 *	"PCIE_MMAP"		-- use pcie direct memory map to access registers.  
				--	need package 'pciutils-devel' which is not available 
				--	on P2040 CPU by default
 *	"P2040_LB"		-- use p2040 cpu local bus to access registers
 *	"PCIE_DEV_DRV"		-- use pcie device driver to access registers for P2040 CPU typically
				-- 	needs to support IOC_GET_REG32 and IOC_SET_REG32 defined below
 *****************************************************/	
#ifndef __MICROBLAZE__
 
#define PCIE_MMAP	1
#define P2040_LB	2
#define PCIE_DEV_DRV	3
#define IMX6_LB		4

/* defined in Makefile */
// #define ACCESS_INTERFACE PCIE_MMAP

#if ACCESS_INTERFACE == P2040_LB || ACCESS_INTERFACE == IMX6_LB
	typedef struct 
	{
		unsigned int	offset;
		unsigned short	value;
	} lb_reg_t;

	#define LB_IOC_GET_REG	_IOWR(LB_MAGIC, 0x02, lb_reg_t)
	#define LB_IOC_SET_REG	_IOW(LB_MAGIC, 0x03, lb_reg_t)
	const char* const get_lb_dev_name();
#endif

/* 	device driver must support IOC_GET_REG32 and IOC_SET_REG32 calls. Change the */
/*	definition of DEV_NAME, IOC_DEVICE_MAGIC, IOC_GET_REG32 and IOC_SET_REG32 accordingly  */
#if ACCESS_INTERFACE == PCIE_DEV_DRV
	typedef struct
	{
		unsigned int addr;
		unsigned int data;
	} reg_t;

	#define DEV_NAME		"/dev/vt_simple_pcie_drv"
	#define IOC_DEVICE_MAGIC 	'N'
	#define IOC_GET_REG32		_IOR(IOC_DEVICE_MAGIC, 0x02, reg_t)
	#define IOC_SET_REG32		_IOW(IOC_DEVICE_MAGIC, 0x03, reg_t)
#elif ACCESS_INTERFACE == P2040_LB
	#define LB_MAGIC 		'L'
#elif ACCESS_INTERFACE == IMX6_LB
	#define LB_MAGIC 		'M'
#endif

#endif /* #ifndef __MICROBLAZE__ */

int write_reg32(int fd, void* base, const unsigned int addr, const unsigned int write_data);
int read_reg32(int fd, void* base, const unsigned int addr, unsigned int* read_data);
void vt_delay(unsigned long micro_seconds);


#endif /*__VT_PLATFORM_H_*/
