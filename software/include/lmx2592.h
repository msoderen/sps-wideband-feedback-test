#ifndef __LMX2592_H_
#define __LMX2592_H_

#include "devices.h"

#define LMX2592_SDIO_RB_SHIFT			0
#define LMX2592_SDIO_RBPOL			0
#define LMX2592_SDIO_CPOL			0
#define LMX2592_SDIO_CPHA			0
#define LMX2592_SDIO_RW_BIT_READ_POL		1
#define LMX2592_SDIO_RW_BIT_POS			24
#define LMX2592_SDIO_DATA_POS			16
#define LMX2592_SDIO_DATA_WIDTH			16
#define LMX2592_SDIO_TRAN_WIDTH			24
#define LMX2592_SDIO_PRIVATE_DATA0		0
#define LMX2592_SDIO_PRIVATE_DATA1		0

/*
 * FMC217 for DAC fs=4GHz
 * RFOUTA = 4GHz
 * RFOUTB = open
 * VCO = 4GHz (VCO range 3.55~7.1GHz)
 * PFD = 200MHz (PFD range 5~200MHz)
 * OSCIN = 100MHz
 * OSC_2X = 1
 * PLL_R_PRE = 1 (12-bit value)
 * MULT = 1
 * PLL_R = 1
 * PLL_N = 10
 * PLL_N_PRE = 0 (1-bit. 0:divide-by-2; 1:divide-by-4)
 * VCO_2X_EN = 0
 *
*/

const reg_with_name_t lmx2592_dac_regs[] =
{
	{ 0x00, 3,	0x2316,	0x221C,	"R0 [13] LD_EN [8:7] FCAL_HPFD_ADJ [6:5] FCAL_LPFD_ADJ "
					"[3] FCAL_EN [2] MUXOUT_SEL [1] RESET [0] POWERDOWN" },	// FCAL_HPFD_ADJ to 2; soft reset
	{ 0x00, 0x80, 	10000, 	0, 	"Delay 10ms" },
	{ 0x01, 3,	0x0808,	0x080B,	"R1 [2:0] CAL_CLK_DIV" },	// change to 0
	{ 0x02, 3,	0x0500,	0x0500,	"R2" },
	{ 0x04, 3,	0x1943,	0x1943,	"R4 [15:8] ACAL_CMP_DLY" },
	{ 0x07, 3,	0x20B2,	0x20B2,	"R7" },
	{ 0x08, 3,	0x1000,	0x1000,	"R8" },
	{ 0x09, 3,	0x0B02,	0x0302,	"R9 [11] OSC_2X [9] REF_EN" },	// enable OSC_2X
	{ 0x0A, 3,	0x10D8,	0x10D8,	"R10 [11:7] MULT" },
	{ 0x0B, 3,	0x0018,	0x0018,	"R11 [11:4] PLL_R" },
	{ 0x0C, 3,	0x7001,	0x7001,	"R12 [11:0] PLL_R_PRE" },
	{ 0x0D, 3,	0x4000,	0x4000,	"R13 [14] CP_EN [9:8] PFD_CTL" },
	{ 0x0E, 3,	0x018D,	0x018D,	"R14 [11:7] CP_IDN [6:2] CP_IUP [1:0] CP_ICOARSE" },
	{ 0x13, 3,	0x0965,	0x0965,	"R19 read as 0x0965" },
	{ 0x17, 3,	0x8842,	0x8842,	"R23 read as 0x8842" },
	{ 0x18, 3,	0x0509,	0x0509,	"R24 read as 0x0509" },
	{ 0x1C, 3,	0x2924,	0x2924,	"R28 read as 0x2924" },
	{ 0x1D, 3,	0x0084,	0x0084,	"R29 read as 0x0084" },
	{ 0x1E, 3,	0x0034,	0x0034,	"R30 [10] MASH_DITHER [0] VCO_2X_EN" },
	{ 0x1F, 3,	0x0481,	0x0401,	"R31 [10] VCO_DISTB_PD [9] VCO_DISTA_PD [7] CHDIV_DIST_PD" },	// power down clkdiv
	{ 0x20, 3,	0x4210,	0x4210,	"R32" },
	{ 0x21, 3,	0x4210,	0x4210,	"R33" },
	{ 0x22, 3,	0xC3D0,	0xC3F0,	"R34 [5] CHDIV_EN" },	// disable
	{ 0x23, 3,	0x001D,	0x021D,	"R35 [12:9] CHDIV_SEG2 [8] CHDIV_SEG3_EN [7] CHDIV_SEG2_EN [2] CHDIV_SEG1 [1] CHDIV_SEG1_EN" },	// power down all
	{ 0x24, 3,	0x0000,	0x0411,	"R36 [11] CHDIV_DISTB_EN [10] CHDIV_DISTA_EN [6:4] CHDIV_SEG_SEL [3:0] CHDIV_SEG3" },	// power down all
	{ 0x25, 3,	0x4000,	0x4000,	"R37 [12] PLL_N_PRE" },
	{ 0x26, 3,	0x0014,	0x0036,	"R38 [12:1] PLL_N" },	// 10
	{ 0x27, 3,	0x8104,	0x8204,	"R39 [13:8] PFD_DLY" },	// change to 1 for integer-N
	{ 0x28, 3,	0x03E8,	0x03E8,	"R40 PLL_DEN[31:16]" },
	{ 0x29, 3,	0x03E8,	0x03E8,	"R41 PLL_DEN[15:0]" },
	{ 0x2A, 3,	0x0000,	0x0000,	"R42 MASH_SEED[31:16]" },
	{ 0x2B, 3,	0x0000,	0x0000,	"R43 MASH_SEED[15:0]" },
	{ 0x2C, 3,	0x0000,	0x0000,	"R44 PLL_NUM[31:16]" },
	{ 0x2D, 3,	0x0000,	0x0000,	"R45 PLL_NUM[15:0]" },
	{ 0x2E, 3,	0x3F90,	0x0FB3,	"R46 [13:8] OUTA_POW [7] OUTB_PD [6] OUTA_PD [5] MASH_EN [2:0] MASH_ORDER" },	// OUTA_POW=max MASH_EN=0 MASH_ORDER=0
	{ 0x2F, 3,	0x08DF,	0x00C0,	"R47 [12:11] OUTA_MUX [5:0] OUTB_POW" }, // OUTA_MUX = 1
	{ 0x30, 3,	0x03FD,	0x03FC,	"R48 [1:0] OUTB_MUX" },	// OUTB_MUX = 1
	{ 0x3E, 3,	0x0000,	0x0000,	"R62" },
	{ 0x40, 3,	0x00AF,	0x00AF,	"R64 [9] ACAL_FAST [8] FCAL_FAST" },
	{ 0x00, 2,	0x231C,	0x221C,	"R0 [13] LD_EN [8:7] FCAL_HPFD_ADJ [6:5] FCAL_LPFD_ADJ "
					"[3] FCAL_EN [2] MUXOUT_SEL [1] RESET [0] POWERDOWN" },	// calibrate
	{ 0x00, 0, 	0x0000, 0x0000, 	NULL}
};

/*
 * FMC217 for ADC fs=3.2GHz
 * RFOUTA = 3.2GHz to ADC
 * RFOUTB = 3.2GHz to HMC7043
 * VCO = 6.4GHz (VCO range 3.55~7.1GHz)
 * PFD = 200MHz (PFD range 5~200MHz)
 * OSCIN = 100MHz
 * OSC_2X = 1
 * PLL_R_PRE = 1 (12-bit value)
 * MULT = 1
 * PLL_R = 1
 * PLL_N = 16
 * PLL_N_PRE = 0 (1-bit. 0:divide-by-2; 1:divide-by-4)
 * VCO_2X_EN = 0
 *
*/

const reg_with_name_t lmx2592_adc_regs[] =
{
	{ 0x00, 3,	0x2316,	0x221C,	"R0 [13] LD_EN [8:7] FCAL_HPFD_ADJ [6:5] FCAL_LPFD_ADJ "
					"[3] FCAL_EN [2] MUXOUT_SEL [1] RESET [0] POWERDOWN" },	// FCAL_HPFD_ADJ to 2; soft reset
	{ 0x00, 0x80, 	10000, 	0, 	"Delay 10ms" },
	{ 0x01, 3,	0x0808,	0x080B,	"R1 [2:0] CAL_CLK_DIV" },	// change to 0
	{ 0x02, 3,	0x0500,	0x0500,	"R2" },
	{ 0x04, 3,	0x1943,	0x1943,	"R4 [15:8] ACAL_CMP_DLY" },
	{ 0x07, 3,	0x20B2,	0x20B2,	"R7" },
	{ 0x08, 3,	0x1000,	0x1000,	"R8" },
	{ 0x09, 3,	0x0B02,	0x0302,	"R9 [11] OSC_2X [9] REF_EN" },	// enable OSC_2X
	{ 0x0A, 3,	0x10D8,	0x10D8,	"R10 [11:7] MULT" },
	{ 0x0B, 3,	0x0018,	0x0018,	"R11 [11:4] PLL_R" },
	{ 0x0C, 3,	0x7001,	0x7001,	"R12 [11:0] PLL_R_PRE" },
	{ 0x0D, 3,	0x4000,	0x4000,	"R13 [14] CP_EN [9:8] PFD_CTL" },
	{ 0x0E, 3,	0x018D,	0x018D,	"R14 [11:7] CP_IDN [6:2] CP_IUP [1:0] CP_ICOARSE" },
	{ 0x13, 3,	0x0965,	0x0965,	"R19 read as 0x0965" },
	{ 0x14, 3,	0x012C,	0x012C,	"R20 read as 0x012C" },
	{ 0x16, 3,	0x2300,	0x2300,	"R22 read as 0x2300" },
	{ 0x17, 3,	0x8842,	0x8842,	"R23 read as 0x8842" },
	{ 0x18, 3,	0x0509,	0x0509,	"R24 read as 0x0509" },
	{ 0x19, 3,	0x0000,	0x0000,	"R25 read as 0x0000" },
	{ 0x1C, 3,	0x2924,	0x2924,	"R28 read as 0x2924" },
	{ 0x1D, 3,	0x0084,	0x0084,	"R29 read as 0x0084" },
	{ 0x1E, 3,	0x0034,	0x0034,	"R30 [10] MASH_DITHER [0] VCO_2X_EN" },
	{ 0x1F, 3,	0x0601,	0x0401,	"R31 [10] VCO_DISTB_PD [9] VCO_DISTA_PD [7] CHDIV_DIST_PD" },	// power down vco_dista and vco_distb; enable chdiv_dist
	{ 0x20, 3,	0x4210,	0x4210,	"R32" },
	{ 0x21, 3,	0x4210,	0x4210,	"R33" },
	{ 0x22, 3,	0xC3F0,	0xC3F0,	"R34 [5] CHDIV_EN" },	// enable
	{ 0x23, 3,	0x021A,	0x021D,	"R35 [12:9] CHDIV_SEG2 [8] CHDIV_SEG3_EN [7] CHDIV_SEG2_EN [2] CHDIV_SEG1 [1] CHDIV_SEG1_EN" },	// enable SEG1 only. SEG1=/2
	{ 0x24, 3,	0x0C11,	0x0411,	"R36 [11] CHDIV_DISTB_EN [10] CHDIV_DISTA_EN [6:4] CHDIV_SEG_SEL [3:0] CHDIV_SEG3" },	// sel SE1; enable divider to both outputs
	{ 0x25, 3,	0x4000,	0x4000,	"R37 [12] PLL_N_PRE" },
	{ 0x26, 3,	0x0020,	0x0036,	"R38 [12:1] PLL_N" },	// 14
	{ 0x27, 3,	0x8204,	0x8204,	"R39 [13:8] PFD_DLY" },	// change to 1 for integer-N
	{ 0x28, 3,	0x0000,	0x03E8,	"R40 PLL_DEN[31:16]" },
	{ 0x29, 3,	0x0001,	0x03E8,	"R41 PLL_DEN[15:0]" },
	{ 0x2A, 3,	0x0000,	0x0000,	"R42 MASH_SEED[31:16]" },
	{ 0x2B, 3,	0x0000,	0x0000,	"R43 MASH_SEED[15:0]" },
	{ 0x2C, 3,	0x0000,	0x0000,	"R44 PLL_NUM[31:16]" },
	{ 0x2D, 3,	0x0000,	0x0000,	"R45 PLL_NUM[15:0]" },
	{ 0x2E, 3,	0x0F22,	0x0FB3,	"R46 [13:8] OUTA_POW [7] OUTB_PD [6] OUTA_PD [5] MASH_EN [2:0] MASH_ORDER" },	// OUTA_POW=max MASH_EN=0 MASH_ORDER=0; enable both outputs
	{ 0x2F, 3,	0x00CF,	0x00C0,	"R47 [12:11] OUTA_MUX [5:0] OUTB_POW" }, // OUTA_MUX = 0 (chdiv); OUTB_POW=max
	{ 0x30, 3,	0x03FC,	0x03FC,	"R48 [1:0] OUTB_MUX" },	// OUTB_MUX = 0 (chdiv)
	{ 0x3B, 3,	0x0000,	0x0000,	"R59 read as 0x0000" },
	{ 0x3D, 3,	0x0001,	0x0001,	"R61 read as 0x0001" },
	{ 0x3E, 3,	0x0000,	0x0000,	"R62" },
	{ 0x40, 3,	0x00AF,	0x00AF,	"R64 [9] ACAL_FAST [8] FCAL_FAST" },
	{ 0x44, 3,	0x0089,	0x0089,	"R68 read as 0x0089" },
	{ 0x45, 3,	0x0000,	0x0000,	"R69 read as 0x0000" },
	{ 0x46, 3,	0x0000,	0x0000,	"R70 read as 0x0000" },
	{ 0x00, 2,	0x231C,	0x221C,	"R0 [13] LD_EN [8:7] FCAL_HPFD_ADJ [6:5] FCAL_LPFD_ADJ "
					"[3] FCAL_EN [2] MUXOUT_SEL [1] RESET [0] POWERDOWN" },	// calibrate
	{ 0x00, 0, 	0x0000, 0x0000, 	NULL}
};

#endif /* __LMX2592_H_ */
