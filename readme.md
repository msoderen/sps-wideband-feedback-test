built in Linux and Vivado v2019.2 (64-bit)


Procedure to clone project:

clone project<br/>
modify the paths in recreate.tcl to your liking<br/>
open vivado<br/>
In tcl console:<br/>
	cd /my/path/to/repo<br/>
	source recreate.tcl<br/>
How to instal ubuntu:[Xilinx ZCU216 + Vadatech FMC217 + embedded Linux test for SPS Wideband Transverse Feedback](https://wikis.cern.ch/pages/viewpage.action?pageId=217176317)

How to program the ADC and DAC:
clone this repo onto the ZCU102 running ubuntu<br/>
cd software<br/>
mkdir build<br/>
cd build<br/>
cmake ..<br/>
make cmd_init<br/>
fmc217_tool i b0 s<br/> 
this will program the ADC 6.4 Gsps and DAC 5 Gsps in loopback mode<br/>
Capture ADC data:<br/>
fmc217_tool c 0 output.txt<br/>
python plot.py output.txt<br/>
