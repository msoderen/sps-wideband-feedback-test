#For you to modify where you want the projects
#----------------------------------------------

#where  IP repositories will end up
set ip_repo "/home/msoderen/abcd/ip_repo"
set modelsim_library "/home/msoderen/vivadoProjects/simlibraries"
set project_dir "/home/msoderen/abcd"
set board_store_dir "/home/msoderen/.Xilinx/Vivado/2019.2/xhub/board_store"

#----------------------------------------------
exec mkdir -p ${ip_repo}


source ./fmc217/recreate.tcl
update_compile_order -fileset sources_1
ipx::package_project -root_dir ${ip_repo}/fmc217_repo -vendor user.org -library user -taxonomy /UserIP  -import_files
ipx::create_xgui_files [ipx::current_core]
ipx::update_checksums [ipx::current_core]
ipx::save_core [ipx::current_core]
set_property  ip_repo_paths  ${ip_repo}/fmc217_repo [current_project]

update_ip_catalog
source ./wideband_feedback/recreate.tcl
