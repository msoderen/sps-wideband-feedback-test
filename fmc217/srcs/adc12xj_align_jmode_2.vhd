----------------------------------------------------------------------------------
-- Company:   		VadaTech Incorporated
-- Engineer: 		Maxwell S.
-- Copyright:		Copyright 2017 VadaTech Incorporated.  All rights reserved.
--
-- Create Date: 	10/06/2017
-- Design Name:
-- Module Name: 	adc12xj_align_jmode_2 - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
--
-- Description: 	Align ADC data for TI ADC12J*/ADC12DJ* devices in bypass mode with 4 lanes
--			Input: 4 lane JESD core data
--			Output: 20 12-bit samples. Earliest sample in LSB
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

entity adc12xj_align_jmode_2 is
port (
	rx_aclk				: in 	std_logic;
	rx_aresetn			: in 	std_logic;	
	jesd204b_tdata			: in	std_logic_vector(127 downto 0);
	jesd204b_start_of_frame		: in	std_logic_vector(3 downto 0);	
	adc_dout			: out	std_logic_vector(119 downto 0)

);
end adc12xj_align_jmode_2;

architecture Behavioral of adc12xj_align_jmode_2 is
	signal rx_data_3sa_lane			: std_logic_vector(143 downto 0);
	signal tail_off				: std_logic;
	signal jesd204b_tdata_d1		: std_logic_vector(127 downto 0);
	signal start_of_frame_d1		: std_logic_vector(3 downto 0);

begin
	process(rx_aclk)
	begin
		if(rising_edge(rx_aclk)) then
			if(rx_aresetn = '0') then
				rx_data_3sa_lane			<= (others => '0');
				tail_off				<= '0';
				jesd204b_tdata_d1			<= (others => '0');
				start_of_frame_d1			<= (others => '0');
			else
				jesd204b_tdata_d1			<= jesd204b_tdata;

				if(jesd204b_start_of_frame /= "0000") then
					start_of_frame_d1		<= jesd204b_start_of_frame;
				end if;

				if(jesd204b_start_of_frame = "0000" and start_of_frame_d1 = "0001") then
					for i in 3 downto 0 loop
						-- s16-s19
						rx_data_3sa_lane(12*i+11 downto 12*i+4)		<= jesd204b_tdata_d1(32*i+23 downto 32*i+16);
						rx_data_3sa_lane(12*i+3 downto 12*i)		<= jesd204b_tdata_d1(32*i+31 downto 32*i+28);
						-- s0-s3
						rx_data_3sa_lane(48+12*i+3 downto 48+12*i)	<= jesd204b_tdata(32*i+15 downto 32*i+12);
						rx_data_3sa_lane(48+12*i+11 downto 48+12*i+4)	<= jesd204b_tdata(32*i+7 downto 32*i+0);
						-- s4-s7 (use s4-s5)
						rx_data_3sa_lane(96+12*i+7 downto 96+12*i)	<= jesd204b_tdata(32*i+23 downto 32*i+16);
						rx_data_3sa_lane(96+12*i+11 downto 96+12*i+8)	<= jesd204b_tdata(32*i+11 downto 32*i+8);
					end loop;
					tail_off						<= '1';
				elsif(jesd204b_start_of_frame = "0001") then
					for i in 3 downto 0 loop
						-- s4-s7 (use s6-s7)
						rx_data_3sa_lane(12*i+11 downto 12*i+8)		<= jesd204b_tdata_d1(32*i+11 downto 32*i+8);
						rx_data_3sa_lane(12*i+7 downto 12*i)		<= jesd204b_tdata_d1(32*i+23 downto 32*i+16);
						-- s8-s11
						rx_data_3sa_lane(48+12*i+3 downto 48+12*i)	<= jesd204b_tdata(32*i+7 downto 32*i+4);
						rx_data_3sa_lane(48+12*i+11 downto 48+12*i+4)	<= jesd204b_tdata_d1(32*i+31 downto 32*i+24);
						-- s12-s15
						rx_data_3sa_lane(96+12*i+7 downto 96+12*i)	<= jesd204b_tdata(32*i+15 downto 32*i+8);
						rx_data_3sa_lane(96+12*i+11 downto 96+12*i+8)	<= jesd204b_tdata(32*i+3 downto 32*i);
					end loop;
					tail_off						<= '0';
				elsif(jesd204b_start_of_frame = "0000" and start_of_frame_d1 = "0010") then
					for i in 3 downto 0 loop
						-- s16-s19
						rx_data_3sa_lane(12*i+11 downto 12*i+4)		<= jesd204b_tdata_d1(32*i+31 downto 32*i+24);
						rx_data_3sa_lane(12*i+3 downto 12*i)		<= jesd204b_tdata(32*i+3 downto 32*i);
						-- s0-s3
						rx_data_3sa_lane(48+12*i+3 downto 48+12*i)	<= jesd204b_tdata(32*i+23 downto 32*i+20);
						rx_data_3sa_lane(48+12*i+11 downto 48+12*i+4)	<= jesd204b_tdata(32*i+15 downto 32*i+8);
						-- s4-s7 (use s4-s5)
						rx_data_3sa_lane(96+12*i+7 downto 96+12*i)	<= jesd204b_tdata(32*i+31 downto 32*i+24);
						rx_data_3sa_lane(96+12*i+11 downto 96+12*i+8)	<= jesd204b_tdata(32*i+19 downto 32*i+16);
					end loop;
					tail_off						<= '1';
				elsif(jesd204b_start_of_frame = "0010") then
					for i in 3 downto 0 loop
						-- s4-s7 (use s6-s7)
						rx_data_3sa_lane(12*i+11 downto 12*i+8)		<= jesd204b_tdata_d1(32*i+19 downto 32*i+16);
						rx_data_3sa_lane(12*i+7 downto 12*i)		<= jesd204b_tdata_d1(32*i+31 downto 32*i+24);
						-- s8-s11
						rx_data_3sa_lane(48+12*i+3 downto 48+12*i)	<= jesd204b_tdata(32*i+15 downto 32*i+12);
						rx_data_3sa_lane(48+12*i+11 downto 48+12*i+4)	<= jesd204b_tdata(32*i+7 downto 32*i);
						-- s12-s15
						rx_data_3sa_lane(96+12*i+7 downto 96+12*i)	<= jesd204b_tdata(32*i+23 downto 32*i+16);
						rx_data_3sa_lane(96+12*i+11 downto 96+12*i+8)	<= jesd204b_tdata(32*i+11 downto 32*i+8);
					end loop;
					tail_off						<= '0';
				elsif(jesd204b_start_of_frame = "0000" and start_of_frame_d1 = "0100") then
					for i in 3 downto 0 loop
						-- s8-s11 (use s10-s11)
						rx_data_3sa_lane(12*i+3 downto 12*i)		<= jesd204b_tdata_d1(32*i+23 downto 32*i+20);
						rx_data_3sa_lane(12*i+11 downto 12*i+4)		<= jesd204b_tdata_d1(32*i+15 downto 32*i+8);
						-- s12-s15
						rx_data_3sa_lane(48+12*i+11 downto 48+12*i+8)	<= jesd204b_tdata_d1(32*i+19 downto 32*i+16);
						rx_data_3sa_lane(48+12*i+7 downto 48+12*i)	<= jesd204b_tdata_d1(32*i+31 downto 32*i+24);
						-- s16-s19
						rx_data_3sa_lane(96+12*i+3 downto 96+12*i)	<= jesd204b_tdata(32*i+15 downto 32*i+12);
						rx_data_3sa_lane(96+12*i+11 downto 96+12*i+4)	<= jesd204b_tdata(32*i+7 downto 32*i);
					end loop;
					tail_off						<= '0';
				elsif(jesd204b_start_of_frame = "0100") then
					for i in 3 downto 0 loop
						-- s0-s3
						rx_data_3sa_lane(12*i+3 downto 12*i)		<= jesd204b_tdata_d1(32*i+31 downto 32*i+28);
						rx_data_3sa_lane(12*i+11 downto 12*i+4)		<= jesd204b_tdata_d1(32*i+23 downto 32*i+16);
						-- s4-s7
						rx_data_3sa_lane(48+12*i+11 downto 48+12*i+8)	<= jesd204b_tdata_d1(32*i+27 downto 32*i+24);
						rx_data_3sa_lane(48+12*i+7 downto 48+12*i)	<= jesd204b_tdata(32*i+7 downto 32*i);
						-- s8-s11 (use s8-s9)
						rx_data_3sa_lane(96+12*i+3 downto 96+12*i)	<= jesd204b_tdata(32*i+23 downto 32*i+20);
						rx_data_3sa_lane(96+12*i+11 downto 96+12*i+4)	<= jesd204b_tdata(32*i+15 downto 32*i+8);
					end loop;
					tail_off						<= '1';
				elsif(jesd204b_start_of_frame = "0000" and start_of_frame_d1 = "1000") then
					for i in 3 downto 0 loop
						-- s8-s11 (use s10-s11)
						rx_data_3sa_lane(12*i+3 downto 12*i)		<= jesd204b_tdata_d1(32*i+31 downto 32*i+28);
						rx_data_3sa_lane(12*i+11 downto 12*i+4)		<= jesd204b_tdata_d1(32*i+23 downto 32*i+16);
						-- s12-s15
						rx_data_3sa_lane(48+12*i+11 downto 48+12*i+8)	<= jesd204b_tdata_d1(32*i+27 downto 32*i+24);
						rx_data_3sa_lane(48+12*i+7 downto 48+12*i)	<= jesd204b_tdata(32*i+7 downto 32*i);
						-- s16-s19
						rx_data_3sa_lane(96+12*i+3 downto 96+12*i)	<= jesd204b_tdata(32*i+23 downto 32*i+20);
						rx_data_3sa_lane(96+12*i+11 downto 96+12*i+4)	<= jesd204b_tdata(32*i+15 downto 32*i+8);
					end loop;
					tail_off						<= '0';
				elsif(jesd204b_start_of_frame = "1000") then
					for i in 3 downto 0 loop
						-- s0-s3
						rx_data_3sa_lane(12*i+3 downto 12*i)		<= jesd204b_tdata(32*i+7 downto 32*i+4);
						rx_data_3sa_lane(12*i+11 downto 12*i+4)		<= jesd204b_tdata_d1(32*i+31 downto 32*i+24);
						-- s4-s7
						rx_data_3sa_lane(48+12*i+11 downto 48+12*i+8)	<= jesd204b_tdata(32*i+3 downto 32*i);
						rx_data_3sa_lane(48+12*i+7 downto 48+12*i)	<= jesd204b_tdata(32*i+15 downto 32*i+8);
						-- s8-s11 (use s8-s9)
						rx_data_3sa_lane(96+12*i+3 downto 96+12*i)	<= jesd204b_tdata(32*i+31 downto 32*i+28);
						rx_data_3sa_lane(96+12*i+11 downto 96+12*i+4)	<= jesd204b_tdata(32*i+23 downto 32*i+16);
					end loop;
					tail_off						<= '1';
				end if;
			end if;
		end if;
	end process;

	process(rx_aclk)
	begin
		if(rising_edge(rx_aclk)) then
			if(rx_aresetn = '0') then
				adc_dout		<= (others => '0');
			else
				if(tail_off = '1') then
					adc_dout	<= rx_data_3sa_lane(119 downto 0);
				else
					adc_dout	<= rx_data_3sa_lane(143 downto 24);
				end if;
			end if;

		end if;
	end process;
	
end Behavioral;