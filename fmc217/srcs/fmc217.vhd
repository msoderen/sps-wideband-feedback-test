----------------------------------------------------------------------------------
-- Company:   		VadaTech Incorporated
-- Engineer: 		Maxwell S.
-- Copyright:		Copyright 2017 VadaTech Incorporated.  All rights reserved.
--
-- Create Date: 	10/04/2017
-- Design Name:
-- Module Name: 	fmc217 - Behavioral
-- Project Name:
-- Target Devices:
-- Tool Versions:
--
-- Description: 	This is the main module of the fmc217 reference design
--				DAC AD9162
--					L=8 F=1 N=NP=16 CF=CS=0 HD=1
--					bypass mode: M=1. S=4
--					2x NRZ mode: M=2. S=2
--				    For speed grade -2 GTH VIRTEX7 and -2 GTX: sampling frequency = @4GHz.
--					SERDES line rate: 4Gsps*16bit*10/8(8b10b)/8=10Gbps
--					SYSREF = 10G/10/F/K=31.25MHz
--				    For speed grade -3 VIRTEX7: sampling frequency = @5GHz.
--					SERDES line rate: 5Gsps*16bit*10/8(8b10b)/8=12.5Gbps
--					SYSREF = 12.5G/10/F/K=39.0625MHz
--					(Not supported by this reference design. The modification should be easy and straight forward)
--				ADC12DJ3200 (Ordering option B=0)
--					8 lanes. 12-bit dual channel. (JMODE = 2)
--					F8 S5 R4 N12 N'12 CS0 Links2 DES0 D1 K32 Per link: L4 M4
--				    For Speed grade -2 GTX transceiver such as AMC518: sampling frequency = 2.5GSPS
--					SERDES line rate: 2.5Gsps*2channel*12.8(effective_bits)*10/8(8b10b)/8(lanes) = 10Gbps
--					SYSREF = 10G/10/F/K=10G/10/8/32=3.90625MHz
--				    For Speed grade -2 VIRTEX7: sampling frequency = 2.8GSPS
--					SERDES line rate: 2.8Gsps*2channel*12.8(effective_bits)*10/8(8b10b)/8(lanes) = 11.2Gbps
--					SYSREF = 11.2G/10/F/K=11.2G/10/8/32=4.375MHz
--				    For Speed grade -3 VIRTEX7: sampling frequency = 3.2GSPS
--					SERDES line rate: 3.2Gsps*2channel*12.8(effective_bits)*10/8(8b10b)/8(lanes) = 12.8Gbps
--					SYSREF = 12.8G/10/F/K=11.2G/10/8/32=5MHz
--					(Not supported by this reference design. The modification should be easy and straight forward)
--				ADC12DJ2700 (Ordering option B=1)
--				    For Speed grade -2 GTX transceiver such as AMC518: sampling frequency = 2.5GSPS
--					SERDES line rate: 2.5Gsps*2channel*12.8(effective_bits)*10/8(8b10b)/8(lanes) = 10Gbps
--				    For Speed grade -2 VIRTEX7: sampling frequency = 2.7GSPS
--					SERDES line rate: 2.7Gsps*2channel*12.8(effective_bits)*10/8(8b10b)/8(lanes) = 10.8Gbps

-- Description: 	This is the main module of the fmc217 reference design

--
-- Version History:
-- 09/18/2018		V1.3	Added FIR filter for ADC->DAC loop back for speed grade -2 GTX transceiver.
-- 06/29/2018		V1.2	Added ordering option B=1 (ADC12DJ2700) support (software change only)
-- 05/10/2018		V1.2    Added ADC single-channel 6.4GSPS acquisition support
-- 02/22/2018		V1.2  	Added DAC 2.8GSPS option for ADC->DAC loop back latency test.
--				    For speed grade -2 VIRTEX7: sampling frequency = @2.8GHz.
--					SERDES line rate: 2.8Gsps*16bit*10/8(8b10b)/8=7Gbps
--					SYSREF = 7G/10/F/K=21.875MHz
--				Updated to Vivado 2017.3
-- 12/08/2017		V1.1	Added ULTRASCALE carrier support
--				Added option A=1 support
-- 10/04/2017		V1.0	Created
----------------------------------------------------------------------------------

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

library UNISIM;
use UNISIM.VComponents.all;

entity fmc217 is
generic (
	-- fixed
	MAJOR_VERSION			: integer range 0 to 255	:= 1;
	MINOR_VERSION			: integer range 0 to 255	:= 3;
	PATCH_VERSION			: integer range 0 to 255	:= 0;
	REV_VERSION			: integer range 0 to 255	:= 0;
	IMAGE_ID 			: std_logic_vector(15 downto 0) := x"F217";
	BAR_SIZE			: integer 			:= 19;		-- AXI space claimed
	-- from carrier must have
	SIGNATURE			: std_logic_vector(15 downto 0) := x"0000";
	FPGA_SERIES			: string			:= "ULTRASCALE";
	--FPGA_FAMILY			: string 			:= "VIRTEX7"; -- changed 2020/02/12-GKO: not used by design
	FPGA_TRANSCEIVER		: string 			:= "GTHE4";
	--LVDS_STANDARD			: string 			:= "LVDS"; -- changed 2020/02/12-GKO: removed from design
	SPEED_GRADE			: integer range 1 to 3 		:= 2		-- currently support 2 only
);
port (
	-- Ports of AXI4-Lite interface
	s_axi_aclk			: in 	std_logic;
	s_axi_aresetn			: in 	std_logic;
	s_axi_awaddr			: in 	std_logic_vector(31 downto 0);
	s_axi_awvalid			: in 	std_logic;
	s_axi_awready			: out 	std_logic;
	s_axi_wdata			: in  	std_logic_vector(31 downto 0);
	s_axi_wstrb			: in  	std_logic_vector(3 downto 0);
	s_axi_wvalid			: in  	std_logic;
	s_axi_wready			: out 	std_logic;
	s_axi_bresp			: out 	std_logic_vector (1 downto 0);
	s_axi_bvalid			: out 	std_logic;
	s_axi_bready			: in  	std_logic;
	s_axi_araddr			: in  	std_logic_vector(31 downto 0);
	s_axi_arvalid			: in  	std_logic;
	s_axi_arready			: out 	std_logic;
	s_axi_rdata			: out 	std_logic_vector (31 downto 0);
	s_axi_rresp			: out 	std_logic_vector (1 downto 0);
	s_axi_rvalid			: out 	std_logic;
	s_axi_rready			: in 	std_logic;

	axi_ruser_strb_n		: in	std_logic_vector(3 downto 0);

	-- ***************** FMC signals ******************

	--============ FMC_LA ==============
	-- HMC7043 SCLKOUT3 for ADC JESD204 core SYSREF
	FMC_LA_P0			: in	std_logic;
	FMC_LA_N0			: in	std_logic;

	-- LMK SDCLKOUT3 for DAC JESD204 core SYSREF
	FMC_LA_P1			: in	std_logic;
	FMC_LA_N1			: in	std_logic;

	-- ADC NCOA0 and NCOA1
	FMC_LA_P2			: out	std_logic;
	FMC_LA_N2			: out	std_logic;

	-- ADC NCOB0 and NCOB1
	FMC_LA_P3			: out	std_logic;
	FMC_LA_N3			: out	std_logic;

	-- ADC TMSTP
	FMC_LA_P4			: out	std_logic;
	FMC_LA_N4			: out	std_logic;

	-- ADC ORA0 and ORA1
	FMC_LA_P5			: in	std_logic;
	FMC_LA_N5			: in	std_logic;

	-- ADC ORB0 and ORB1
	FMC_LA_P6			: in	std_logic;
	FMC_LA_N6			: in	std_logic;

	-- ADC SDO and ADC CALSTAT
	FMC_LA_P7			: in	std_logic;
	FMC_LA_N7			: in	std_logic;

	-- ADC PD and ADC CALTRIG
	FMC_LA_P8			: out	std_logic;
	FMC_LA_N8			: out	std_logic;

	-- ADC nSCS and ADC SCLK
	FMC_LA_P9			: out	std_logic;
	FMC_LA_N9			: out	std_logic;

	-- ADC SDI and ADC nSYNCSE
	FMC_LA_P10			: out	std_logic;
	FMC_LA_N10			: out	std_logic;

	-- HMC7043 RFSYNCIN
	FMC_LA_P11			: out	std_logic;
	FMC_LA_N11			: out	std_logic;

	-- LMX2592_ADC CE and LMX2592_ADC CSB
	FMC_LA_P12			: out	std_logic;
	FMC_LA_N12			: out	std_logic;

	-- LMX2592_ADC SCK and LMX2592_ADC SDI
	FMC_LA_P13			: out	std_logic;
	FMC_LA_N13			: out	std_logic;

	-- LMX2592_ADC MUXOUT and LMX2592_DAC MUXOUT
	FMC_LA_P14			: in	std_logic;
	FMC_LA_N14			: in	std_logic;

	-- LMX2592_DAC CE and LMX2592_DAC CSB
	FMC_LA_P15			: out	std_logic;
	FMC_LA_N15			: out	std_logic;

	-- LMX2592_DAC SCK and LMX2592_DAC SDI
	FMC_LA_P16			: out	std_logic;
	FMC_LA_N16			: out	std_logic;

	-- DAC SYNC
	FMC_LA_P17			: in	std_logic;
	FMC_LA_N17			: in	std_logic;

	-- Front panel TRIG_IN for ordering option A=0; TRIG_IN0 for A=1
	FMC_LA_P18			: in	std_logic;
	FMC_LA_N18			: in	std_logic;

	-- DAC SDI and DAC SDO
	FMC_LA_P19			: out	std_logic;
	FMC_LA_N19			: in	std_logic;

	-- DAC SCLK and DAC nCS
	FMC_LA_P20			: out	std_logic;
	FMC_LA_N20			: out	std_logic;

	-- DAC nIRQ and DAC nRESET
	FMC_LA_P21			: in	std_logic;
	FMC_LA_N21			: out	std_logic;

	-- DAC TX_EN and not used
	FMC_LA_P22			: out	std_logic;
	-- FMC_LA_N22			: out	std_logic;

	-- Front panel TRIG_OUT for ordering option A=0
	FMC_LA_P23			: out	std_logic;
	FMC_LA_N23			: out	std_logic;

	-- LMK DIR_SDIO and LMK DIR_SYNC
	FMC_LA_P24			: out	std_logic;
	FMC_LA_N24			: out	std_logic;

	-- LMK SDIO and LMK RESET
	FMC_LA_P25			: inout	std_logic;
	FMC_LA_N25			: out	std_logic;

	-- LMK nCS and LMK SYNC/SYSREF_REQ
	FMC_LA_P26			: out	std_logic;
	FMC_LA_N26			: out	std_logic;

	-- LMK SCK and HMC7043 RESET
	FMC_LA_P27			: out	std_logic;
	FMC_LA_N27			: out	std_logic;

	-- LMK STATUS_LD1 and LMK STATUS_LD2
	FMC_LA_P28			: in	std_logic;
	FMC_LA_N28			: in	std_logic;

	-- HMC7043 SLEN and HMC7043 SCLK
	FMC_LA_P29			: out	std_logic;
	FMC_LA_N29			: out	std_logic;

	-- Not used and HMC7043 SDATA
	-- FMC_LA_P30			: inout	std_logic;
	FMC_LA_N30			: inout	std_logic;

	-- PE43713_ADC SI and PE43713_ADC CLK
	FMC_LA_P31			: out	std_logic;
	FMC_LA_N31			: out	std_logic;

	-- PE43713_ADC LE and PE43713_DAC SI
	FMC_LA_P32			: out	std_logic;
	FMC_LA_N32			: out	std_logic;

	-- PE43713_DAC CLK and PE43713_DAC LE
	FMC_LA_P33			: out	std_logic;
	FMC_LA_N33			: out	std_logic;

	--============= FMC_HA ================
	-- Front panel TRIG_IN1 for ordering option A=1
--	FMC_HA_P0			: in	std_logic;
--	FMC_HA_N0			: in	std_logic;

	-- Not used others

	--============= FMC_HB  ============
	-- Not used

	--============= FMC GTX Lanes =========================
	-- ADC
	FMC_RX0_P			: in 	std_logic;
	FMC_RX0_N			: in 	std_logic;
	FMC_RX1_P			: in 	std_logic;
	FMC_RX1_N			: in 	std_logic;
	FMC_RX2_P			: in 	std_logic;
	FMC_RX2_N			: in 	std_logic;
	FMC_RX3_P			: in 	std_logic;
	FMC_RX3_N			: in 	std_logic;
	FMC_RX4_P			: in 	std_logic;
	FMC_RX4_N			: in 	std_logic;
	FMC_RX5_P			: in 	std_logic;
	FMC_RX5_N			: in 	std_logic;
	FMC_RX6_P			: in 	std_logic;
	FMC_RX6_N			: in 	std_logic;
	FMC_RX7_P			: in 	std_logic;
	FMC_RX7_N			: in 	std_logic;
	-- DAC
	FMC_TX0_P			: out	std_logic;
	FMC_TX0_N			: out	std_logic;
	FMC_TX1_P			: out	std_logic;
	FMC_TX1_N			: out	std_logic;
	FMC_TX2_P			: out	std_logic;
	FMC_TX2_N			: out	std_logic;
	FMC_TX3_P			: out	std_logic;
	FMC_TX3_N			: out	std_logic;
	FMC_TX4_P			: out	std_logic;
	FMC_TX4_N			: out	std_logic;
	FMC_TX5_P			: out	std_logic;
	FMC_TX5_N			: out	std_logic;
	FMC_TX6_P			: out	std_logic;
	FMC_TX6_N			: out	std_logic;
	FMC_TX7_P			: out	std_logic;
	FMC_TX7_N			: out	std_logic;
	-- MGT118 REFCLK0 (HMC7043 CLKOUT2 for ADC JESD204 core refclk)
	FMC_GBTCLK0_P			: in	std_logic;
	FMC_GBTCLK0_N			: in	std_logic;
    a0_core_clk_o           : out   std_logic;
	-- MGT118 REFCLK1 (LMK04828 DCLKOUT2 for DAC JESD204 core refclk)
	FMC_GBTCLK1_P			: in	std_logic;
	FMC_GBTCLK1_N			: in	std_logic;

	-- ******************* End of FMC signals **********************
	--Parallel ADC data outputs
	adc_data0 			 	: out std_logic_vector (11 downto 0 );
	adc_data1  				: out std_logic_vector (11 downto 0 );
	adc_data2 				: out std_logic_vector (11 downto 0 );
	adc_data3 				: out std_logic_vector (11 downto 0 );
	adc_data4					: out std_logic_vector (11 downto 0 );
	adc_data5					: out std_logic_vector (11 downto 0 );
	adc_data6					: out std_logic_vector (11 downto 0 );
	adc_data7					: out std_logic_vector (11 downto 0 );
	adc_data8					: out std_logic_vector (11 downto 0 );
	adc_data9					: out std_logic_vector (11 downto 0 );
	adc_data10				: out std_logic_vector (11 downto 0 );
	adc_data11				: out std_logic_vector (11 downto 0 );
	adc_data12				: out std_logic_vector (11 downto 0 );
	adc_data13				: out std_logic_vector (11 downto 0 );
	adc_data14				: out std_logic_vector (11 downto 0 );
	adc_data15				: out std_logic_vector (11 downto 0 );
	adc_data16				: out std_logic_vector (11 downto 0 );
	adc_data17				: out std_logic_vector (11 downto 0 );
	adc_data18				: out std_logic_vector (11 downto 0 );
	adc_data19				: out std_logic_vector (11 downto 0 );
	d0_core_clk_o : out std_logic;
	dac_data0 : in std_logic_vector(15 downto 0);
	dac_data1 : in std_logic_vector(15 downto 0);
	dac_data2 : in std_logic_vector(15 downto 0);
	dac_data3 : in std_logic_vector(15 downto 0);
	dac_data4 : in std_logic_vector(15 downto 0);
	dac_data5 : in std_logic_vector(15 downto 0);
	dac_data6 : in std_logic_vector(15 downto 0);
	dac_data7 : in std_logic_vector(15 downto 0);
	dac_data8 : in std_logic_vector(15 downto 0);
    dac_data9 : in std_logic_vector(15 downto 0);
    dac_data10 : in std_logic_vector(15 downto 0);
    dac_data11 : in std_logic_vector(15 downto 0);
    dac_data12 : in std_logic_vector(15 downto 0);
    dac_data13 : in std_logic_vector(15 downto 0);
    dac_data14 : in std_logic_vector(15 downto 0);
    dac_data15 : in std_logic_vector(15 downto 0)
);
end fmc217;

architecture Behavioral of fmc217 is
	constant ZEROES			: std_logic_vector(31 downto 0)	:= x"00000000";

	constant NUM_OF_MASTERS		: integer	:= 1;
	constant MASTER_PORT0		: integer	:= 0;

	constant NUM_OF_PORTS		: integer	:= 14;

	constant AMC_COMMON_PORT	: integer	:= 0;
	constant AXI_GPIO_PORT		: integer	:= 1;
	constant SDIO_LMK_PORT		: integer	:= 2;
	constant SDIO_LMXA_PORT		: integer	:= 3;
	constant SDIO_LMXD_PORT		: integer	:= 4;
	constant SDIO_HMC_PORT		: integer	:= 5;
	constant SDIO_ADC_PORT		: integer	:= 6;
	constant SDIO_DAC_PORT		: integer	:= 7;
	constant JESD_A0_PORT		: integer	:= 8;
	constant JESD_A1_PORT		: integer	:= 9;
	constant JESD_D0_PORT		: integer	:= 10;
	constant A0_RD_PORT		: integer	:= 11;
	constant A1_RD_PORT		: integer	:= 12;
	constant BRAM_D0_PORT		: integer	:= 13;

	constant AMC_COMMON_AWIDTH	: integer	:= 12;
	constant AXI_GPIO_AWIDTH	: integer	:= 12;
	constant SDIO_LMK_AWIDTH	: integer	:= 7;
	constant SDIO_LMXA_AWIDTH	: integer	:= 7;
	constant SDIO_LMXD_AWIDTH	: integer	:= 7;
	constant SDIO_HMC_AWIDTH	: integer	:= 7;
	constant SDIO_ADC_AWIDTH	: integer	:= 7;
	constant SDIO_DAC_AWIDTH	: integer	:= 7;
	constant JESD_A0_AWIDTH		: integer	:= 12;
	constant JESD_A1_AWIDTH		: integer	:= 12;
	constant JESD_D0_AWIDTH	: integer	:= 12;
	constant A0_RD_AWIDTH		: integer	:= 12;
	constant A1_RD_AWIDTH		: integer	:= 12;
	constant BRAM_D0_AWIDTH		: integer	:= 18;

	component vt_single_sync is
	generic (
		STAGES				: integer	:= 2;
		STARTUP_VALUE			: std_logic	:= '1'
	);
	port (
		clk				: in  std_logic;
		port_i				: in  std_logic;
		port_o				: out std_logic
	);
	end component;

	component axi_crossbar_0 is
	port (
		aclk 				: in  std_logic;
		aresetn 			: in  std_logic;
		s_axi_awaddr			: in std_logic_vector ( NUM_OF_MASTERS*32-1 downto 0 );
		s_axi_awprot			: in std_logic_vector ( NUM_OF_MASTERS*3-1 downto 0 );
		s_axi_awvalid			: in std_logic_vector ( NUM_OF_MASTERS-1 downto 0 );
		s_axi_awready			: out std_logic_vector ( NUM_OF_MASTERS-1 downto 0 );
		s_axi_wdata			: in std_logic_vector ( NUM_OF_MASTERS*32-1 downto 0 );
		s_axi_wstrb			: in std_logic_vector ( NUM_OF_MASTERS*4-1 downto 0 );
		s_axi_wvalid			: in std_logic_vector ( NUM_OF_MASTERS-1 downto 0 );
		s_axi_wready			: out std_logic_vector ( NUM_OF_MASTERS-1 downto 0 );
		s_axi_bresp			: out std_logic_vector ( NUM_OF_MASTERS*2-1 downto 0 );
		s_axi_bvalid			: out std_logic_vector ( NUM_OF_MASTERS-1 downto 0 );
		s_axi_bready			: in std_logic_vector ( NUM_OF_MASTERS-1 downto 0 );
		s_axi_araddr			: in std_logic_vector ( NUM_OF_MASTERS*32-1 downto 0 );
		s_axi_arprot			: in std_logic_vector ( NUM_OF_MASTERS*3-1 downto 0 );
		s_axi_arvalid			: in std_logic_vector ( NUM_OF_MASTERS-1 downto 0 );
		s_axi_arready			: out std_logic_vector ( NUM_OF_MASTERS-1 downto 0 );
		s_axi_rdata			: out std_logic_vector ( NUM_OF_MASTERS*32-1 downto 0 );
		s_axi_rresp			: out std_logic_vector ( NUM_OF_MASTERS*2-1 downto 0 );
		s_axi_rvalid			: out std_logic_vector ( NUM_OF_MASTERS-1 downto 0 );
		s_axi_rready			: in std_logic_vector ( NUM_OF_MASTERS-1 downto 0 );
		m_axi_awaddr			: out	std_logic_vector ( NUM_OF_PORTS*32-1 downto 0 );
		m_axi_awprot			: out	std_logic_vector ( NUM_OF_PORTS*3-1 downto 0 );
		m_axi_awvalid			: out	std_logic_vector ( NUM_OF_PORTS-1 downto 0 );
		m_axi_awready			: in	std_logic_vector ( NUM_OF_PORTS-1 downto 0 );
		m_axi_wdata			: out	std_logic_vector ( NUM_OF_PORTS*32-1 downto 0 );
		m_axi_wstrb			: out	std_logic_vector ( NUM_OF_PORTS*4-1 downto 0 );
		m_axi_wvalid			: out	std_logic_vector ( NUM_OF_PORTS-1 downto 0 );
		m_axi_wready			: in	std_logic_vector ( NUM_OF_PORTS-1 downto 0 );
		m_axi_bresp			: in	std_logic_vector ( NUM_OF_PORTS*2-1 downto 0 );
		m_axi_bvalid			: in	std_logic_vector ( NUM_OF_PORTS-1 downto 0 );
		m_axi_bready			: out	std_logic_vector ( NUM_OF_PORTS-1 downto 0 );
		m_axi_araddr			: out	std_logic_vector ( NUM_OF_PORTS*32-1 downto 0 );
		m_axi_arprot			: out	std_logic_vector ( NUM_OF_PORTS*3-1 downto 0 );
		m_axi_arvalid			: out	std_logic_vector ( NUM_OF_PORTS-1 downto 0 );
		m_axi_arready			: in	std_logic_vector ( NUM_OF_PORTS-1 downto 0 );
		m_axi_rdata			: in	std_logic_vector ( NUM_OF_PORTS*32-1 downto 0 );
		m_axi_rresp			: in	std_logic_vector ( NUM_OF_PORTS*2-1 downto 0 );
		m_axi_rvalid			: in	std_logic_vector ( NUM_OF_PORTS-1 downto 0 );
		m_axi_rready			: out	std_logic_vector ( NUM_OF_PORTS-1 downto 0 )
	);
	end component;

	component amc_common_reg is
	generic (
		MAJOR_VERSION			: integer range 0 to 255	:= 1;
		MINOR_VERSION			: integer range 0 to 255	:= 0;
		PATCH_VERSION			: integer range 0 to 255	:= 0;
		REV_VERSION			: integer range 0 to 255	:= 0;
		SIGNATURE			: std_logic_vector(15 downto 0) := x"0000";
		IMAGE_ID 			: std_logic_vector(15 downto 0) := x"0000";
		-- Do not change
		C_S_AXI_ADDR_WIDTH		: integer			:= 12
	);
	port (
		-- Ports of AXI4-Lite interface
		s_axi_aclk			: in std_logic;
		s_axi_aresetn			: in std_logic;
		s_axi_awaddr			: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		s_axi_awvalid			: in std_logic;
		s_axi_awready			: out std_logic;
		s_axi_wdata			: in std_logic_vector(31 downto 0);
		s_axi_wstrb			: in std_logic_vector(3 downto 0);
		s_axi_wvalid			: in std_logic;
		s_axi_wready			: out std_logic;
		s_axi_bresp			: out std_logic_vector (1 downto 0);
		s_axi_bvalid			: out std_logic;
		s_axi_bready			: in std_logic;
		s_axi_araddr			: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		s_axi_arvalid			: in std_logic;
		s_axi_arready			: out std_logic;
		s_axi_rdata			: out std_logic_vector (31 downto 0);
		s_axi_rresp			: out std_logic_vector (1 downto 0);
		s_axi_rvalid			: out std_logic;
		s_axi_rready			: in std_logic
	);
	end component;

	component axi_gpio_32 is
	generic (
		MAJOR_VERSION			: integer range 0 to 255	:= 1;
		MINOR_VERSION			: integer range 0 to 255	:= 0;
		PATCH_VERSION			: integer range 0 to 255	:= 0;
		REV_VERSION			: integer range 0 to 255	:= 0;
		SIGNATURE			: std_logic_vector(15 downto 0) := x"0000";
		IMAGE_ID			: std_logic_vector(15 downto 0) := x"0001";

		GPIO_INIT_0			: std_logic_vector(31 downto 0)	:= x"00000000";
		GPIO_INIT_1			: std_logic_vector(31 downto 0)	:= x"00000000";
		GPIO_INIT_2			: std_logic_vector(31 downto 0)	:= x"00000000";
		GPIO_INIT_3			: std_logic_vector(31 downto 0)	:= x"00000000";
		GPIO_INIT_4			: std_logic_vector(31 downto 0)	:= x"00000000";
		GPIO_INIT_5			: std_logic_vector(31 downto 0)	:= x"00000000";
		GPIO_INIT_6			: std_logic_vector(31 downto 0)	:= x"00000000";
		GPIO_INIT_7			: std_logic_vector(31 downto 0)	:= x"00000000";
		GPIO_INIT_8			: std_logic_vector(31 downto 0)	:= x"00000000";
		GPIO_INIT_9			: std_logic_vector(31 downto 0)	:= x"00000000";
		GPIO_INIT_10			: std_logic_vector(31 downto 0)	:= x"00000000";
		GPIO_INIT_11			: std_logic_vector(31 downto 0)	:= x"00000000";
		GPIO_INIT_12			: std_logic_vector(31 downto 0)	:= x"00000000";
		GPIO_INIT_13			: std_logic_vector(31 downto 0)	:= x"00000000";
		GPIO_INIT_14			: std_logic_vector(31 downto 0)	:= x"00000000";
		GPIO_INIT_15			: std_logic_vector(31 downto 0)	:= x"00000000";
		GPIO_INIT_16			: std_logic_vector(31 downto 0)	:= x"00000000";
		GPIO_INIT_17			: std_logic_vector(31 downto 0)	:= x"00000000";
		GPIO_INIT_18			: std_logic_vector(31 downto 0)	:= x"00000000";
		GPIO_INIT_19			: std_logic_vector(31 downto 0)	:= x"00000000";
		GPIO_INIT_20			: std_logic_vector(31 downto 0)	:= x"00000000";
		GPIO_INIT_21			: std_logic_vector(31 downto 0)	:= x"00000000";
		GPIO_INIT_22			: std_logic_vector(31 downto 0)	:= x"00000000";
		GPIO_INIT_23			: std_logic_vector(31 downto 0)	:= x"00000000";
		GPIO_INIT_24			: std_logic_vector(31 downto 0)	:= x"00000000";
		GPIO_INIT_25			: std_logic_vector(31 downto 0)	:= x"00000000";
		GPIO_INIT_26			: std_logic_vector(31 downto 0)	:= x"00000000";
		GPIO_INIT_27			: std_logic_vector(31 downto 0)	:= x"00000000";
		GPIO_INIT_28			: std_logic_vector(31 downto 0)	:= x"00000000";
		GPIO_INIT_29			: std_logic_vector(31 downto 0)	:= x"00000000";
		GPIO_INIT_30			: std_logic_vector(31 downto 0)	:= x"00000000";
		GPIO_INIT_31			: std_logic_vector(31 downto 0)	:= x"00000000";
		-- Do not change
		C_S_AXI_ADDR_WIDTH		: integer			:= 12
	);
	port (
		-- Ports of AXI4-Lite interface
		s_axi_aclk			: in	std_logic;
		s_axi_aresetn			: in	std_logic;
		s_axi_awaddr			: in	std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		s_axi_awvalid			: in	std_logic;
		s_axi_awready			: out	std_logic;
		s_axi_wdata			: in	std_logic_vector(31 downto 0);
		s_axi_wstrb			: in	std_logic_vector(3 downto 0);
		s_axi_wvalid			: in	std_logic;
		s_axi_wready			: out	std_logic;
		s_axi_bresp			: out	std_logic_vector (1 downto 0);
		s_axi_bvalid			: out	std_logic;
		s_axi_bready			: in	std_logic;
		s_axi_araddr			: in	std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		s_axi_arvalid			: in	std_logic;
		s_axi_arready			: out	std_logic;
		s_axi_rdata			: out	std_logic_vector (31 downto 0);
		s_axi_rresp			: out	std_logic_vector (1 downto 0);
		s_axi_rvalid			: out	std_logic;
		s_axi_rready			: in	std_logic;

		-- IO
		gpio_out_0			: out	std_logic_vector(31 downto 0)	:= GPIO_INIT_0;
		gpio_out_1			: out	std_logic_vector(31 downto 0)	:= GPIO_INIT_1;
		gpio_out_2			: out	std_logic_vector(31 downto 0)	:= GPIO_INIT_2;
		gpio_out_3			: out	std_logic_vector(31 downto 0)	:= GPIO_INIT_3;
		gpio_out_4			: out	std_logic_vector(31 downto 0)	:= GPIO_INIT_4;
		gpio_out_5			: out	std_logic_vector(31 downto 0)	:= GPIO_INIT_5;
		gpio_out_6			: out	std_logic_vector(31 downto 0)	:= GPIO_INIT_6;
		gpio_out_7			: out	std_logic_vector(31 downto 0)	:= GPIO_INIT_7;
		gpio_out_8			: out	std_logic_vector(31 downto 0)	:= GPIO_INIT_8;
		gpio_out_9			: out	std_logic_vector(31 downto 0)	:= GPIO_INIT_9;
		gpio_out_10			: out	std_logic_vector(31 downto 0)	:= GPIO_INIT_10;
		gpio_out_11			: out	std_logic_vector(31 downto 0)	:= GPIO_INIT_11;
		gpio_out_12			: out	std_logic_vector(31 downto 0)	:= GPIO_INIT_12;
		gpio_out_13			: out	std_logic_vector(31 downto 0)	:= GPIO_INIT_13;
		gpio_out_14			: out	std_logic_vector(31 downto 0)	:= GPIO_INIT_14;
		gpio_out_15			: out	std_logic_vector(31 downto 0)	:= GPIO_INIT_15;
		gpio_out_16			: out	std_logic_vector(31 downto 0)	:= GPIO_INIT_16;
		gpio_out_17			: out	std_logic_vector(31 downto 0)	:= GPIO_INIT_17;
		gpio_out_18			: out	std_logic_vector(31 downto 0)	:= GPIO_INIT_18;
		gpio_out_19			: out	std_logic_vector(31 downto 0)	:= GPIO_INIT_19;
		gpio_out_20			: out	std_logic_vector(31 downto 0)	:= GPIO_INIT_20;
		gpio_out_21			: out	std_logic_vector(31 downto 0)	:= GPIO_INIT_21;
		gpio_out_22			: out	std_logic_vector(31 downto 0)	:= GPIO_INIT_22;
		gpio_out_23			: out	std_logic_vector(31 downto 0)	:= GPIO_INIT_23;
		gpio_out_24			: out	std_logic_vector(31 downto 0)	:= GPIO_INIT_24;
		gpio_out_25			: out	std_logic_vector(31 downto 0)	:= GPIO_INIT_25;
		gpio_out_26			: out	std_logic_vector(31 downto 0)	:= GPIO_INIT_26;
		gpio_out_27			: out	std_logic_vector(31 downto 0)	:= GPIO_INIT_27;
		gpio_out_28			: out	std_logic_vector(31 downto 0)	:= GPIO_INIT_28;
		gpio_out_29			: out	std_logic_vector(31 downto 0)	:= GPIO_INIT_29;
		gpio_out_30			: out	std_logic_vector(31 downto 0)	:= GPIO_INIT_30;
		gpio_out_31			: out	std_logic_vector(31 downto 0)	:= GPIO_INIT_31;

		gpio_out_en			: out	std_logic_vector(31 downto 0)	:= x"00000000";

		gpio_in_0			: in	std_logic_vector(31 downto 0);
		gpio_in_1			: in	std_logic_vector(31 downto 0);
		gpio_in_2			: in	std_logic_vector(31 downto 0);
		gpio_in_3			: in	std_logic_vector(31 downto 0);
		gpio_in_4			: in	std_logic_vector(31 downto 0);
		gpio_in_5			: in	std_logic_vector(31 downto 0);
		gpio_in_6			: in	std_logic_vector(31 downto 0);
		gpio_in_7			: in	std_logic_vector(31 downto 0);
		gpio_in_8			: in	std_logic_vector(31 downto 0);
		gpio_in_9			: in	std_logic_vector(31 downto 0);
		gpio_in_10			: in	std_logic_vector(31 downto 0);
		gpio_in_11			: in	std_logic_vector(31 downto 0);
		gpio_in_12			: in	std_logic_vector(31 downto 0);
		gpio_in_13			: in	std_logic_vector(31 downto 0);
		gpio_in_14			: in	std_logic_vector(31 downto 0);
		gpio_in_15			: in	std_logic_vector(31 downto 0);
		gpio_in_16			: in	std_logic_vector(31 downto 0);
		gpio_in_17			: in	std_logic_vector(31 downto 0);
		gpio_in_18			: in	std_logic_vector(31 downto 0);
		gpio_in_19			: in	std_logic_vector(31 downto 0);
		gpio_in_20			: in	std_logic_vector(31 downto 0);
		gpio_in_21			: in	std_logic_vector(31 downto 0);
		gpio_in_22			: in	std_logic_vector(31 downto 0);
		gpio_in_23			: in	std_logic_vector(31 downto 0);
		gpio_in_24			: in	std_logic_vector(31 downto 0);
		gpio_in_25			: in	std_logic_vector(31 downto 0);
		gpio_in_26			: in	std_logic_vector(31 downto 0);
		gpio_in_27			: in	std_logic_vector(31 downto 0);
		gpio_in_28			: in	std_logic_vector(31 downto 0);
		gpio_in_29			: in	std_logic_vector(31 downto 0);
		gpio_in_30			: in	std_logic_vector(31 downto 0);
		gpio_in_31			: in	std_logic_vector(31 downto 0);

		program_n			: out	std_logic	:= '1';
		scratch				: out	std_logic_vector(31 downto 0)
	);
	end component;

	component axi_spi_sdio is
	generic (
		C_SDI_CS_IDLE			: std_logic	:= '1';
		C_SDI_NUM_OF_SLAVES		: integer	:= 1;
		C_SDI_FREQ_RATIO		: integer	:= 16;
		C_S_AXI_ADDR_WIDTH		: integer	:= 7
	);
	port (
		s_axi_aclk			: in std_logic;
		s_axi_aresetn			: in std_logic;
		s_axi_awaddr			: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		s_axi_awvalid			: in std_logic;
		s_axi_awready			: out std_logic;
		s_axi_wdata			: in std_logic_vector(31 downto 0);
		s_axi_wstrb			: in std_logic_vector(3 downto 0);
		s_axi_wvalid			: in std_logic;
		s_axi_wready			: out std_logic;
		s_axi_bresp			: out std_logic_vector(1 downto 0);
		s_axi_bvalid			: out std_logic;
		s_axi_bready			: in std_logic;
		s_axi_araddr			: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		s_axi_arvalid			: in std_logic;
		s_axi_arready			: out std_logic;
		s_axi_rdata			: out std_logic_vector(31 downto 0);
		s_axi_rresp			: out std_logic_vector(1 downto 0);
		s_axi_rvalid			: out std_logic;
		s_axi_rready			: in std_logic;
		spi_sdio_o			: out std_logic_vector(C_SDI_NUM_OF_SLAVES-1 downto 0);
		spi_sdio_i			: in std_logic_vector(C_SDI_NUM_OF_SLAVES-1 downto 0);
		spi_sclk			: out std_logic_vector(C_SDI_NUM_OF_SLAVES-1 downto 0);
		spi_sdio_t			: out std_logic_vector(C_SDI_NUM_OF_SLAVES-1 downto 0);
		spi_ncs				: out std_logic_vector(C_SDI_NUM_OF_SLAVES-1 downto 0);
		sclk_sb				: out std_logic;
		sdout_sb			: out std_logic;
		sdout_t_sb			: out std_logic;
		ip2intc_irpt			: out std_logic
	);
	end component;

--	component clk_wiz_0 is
--	port (
--		clk_in1           		: in  std_logic;
--		clk_out1          		: out std_logic;
--		clk_out2          		: out std_logic;
--		reset             		: in  std_logic;
--		locked            		: out std_logic
--	);
--	end component;

	component fifo_generator_8 is
	port (
		rst 				: in std_logic;
		wr_clk 				: in std_logic;
		rd_clk 				: in std_logic;
		din 				: in std_logic_vector (127 downto 0 );
		wr_en 				: in std_logic;
		rd_en 				: in std_logic;
		dout 				: out std_logic_vector (31 downto 0 );
		full 				: out std_logic;
		empty 				: out std_logic;
		rd_data_count			: out std_logic_vector(14 downto 0)
	);
	end component;

	component fifo_generator_2 is
	port (
		rst 				: in std_logic;
		wr_clk 				: in std_logic;
		rd_clk 				: in std_logic;
		din 				: in std_logic_vector (31 downto 0 );
		wr_en 				: in std_logic;
		rd_en 				: in std_logic;
		dout 				: out std_logic_vector (31 downto 0 );
		full 				: out std_logic;
		empty 				: out std_logic;
		rd_data_count			: out std_logic_vector(12 downto 0)
	);
	end component;

	component jesd204_support is
	generic (
		FPGA_TRANSCEIVER		: string 	:= "GTHE2";  -- generate GTHE4 
		FPGA_SERIES			: string	:= "7SERIES";    -- GT_PRBSSEL_SIZE = 3 or 4
		SPEED_GRADE			: integer	:= 2
	);
	port (
		rx_refclk			: in  std_logic;
		tx_refclk			: in  std_logic;
		rx_core_clk 			: in  std_logic;
		tx_core_clk 			: in  std_logic;
		common0_pll_clk_out 		: out std_logic;
		common0_pll_refclk_out		: out std_logic;
		common0_pll_lock_out 		: out std_logic;
		common1_pll_clk_out 		: out std_logic;
		common1_pll_refclk_out		: out std_logic;
		common1_pll_lock_out 		: out std_logic;
		rx_reset 			: in  std_logic;
		rx_sysref 			: in  std_logic;
		rxp 				: in  std_logic_vector ( 7 downto 0 );
		rxn 				: in  std_logic_vector ( 7 downto 0 );
		rx0_sync 			: out std_logic;
		rx0_aresetn 			: out std_logic;
		rx0_start_of_frame 		: out std_logic_vector ( 3 downto 0 );
		rx0_end_of_frame 		: out std_logic_vector ( 3 downto 0 );
		rx0_start_of_multiframe 	: out std_logic_vector ( 3 downto 0 );
		rx0_end_of_multiframe 		: out std_logic_vector ( 3 downto 0 );
		rx0_frame_error 		: out std_logic_vector ( 15 downto 0 );
		rx0_tvalid 			: out std_logic;
		rx0_tdata 			: out std_logic_vector ( 127 downto 0 );
		rx1_sync 			: out std_logic;
		rx1_aresetn 			: out std_logic;
		rx1_start_of_frame 		: out std_logic_vector ( 3 downto 0 );
		rx1_end_of_frame 		: out std_logic_vector ( 3 downto 0 );
		rx1_start_of_multiframe 	: out std_logic_vector ( 3 downto 0 );
		rx1_end_of_multiframe 		: out std_logic_vector ( 3 downto 0 );
		rx1_frame_error 		: out std_logic_vector ( 15 downto 0 );
		rx1_tvalid 			: out std_logic;
		rx1_tdata 			: out std_logic_vector ( 127 downto 0 );
		tx_reset 			: in  std_logic;
		tx_sysref 			: in  std_logic;
		tx_sync 			: in  std_logic;
		txp 				: out std_logic_vector ( 7 downto 0 );
		txn 				: out std_logic_vector ( 7 downto 0 );
		tx_aresetn 			: out std_logic;
		tx_start_of_frame 		: out std_logic_vector ( 3 downto 0 );
		tx_start_of_multiframe 		: out std_logic_vector ( 3 downto 0 );
		tx_tready 			: out std_logic;
		tx_tdata 			: in  std_logic_vector ( 255 downto 0 );
		drpclk 				: in  std_logic;
		txpostcursor			: in  std_logic_vector (4 downto 0);
		txprecursor			: in  std_logic_vector (4 downto 0);
		txdiffctrl			: in  std_logic_vector (3 downto 0);
		rxlpmen				: in  std_logic;
		rx0_s_axi_aclk 			: in  std_logic;
		rx0_s_axi_aresetn 		: in  std_logic;
		rx0_s_axi_awaddr 		: in  std_logic_vector ( 11 downto 0 );
		rx0_s_axi_awvalid 		: in  std_logic;
		rx0_s_axi_awready 		: out std_logic;
		rx0_s_axi_wdata 			: in  std_logic_vector ( 31 downto 0 );
		rx0_s_axi_wstrb 			: in  std_logic_vector ( 3 downto 0 );
		rx0_s_axi_wvalid 		: in  std_logic;
		rx0_s_axi_wready 		: out std_logic;
		rx0_s_axi_bresp 			: out std_logic_vector ( 1 downto 0 );
		rx0_s_axi_bvalid 		: out std_logic;
		rx0_s_axi_bready 		: in  std_logic;
		rx0_s_axi_araddr 		: in  std_logic_vector ( 11 downto 0 );
		rx0_s_axi_arvalid 		: in  std_logic;
		rx0_s_axi_arready 		: out std_logic;
		rx0_s_axi_rdata 			: out std_logic_vector ( 31 downto 0 );
		rx0_s_axi_rresp 			: out std_logic_vector ( 1 downto 0 );
		rx0_s_axi_rvalid 		: out std_logic;
		rx0_s_axi_rready 		: in  std_logic;
		rx1_s_axi_aclk 			: in  std_logic;
		rx1_s_axi_aresetn 		: in  std_logic;
		rx1_s_axi_awaddr 		: in  std_logic_vector ( 11 downto 0 );
		rx1_s_axi_awvalid 		: in  std_logic;
		rx1_s_axi_awready 		: out std_logic;
		rx1_s_axi_wdata 			: in  std_logic_vector ( 31 downto 0 );
		rx1_s_axi_wstrb 			: in  std_logic_vector ( 3 downto 0 );
		rx1_s_axi_wvalid 		: in  std_logic;
		rx1_s_axi_wready 		: out std_logic;
		rx1_s_axi_bresp 			: out std_logic_vector ( 1 downto 0 );
		rx1_s_axi_bvalid 		: out std_logic;
		rx1_s_axi_bready 		: in  std_logic;
		rx1_s_axi_araddr 		: in  std_logic_vector ( 11 downto 0 );
		rx1_s_axi_arvalid 		: in  std_logic;
		rx1_s_axi_arready 		: out std_logic;
		rx1_s_axi_rdata 			: out std_logic_vector ( 31 downto 0 );
		rx1_s_axi_rresp 			: out std_logic_vector ( 1 downto 0 );
		rx1_s_axi_rvalid 		: out std_logic;
		rx1_s_axi_rready 		: in  std_logic;
		tx_s_axi_aclk 			: in  std_logic;
		tx_s_axi_aresetn 		: in  std_logic;
		tx_s_axi_awaddr 		: in  std_logic_vector ( 11 downto 0 );
		tx_s_axi_awvalid 		: in  std_logic;
		tx_s_axi_awready 		: out std_logic;
		tx_s_axi_wdata 			: in  std_logic_vector ( 31 downto 0 );
		tx_s_axi_wstrb 			: in  std_logic_vector ( 3 downto 0 );
		tx_s_axi_wvalid 		: in  std_logic;
		tx_s_axi_wready 		: out std_logic;
		tx_s_axi_bresp 			: out std_logic_vector ( 1 downto 0 );
		tx_s_axi_bvalid 		: out std_logic;
		tx_s_axi_bready 		: in  std_logic;
		tx_s_axi_araddr 		: in  std_logic_vector ( 11 downto 0 );
		tx_s_axi_arvalid 		: in  std_logic;
		tx_s_axi_arready 		: out std_logic;
		tx_s_axi_rdata 			: out std_logic_vector ( 31 downto 0 );
		tx_s_axi_rresp 			: out std_logic_vector ( 1 downto 0 );
		tx_s_axi_rvalid 		: out std_logic;
		tx_s_axi_rready 		: in  std_logic
	);
	end component;

	component axi_lite_rd_fifo is
	generic (
		FIFO_DWIDTH			: integer	:= 32;	-- support 32 only now
		C_S_AXI_ADDR_WIDTH		: integer	:= 12;
		LITTLE_ENDIAN16			: boolean	:= true
	);
	port (
		-- Ports of AXI-Lite interface
		s_axi_aclk			: in std_logic;
		s_axi_aresetn			: in std_logic;
		s_axi_awaddr			: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		s_axi_awvalid			: in std_logic;
		s_axi_awready			: out std_logic;
		s_axi_wdata			: in std_logic_vector(31 downto 0);
		s_axi_wstrb			: in std_logic_vector(3 downto 0);
		s_axi_wvalid			: in std_logic;
		s_axi_wready			: out std_logic;
		s_axi_bresp			: out std_logic_vector (1 downto 0);
		s_axi_bvalid			: out std_logic;
		s_axi_bready			: in std_logic;
		s_axi_araddr			: in std_logic_vector(C_S_AXI_ADDR_WIDTH-1 downto 0);
		s_axi_arvalid			: in std_logic;
		s_axi_arready			: out std_logic;
		s_axi_rdata			: out std_logic_vector (31 downto 0);
		s_axi_rresp			: out std_logic_vector (1 downto 0);
		s_axi_rvalid			: out std_logic;
		s_axi_rready			: in std_logic;
		fifo_rd_en			: out std_logic;
		fifo_dout			: in std_logic_vector(31 downto 0)
	);
	end component;

	component axi_register_slice_0 is
	port (
		aclk				: in std_logic;
		aresetn				: in std_logic;
		s_axi_awaddr			: in std_logic_vector(31 downto 0);
		s_axi_awprot 			: in std_logic_vector ( 2 downto 0 );
		s_axi_awvalid			: in std_logic;
		s_axi_awready			: out std_logic;
		s_axi_wdata			: in std_logic_vector(31 downto 0);
		s_axi_wstrb			: in std_logic_vector(3 downto 0);
		s_axi_wvalid			: in std_logic;
		s_axi_wready			: out std_logic;
		s_axi_bresp			: out std_logic_vector (1 downto 0);
		s_axi_bvalid			: out std_logic;
		s_axi_bready			: in std_logic;
		s_axi_araddr			: in std_logic_vector(31 downto 0);
		s_axi_arprot 			: in std_logic_vector ( 2 downto 0 );
		s_axi_arvalid			: in std_logic;
		s_axi_arready			: out std_logic;
		s_axi_rdata			: out std_logic_vector (31 downto 0);
		s_axi_rresp			: out std_logic_vector (1 downto 0);
		s_axi_rvalid			: out std_logic;
		s_axi_rready			: in std_logic;
		m_axi_awaddr			: out std_logic_vector ( 31 downto 0 );
		m_axi_awprot			: out std_logic_vector ( 2 downto 0 );
		m_axi_awvalid			: out std_logic;
		m_axi_awready			: in  std_logic;
		m_axi_wdata			: out std_logic_vector ( 31 downto 0 );
		m_axi_wstrb			: out std_logic_vector ( 3 downto 0 );
		m_axi_wvalid			: out std_logic;
		m_axi_wready			: in  std_logic;
		m_axi_bresp			: in  std_logic_vector ( 1 downto 0 );
		m_axi_bvalid			: in  std_logic;
		m_axi_bready			: out std_logic;
		m_axi_araddr			: out std_logic_vector ( 31 downto 0 );
		m_axi_arprot			: out std_logic_vector ( 2 downto 0 );
		m_axi_arvalid			: out std_logic;
		m_axi_arready			: in  std_logic;
		m_axi_rdata			: in  std_logic_vector ( 31 downto 0 );
		m_axi_rresp			: in  std_logic_vector ( 1 downto 0 );
		m_axi_rvalid			: in  std_logic;
		m_axi_rready			: out std_logic
	);
	end component;


	component blk_mem_gen_0 is
	port (
		clka  				: in std_logic;
		ena   				: in std_logic;
		wea   				: in std_logic_vector ( 0 to 0 );
		addra 				: in std_logic_vector ( 15 downto 0 );
		dina  				: in std_logic_vector ( 31 downto 0 );
		douta  				: out std_logic_vector ( 31 downto 0 );
		clkb  				: in std_logic;
		enb   				: in std_logic;
		web   				: in std_logic_vector ( 0 to 0 );
		addrb 				: in std_logic_vector ( 12 downto 0 );
		dinb 				: in std_logic_vector ( 255 downto 0 );
		doutb 				: out std_logic_vector ( 255 downto 0 )
	);
	end component;

	component axi_bram_ctrl_0 is
	port (
		s_axi_aclk			: in std_logic;
		s_axi_aresetn			: in std_logic;
		s_axi_awaddr			: in std_logic_vector(17 downto 0);
		s_axi_awprot 			: in std_logic_vector ( 2 downto 0 );
		s_axi_awvalid			: in std_logic;
		s_axi_awready			: out std_logic;
		s_axi_wdata			: in std_logic_vector(31 downto 0);
		s_axi_wstrb			: in std_logic_vector(3 downto 0);
		s_axi_wvalid			: in std_logic;
		s_axi_wready			: out std_logic;
		s_axi_bresp			: out std_logic_vector (1 downto 0);
		s_axi_bvalid			: out std_logic;
		s_axi_bready			: in std_logic;
		s_axi_araddr			: in std_logic_vector(17 downto 0);
		s_axi_arprot 			: in std_logic_vector ( 2 downto 0 );
		s_axi_arvalid			: in std_logic;
		s_axi_arready			: out std_logic;
		s_axi_rdata			: out std_logic_vector (31 downto 0);
		s_axi_rresp			: out std_logic_vector (1 downto 0);
		s_axi_rvalid			: out std_logic;
		s_axi_rready			: in std_logic;
		bram_rst_a  			: out std_logic;
		bram_clk_a  			: out std_logic;
		bram_en_a   			: out std_logic;
		bram_we_a   			: out std_logic_vector ( 3 downto 0 );
		bram_addr_a 			: out std_logic_vector ( 17 downto 0 );
		bram_wrdata_a 			: out std_logic_vector ( 31 downto 0 );
		bram_rddata_a 			: in std_logic_vector ( 31 downto 0 )
	);
	end component;

	component adc12xj_align_jmode_2 is
	port (
		rx_aclk				: in 	std_logic;
		rx_aresetn			: in 	std_logic;
		jesd204b_tdata			: in	std_logic_vector(127 downto 0);
		jesd204b_start_of_frame		: in	std_logic_vector(3 downto 0);
		adc_dout			: out	std_logic_vector(119 downto 0)
	);
	end component;


	
	-- ===== signals common ==================
	signal s_axi_areset			: std_logic;

	-- axi4lite connections
	signal cb_m_axi_awaddr 			: std_logic_vector (NUM_OF_PORTS*32-1 downto 0 );
	signal cb_m_axi_awprot 			: std_logic_vector (NUM_OF_PORTS*3-1 downto 0 );
	signal cb_m_axi_awvalid 		: std_logic_vector (NUM_OF_PORTS-1 downto 0 );
	signal cb_m_axi_awready 		: std_logic_vector (NUM_OF_PORTS-1 downto 0 );
	signal cb_m_axi_wdata 			: std_logic_vector (NUM_OF_PORTS*32-1 downto 0 );
	signal cb_m_axi_wstrb 			: std_logic_vector (NUM_OF_PORTS*4-1 downto 0 );
	signal cb_m_axi_wvalid 			: std_logic_vector (NUM_OF_PORTS-1 downto 0 );
	signal cb_m_axi_wready 			: std_logic_vector (NUM_OF_PORTS-1 downto 0 );
	signal cb_m_axi_bresp 			: std_logic_vector (NUM_OF_PORTS*2-1 downto 0 );
	signal cb_m_axi_bvalid 			: std_logic_vector (NUM_OF_PORTS-1 downto 0 );
	signal cb_m_axi_bready 			: std_logic_vector (NUM_OF_PORTS-1 downto 0 );
	signal cb_m_axi_araddr 			: std_logic_vector (NUM_OF_PORTS*32-1 downto 0 );
	signal cb_m_axi_arprot 			: std_logic_vector (NUM_OF_PORTS*3-1 downto 0 );
	signal cb_m_axi_arvalid 		: std_logic_vector (NUM_OF_PORTS-1 downto 0 );
	signal cb_m_axi_arready 		: std_logic_vector (NUM_OF_PORTS-1 downto 0 );
	signal cb_m_axi_rdata 			: std_logic_vector (NUM_OF_PORTS*32-1 downto 0 );
	signal cb_m_axi_rresp 			: std_logic_vector (NUM_OF_PORTS*2-1 downto 0 );
	signal cb_m_axi_rvalid 			: std_logic_vector (NUM_OF_PORTS-1 downto 0 );
	signal cb_m_axi_rready 			: std_logic_vector (NUM_OF_PORTS-1 downto 0 );

	-- signals for GPIO
	signal gpio_out_0			: std_logic_vector(31 downto 0);
	signal gpio_out_1			: std_logic_vector(31 downto 0);
	signal gpio_out_2			: std_logic_vector(31 downto 0);
	signal gpio_out_3			: std_logic_vector(31 downto 0);
	signal gpio_out_4			: std_logic_vector(31 downto 0);
	signal gpio_out_5			: std_logic_vector(31 downto 0);
	signal gpio_out_6			: std_logic_vector(31 downto 0);
	signal gpio_out_7			: std_logic_vector(31 downto 0);
	signal gpio_out_8			: std_logic_vector(31 downto 0);
	signal gpio_out_9			: std_logic_vector(31 downto 0);
	signal gpio_out_10			: std_logic_vector(31 downto 0);
	signal gpio_out_11			: std_logic_vector(31 downto 0);
	signal gpio_out_12			: std_logic_vector(31 downto 0);
	signal gpio_out_13			: std_logic_vector(31 downto 0);
	signal gpio_out_14			: std_logic_vector(31 downto 0);
	signal gpio_out_15			: std_logic_vector(31 downto 0);
	signal gpio_out_16			: std_logic_vector(31 downto 0);
	signal gpio_out_17			: std_logic_vector(31 downto 0);
	signal gpio_out_18			: std_logic_vector(31 downto 0);
	signal gpio_out_19			: std_logic_vector(31 downto 0);
	signal gpio_out_20			: std_logic_vector(31 downto 0);
	signal gpio_out_21			: std_logic_vector(31 downto 0);
	signal gpio_out_22			: std_logic_vector(31 downto 0);
	signal gpio_out_23			: std_logic_vector(31 downto 0);
	signal gpio_out_24			: std_logic_vector(31 downto 0);
	signal gpio_out_25			: std_logic_vector(31 downto 0);
	signal gpio_out_26			: std_logic_vector(31 downto 0);
	signal gpio_out_27			: std_logic_vector(31 downto 0);
	signal gpio_out_28			: std_logic_vector(31 downto 0);
	signal gpio_out_29			: std_logic_vector(31 downto 0);
	signal gpio_out_30			: std_logic_vector(31 downto 0);
	signal gpio_out_31			: std_logic_vector(31 downto 0);
	signal gpio_out_en			: std_logic_vector(31 downto 0);
	signal gpio_in_0			: std_logic_vector(31 downto 0)    := (others => '0');
	signal gpio_in_1			: std_logic_vector(31 downto 0)    := (others => '0');
	signal gpio_in_2			: std_logic_vector(31 downto 0)    := (others => '0');
	signal gpio_in_3			: std_logic_vector(31 downto 0)    := (others => '0');
	signal gpio_in_4			: std_logic_vector(31 downto 0)    := (others => '0');
	signal gpio_in_5			: std_logic_vector(31 downto 0)    := (others => '0');
	signal gpio_in_6			: std_logic_vector(31 downto 0)    := (others => '0');
	signal gpio_in_7			: std_logic_vector(31 downto 0)    := (others => '0');
	signal gpio_in_8			: std_logic_vector(31 downto 0)    := (others => '0');
	signal gpio_in_9			: std_logic_vector(31 downto 0)    := (others => '0');
	signal gpio_in_10			: std_logic_vector(31 downto 0)    := (others => '0');
	signal gpio_in_11			: std_logic_vector(31 downto 0)    := (others => '0');
	signal gpio_in_12			: std_logic_vector(31 downto 0)    := (others => '0');
	signal gpio_in_13			: std_logic_vector(31 downto 0)    := (others => '0');
	signal gpio_in_14			: std_logic_vector(31 downto 0)    := (others => '0');
	signal gpio_in_15			: std_logic_vector(31 downto 0)    := (others => '0');
	signal gpio_in_16			: std_logic_vector(31 downto 0)    := (others => '0');
	signal gpio_in_17			: std_logic_vector(31 downto 0)    := (others => '0');
	signal gpio_in_18			: std_logic_vector(31 downto 0)    := (others => '0');
	signal gpio_in_19			: std_logic_vector(31 downto 0)    := (others => '0');
	signal gpio_in_20			: std_logic_vector(31 downto 0)    := (others => '0');
	signal gpio_in_21			: std_logic_vector(31 downto 0)    := (others => '0');
	signal gpio_in_22			: std_logic_vector(31 downto 0)    := (others => '0');
	signal gpio_in_23			: std_logic_vector(31 downto 0)    := (others => '0');
	signal gpio_in_24			: std_logic_vector(31 downto 0)    := (others => '0');
	signal gpio_in_25			: std_logic_vector(31 downto 0)    := (others => '0');
	signal gpio_in_26			: std_logic_vector(31 downto 0)    := (others => '0');
	signal gpio_in_27			: std_logic_vector(31 downto 0)    := (others => '0');
	signal gpio_in_28			: std_logic_vector(31 downto 0)    := (others => '0');
	signal gpio_in_29			: std_logic_vector(31 downto 0)    := (others => '0');
	signal gpio_in_30			: std_logic_vector(31 downto 0)    := (others => '0');
	signal gpio_in_31			: std_logic_vector(31 downto 0)    := (others => '0');
	signal scratch				: std_logic_vector(31 downto 0)    := (others => '0');

	-- ===== signals fmc specific ==========
	-- sdio signals
	signal lmk_spi_sdio_i 			: std_logic;
	signal lmk_spi_sdio_o 			: std_logic;
	signal lmk_spi_sdio_t 			: std_logic;

	signal hmc_spi_sdio_o               	: std_logic;
	signal hmc_spi_sdio_i               	: std_logic;
	signal hmc_spi_sdio_t 			: std_logic;

	signal adc_sdio_i 			: std_logic;
	signal adc_sdio_o 			: std_logic;
	signal adc_sdio_t 			: std_logic;

	-- gpio output signals
	signal adc_acq_ctrl			: std_logic_vector(3 downto 0);
	signal dac_ptn				: std_logic_vector(3 downto 0);

	signal a0_usr_rst			: std_logic	:= '1';
	signal a0_clk_dis			: std_logic	:= '1';
	signal a0_sysref_dis			: std_logic	:= '1';
	signal a0_refclk_dis			: std_logic	:= '1';
	signal a0_refclk_pre			: std_logic	:= '1';

	signal d0_usr_rst			: std_logic	:= '1';
	signal d0_clk_dis			: std_logic	:= '1';
	signal d0_sysref_dis			: std_logic	:= '1';
	signal d0_refclk_dis			: std_logic	:= '1';
	signal d0_refclk_pre			: std_logic	:= '1';

	signal dac_mem_rd_len			: std_logic_vector(13 downto 0);
	signal adc_ch_sel			: std_logic;
	signal adc_ch_sel_sync			: std_logic;

	-- JESD204 signals
	signal a0_refclk			: std_logic;
	signal d0_refclk			: std_logic;
	signal a0_core_clk			: std_logic;
	signal d0_core_clk			: std_logic;
	signal a0_sysref			: std_logic;
	signal d0_sysref			: std_logic;
	signal d0_sync				: std_logic;

	signal rxp 				: std_logic_vector ( 7 downto 0 );
	signal rxn 				: std_logic_vector ( 7 downto 0 );
	signal txp 				: std_logic_vector ( 7 downto 0 );
	signal txn 				: std_logic_vector ( 7 downto 0 );

	signal a0_sync				: std_logic;
	signal a0_aresetn 			: std_logic;
	signal a0_start_of_frame 		: std_logic_vector ( 3 downto 0 );
	signal a0_end_of_frame 			: std_logic_vector ( 3 downto 0 );
	signal a0_start_of_multiframe 		: std_logic_vector ( 3 downto 0 );
	signal a0_end_of_multiframe 		: std_logic_vector ( 3 downto 0 );
	signal a0_frame_error 			: std_logic_vector ( 15 downto 0 );
	signal a0_tvalid 			: std_logic;
	signal a0_tdata 			: std_logic_vector ( 127 downto 0 );
	signal a0_aresetn_d1 			: std_logic;
	signal a0_start_of_frame_d1 		: std_logic_vector ( 3 downto 0 );
	signal a0_tvalid_d1			: std_logic;
	signal a0_tdata_d1 			: std_logic_vector ( 127 downto 0 );

	signal a0_aresetn_d2 			: std_logic;
	signal a0_start_of_frame_d2 		: std_logic_vector ( 3 downto 0 );
	signal a0_tvalid_d2			: std_logic;
	signal a0_tdata_d2 			: std_logic_vector ( 127 downto 0 );

	signal a1_sync				: std_logic;
	signal a1_aresetn 			: std_logic;
	signal a1_start_of_frame 		: std_logic_vector ( 3 downto 0 );
	signal a1_end_of_frame 			: std_logic_vector ( 3 downto 0 );
	signal a1_start_of_multiframe 		: std_logic_vector ( 3 downto 0 );
	signal a1_end_of_multiframe 		: std_logic_vector ( 3 downto 0 );
	signal a1_frame_error 			: std_logic_vector ( 15 downto 0 );
	signal a1_tvalid 			: std_logic;
	signal a1_tdata 			: std_logic_vector ( 127 downto 0 );
	signal a1_aresetn_d1 			: std_logic;
	signal a1_start_of_frame_d1 		: std_logic_vector ( 3 downto 0 );
	signal a1_tvalid_d1			: std_logic;
	signal a1_tdata_d1 			: std_logic_vector ( 127 downto 0 );

	signal a1_aresetn_d2 			: std_logic;
	signal a1_start_of_frame_d2 		: std_logic_vector ( 3 downto 0 );
	signal a1_tvalid_d2			: std_logic;
	signal a1_tdata_d2 			: std_logic_vector ( 127 downto 0 );

	signal d0_aresetn 			: std_logic;
	signal d0_start_of_frame 		: std_logic_vector ( 3 downto 0 );
	signal d0_start_of_multiframe 		: std_logic_vector ( 3 downto 0 );
	signal d0_tready 			: std_logic;
	signal d0_tdata 			: std_logic_vector ( 255 downto 0 );

	-- ADC samples alignment signals
	signal a0_dout				: std_logic_vector(119 downto 0);
	signal a1_dout				: std_logic_vector(119 downto 0);

	-- ADC read fifo signals
	signal rst_fifo				: std_logic;
	signal adc_wr_en			: std_logic;
	signal fifo_wr_en			: std_logic;
	signal a0_dout_d1			: std_logic_vector(119 downto 0);
	signal a1_dout_d1			: std_logic_vector(119 downto 0);
	signal a0_dout_d2			: std_logic_vector(119 downto 0);
	signal a1_dout_d2			: std_logic_vector(119 downto 0);
	signal a0_dout_d3			: std_logic_vector(119 downto 0);
	signal a1_dout_d3			: std_logic_vector(119 downto 0);

	signal a0_rd_f0_din_ar 			: std_logic_vector (127 downto 0 );
	signal a1_rd_f0_din_ar 			: std_logic_vector (127 downto 0 );
	signal a0_rd_f1_din_ar 			: std_logic_vector (31 downto 0 );
	signal a1_rd_f1_din_ar 			: std_logic_vector (31 downto 0 );

	signal a0_rd_f0_din_ar_d1		: std_logic_vector (127 downto 0 );
	signal a1_rd_f0_din_ar_d1		: std_logic_vector (127 downto 0 );
	signal a0_rd_f1_din_ar_d1		: std_logic_vector (31 downto 0 );
	signal a1_rd_f1_din_ar_d1		: std_logic_vector (31 downto 0 );

	signal a0_rd_f0_rd_en			: std_logic;
	signal a0_rd_f0_empty			: std_logic;
	signal a0_rd_f0_full			: std_logic;
	signal a0_rd_f0_dout			: std_logic_vector(31 downto 0);
	signal a0_rd_f1_rd_en			: std_logic;
	signal a0_rd_f1_empty			: std_logic;
	signal a0_rd_f1_full			: std_logic;
	signal a0_rd_f1_dout			: std_logic_vector(31 downto 0);

	signal a1_rd_f0_rd_en			: std_logic;
	signal a1_rd_f0_empty			: std_logic;
	signal a1_rd_f0_full			: std_logic;
	signal a1_rd_f0_dout			: std_logic_vector(31 downto 0);
	signal a1_rd_f1_rd_en			: std_logic;
	signal a1_rd_f1_empty			: std_logic;
	signal a1_rd_f1_full			: std_logic;
	signal a1_rd_f1_dout			: std_logic_vector(31 downto 0);

	signal adc_rd_f0_rd_en			: std_logic;
	signal adc_rd_f1_rd_en			: std_logic;
	signal adc_rd_f0_dout			: std_logic_vector(31 downto 0);
	signal adc_rd_f1_dout			: std_logic_vector(31 downto 0);

	-- DAC data and pattern signals
	signal bram_d0_axi_awaddr		: std_logic_vector ( 31 downto 0 );
	signal bram_d0_axi_awprot		: std_logic_vector ( 2 downto 0 );
	signal bram_d0_axi_awvalid		: std_logic;
	signal bram_d0_axi_awready		: std_logic;
	signal bram_d0_axi_wdata		: std_logic_vector ( 31 downto 0 );
	signal bram_d0_axi_wstrb		: std_logic_vector ( 3 downto 0 );
	signal bram_d0_axi_wvalid		: std_logic;
	signal bram_d0_axi_wready		: std_logic;
	signal bram_d0_axi_bresp		: std_logic_vector ( 1 downto 0 );
	signal bram_d0_axi_bvalid		: std_logic;
	signal bram_d0_axi_bready		: std_logic;
	signal bram_d0_axi_araddr		: std_logic_vector ( 31 downto 0 );
	signal bram_d0_axi_arprot		: std_logic_vector ( 2 downto 0 );
	signal bram_d0_axi_arvalid		: std_logic;
	signal bram_d0_axi_arready		: std_logic;
	signal bram_d0_axi_rdata		: std_logic_vector ( 31 downto 0 );
	signal bram_d0_axi_rresp		: std_logic_vector ( 1 downto 0 );
	signal bram_d0_axi_rvalid		: std_logic;
	signal bram_d0_axi_rready		: std_logic;

	signal bram_d0_en_i   			: std_logic;
	signal bram_d0_we_i   			: std_logic_vector ( 3 downto 0 );
	signal bram_d0_addr_i 			: std_logic_vector ( 17 downto 0 );
	signal bram_d0_din_i 			: std_logic_vector ( 31 downto 0 );
	signal bram_d0_dout_i 			: std_logic_vector ( 31 downto 0 );

	signal bram_d0_we_i_d1   		: std_logic;
	signal bram_d0_din_i_w 			: std_logic_vector ( 31 downto 0 );
	signal bram_d0_addr_b_d1		: std_logic_vector ( 12 downto 0 );
	signal bram_d0_dout_b 			: std_logic_vector ( 255 downto 0 );

	signal dac_mem_rd_len_d1		: std_logic_vector(13 downto 0);
	signal dac_mem_rd_len_d2		: std_logic_vector(13 downto 0);
	signal dac_mem_rd_len_1_d1		: std_logic_vector(13 downto 0);
	signal dac_ptn_sync			: std_logic_vector(3 downto 0);
	signal dac_ptn_d1			: std_logic_vector(3 downto 0);
	signal dac_ptn_d2			: std_logic_vector(3 downto 0);
	signal bram_d0_addr_b 			: std_logic_vector ( 12 downto 0 );

	signal sine_out_c16			: std_logic_vector(16*16-1 downto 0);
	signal d0_data_bram			: std_logic_vector(16*16-1 downto 0);
	signal d0_tdata_p 			: std_logic_vector ( 255 downto 0 );
	signal d0_tready_d1			: std_logic;

	-- trigger
	signal trigger_in			: std_logic;
	signal trigger_out			: std_logic;
	signal trig_out_sel			: std_logic_vector(3 downto 0);

	-- ADC to DAC loop back fifo signals
	signal a0_dout_db1			: std_logic_vector(20*12-1 downto 0);
	signal a1_dout_db1			: std_logic_vector(20*12-1 downto 0);
	signal lf_rst_pre			: std_logic;
	signal lf_rst				: std_logic;
	signal lf_wr_en				: std_logic				:= '0';
	signal lf_din				: std_logic_vector(20*12-1 downto 0)	:= (others => '0');
	signal lf_rd_en				: std_logic				:= '0';
	signal lf_full				: std_logic				:= '0';
	signal lf_empty				: std_logic				:= '0';
	signal lf_data_count			: std_logic_vector(8 downto 0)		:= (others => '0');
	signal lf_dout				: std_logic_vector(20*12-1 downto 0)	:= (others => '0');
	signal lf_dout_d1			: std_logic_vector(20*12-1 downto 0);
	signal lf_dout_d2			: std_logic_vector(20*12-1 downto 0);
	signal lf_rd_cnt			: integer range 0 to 4;

	signal lb_en				: std_logic;
	signal adc_to_dac_12bit			: std_logic_vector(16*12-1 downto 0);
	signal adc_to_dac_16bit			: std_logic_vector(16*16-1 downto 0);

	-- others
	signal hmc7043_rfsyncin			: std_logic;
	signal adc_tmstp			: std_logic;

	signal txpostcursor			: std_logic_vector (4 downto 0);
	signal txprecursor			: std_logic_vector (4 downto 0);
	signal txdiffctrl			: std_logic_vector (3 downto 0);
	signal rxlpmen				: std_logic;

	signal a0_core_clk_div2			: std_logic;
	signal d0_core_clk_div2			: std_logic;

	-- ADC to DAC loop for speed grade 2 GTX2 
	signal adc_dout_dac			: std_logic_vector(279 downto 0);
	signal fir_din_12			: std_logic_vector(119 downto 0);
	signal fir_din				: std_logic_vector(159 downto 0);
	signal fir_dout				: std_logic_vector(1279 downto 0);
	signal s_axis_data_tready		: std_logic;
	

    -- MODIF 2020/01/16-GKO
    signal axi_crossbar_s_axi_awaddr    : std_logic_vector(31 downto 0); 
    signal axi_crossbar_s_axi_araddr    : std_logic_vector(31 downto 0);

begin
	-- ===== common code =============

	s_axi_areset			<= not s_axi_aresetn;

    -- MODIF 2020/01/16-GKO
    axi_crossbar_s_axi_awaddr <= ZEROES(31 downto BAR_SIZE) & s_axi_awaddr(BAR_SIZE - 1 downto 0);
    axi_crossbar_s_axi_araddr <= ZEROES(31 downto BAR_SIZE) & s_axi_araddr(BAR_SIZE - 1 downto 0);
    

	axi_crossbar_0_inst: axi_crossbar_0
	port map(
		aclk				=> s_axi_aclk,
		aresetn				=> s_axi_aresetn,
		s_axi_awaddr			=> axi_crossbar_s_axi_awaddr,     -- MODIF 2020/01/16-GKO
		s_axi_awprot			=> "000",
		s_axi_awvalid(0)		=> s_axi_awvalid,
		s_axi_awready(0)		=> s_axi_awready,
		s_axi_wdata			=> s_axi_wdata,
		s_axi_wstrb			=> s_axi_wstrb,
		s_axi_wvalid(0)			=> s_axi_wvalid,
		s_axi_wready(0)			=> s_axi_wready,
		s_axi_bresp			=> s_axi_bresp,
		s_axi_bvalid(0)			=> s_axi_bvalid,
		s_axi_bready(0)			=> s_axi_bready,
		s_axi_araddr			=> axi_crossbar_s_axi_araddr,    -- MODIF 2020/01/16-GKO
		s_axi_arprot			=> "000",
		s_axi_arvalid(0)		=> s_axi_arvalid,
		s_axi_arready(0)		=> s_axi_arready,

		s_axi_rdata			=> s_axi_rdata,
		s_axi_rresp			=> s_axi_rresp,
		s_axi_rvalid(0)			=> s_axi_rvalid,
		s_axi_rready(0)			=> s_axi_rready,
		m_axi_awaddr			=> cb_m_axi_awaddr,
		m_axi_awprot			=> cb_m_axi_awprot,
		m_axi_awvalid			=> cb_m_axi_awvalid,
		m_axi_awready			=> cb_m_axi_awready,
		m_axi_wdata			=> cb_m_axi_wdata,
		m_axi_wstrb			=> cb_m_axi_wstrb,
		m_axi_wvalid			=> cb_m_axi_wvalid,
		m_axi_wready			=> cb_m_axi_wready,
		m_axi_bresp			=> cb_m_axi_bresp,
		m_axi_bvalid			=> cb_m_axi_bvalid,
		m_axi_bready			=> cb_m_axi_bready,
		m_axi_araddr			=> cb_m_axi_araddr,
		m_axi_arprot			=> cb_m_axi_arprot,
		m_axi_arvalid			=> cb_m_axi_arvalid,
		m_axi_arready			=> cb_m_axi_arready,
		m_axi_rdata			=> cb_m_axi_rdata,
		m_axi_rresp			=> cb_m_axi_rresp,
		m_axi_rvalid			=> cb_m_axi_rvalid,
		m_axi_rready			=> cb_m_axi_rready
	);

	amc_common_reg_inst: amc_common_reg
	generic map(
		MAJOR_VERSION			=> MAJOR_VERSION,
		MINOR_VERSION			=> MINOR_VERSION,
		PATCH_VERSION			=> PATCH_VERSION,
		REV_VERSION			=> REV_VERSION,
		SIGNATURE			=> SIGNATURE,
		IMAGE_ID 			=> IMAGE_ID
	)
	port map(
		s_axi_aclk			=> s_axi_aclk,
		s_axi_aresetn			=> s_axi_aresetn,
		s_axi_awaddr			=> cb_m_axi_awaddr(32*AMC_COMMON_PORT+AMC_COMMON_AWIDTH-1 downto 32*AMC_COMMON_PORT),
		s_axi_awvalid			=> cb_m_axi_awvalid(AMC_COMMON_PORT),
		s_axi_awready			=> cb_m_axi_awready(AMC_COMMON_PORT),
		s_axi_wdata			=> cb_m_axi_wdata(32*(AMC_COMMON_PORT+1)-1 downto 32*AMC_COMMON_PORT),
		s_axi_wstrb			=> cb_m_axi_wstrb(4*(AMC_COMMON_PORT+1)-1 downto 4*AMC_COMMON_PORT),
		s_axi_wvalid			=> cb_m_axi_wvalid(AMC_COMMON_PORT),
		s_axi_wready			=> cb_m_axi_wready(AMC_COMMON_PORT),
		s_axi_bresp			=> cb_m_axi_bresp(2*(AMC_COMMON_PORT+1)-1 downto 2*AMC_COMMON_PORT),
		s_axi_bvalid			=> cb_m_axi_bvalid(AMC_COMMON_PORT),
		s_axi_bready			=> cb_m_axi_bready(AMC_COMMON_PORT),
		s_axi_araddr			=> cb_m_axi_araddr(32*AMC_COMMON_PORT+AMC_COMMON_AWIDTH-1 downto 32*AMC_COMMON_PORT),
		s_axi_arvalid			=> cb_m_axi_arvalid(AMC_COMMON_PORT),
		s_axi_arready			=> cb_m_axi_arready(AMC_COMMON_PORT),
		s_axi_rdata			=> cb_m_axi_rdata(32*(AMC_COMMON_PORT+1)-1 downto 32*AMC_COMMON_PORT),
		s_axi_rresp			=> cb_m_axi_rresp(2*(AMC_COMMON_PORT+1)-1 downto 2*AMC_COMMON_PORT),
		s_axi_rvalid			=> cb_m_axi_rvalid(AMC_COMMON_PORT),
		s_axi_rready			=> cb_m_axi_rready(AMC_COMMON_PORT)
	);

	-- ===== fmc specific code =============

	-- SPI_SDIO instantiation for LMK04828 PLL
	axi_spi_sdio_lmk04828_inst: axi_spi_sdio
	generic map(
		C_SDI_CS_IDLE			=> '1',
		C_SDI_NUM_OF_SLAVES		=> 1,
		C_SDI_FREQ_RATIO		=> 16,
		C_S_AXI_ADDR_WIDTH		=> 7
	)
	port map(
		s_axi_aclk			=> s_axi_aclk,
		s_axi_aresetn			=> s_axi_aresetn,
		s_axi_awaddr			=> cb_m_axi_awaddr(32*SDIO_LMK_PORT+SDIO_LMK_AWIDTH-1 downto 32*SDIO_LMK_PORT),
		s_axi_awvalid			=> cb_m_axi_awvalid(SDIO_LMK_PORT),
		s_axi_awready			=> cb_m_axi_awready(SDIO_LMK_PORT),
		s_axi_wdata			=> cb_m_axi_wdata(32*(SDIO_LMK_PORT+1)-1 downto 32*SDIO_LMK_PORT),
		s_axi_wstrb			=> cb_m_axi_wstrb(4*(SDIO_LMK_PORT+1)-1 downto 4*SDIO_LMK_PORT),
		s_axi_wvalid			=> cb_m_axi_wvalid(SDIO_LMK_PORT),
		s_axi_wready			=> cb_m_axi_wready(SDIO_LMK_PORT),
		s_axi_bresp			=> cb_m_axi_bresp(2*(SDIO_LMK_PORT+1)-1 downto 2*SDIO_LMK_PORT),
		s_axi_bvalid			=> cb_m_axi_bvalid(SDIO_LMK_PORT),
		s_axi_bready			=> cb_m_axi_bready(SDIO_LMK_PORT),
		s_axi_araddr			=> cb_m_axi_araddr(32*SDIO_LMK_PORT+SDIO_LMK_AWIDTH-1 downto 32*SDIO_LMK_PORT),
		s_axi_arvalid			=> cb_m_axi_arvalid(SDIO_LMK_PORT),
		s_axi_arready			=> cb_m_axi_arready(SDIO_LMK_PORT),
		s_axi_rdata			=> cb_m_axi_rdata(32*(SDIO_LMK_PORT+1)-1 downto 32*SDIO_LMK_PORT),
		s_axi_rresp			=> cb_m_axi_rresp(2*(SDIO_LMK_PORT+1)-1 downto 2*SDIO_LMK_PORT),
		s_axi_rvalid			=> cb_m_axi_rvalid(SDIO_LMK_PORT),
		s_axi_rready			=> cb_m_axi_rready(SDIO_LMK_PORT),
		spi_sdio_o(0)			=> lmk_spi_sdio_o,
		spi_sdio_i(0)			=> lmk_spi_sdio_i,
		spi_sclk(0)			=> FMC_LA_P27,
		spi_sdio_t(0)			=> lmk_spi_sdio_t,
		spi_ncs(0)			=> FMC_LA_P26,
		sclk_sb				=> open,
		sdout_sb			=> open,
		sdout_t_sb			=> open,
		ip2intc_irpt			=> open
	);

	IOBUF_inst_lmk: IOBUF
	port map (
		O				=> lmk_spi_sdio_i,
		IO				=> FMC_LA_P25,
		I				=> lmk_spi_sdio_o,
		T				=> lmk_spi_sdio_t
	);

	FMC_LA_P24				<= not(lmk_spi_sdio_t);
	FMC_LA_N24				<= '1';

	-- SPI_SDIO instantiation for accessing LMX2592 to ADC
	axi_spi_sdio_inst_lmxa: axi_spi_sdio
	generic map(
		C_SDI_CS_IDLE			=> '1',
		C_SDI_NUM_OF_SLAVES		=> 1,
		C_SDI_FREQ_RATIO		=> 16,
		C_S_AXI_ADDR_WIDTH		=> 7
	)
	port map(
		s_axi_aclk			=> s_axi_aclk,
		s_axi_aresetn			=> s_axi_aresetn,
		s_axi_awaddr			=> cb_m_axi_awaddr(32*SDIO_LMXA_PORT+SDIO_LMXA_AWIDTH-1 downto 32*SDIO_LMXA_PORT),
		s_axi_awvalid			=> cb_m_axi_awvalid(SDIO_LMXA_PORT),
		s_axi_awready			=> cb_m_axi_awready(SDIO_LMXA_PORT),
		s_axi_wdata			=> cb_m_axi_wdata(32*(SDIO_LMXA_PORT+1)-1 downto 32*SDIO_LMXA_PORT),
		s_axi_wstrb			=> cb_m_axi_wstrb(4*(SDIO_LMXA_PORT+1)-1 downto 4*SDIO_LMXA_PORT),
		s_axi_wvalid			=> cb_m_axi_wvalid(SDIO_LMXA_PORT),
		s_axi_wready			=> cb_m_axi_wready(SDIO_LMXA_PORT),
		s_axi_bresp			=> cb_m_axi_bresp(2*(SDIO_LMXA_PORT+1)-1 downto 2*SDIO_LMXA_PORT),
		s_axi_bvalid			=> cb_m_axi_bvalid(SDIO_LMXA_PORT),
		s_axi_bready			=> cb_m_axi_bready(SDIO_LMXA_PORT),
		s_axi_araddr			=> cb_m_axi_araddr(32*SDIO_LMXA_PORT+SDIO_LMXA_AWIDTH-1 downto 32*SDIO_LMXA_PORT),
		s_axi_arvalid			=> cb_m_axi_arvalid(SDIO_LMXA_PORT),
		s_axi_arready			=> cb_m_axi_arready(SDIO_LMXA_PORT),
		s_axi_rdata			=> cb_m_axi_rdata(32*(SDIO_LMXA_PORT+1)-1 downto 32*SDIO_LMXA_PORT),
		s_axi_rresp			=> cb_m_axi_rresp(2*(SDIO_LMXA_PORT+1)-1 downto 2*SDIO_LMXA_PORT),
		s_axi_rvalid			=> cb_m_axi_rvalid(SDIO_LMXA_PORT),
		s_axi_rready			=> cb_m_axi_rready(SDIO_LMXA_PORT),
		spi_sdio_o(0)			=> FMC_LA_N13,
		spi_sdio_i(0)			=> FMC_LA_P14,
		spi_sclk(0)			=> FMC_LA_P13,
		spi_sdio_t			=> open,
		spi_ncs(0)			=> FMC_LA_N12,
		sclk_sb				=> open,
		sdout_sb			=> open,
		sdout_t_sb			=> open,
		ip2intc_irpt			=> open
	);

	-- SPI_SDIO instantiation for accessing LMX2592 to DAC
	axi_spi_sdio_inst_lmxd: axi_spi_sdio
	generic map(
		C_SDI_CS_IDLE			=> '1',
		C_SDI_NUM_OF_SLAVES		=> 1,
		C_SDI_FREQ_RATIO		=> 16,
		C_S_AXI_ADDR_WIDTH		=> 7
	)
	port map(
		s_axi_aclk			=> s_axi_aclk,
		s_axi_aresetn			=> s_axi_aresetn,
		s_axi_awaddr			=> cb_m_axi_awaddr(32*SDIO_LMXD_PORT+SDIO_LMXD_AWIDTH-1 downto 32*SDIO_LMXD_PORT),
		s_axi_awvalid			=> cb_m_axi_awvalid(SDIO_LMXD_PORT),
		s_axi_awready			=> cb_m_axi_awready(SDIO_LMXD_PORT),
		s_axi_wdata			=> cb_m_axi_wdata(32*(SDIO_LMXD_PORT+1)-1 downto 32*SDIO_LMXD_PORT),
		s_axi_wstrb			=> cb_m_axi_wstrb(4*(SDIO_LMXD_PORT+1)-1 downto 4*SDIO_LMXD_PORT),
		s_axi_wvalid			=> cb_m_axi_wvalid(SDIO_LMXD_PORT),
		s_axi_wready			=> cb_m_axi_wready(SDIO_LMXD_PORT),
		s_axi_bresp			=> cb_m_axi_bresp(2*(SDIO_LMXD_PORT+1)-1 downto 2*SDIO_LMXD_PORT),
		s_axi_bvalid			=> cb_m_axi_bvalid(SDIO_LMXD_PORT),
		s_axi_bready			=> cb_m_axi_bready(SDIO_LMXD_PORT),
		s_axi_araddr			=> cb_m_axi_araddr(32*SDIO_LMXD_PORT+SDIO_LMXD_AWIDTH-1 downto 32*SDIO_LMXD_PORT),
		s_axi_arvalid			=> cb_m_axi_arvalid(SDIO_LMXD_PORT),
		s_axi_arready			=> cb_m_axi_arready(SDIO_LMXD_PORT),
		s_axi_rdata			=> cb_m_axi_rdata(32*(SDIO_LMXD_PORT+1)-1 downto 32*SDIO_LMXD_PORT),
		s_axi_rresp			=> cb_m_axi_rresp(2*(SDIO_LMXD_PORT+1)-1 downto 2*SDIO_LMXD_PORT),
		s_axi_rvalid			=> cb_m_axi_rvalid(SDIO_LMXD_PORT),
		s_axi_rready			=> cb_m_axi_rready(SDIO_LMXD_PORT),
		spi_sdio_o(0)			=> FMC_LA_N16,
		spi_sdio_i(0)			=> FMC_LA_N14,
		spi_sclk(0)			=> FMC_LA_P16,
		spi_sdio_t			=> open,
		spi_ncs(0)			=> FMC_LA_N15,
		sclk_sb				=> open,
		sdout_sb			=> open,
		sdout_t_sb			=> open,
		ip2intc_irpt			=> open
	);

	-- SPI_SDIO instantiation for HMC7043
	axi_spi_sdio_inst_hmc: axi_spi_sdio
	generic map(
		C_SDI_CS_IDLE			=> '1',
		C_SDI_NUM_OF_SLAVES		=> 1,
		C_SDI_FREQ_RATIO		=> 16,
		C_S_AXI_ADDR_WIDTH		=> 7
	)
	port map(
		s_axi_aclk			=> s_axi_aclk,
		s_axi_aresetn			=> s_axi_aresetn,
		s_axi_awaddr			=> cb_m_axi_awaddr(32*SDIO_HMC_PORT+SDIO_HMC_AWIDTH-1 downto 32*SDIO_HMC_PORT),
		s_axi_awvalid			=> cb_m_axi_awvalid(SDIO_HMC_PORT),
		s_axi_awready			=> cb_m_axi_awready(SDIO_HMC_PORT),
		s_axi_wdata			=> cb_m_axi_wdata(32*(SDIO_HMC_PORT+1)-1 downto 32*SDIO_HMC_PORT),
		s_axi_wstrb			=> cb_m_axi_wstrb(4*(SDIO_HMC_PORT+1)-1 downto 4*SDIO_HMC_PORT),
		s_axi_wvalid			=> cb_m_axi_wvalid(SDIO_HMC_PORT),
		s_axi_wready			=> cb_m_axi_wready(SDIO_HMC_PORT),
		s_axi_bresp			=> cb_m_axi_bresp(2*(SDIO_HMC_PORT+1)-1 downto 2*SDIO_HMC_PORT),
		s_axi_bvalid			=> cb_m_axi_bvalid(SDIO_HMC_PORT),
		s_axi_bready			=> cb_m_axi_bready(SDIO_HMC_PORT),
		s_axi_araddr			=> cb_m_axi_araddr(32*SDIO_HMC_PORT+SDIO_HMC_AWIDTH-1 downto 32*SDIO_HMC_PORT),
		s_axi_arvalid			=> cb_m_axi_arvalid(SDIO_HMC_PORT),
		s_axi_arready			=> cb_m_axi_arready(SDIO_HMC_PORT),
		s_axi_rdata			=> cb_m_axi_rdata(32*(SDIO_HMC_PORT+1)-1 downto 32*SDIO_HMC_PORT),
		s_axi_rresp			=> cb_m_axi_rresp(2*(SDIO_HMC_PORT+1)-1 downto 2*SDIO_HMC_PORT),
		s_axi_rvalid			=> cb_m_axi_rvalid(SDIO_HMC_PORT),
		s_axi_rready			=> cb_m_axi_rready(SDIO_HMC_PORT),
		spi_sdio_o(0)			=> hmc_spi_sdio_o,
		spi_sdio_i(0)			=> hmc_spi_sdio_i,
		spi_sclk(0)			=> FMC_LA_N29,
		spi_sdio_t(0)			=> hmc_spi_sdio_t,
		spi_ncs(0)			=> FMC_LA_P29,
		sclk_sb				=> open,
		sdout_sb			=> open,
		sdout_t_sb			=> open,
		ip2intc_irpt			=> open
	);

	IOBUF_inst_hmc: IOBUF
	port map (
		O				=> hmc_spi_sdio_i,
		IO				=> FMC_LA_N30,
		I				=> hmc_spi_sdio_o,
		T				=> hmc_spi_sdio_t
	);

	-- SPI_SDIO instantiation for accessing the ADC12DJ3200/2700/1600 ADC
	axi_spi_sdio_inst_adc: axi_spi_sdio
	generic map(
		C_SDI_CS_IDLE			=> '1',
		C_SDI_NUM_OF_SLAVES		=> 1,
		C_SDI_FREQ_RATIO		=> 16,
		C_S_AXI_ADDR_WIDTH		=> 7
	)
	port map(
		s_axi_aclk			=> s_axi_aclk,
		s_axi_aresetn			=> s_axi_aresetn,
		s_axi_awaddr			=> cb_m_axi_awaddr(32*SDIO_ADC_PORT+SDIO_ADC_AWIDTH-1 downto 32*SDIO_ADC_PORT),
		s_axi_awvalid			=> cb_m_axi_awvalid(SDIO_ADC_PORT),
		s_axi_awready			=> cb_m_axi_awready(SDIO_ADC_PORT),
		s_axi_wdata			=> cb_m_axi_wdata(32*(SDIO_ADC_PORT+1)-1 downto 32*SDIO_ADC_PORT),
		s_axi_wstrb			=> cb_m_axi_wstrb(4*(SDIO_ADC_PORT+1)-1 downto 4*SDIO_ADC_PORT),
		s_axi_wvalid			=> cb_m_axi_wvalid(SDIO_ADC_PORT),
		s_axi_wready			=> cb_m_axi_wready(SDIO_ADC_PORT),
		s_axi_bresp			=> cb_m_axi_bresp(2*(SDIO_ADC_PORT+1)-1 downto 2*SDIO_ADC_PORT),
		s_axi_bvalid			=> cb_m_axi_bvalid(SDIO_ADC_PORT),
		s_axi_bready			=> cb_m_axi_bready(SDIO_ADC_PORT),
		s_axi_araddr			=> cb_m_axi_araddr(32*SDIO_ADC_PORT+SDIO_ADC_AWIDTH-1 downto 32*SDIO_ADC_PORT),
		s_axi_arvalid			=> cb_m_axi_arvalid(SDIO_ADC_PORT),
		s_axi_arready			=> cb_m_axi_arready(SDIO_ADC_PORT),
		s_axi_rdata			=> cb_m_axi_rdata(32*(SDIO_ADC_PORT+1)-1 downto 32*SDIO_ADC_PORT),
		s_axi_rresp			=> cb_m_axi_rresp(2*(SDIO_ADC_PORT+1)-1 downto 2*SDIO_ADC_PORT),
		s_axi_rvalid			=> cb_m_axi_rvalid(SDIO_ADC_PORT),
		s_axi_rready			=> cb_m_axi_rready(SDIO_ADC_PORT),
		spi_sdio_o(0)			=> FMC_LA_P10,
		spi_sdio_i(0)			=> FMC_LA_P7,
		spi_sclk(0)			=> FMC_LA_N9,
		spi_sdio_t			=> open,
		spi_ncs(0)			=> FMC_LA_P9,
		sclk_sb				=> open,
		sdout_sb			=> open,
		sdout_t_sb			=> open,
		ip2intc_irpt			=> open
	);

	-- SPI_SDIO instantiation for accessing the AD9162 DAC chips
	axi_spi_sdio_inst_dac: axi_spi_sdio
	generic map(
		C_SDI_CS_IDLE			=> '1',
		C_SDI_NUM_OF_SLAVES		=> 1,
		C_SDI_FREQ_RATIO		=> 16,
		C_S_AXI_ADDR_WIDTH		=> 7
	)
	port map(
		s_axi_aclk			=> s_axi_aclk,
		s_axi_aresetn			=> s_axi_aresetn,
		s_axi_awaddr			=> cb_m_axi_awaddr(32*SDIO_DAC_PORT+SDIO_DAC_AWIDTH-1 downto 32*SDIO_DAC_PORT),
		s_axi_awvalid			=> cb_m_axi_awvalid(SDIO_DAC_PORT),
		s_axi_awready			=> cb_m_axi_awready(SDIO_DAC_PORT),
		s_axi_wdata			=> cb_m_axi_wdata(32*(SDIO_DAC_PORT+1)-1 downto 32*SDIO_DAC_PORT),
		s_axi_wstrb			=> cb_m_axi_wstrb(4*(SDIO_DAC_PORT+1)-1 downto 4*SDIO_DAC_PORT),
		s_axi_wvalid			=> cb_m_axi_wvalid(SDIO_DAC_PORT),
		s_axi_wready			=> cb_m_axi_wready(SDIO_DAC_PORT),
		s_axi_bresp			=> cb_m_axi_bresp(2*(SDIO_DAC_PORT+1)-1 downto 2*SDIO_DAC_PORT),
		s_axi_bvalid			=> cb_m_axi_bvalid(SDIO_DAC_PORT),
		s_axi_bready			=> cb_m_axi_bready(SDIO_DAC_PORT),
		s_axi_araddr			=> cb_m_axi_araddr(32*SDIO_DAC_PORT+SDIO_DAC_AWIDTH-1 downto 32*SDIO_DAC_PORT),
		s_axi_arvalid			=> cb_m_axi_arvalid(SDIO_DAC_PORT),
		s_axi_arready			=> cb_m_axi_arready(SDIO_DAC_PORT),
		s_axi_rdata			=> cb_m_axi_rdata(32*(SDIO_DAC_PORT+1)-1 downto 32*SDIO_DAC_PORT),
		s_axi_rresp			=> cb_m_axi_rresp(2*(SDIO_DAC_PORT+1)-1 downto 2*SDIO_DAC_PORT),
		s_axi_rvalid			=> cb_m_axi_rvalid(SDIO_DAC_PORT),
		s_axi_rready			=> cb_m_axi_rready(SDIO_DAC_PORT),
		spi_sdio_o(0)			=> FMC_LA_P19,
		spi_sdio_i(0)			=> FMC_LA_N19,
		spi_sclk(0)			=> FMC_LA_P20,
		spi_sdio_t			=> open,
		spi_ncs(0)			=> FMC_LA_N20,
		sclk_sb				=> open,
		sdout_sb			=> open,
		sdout_t_sb			=> open,
		ip2intc_irpt			=> open
	);

	-- ===== GPIO ==================
	axi_gpio_32_inst: axi_gpio_32
	generic map(
		MAJOR_VERSION			=> MAJOR_VERSION,
		MINOR_VERSION			=> MINOR_VERSION,
		PATCH_VERSION			=> PATCH_VERSION,
		REV_VERSION			=> REV_VERSION,
		SIGNATURE			=> SIGNATURE,
		IMAGE_ID			=> IMAGE_ID,
		GPIO_INIT_11			=> x"000000FF",		-- JESD control
		GPIO_INIT_16			=> x"00000008"		-- txdiffctrl
	)
	port map(
		s_axi_aclk			=> s_axi_aclk,
		s_axi_aresetn			=> s_axi_aresetn,
		s_axi_awaddr			=> cb_m_axi_awaddr(32*AXI_GPIO_PORT+AXI_GPIO_AWIDTH-1 downto 32*AXI_GPIO_PORT),
		s_axi_awvalid			=> cb_m_axi_awvalid(AXI_GPIO_PORT),
		s_axi_awready			=> cb_m_axi_awready(AXI_GPIO_PORT),
		s_axi_wdata			=> cb_m_axi_wdata(32*(AXI_GPIO_PORT+1)-1 downto 32*AXI_GPIO_PORT),
		s_axi_wstrb			=> cb_m_axi_wstrb(4*(AXI_GPIO_PORT+1)-1 downto 4*AXI_GPIO_PORT),
		s_axi_wvalid			=> cb_m_axi_wvalid(AXI_GPIO_PORT),
		s_axi_wready			=> cb_m_axi_wready(AXI_GPIO_PORT),
		s_axi_bresp			=> cb_m_axi_bresp(2*(AXI_GPIO_PORT+1)-1 downto 2*AXI_GPIO_PORT),
		s_axi_bvalid			=> cb_m_axi_bvalid(AXI_GPIO_PORT),
		s_axi_bready			=> cb_m_axi_bready(AXI_GPIO_PORT),
		s_axi_araddr			=> cb_m_axi_araddr(32*AXI_GPIO_PORT+AXI_GPIO_AWIDTH-1 downto 32*AXI_GPIO_PORT),
		s_axi_arvalid			=> cb_m_axi_arvalid(AXI_GPIO_PORT),
		s_axi_arready			=> cb_m_axi_arready(AXI_GPIO_PORT),
		s_axi_rdata			=> cb_m_axi_rdata(32*(AXI_GPIO_PORT+1)-1 downto 32*AXI_GPIO_PORT),
		s_axi_rresp			=> cb_m_axi_rresp(2*(AXI_GPIO_PORT+1)-1 downto 2*AXI_GPIO_PORT),
		s_axi_rvalid			=> cb_m_axi_rvalid(AXI_GPIO_PORT),
		s_axi_rready			=> cb_m_axi_rready(AXI_GPIO_PORT),

		gpio_out_0			=> gpio_out_0,
		gpio_out_1			=> gpio_out_1,
		gpio_out_2			=> gpio_out_2,
		gpio_out_3			=> gpio_out_3,
		gpio_out_4			=> gpio_out_4,
		gpio_out_5			=> gpio_out_5,
		gpio_out_6			=> gpio_out_6,
		gpio_out_7			=> gpio_out_7,
		gpio_out_8			=> gpio_out_8,
		gpio_out_9			=> gpio_out_9,
		gpio_out_10			=> gpio_out_10,
		gpio_out_11			=> gpio_out_11,
		gpio_out_12			=> gpio_out_12,
		gpio_out_13			=> gpio_out_13,
		gpio_out_14			=> gpio_out_14,
		gpio_out_15			=> gpio_out_15,
		gpio_out_16			=> gpio_out_16,
		gpio_out_17			=> gpio_out_17,
		gpio_out_18			=> gpio_out_18,
		gpio_out_19			=> gpio_out_19,
		gpio_out_20			=> gpio_out_20,
		gpio_out_21			=> gpio_out_21,
		gpio_out_22			=> gpio_out_22,
		gpio_out_23			=> gpio_out_23,
		gpio_out_24			=> gpio_out_24,
		gpio_out_25			=> gpio_out_25,
		gpio_out_26			=> gpio_out_26,
		gpio_out_27			=> gpio_out_27,
		gpio_out_28			=> gpio_out_28,
		gpio_out_29			=> gpio_out_29,
		gpio_out_30			=> gpio_out_30,
		gpio_out_31			=> gpio_out_31,
		gpio_out_en			=> gpio_out_en,
		gpio_in_0			=> gpio_in_0,
		gpio_in_1			=> gpio_in_1,
		gpio_in_2			=> gpio_in_2,
		gpio_in_3			=> gpio_in_3,
		gpio_in_4			=> gpio_in_4,
		gpio_in_5			=> gpio_in_5,
		gpio_in_6			=> gpio_in_6,
		gpio_in_7			=> gpio_in_7,
		gpio_in_8			=> gpio_in_8,
		gpio_in_9			=> gpio_in_9,
		gpio_in_10			=> gpio_in_10,
		gpio_in_11			=> gpio_in_11,
		gpio_in_12			=> gpio_in_12,
		gpio_in_13			=> gpio_in_13,
		gpio_in_14			=> gpio_in_14,
		gpio_in_15			=> gpio_in_15,
		gpio_in_16			=> gpio_in_16,
		gpio_in_17			=> gpio_in_17,
		gpio_in_18			=> gpio_in_18,
		gpio_in_19			=> gpio_in_19,
		gpio_in_20			=> gpio_in_20,
		gpio_in_21			=> gpio_in_21,
		gpio_in_22			=> gpio_in_22,
		gpio_in_23			=> gpio_in_23,
		gpio_in_24			=> gpio_in_24,
		gpio_in_25			=> gpio_in_25,
		gpio_in_26			=> gpio_in_26,
		gpio_in_27			=> gpio_in_27,
		gpio_in_28			=> gpio_in_28,
		gpio_in_29			=> gpio_in_29,
		gpio_in_30			=> gpio_in_30,
		gpio_in_31			=> gpio_in_31,

		program_n			=> open,
		scratch				=> scratch
	);

	-- 0 0x0: Option
	gpio_in_0(1 downto 0)			<= std_logic_vector(to_unsigned(SPEED_GRADE, 2));

	-- 1 0x4: HMC7043 I/O
	gpio_in_1(1 downto 0)			<= gpio_out_1(1 downto 0);
	FMC_LA_N27				<= gpio_out_1(0);	-- RESET
	hmc7043_rfsyncin			<= gpio_out_1(1);	-- RFSYNCIN

	OBUFDS_inst_hmc7043_rfsyncin: OBUFDS
	port map (
		O  				=> FMC_LA_P11,
		OB 				=> FMC_LA_N11,
		I  				=> hmc7043_rfsyncin
	);

	-- 2 0x8: adc_acq_ctrl
	gpio_in_2(3 downto 0)			<= gpio_out_2(3 downto 0);
	adc_acq_ctrl				<= gpio_out_2(3 downto 0);

	-- 3 0xC: adc fifo status
	gpio_in_3(0)				<= a0_rd_f0_full;
	gpio_in_3(1)				<= a0_rd_f0_empty;
	gpio_in_3(2)				<= a0_rd_f1_full;
	gpio_in_3(3)				<= a0_rd_f1_empty;

	gpio_in_3(4)				<= a1_rd_f0_full;
	gpio_in_3(5)				<= a1_rd_f0_empty;
	gpio_in_3(6)				<= a1_rd_f1_full;
	gpio_in_3(7)				<= a1_rd_f1_empty;

	-- 4 0x10: DAC I/O
	gpio_in_4(1 downto 0)			<= gpio_out_4(1 downto 0);
	FMC_LA_N21				<= gpio_out_4(0);	-- nRESET
	FMC_LA_P22				<= gpio_out_4(1);	-- TX_EN

	gpio_in_4(2)				<= FMC_LA_P21;		-- IRQn

	-- 5 0x14: DAC pattern
	gpio_in_5(3 downto 0)			<= gpio_out_5(3 downto 0);
	dac_ptn					<= gpio_out_5(3 downto 0);

	-- 6 0x18: front panel TRIG ports
	gpio_in_6(3 downto 0)			<= gpio_out_6(3 downto 0);
	trig_out_sel				<= gpio_out_6(3 downto 0); 	-- source select for TRIG_OUT for option A=0 only
	gpio_in_6(4)				<= trigger_in;		-- Fixed TRIG_IN / TRIG_IN0 port input value
	gpio_in_6(5)				<= '0';		-- TRIG_IN1 input value for option A = 1 only

	-- 7 0x1C: LMK04828 I/O
	gpio_in_7(1 downto 0)			<= gpio_out_7(1 downto 0);
	FMC_LA_N25				<= gpio_out_7(0);	-- reset
	FMC_LA_N26				<= gpio_out_7(1);	-- sysref_req
	gpio_out_7(4)				<= FMC_LA_P28;		-- STATUS_LD1
	gpio_out_7(5)				<= FMC_LA_N28;		-- STATUS_LD2

	-- 8 0x20: LMX2592 ADC I/O
	gpio_in_8(0)				<= gpio_out_8(0);
	FMC_LA_P12				<= gpio_out_8(0);	-- CE
	gpio_in_8(1)				<= FMC_LA_P14;		-- MUXOUT. LD/Readback

	-- 9 0x24: LMX2592 DAC I/O
	gpio_in_9(0)				<= gpio_out_9(0);
	FMC_LA_P15				<= gpio_out_9(0);	-- CE
	gpio_in_9(1)				<= FMC_LA_N14;		-- MUXOUT. LD/Readback

	-- 10 0x28: ADC I/O
	gpio_in_10(6 downto 0)			<= gpio_out_10(6 downto 0);
	FMC_LA_P8				<= gpio_out_10(0);	-- ADC PD
	FMC_LA_P2				<= gpio_out_10(1);	-- ADC NCOA0
	FMC_LA_N2				<= gpio_out_10(2);	-- ADC NCOA1
	FMC_LA_P3				<= gpio_out_10(3);	-- ADC NCOB0
	FMC_LA_N3				<= gpio_out_10(4);	-- ADC NCOB1
	adc_tmstp				<= gpio_out_10(5);	-- ADC TMSTP
	FMC_LA_N8				<= gpio_out_10(6);	-- ADC CALTRIG
	gpio_in_10(7)				<= FMC_LA_P5;		-- ADC ORA0
	gpio_in_10(8)				<= FMC_LA_N5;		-- ADC ORA1
	gpio_in_10(9)				<= FMC_LA_P6;		-- ADC ORB0
	gpio_in_10(10)				<= FMC_LA_N6;		-- ADC ORB1
	gpio_in_10(11)				<= FMC_LA_N7;		-- ADC CALSTAT

	OBUFDS_inst_adc_tmstp : OBUFDS
	port map (
		O  				=> FMC_LA_P4,
		OB 				=> FMC_LA_N4,
		I  				=> adc_tmstp
	);

	-- 11 0x2C: JESD204 control: default all '1'
	gpio_in_11(7 downto 0)			<= gpio_out_11(7 downto 0);
	a0_usr_rst				<= gpio_out_11(0);
	a0_clk_dis				<= gpio_out_11(1);
	a0_sysref_dis				<= gpio_out_11(2);
	a0_refclk_dis				<= gpio_out_11(3);
	d0_usr_rst				<= gpio_out_11(4);
	d0_clk_dis				<= gpio_out_11(5);
	d0_sysref_dis				<= gpio_out_11(6);
	d0_refclk_dis				<= gpio_out_11(7);

	-- 12 0x30: DAC memory data length
	gpio_in_12(13 downto 0)			<= gpio_out_12(13 downto 0);
	dac_mem_rd_len				<= gpio_out_12(13 downto 0);

	-- 13 0x34: ADC channel select
	gpio_in_13(0)				<= gpio_out_13(0);
	process(s_axi_aclk)
	begin
		if(rising_edge(s_axi_aclk)) then
			adc_ch_sel		<= gpio_out_13(0);
		end if;
	end process;

	process(a0_core_clk)
	begin
		if(rising_edge(a0_core_clk)) then
			adc_ch_sel_sync		<= gpio_out_13(0);
		end if;
	end process;
	
	-- 14 0x38: txpostcursor
	gpio_in_14(4 downto 0)			<= gpio_out_14(4 downto 0);
	txpostcursor				<= gpio_out_14(4 downto 0);

	-- 15 0x3C: txprecursor
	gpio_in_15(4 downto 0)			<= gpio_out_15(4 downto 0);
	txprecursor				<= gpio_out_15(4 downto 0);

	-- 16 0x40: txdiffctrl
	gpio_in_16(3 downto 0)			<= gpio_out_16(3 downto 0);
	txdiffctrl				<= gpio_out_16(3 downto 0);

	-- 17 0x44: rxlpmen
	gpio_in_17(0)				<= gpio_out_17(0);
	rxlpmen					<= gpio_out_17(0);

	-- 18 0x48: PE43713_ADC I/O for -01 ordering option only
	gpio_in_18(2 downto 0)			<= gpio_out_18(2 downto 0);
	FMC_LA_P31				<= gpio_out_18(0);	-- SI
	FMC_LA_N31				<= gpio_out_18(1);	-- CLK
	FMC_LA_P32				<= gpio_out_18(2);	-- LE

	-- 19 0x4C: PE43713_DAC I/O
	gpio_in_19(2 downto 0)			<= gpio_out_19(2 downto 0);
	FMC_LA_N32				<= gpio_out_19(0);	-- SI
	FMC_LA_P33				<= gpio_out_19(1);	-- CLK
	FMC_LA_N33				<= gpio_out_19(2);	-- LE



	-- ===== ADC and DAC JESD204 instantiation ========
		-------- Bring in 187.5MHz jesd204b refclk for DAC JESD core
		IBUFDS_gte4_inst_dac_refclk : IBUFDS_GTE4
		port map (
			O				=> d0_refclk,
			ODIV2				=> d0_refclk_pre,
			CEB				=> d0_refclk_dis,
			I				=> FMC_GBTCLK1_P,
			IB				=> FMC_GBTCLK1_N
		);

		------- use the 187.5MHz jesd204b refclk as the dac core clock
		BUFG_GT_inst_dac_core_clk : BUFG_GT
		port map (
			CE				=> '1',
			CEMASK				=> '1',
			CLR				=> '0',
			CLRMASK				=> '1',
			DIV				=> "000",
			I				=> d0_refclk_pre,
			O				=> d0_core_clk
		);
		d0_core_clk_o<=d0_core_clk;

		-------- Bring in 300MHz jesd204b refclk for ADC JESD core
		IBUFDS_gte4_inst_adc_refclk : IBUFDS_GTE4
		port map (
			O				=> a0_refclk,
			ODIV2				=> a0_refclk_pre,
			CEB				=> a0_refclk_dis,
			I				=> FMC_GBTCLK0_P,
			IB				=> FMC_GBTCLK0_N
		);

		------- use the 300MHz adc jesd204b refclk as the adc core clock
		BUFG_GT_inst_adc_core_clk : BUFG_GT
		port map (
			CE				=> '1',
			CEMASK				=> '1',
			CLR				=> '0',
			CLRMASK				=> '1',
			DIV				=> "000",
			I				=> a0_refclk_pre,
			O				=> a0_core_clk
		);

        a0_core_clk_o <= a0_core_clk;

	-------- Bring in ADC sysref.
	IBUFDS_inst_a0_sysref : IBUFDS_IBUFDISABLE
	generic map (
		SIM_DEVICE			=> "ULTRASCALE",  -- changed 2020/02/12-GKO: see UG974 (V2018.3), p.333/334
--		IOSTANDARD 			=> LVDS_STANDARD, -- changed 2020/02/12-GKO: see UG912 (V2015.4), p.228: cannot be aplied to GTs
--		DIFF_TERM			=> "TRUE",        -- changed 2020/02/12-GKO: see UG576 (V1.6), p.326: MGTREFCLKP/N internally terminated with 100? differential impedance
		USE_IBUFDISABLE			=> "TRUE"
	)
	port map (
		O				=> a0_sysref,
		I				=> FMC_LA_P0,
		IB				=> FMC_LA_N0,
		IBUFDISABLE			=> a0_sysref_dis
	);

	-------- Bring in DAC sysref.
	IBUFDS_inst_d0_sysref : IBUFDS_IBUFDISABLE
	generic map (
		SIM_DEVICE			=> "ULTRASCALE",   -- changed 2020/02/12-GKO: see UG974 (V2018.3), p.333/334
--		IOSTANDARD 			=> LVDS_STANDARD,  -- changed 2020/02/12-GKO: see UG912 (V2015.4), p.228: cannot be aplied to GTs
--		DIFF_TERM			=> "TRUE",         -- changed 2020/02/12-GKO: see UG576 (V1.6), p.326: MGTREFCLKP/N internally terminated with 100? differential impedance
		USE_IBUFDISABLE			=> "TRUE"
	)
	port map (
		O				=> d0_sysref,
		I				=> FMC_LA_P1,
		IB				=> FMC_LA_N1,
		IBUFDISABLE			=> d0_sysref_dis
	);

	-------- ADC sync
	FMC_LA_N10				<= a0_sync and a1_sync;

	-------- DAC sync
	IBUFDS_inst_dac_sync : IBUFDS
	port map (
		O				=> d0_sync,
		I				=> FMC_LA_P17,
		IB				=> FMC_LA_N17
	);

	rxp	<= FMC_RX7_P & FMC_RX6_P & FMC_RX5_P & FMC_RX4_P & FMC_RX3_P & FMC_RX2_P & FMC_RX1_P & FMC_RX0_P;

	rxn	<= FMC_RX7_N & FMC_RX6_N & FMC_RX5_N & FMC_RX4_N & FMC_RX3_N & FMC_RX2_N & FMC_RX1_N & FMC_RX0_N;

	FMC_TX0_P		 			<= txp(0);
	FMC_TX1_P		 			<= txp(1);
	FMC_TX2_P		 			<= txp(2);
	FMC_TX3_P		 			<= txp(3);
	FMC_TX4_P		 			<= txp(4);
	FMC_TX5_P		 			<= txp(5);
	FMC_TX6_P		 			<= txp(6);
	FMC_TX7_P		 			<= txp(7);

	FMC_TX0_N		 			<= txn(0);
	FMC_TX1_N		 			<= txn(1);
	FMC_TX2_N		 			<= txn(2);
	FMC_TX3_N		 			<= txn(3);
	FMC_TX4_N		 			<= txn(4);
	FMC_TX5_N		 			<= txn(5);
	FMC_TX6_N		 			<= txn(6);
	FMC_TX7_N		 			<= txn(7);

	jesd204_support_inst: jesd204_support
	generic map (
		FPGA_TRANSCEIVER		=> FPGA_TRANSCEIVER,
		FPGA_SERIES			=> FPGA_SERIES,
		SPEED_GRADE			=> SPEED_GRADE
	)
	port map(
		rx_refclk			=> a0_refclk,
		tx_refclk			=> d0_refclk,
		rx_core_clk			=> a0_core_clk,
		tx_core_clk			=> d0_core_clk,
		common0_pll_clk_out 		=> open,
		common0_pll_refclk_out 		=> open,
		common0_pll_lock_out 		=> open,
		common1_pll_clk_out 		=> open,
		common1_pll_refclk_out 		=> open,
		common1_pll_lock_out 		=> open,
		rx_reset 			=> a0_usr_rst,
		rx_sysref 			=> a0_sysref,
		rxp				=> rxp,
		rxn				=> rxn,
		rx0_sync 			=> a0_sync,
		rx0_aresetn 			=> a0_aresetn,
		rx0_start_of_frame 		=> a0_start_of_frame,
		rx0_end_of_frame 		=> a0_end_of_frame,
		rx0_start_of_multiframe 	=> a0_start_of_multiframe,
		rx0_end_of_multiframe 		=> a0_end_of_multiframe,
		rx0_frame_error 		=> a0_frame_error,
		rx0_tvalid 			=> a0_tvalid,
		rx0_tdata 			=> a0_tdata,
		rx1_sync 			=> a1_sync,
		rx1_aresetn 			=> a1_aresetn,
		rx1_start_of_frame 		=> a1_start_of_frame,
		rx1_end_of_frame 		=> a1_end_of_frame,
		rx1_start_of_multiframe 	=> a1_start_of_multiframe,
		rx1_end_of_multiframe 		=> a1_end_of_multiframe,
		rx1_frame_error 		=> a1_frame_error,
		rx1_tvalid 			=> a1_tvalid,
		rx1_tdata 			=> a1_tdata,
		tx_reset			=> d0_usr_rst,
		tx_sysref			=> d0_sysref,
		tx_sync				=> d0_sync,
		txp				=> txp,
		txn				=> txn,
		tx_aresetn			=> d0_aresetn,
		tx_start_of_frame		=> d0_start_of_frame,
		tx_start_of_multiframe		=> d0_start_of_multiframe,
		tx_tready			=> d0_tready,
		tx_tdata			=> d0_tdata,
		drpclk				=> s_axi_aclk,
		txpostcursor			=> txpostcursor,
		txprecursor			=> txprecursor,
		txdiffctrl			=> txdiffctrl,
		rxlpmen				=> rxlpmen,
		rx0_s_axi_aclk			=> s_axi_aclk,
		rx0_s_axi_aresetn		=> '1',
		rx0_s_axi_awaddr		=> cb_m_axi_awaddr(32*JESD_A0_PORT+JESD_A0_AWIDTH-1 downto 32*JESD_A0_PORT),
		rx0_s_axi_awvalid		=> cb_m_axi_awvalid(JESD_A0_PORT),
		rx0_s_axi_awready		=> cb_m_axi_awready(JESD_A0_PORT),
		rx0_s_axi_wdata			=> cb_m_axi_wdata(32*(JESD_A0_PORT+1)-1 downto 32*JESD_A0_PORT),
		rx0_s_axi_wstrb			=> cb_m_axi_wstrb(4*(JESD_A0_PORT+1)-1 downto 4*JESD_A0_PORT),
		rx0_s_axi_wvalid		=> cb_m_axi_wvalid(JESD_A0_PORT),
		rx0_s_axi_wready		=> cb_m_axi_wready(JESD_A0_PORT),
		rx0_s_axi_bresp			=> cb_m_axi_bresp(2*(JESD_A0_PORT+1)-1 downto 2*JESD_A0_PORT),
		rx0_s_axi_bvalid		=> cb_m_axi_bvalid(JESD_A0_PORT),
		rx0_s_axi_bready		=> cb_m_axi_bready(JESD_A0_PORT),
		rx0_s_axi_araddr		=> cb_m_axi_araddr(32*JESD_A0_PORT+JESD_A0_AWIDTH-1 downto 32*JESD_A0_PORT),
		rx0_s_axi_arvalid		=> cb_m_axi_arvalid(JESD_A0_PORT),
		rx0_s_axi_arready		=> cb_m_axi_arready(JESD_A0_PORT),
		rx0_s_axi_rdata			=> cb_m_axi_rdata(32*(JESD_A0_PORT+1)-1 downto 32*JESD_A0_PORT),
		rx0_s_axi_rresp			=> cb_m_axi_rresp(2*(JESD_A0_PORT+1)-1 downto 2*JESD_A0_PORT),
		rx0_s_axi_rvalid		=> cb_m_axi_rvalid(JESD_A0_PORT),
		rx0_s_axi_rready		=> cb_m_axi_rready(JESD_A0_PORT),
		rx1_s_axi_aclk			=> s_axi_aclk,
		rx1_s_axi_aresetn		=> '1',
		rx1_s_axi_awaddr		=> cb_m_axi_awaddr(32*JESD_A1_PORT+JESD_A1_AWIDTH-1 downto 32*JESD_A1_PORT),
		rx1_s_axi_awvalid		=> cb_m_axi_awvalid(JESD_A1_PORT),
		rx1_s_axi_awready		=> cb_m_axi_awready(JESD_A1_PORT),
		rx1_s_axi_wdata			=> cb_m_axi_wdata(32*(JESD_A1_PORT+1)-1 downto 32*JESD_A1_PORT),
		rx1_s_axi_wstrb			=> cb_m_axi_wstrb(4*(JESD_A1_PORT+1)-1 downto 4*JESD_A1_PORT),
		rx1_s_axi_wvalid		=> cb_m_axi_wvalid(JESD_A1_PORT),
		rx1_s_axi_wready		=> cb_m_axi_wready(JESD_A1_PORT),
		rx1_s_axi_bresp			=> cb_m_axi_bresp(2*(JESD_A1_PORT+1)-1 downto 2*JESD_A1_PORT),
		rx1_s_axi_bvalid		=> cb_m_axi_bvalid(JESD_A1_PORT),
		rx1_s_axi_bready		=> cb_m_axi_bready(JESD_A1_PORT),
		rx1_s_axi_araddr		=> cb_m_axi_araddr(32*JESD_A1_PORT+JESD_A1_AWIDTH-1 downto 32*JESD_A1_PORT),
		rx1_s_axi_arvalid		=> cb_m_axi_arvalid(JESD_A1_PORT),
		rx1_s_axi_arready		=> cb_m_axi_arready(JESD_A1_PORT),
		rx1_s_axi_rdata			=> cb_m_axi_rdata(32*(JESD_A1_PORT+1)-1 downto 32*JESD_A1_PORT),
		rx1_s_axi_rresp			=> cb_m_axi_rresp(2*(JESD_A1_PORT+1)-1 downto 2*JESD_A1_PORT),
		rx1_s_axi_rvalid		=> cb_m_axi_rvalid(JESD_A1_PORT),
		rx1_s_axi_rready		=> cb_m_axi_rready(JESD_A1_PORT),
		tx_s_axi_aclk			=> s_axi_aclk,
		tx_s_axi_aresetn		=> '1',
		tx_s_axi_awaddr			=> cb_m_axi_awaddr(32*JESD_D0_PORT+JESD_D0_AWIDTH-1 downto 32*JESD_D0_PORT),
		tx_s_axi_awvalid		=> cb_m_axi_awvalid(JESD_D0_PORT),
		tx_s_axi_awready		=> cb_m_axi_awready(JESD_D0_PORT),
		tx_s_axi_wdata			=> cb_m_axi_wdata(32*(JESD_D0_PORT+1)-1 downto 32*JESD_D0_PORT),
		tx_s_axi_wstrb			=> cb_m_axi_wstrb(4*(JESD_D0_PORT+1)-1 downto 4*JESD_D0_PORT),
		tx_s_axi_wvalid			=> cb_m_axi_wvalid(JESD_D0_PORT),
		tx_s_axi_wready			=> cb_m_axi_wready(JESD_D0_PORT),
		tx_s_axi_bresp			=> cb_m_axi_bresp(2*(JESD_D0_PORT+1)-1 downto 2*JESD_D0_PORT),
		tx_s_axi_bvalid			=> cb_m_axi_bvalid(JESD_D0_PORT),
		tx_s_axi_bready			=> cb_m_axi_bready(JESD_D0_PORT),
		tx_s_axi_araddr			=> cb_m_axi_araddr(32*JESD_D0_PORT+JESD_D0_AWIDTH-1 downto 32*JESD_D0_PORT),
		tx_s_axi_arvalid		=> cb_m_axi_arvalid(JESD_D0_PORT),
		tx_s_axi_arready		=> cb_m_axi_arready(JESD_D0_PORT),
		tx_s_axi_rdata			=> cb_m_axi_rdata(32*(JESD_D0_PORT+1)-1 downto 32*JESD_D0_PORT),
		tx_s_axi_rresp			=> cb_m_axi_rresp(2*(JESD_D0_PORT+1)-1 downto 2*JESD_D0_PORT),
		tx_s_axi_rvalid			=> cb_m_axi_rvalid(JESD_D0_PORT),
		tx_s_axi_rready			=> cb_m_axi_rready(JESD_D0_PORT)
	);
--	d0_tdata(255 downto 240)<=dac_data15;
--	d0_tdata(239 downto 224)<=dac_data14;
--	d0_tdata(223 downto 208)<=dac_data13;
--	d0_tdata(207 downto 192)<=dac_data12;
--	d0_tdata(191 downto 176)<=dac_data11;
--	d0_tdata(175 downto 160)<=dac_data10;
--	d0_tdata(159 downto 144)<=dac_data9;
--	d0_tdata(143 downto 128)<=dac_data8;
--	d0_tdata(127 downto 112)<=dac_data7;
--	d0_tdata(111 downto 96)<=dac_data6;
--	d0_tdata(95 downto 80)<=dac_data5;
--	d0_tdata(79 downto 64)<=dac_data4;
--	d0_tdata(63 downto 48)<=dac_data3;
--	d0_tdata(47 downto 32)<=dac_data2;
--	d0_tdata(31 downto 16)<=dac_data1;
--	d0_tdata(15 downto 0)<=dac_data0;
	
		-- ===== DAC data and pattern =============
	-- LMF=811 map (bypass mode)
	



	-- d0_tdata[ 31:  0] I12[15:8]  I8[15:8] I4[15:8] I0[15:8]
	-- d0_tdata[ 63: 32] I12[ 7:0]  I8[ 7:0] I4[ 7:0] I0[ 7:0]
	d0_tdata(31 downto 0)<=dac_data12(15 downto 8) & dac_data8(15 downto 8) & dac_data4(15 downto 8) & dac_data0(15 downto 8);
	d0_tdata(63 downto 32)<=dac_data12(7 downto 0) & dac_data8(7 downto 0) & dac_data4(7 downto 0) & dac_data0(7 downto 0);
	-- d0_tdata[ 95: 64] I13[15:8]  I9[15:8] I5[15:8] I1[15:8]
	-- d0_tdata[127: 96] I13[ 7:0]  I9[ 7:0] I5[ 7:0] I1[ 7:0]
	d0_tdata(95 downto 64)<=dac_data13(15 downto 8) & dac_data9(15 downto 8) & dac_data5(15 downto 8) & dac_data1(15 downto 8);
	d0_tdata(127 downto 96)<=dac_data13(7 downto 0) & dac_data9(7 downto 0) & dac_data5(7 downto 0) & dac_data1(7 downto 0);

	-- d0_tdata[159:128] I14[15:8] I10[15:8] I6[15:8] I2[15:8]
	-- d0_tdata[191:160] I14[ 7:0] I10[ 7:0] I6[ 7:0] I2[ 7:0]
    d0_tdata(159 downto 128)<=dac_data14(15 downto 8) & dac_data10(15 downto 8) & dac_data6(15 downto 8) & dac_data2(15 downto 8);
    d0_tdata(191 downto 160)<=dac_data14(7 downto 0) & dac_data10(7 downto 0) & dac_data6(7 downto 0) & dac_data2(7 downto 0);

	-- d0_tdata[223:192] I15[15:8] I11[15:8] I7[15:8] I3[15:8]
	-- d0_tdata[255:224] I15[ 7:0] I11[ 7:0] I7[ 7:0] I3[ 7:0]
	d0_tdata(223 downto 192)<=dac_data15(15 downto 8) & dac_data11(15 downto 8) & dac_data7(15 downto 8) & dac_data3(15 downto 8);
	d0_tdata(255 downto 224)<=dac_data15(7 downto 0) & dac_data11(7 downto 0) & dac_data7(7 downto 0) & dac_data3(7 downto 0);

	-- ===== ADC samples alignment ================
	process(a0_core_clk)
	begin
		if(rising_edge(a0_core_clk)) then
			a0_aresetn_d1		<= a0_aresetn;
			a0_tvalid_d1		<= a0_tvalid;
			a0_tdata_d1		<= a0_tdata;
			a0_start_of_frame_d1	<= a0_start_of_frame;

			a0_aresetn_d2		<= a0_aresetn_d1;
			a0_tvalid_d2		<= a0_tvalid_d1;
			a0_tdata_d2		<= a0_tdata_d1;
			a0_start_of_frame_d2	<= a0_start_of_frame_d1;

			a1_aresetn_d1		<= a1_aresetn;
			a1_tvalid_d1		<= a1_tvalid;
			a1_tdata_d1		<= a1_tdata;
			a1_start_of_frame_d1	<= a1_start_of_frame;

			a1_aresetn_d2		<= a1_aresetn_d1;
			a1_tvalid_d2		<= a1_tvalid_d1;
			a1_tdata_d2		<= a1_tdata_d1;
			a1_start_of_frame_d2	<= a1_start_of_frame_d1;
		end if;
	end process;

	adc12xj_align_inst_a0: adc12xj_align_jmode_2
	port map(
		rx_aclk				=> a0_core_clk,
		rx_aresetn			=> a0_aresetn_d1,
		jesd204b_tdata			=> a0_tdata_d1,
		jesd204b_start_of_frame		=> a0_start_of_frame_d1,
		adc_dout			=> a0_dout
	);

	adc12xj_align_inst_a1: adc12xj_align_jmode_2
	port map(
		rx_aclk				=> a0_core_clk,
		rx_aresetn			=> a1_aresetn_d1,
		jesd204b_tdata			=> a1_tdata_d1,
		jesd204b_start_of_frame		=> a1_start_of_frame_d1,
		adc_dout			=> a1_dout
	);

	-- ===== ADC read ========================
	-- FIFO reset generation. The FIFO requires a minimum asynchronous reset pulse of write clock period.
	-- After reset is detected on the rising clock edge of write clock, 3 write
	--	clock periods are required to complete proper reset synchronization. During this time, the
	--	full, almost_full, and prog_full flags are asserted. After reset is deasserted, these
	--	flags deassert after three clock periods (wr_clk/clk) and the FIFO can then accept write
	--	operations.
	rst_fifo				<= adc_acq_ctrl(2);

	-- Synchronized with each adc clk to enable write to fifo
	adc_wr_en				<= adc_acq_ctrl(3);

	process(a0_core_clk)
	begin
		if(rising_edge(a0_core_clk)) then
			fifo_wr_en		<= adc_wr_en;
		
			a0_dout_d1		<= a0_dout;
			a1_dout_d1		<= a1_dout;
		end if;
	end process;

	------- Fifo output aspect ratio re-ordering
	 --split 10 adc samples to 8 and 2 samples. convert samples to 16-bit and reorder
	 --a0_dout_d1 (12-bit): N9 N8 ... N0
	 --a0_rd_f0_din_ar (16-bit 0000+12-bit data): N0 N1 ... N7
	 --a0_rd_f1_din_ar (16-bit 0000+12-bit data): N8 N9
	
	process(a0_core_clk)
	begin
		if(rising_edge(a0_core_clk)) then
			loop_fifo_16: for i in 7 downto 0 loop
				a0_rd_f0_din_ar(16*(i+1)-1 downto 16*(i))	<= x"0" & a0_dout_d1(12*(7-i+1)-1 downto 12*(7-i));
				a1_rd_f0_din_ar(16*(i+1)-1 downto 16*(i))	<= x"0" & a1_dout_d1(12*(7-i+1)-1 downto 12*(7-i));
			end loop;
		
			loop_fifo_4: for i in 1 downto 0 loop
				a0_rd_f1_din_ar(16*(i+1)-1 downto 16*(i))	<= x"0" & a0_dout_d1(12*(9-i+1)-1 downto 12*(9-i));
				a1_rd_f1_din_ar(16*(i+1)-1 downto 16*(i))	<= x"0" & a1_dout_d1(12*(9-i+1)-1 downto 12*(9-i));
			end loop;		
		end if;
	end process;

	--Deinterleave and assign ADC Data to outputs
	adc_data0  <= a0_dout_d1(11 downto 0);
	adc_data1  <= a1_dout_d1(11 downto 0);
	adc_data2  <= a0_dout_d1(23 downto 12);
	adc_data3  <= a1_dout_d1(23 downto 12);
	adc_data4	 <= a0_dout_d1(35 downto 24);
	adc_data5	 <= a1_dout_d1(35 downto 24);
	adc_data6	 <= a0_dout_d1(47 downto 36);
	adc_data7	 <= a1_dout_d1(47 downto 36);
	adc_data8	 <= a0_dout_d1(59 downto 48);
	adc_data9	 <= a1_dout_d1(59 downto 48);
	adc_data10 <= a0_dout_d1(71 downto 60);
	adc_data11 <= a1_dout_d1(71 downto 60);
	adc_data12 <= a0_dout_d1(83 downto 72);
	adc_data13 <= a1_dout_d1(83 downto 72);
	adc_data14 <= a0_dout_d1(95 downto 84);
	adc_data15 <= a1_dout_d1(95 downto 84);
	adc_data16 <= a0_dout_d1(107 downto 96);
	adc_data17 <= a1_dout_d1(107 downto 96);
	adc_data18 <= a0_dout_d1(119 downto 108);
	adc_data19 <= a1_dout_d1(119 downto 108);
	 
	
	 --The FIFOs have been commented out for now as the use too many BRAM elements to implement the design R.Borner 12/01/2021

	-- Instantiate ADC read fifo
	-- adc0 link 1 (ADC0 channel A) data fifo
	fifo_generator_8_inst_a0: fifo_generator_8
	port map(
		rst				=> rst_fifo,
		wr_clk				=> a0_core_clk,
		rd_clk				=> s_axi_aclk, --This should be deleted at some point
		din				=> a0_rd_f0_din_ar,
		wr_en				=> fifo_wr_en,
		rd_en				=> a0_rd_f0_rd_en,
		dout				=> a0_rd_f0_dout,
		full				=> a0_rd_f0_full,
		empty				=> a0_rd_f0_empty,
		rd_data_count			=> open
	);

	fifo_generator_2_inst_a0: fifo_generator_2 
	port map(
		rst				=> rst_fifo,
		wr_clk				=> a0_core_clk,
		rd_clk				=> s_axi_aclk,
		din				=> a0_rd_f1_din_ar,
		wr_en				=> fifo_wr_en,
		rd_en				=> a0_rd_f1_rd_en,
		dout				=> a0_rd_f1_dout,
		full				=> a0_rd_f1_full,
		empty				=> a0_rd_f1_empty,
		rd_data_count			=> open
	);
	 --adc0 link 1 (ADC0 channel B) data fifo
	fifo_generator_8_inst_a1: fifo_generator_8
	port map(
		rst				=> rst_fifo,
		wr_clk				=> a0_core_clk,
		rd_clk				=> s_axi_aclk,
		din				=> a1_rd_f0_din_ar,
		wr_en				=> fifo_wr_en,
		rd_en				=> a1_rd_f0_rd_en,
		dout				=> a1_rd_f0_dout,
		full				=> a1_rd_f0_full,
		empty				=> a1_rd_f0_empty,
		rd_data_count			=> open
	);

	fifo_generator_2_inst_a1: fifo_generator_2
	port map(
		rst				=> rst_fifo,
		wr_clk				=> a0_core_clk,
		rd_clk				=> s_axi_aclk,
		din				=> a1_rd_f1_din_ar,
		wr_en				=> fifo_wr_en,
		rd_en				=> a1_rd_f1_rd_en,
		dout				=> a1_rd_f1_dout,
		full				=> a1_rd_f1_full,
		empty				=> a1_rd_f1_empty,
		rd_data_count			=> open
	);

	-- Read ADC
	axi_lite_rd_fifo_inst_adc_8sa: axi_lite_rd_fifo
	generic map (
		LITTLE_ENDIAN16			=> false
	)
	port map(
		s_axi_aclk			=> s_axi_aclk,
		s_axi_aresetn			=> s_axi_aresetn,
		s_axi_awaddr			=> cb_m_axi_awaddr(32*A0_RD_PORT+A0_RD_AWIDTH-1 downto 32*A0_RD_PORT),
		s_axi_awvalid			=> cb_m_axi_awvalid(A0_RD_PORT),
		s_axi_awready			=> cb_m_axi_awready(A0_RD_PORT),
		s_axi_wdata			=> cb_m_axi_wdata(32*(A0_RD_PORT+1)-1 downto 32*A0_RD_PORT),
		s_axi_wstrb			=> cb_m_axi_wstrb(4*(A0_RD_PORT+1)-1 downto 4*A0_RD_PORT),
		s_axi_wvalid			=> cb_m_axi_wvalid(A0_RD_PORT),
		s_axi_wready			=> cb_m_axi_wready(A0_RD_PORT),
		s_axi_bresp			=> cb_m_axi_bresp(2*(A0_RD_PORT+1)-1 downto 2*A0_RD_PORT),
		s_axi_bvalid			=> cb_m_axi_bvalid(A0_RD_PORT),
		s_axi_bready			=> cb_m_axi_bready(A0_RD_PORT),
		s_axi_araddr			=> cb_m_axi_araddr(32*A0_RD_PORT+A0_RD_AWIDTH-1 downto 32*A0_RD_PORT),
		s_axi_arvalid			=> cb_m_axi_arvalid(A0_RD_PORT),
		s_axi_arready			=> cb_m_axi_arready(A0_RD_PORT),
		s_axi_rdata			=> cb_m_axi_rdata(32*(A0_RD_PORT+1)-1 downto 32*A0_RD_PORT),
		s_axi_rresp			=> cb_m_axi_rresp(2*(A0_RD_PORT+1)-1 downto 2*A0_RD_PORT),
		s_axi_rvalid			=> cb_m_axi_rvalid(A0_RD_PORT),
		s_axi_rready			=> cb_m_axi_rready(A0_RD_PORT),
		fifo_rd_en			=> adc_rd_f0_rd_en,
		fifo_dout			=> adc_rd_f0_dout
	);

	process(s_axi_aclk)
	begin
		if(rising_edge(s_axi_aclk)) then
			a0_rd_f0_rd_en		<= adc_rd_f0_rd_en and (not adc_ch_sel);
			a1_rd_f0_rd_en		<= adc_rd_f0_rd_en and adc_ch_sel;
		end if;
	end process;
	-- a0_rd_f0_rd_en				<= adc_rd_f0_rd_en when adc_ch_sel = '0' else '0';
	-- a1_rd_f0_rd_en				<= adc_rd_f0_rd_en when adc_ch_sel = '1' else '0';
	adc_rd_f0_dout				<= a0_rd_f0_dout when adc_ch_sel = '0' else a1_rd_f0_dout;


	axi_lite_rd_fifo_inst_adc_2sa: axi_lite_rd_fifo
	generic map (
		LITTLE_ENDIAN16			=> false
	)
	port map(
		s_axi_aclk			=> s_axi_aclk,
		s_axi_aresetn			=> s_axi_aresetn,
		s_axi_awaddr			=> cb_m_axi_awaddr(32*A1_RD_PORT+A1_RD_AWIDTH-1 downto 32*A1_RD_PORT),
		s_axi_awvalid			=> cb_m_axi_awvalid(A1_RD_PORT),
		s_axi_awready			=> cb_m_axi_awready(A1_RD_PORT),
		s_axi_wdata			=> cb_m_axi_wdata(32*(A1_RD_PORT+1)-1 downto 32*A1_RD_PORT),
		s_axi_wstrb			=> cb_m_axi_wstrb(4*(A1_RD_PORT+1)-1 downto 4*A1_RD_PORT),
		s_axi_wvalid			=> cb_m_axi_wvalid(A1_RD_PORT),
		s_axi_wready			=> cb_m_axi_wready(A1_RD_PORT),
		s_axi_bresp			=> cb_m_axi_bresp(2*(A1_RD_PORT+1)-1 downto 2*A1_RD_PORT),
		s_axi_bvalid			=> cb_m_axi_bvalid(A1_RD_PORT),
		s_axi_bready			=> cb_m_axi_bready(A1_RD_PORT),
		s_axi_araddr			=> cb_m_axi_araddr(32*A1_RD_PORT+A1_RD_AWIDTH-1 downto 32*A1_RD_PORT),
		s_axi_arvalid			=> cb_m_axi_arvalid(A1_RD_PORT),
		s_axi_arready			=> cb_m_axi_arready(A1_RD_PORT),
		s_axi_rdata			=> cb_m_axi_rdata(32*(A1_RD_PORT+1)-1 downto 32*A1_RD_PORT),
		s_axi_rresp			=> cb_m_axi_rresp(2*(A1_RD_PORT+1)-1 downto 2*A1_RD_PORT),
		s_axi_rvalid			=> cb_m_axi_rvalid(A1_RD_PORT),
		s_axi_rready			=> cb_m_axi_rready(A1_RD_PORT),
		fifo_rd_en			=> adc_rd_f1_rd_en,
		fifo_dout			=> adc_rd_f1_dout
	);

	process(s_axi_aclk)
	begin
		if(rising_edge(s_axi_aclk)) then
			a0_rd_f1_rd_en		<= adc_rd_f1_rd_en and (not adc_ch_sel);
			a1_rd_f1_rd_en		<= adc_rd_f1_rd_en and adc_ch_sel;
		end if;
	end process;

	-- a0_rd_f1_rd_en				<= adc_rd_f1_rd_en when adc_ch_sel = '0' else '0';
	-- a1_rd_f1_rd_en				<= adc_rd_f1_rd_en when adc_ch_sel = '1' else '0';
	adc_rd_f1_dout				<= a0_rd_f1_dout when adc_ch_sel = '0' else a1_rd_f1_dout;


	
	-- ===== ADC to DAC loop back for ULTRASCALE/GTH transceiver =============
	-- DAC core clock 7G/40=175MHz. Samples/clock=2.8G/175=16
	-- ADC core clock 11.2G/40=280MHz. Samples/clock=2.8G/280=10

	-- 16*5=10*8
	-- fifo write 20 samples @280MHz @1/2 rate
	-- fifo read 20 samples @175MHz @4/5 rate

	-- a0_dout_d1 (12-bit): N9 N8 ... N0

	process(a0_core_clk)
	begin
		if(rising_edge(a0_core_clk)) then
			a0_dout_d2		<= a0_dout_d1;
			a0_dout_d3		<= a0_dout_d2;
			a1_dout_d2		<= a1_dout_d1;
			a1_dout_d3		<= a1_dout_d2;

			lf_wr_en		<= not lf_wr_en;

			if(lf_wr_en = '1') then
				a0_dout_db1	<= a0_dout_d2 & a0_dout_d3;
				a1_dout_db1	<= a1_dout_d2 & a1_dout_d3;
			end if;

			if(adc_ch_sel_sync = '0') then
				lf_din		<= a0_dout_db1;
				fir_din_12	<= a0_dout_d3;
			else
				lf_din		<= a1_dout_db1;
				fir_din_12	<= a1_dout_d3;
			end if;
			
			-- change to width required by the FIR
			for_i: for i in 9 downto 0 loop
				fir_din(16*(i+1)-5 downto 16*i)		<= fir_din_12(12*(i+1)-1 downto 12*i);
			end loop;
			
		end if;
	end process;

	lf_rst_pre 				<= not (a0_aresetn and a1_aresetn and d0_aresetn);

	-- set false path for lf_rst
	vt_single_sync_inst_lf_rst: vt_single_sync
	generic map(
		STAGES				=> 2,
		STARTUP_VALUE			=> '1'
	)
	port map(
		clk				=> d0_core_clk,
		port_i				=> lf_rst_pre,
		port_o				=> lf_rst
	);


	
	

	-- ===== DAC data and pattern =============
	-- LMF=811 map (bypass mode)
	-- d0_tdata[ 31:  0] I12[15:8]  I8[15:8] I4[15:8] I0[15:8]
	-- d0_tdata[ 63: 32] I12[ 7:0]  I8[ 7:0] I4[ 7:0] I0[ 7:0]
	-- d0_tdata[ 95: 64] I13[15:8]  I9[15:8] I5[15:8] I1[15:8]
	-- d0_tdata[127: 96] I13[ 7:0]  I9[ 7:0] I5[ 7:0] I1[ 7:0]
	-- d0_tdata[159:128] I14[15:8] I10[15:8] I6[15:8] I2[15:8]
	-- d0_tdata[191:160] I14[ 7:0] I10[ 7:0] I6[ 7:0] I2[ 7:0]
	-- d0_tdata[223:192] I15[15:8] I11[15:8] I7[15:8] I3[15:8]
	-- d0_tdata[255:224] I15[ 7:0] I11[ 7:0] I7[ 7:0] I3[ 7:0]

	-- dac0 BRAM data
	axi_register_slice_0_inst_d0: axi_register_slice_0
	port map(
		aclk				=> s_axi_aclk,
		aresetn				=> s_axi_aresetn,
		s_axi_awaddr			=> cb_m_axi_awaddr(32*BRAM_D0_PORT+32-1 downto 32*BRAM_D0_PORT),
		s_axi_awprot 			=> "000",
		s_axi_awvalid			=> cb_m_axi_awvalid(BRAM_D0_PORT),
		s_axi_awready			=> cb_m_axi_awready(BRAM_D0_PORT),
		s_axi_wdata			=> cb_m_axi_wdata(32*(BRAM_D0_PORT+1)-1 downto 32*BRAM_D0_PORT),
		s_axi_wstrb			=> cb_m_axi_wstrb(4*(BRAM_D0_PORT+1)-1 downto 4*BRAM_D0_PORT),
		s_axi_wvalid			=> cb_m_axi_wvalid(BRAM_D0_PORT),
		s_axi_wready			=> cb_m_axi_wready(BRAM_D0_PORT),
		s_axi_bresp			=> cb_m_axi_bresp(2*(BRAM_D0_PORT+1)-1 downto 2*BRAM_D0_PORT),
		s_axi_bvalid			=> cb_m_axi_bvalid(BRAM_D0_PORT),
		s_axi_bready			=> cb_m_axi_bready(BRAM_D0_PORT),
		s_axi_araddr			=> cb_m_axi_araddr(32*BRAM_D0_PORT+32-1 downto 32*BRAM_D0_PORT),
		s_axi_arprot 			=> "000",
		s_axi_arvalid			=> cb_m_axi_arvalid(BRAM_D0_PORT),
		s_axi_arready			=> cb_m_axi_arready(BRAM_D0_PORT),
		s_axi_rdata			=> cb_m_axi_rdata(32*(BRAM_D0_PORT+1)-1 downto 32*BRAM_D0_PORT),
		s_axi_rresp			=> cb_m_axi_rresp(2*(BRAM_D0_PORT+1)-1 downto 2*BRAM_D0_PORT),
		s_axi_rvalid			=> cb_m_axi_rvalid(BRAM_D0_PORT),
		s_axi_rready			=> cb_m_axi_rready(BRAM_D0_PORT),
		m_axi_awaddr			=> bram_d0_axi_awaddr,
		m_axi_awprot 			=> bram_d0_axi_awprot,
		m_axi_awvalid			=> bram_d0_axi_awvalid,
		m_axi_awready			=> bram_d0_axi_awready,
		m_axi_wdata			=> bram_d0_axi_wdata,
		m_axi_wstrb			=> bram_d0_axi_wstrb,
		m_axi_wvalid			=> bram_d0_axi_wvalid,
		m_axi_wready			=> bram_d0_axi_wready,
		m_axi_bresp			=> bram_d0_axi_bresp,
		m_axi_bvalid			=> bram_d0_axi_bvalid,
		m_axi_bready			=> bram_d0_axi_bready,
		m_axi_araddr			=> bram_d0_axi_araddr,
		m_axi_arprot 			=> bram_d0_axi_arprot,
		m_axi_arvalid			=> bram_d0_axi_arvalid,
		m_axi_arready			=> bram_d0_axi_arready,
		m_axi_rdata			=> bram_d0_axi_rdata,
		m_axi_rresp			=> bram_d0_axi_rresp,
		m_axi_rvalid			=> bram_d0_axi_rvalid,
		m_axi_rready			=> bram_d0_axi_rready
	);

	axi_bram_ctrl_0_inst_d0: axi_bram_ctrl_0
	port map (
		s_axi_aclk			=> s_axi_aclk,
		s_axi_aresetn			=> s_axi_aresetn,
		s_axi_awaddr			=> bram_d0_axi_awaddr(BRAM_D0_AWIDTH-1 downto 0),
		s_axi_awprot 			=> bram_d0_axi_awprot,
		s_axi_awvalid			=> bram_d0_axi_awvalid,
		s_axi_awready			=> bram_d0_axi_awready,
		s_axi_wdata			=> bram_d0_axi_wdata,
		s_axi_wstrb			=> bram_d0_axi_wstrb,
		s_axi_wvalid			=> bram_d0_axi_wvalid,
		s_axi_wready			=> bram_d0_axi_wready,
		s_axi_bresp			=> bram_d0_axi_bresp,
		s_axi_bvalid			=> bram_d0_axi_bvalid,
		s_axi_bready			=> bram_d0_axi_bready,
		s_axi_araddr			=> bram_d0_axi_araddr(BRAM_D0_AWIDTH-1 downto 0),
		s_axi_arprot 			=> bram_d0_axi_arprot,
		s_axi_arvalid			=> bram_d0_axi_arvalid,
		s_axi_arready			=> bram_d0_axi_arready,
		s_axi_rdata			=> bram_d0_axi_rdata,
		s_axi_rresp			=> bram_d0_axi_rresp,
		s_axi_rvalid			=> bram_d0_axi_rvalid,
		s_axi_rready			=> bram_d0_axi_rready,
		bram_rst_a  			=> open,
		bram_clk_a  			=> open,
		bram_en_a   			=> bram_d0_en_i,
		bram_we_a   			=> bram_d0_we_i,
		bram_addr_a 			=> bram_d0_addr_i,
		bram_wrdata_a 			=> bram_d0_din_i,
		bram_rddata_a 			=> bram_d0_dout_i
	);

	-- when 'Byte Write Enable' is set, blk_mem_gen_0 doesn't work correctly
	-- work around here
	process(s_axi_aclk)
	begin
		if(rising_edge(s_axi_aclk)) then
			bram_d0_we_i_d1		<= bram_d0_we_i(3);

			for i in 3 downto 0 loop
				if(bram_d0_we_i(i) = '1') then
					bram_d0_din_i_w(8*(i+1)-1 downto 8*i)
						<= bram_d0_din_i(8*(i+1)-1 downto 8*i);
				end if;
			end loop;
		end if;
	end process;

	--Commented out to save resources

	-- BRAM input[31:0]:   	I1[15:0] I0[15:0]
	-- BRAM output[255:0]: 	I15[15:0] I14[15:0] ... I1[15:0] I0[15:0]
	blk_mem_gen_0_inst_d0: blk_mem_gen_0
	port map(
		clka  				=> s_axi_aclk,
		ena   				=> '1',
		wea(0) 				=> bram_d0_we_i_d1,
		addra 				=> bram_d0_addr_i(17 downto 2),
		dina  				=> bram_d0_din_i_w,
		douta  				=> bram_d0_dout_i,
		clkb  				=> d0_core_clk,
		enb   				=> '1',
		web   				=> (others => '0'),
		addrb 				=> bram_d0_addr_b_d1,
		dinb 				=> (others => '0'),
		doutb 				=> bram_d0_dout_b
	);

	-- BRAM read address
	process(d0_core_clk)
	begin
		if(rising_edge(d0_core_clk)) then
			dac_mem_rd_len_d1	<= dac_mem_rd_len;
			dac_mem_rd_len_d2	<= dac_mem_rd_len_d1;
			dac_mem_rd_len_1_d1	<= std_logic_vector(unsigned(dac_mem_rd_len) - 1);
			dac_ptn_sync		<= dac_ptn;
			dac_ptn_d1		<= dac_ptn_sync;
			dac_ptn_d2		<= dac_ptn_d1;
			bram_d0_addr_b_d1	<= bram_d0_addr_b;

			if(dac_mem_rd_len_d2 = "00" & x"000" or dac_mem_rd_len_1_d1 = "00" & x"000") then
				bram_d0_addr_b	<= (others => '0');
			elsif(dac_ptn_d2 = x"1" and bram_d0_addr_b < dac_mem_rd_len_1_d1(12 downto 0)) then
				bram_d0_addr_b	<= std_logic_vector(unsigned(bram_d0_addr_b) + 1);
			else
				bram_d0_addr_b	<= (others => '0');
			end if;
		end if;
	end process;

	-- fdac/16 tone pattern
	sine_out_c16	<= 	x"0000" &
				x"30FB" &
				x"5A81" &
				x"7640" &
				x"7FFF" &
				x"7640" &
				x"5A81" &
				x"30FB" &
				x"0000" &
				x"CF05" &
				x"A57F" &
				x"89C0" &
				x"8001" &
				x"89C0" &
				x"A57F" &
				x"CF05"
				;


	
	-- ================ others ===============================
	-- Connect front panel trig_in (A=0)
	IBUFDS_inst_trig_in0 : IBUFDS
	port map (
		O				=> trigger_in,
		I				=> FMC_LA_P18,
		IB				=> FMC_LA_N18
	);

	-- Connect front panel trig_out (A=0 only)
	OBUFDS_inst_trig_out : OBUFDS
	port map (
		I 				=> trigger_out,
		O				=> FMC_LA_P23,
		OB				=> FMC_LA_N23
	);

	process(a0_core_clk)
	begin
		if(rising_edge(a0_core_clk)) then
			a0_core_clk_div2	<= not a0_core_clk_div2;
		end if;
	end process;

	process(d0_core_clk)
	begin
		if(rising_edge(d0_core_clk)) then
			d0_core_clk_div2	<= not d0_core_clk_div2;
		end if;
	end process;

	-- only available when ordering option A=0
	trigger_out				<= a0_core_clk_div2		when trig_out_sel = x"1" else
						   d0_core_clk_div2		when trig_out_sel = x"2" else
						   a0_sysref			when trig_out_sel = x"3" else
						   d0_sysref			when trig_out_sel = x"4" else
						   a0_sync			when trig_out_sel = x"5" else
						   a1_sync			when trig_out_sel = x"6" else
						   d0_sync			when trig_out_sel = x"7" else
						   a0_tvalid			when trig_out_sel = x"8" else
						   a1_tvalid			when trig_out_sel = x"9" else
						   d0_tready			when trig_out_sel = x"a" else
						   s_axi_aclk			when trig_out_sel = x"b" else
						   a0_aresetn			when trig_out_sel = x"c" else
						   a1_aresetn			when trig_out_sel = x"d" else
						   trigger_in;

end Behavioral;
