----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/06/2023 06:21:59 PM
-- Design Name: 
-- Module Name: sampler - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity sampler1 is
    Port ( clk : in STD_LOGIC;
           tdata : in STD_LOGIC_VECTOR (15 downto 0);
           out0 : out STD_LOGIC_VECTOR (11 downto 0);
           out1 : out STD_LOGIC_VECTOR (11 downto 0);
           out2 : out STD_LOGIC_VECTOR (11 downto 0);
           out3 : out STD_LOGIC_VECTOR (11 downto 0);
           out4 : out STD_LOGIC_VECTOR (11 downto 0);
           out5 : out STD_LOGIC_VECTOR (11 downto 0);
           out6 : out STD_LOGIC_VECTOR (11 downto 0);
           out7 : out STD_LOGIC_VECTOR (11 downto 0);
           out8 : out STD_LOGIC_VECTOR (11 downto 0);
           out9 : out STD_LOGIC_VECTOR (11 downto 0);
           out10 : out STD_LOGIC_VECTOR (11 downto 0);
           out11 : out STD_LOGIC_VECTOR (11 downto 0);
           out12 : out STD_LOGIC_VECTOR (11 downto 0);
           out13 : out STD_LOGIC_VECTOR (11 downto 0);
           out14 : out STD_LOGIC_VECTOR (11 downto 0);
           out15 : out STD_LOGIC_VECTOR (11 downto 0);
           out16 : out STD_LOGIC_VECTOR (11 downto 0);
           out17 : out STD_LOGIC_VECTOR (11 downto 0);
           out18 : out STD_LOGIC_VECTOR (11 downto 0);
           out19 : out STD_LOGIC_VECTOR (11 downto 0)
           );
end sampler1;

architecture Behavioral of sampler1 is
type State is (state0,state1,state2,state3,state4,state5,state6,state7,state8,state9,state10,state11,state12,state13,state14,state15,state16,state17,state18,state19);

signal currentState : State :=state0; 
signal out0_s : std_logic_vector(11 downto 0);
signal out1_s : std_logic_vector(11 downto 0);
signal out2_s : std_logic_vector(11 downto 0);
signal out3_s : std_logic_vector(11 downto 0);
signal out4_s : std_logic_vector(11 downto 0);
signal out5_s : std_logic_vector(11 downto 0);
signal out6_s : std_logic_vector(11 downto 0);
signal out7_s : std_logic_vector(11 downto 0);
signal out8_s : std_logic_vector(11 downto 0);
signal out9_s : std_logic_vector(11 downto 0);
signal out10_s : std_logic_vector(11 downto 0);
signal out11_s : std_logic_vector(11 downto 0);
signal out12_s : std_logic_vector(11 downto 0);
signal out13_s : std_logic_vector(11 downto 0);
signal out14_s : std_logic_vector(11 downto 0);
signal out15_s : std_logic_vector(11 downto 0);
signal out16_s : std_logic_vector(11 downto 0);
signal out17_s : std_logic_vector(11 downto 0);
signal out18_s : std_logic_vector(11 downto 0);
begin

process(clk) is 

begin
if rising_edge(clk) then
case currentState is
    when state0=>
        out0_s<=tdata(11 downto 0);
        currentState<=state1;
    when state1=>
        out1_s<=tdata(11 downto 0);
        currentState<=state2;
    when state2=>
        out2_s<=tdata(11 downto 0);
        currentState<=state3;
    when state3=>
        out3_s<=tdata(11 downto 0);
        currentState<=state4;
    when state4=>
        out4_s<=tdata(11 downto 0);
        currentState<=state5;
    when state5=>
        out5_s<=tdata(11 downto 0);
        currentState<=state6;
    when state6=>
        out6_s<=tdata(11 downto 0);
        currentState<=state7;
    when state7=>
        out7_s<=tdata(11 downto 0);
        currentState<=state8;
    when state8=>
        out8_s<=tdata(11 downto 0);
        currentState<=state9;
    when state9=>
        out9_s<=tdata(11 downto 0);
        currentState<=state10;
    when state10=>
        out10_s<=tdata(11 downto 0);
        currentState<=state11;
    when state11=>
        out11_s<=tdata(11 downto 0);
        currentState<=state12;
    when state12=>
        out12_s<=tdata(11 downto 0);
        currentState<=state13;
    when state13=>
        out13_s<=tdata(11 downto 0);
        currentState<=state14;
    when state14=>
        out14_s<=tdata(11 downto 0);
        currentState<=state15;
    when state15=>
        out15_s<=tdata(11 downto 0);
        currentState<=state16;
    when state16=>
        out16_s<=tdata(11 downto 0);
        currentState<=state17;
    when state17=>
        out17_s<=tdata(11 downto 0);
        currentState<=state18;
    when state18=>
        out18_s<=tdata(11 downto 0);
        currentState<=state19;
    when state19=>
        out0<=out0_s;
        out1<=out1_s;
        out2<=out2_s;
        out3<=out3_s;
        out4<=out4_s;
        out5<=out5_s;
        out6<=out6_s;
        out7<=out7_s;
        out8<=out8_s;
        out9<=out9_s;
        out10<=out10_s;
        out11<=out11_s;
        out12<=out12_s;
        out13<=out13_s;
        out14<=out14_s;
        out15<=out15_s;
        out16<=out16_s;
        out17<=out17_s;
        out18<=out18_s;
        out19<=tdata(11 downto 0);
        currentState<=state0;


end case;

end if;

end process;


end Behavioral;
