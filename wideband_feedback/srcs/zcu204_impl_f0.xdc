# fmc clocks
create_clock -period 3.125 -name adc_refclk [get_ports FMC0_GBTCLK0_P]
create_clock -period 5.000 -name dac_refclk [get_ports FMC0_GBTCLK1_P]