
# HMC7043 SCLKOUT3, ADC JESD204B core sysref clock (fs/640 = 4.6875 MHz)
set_property PACKAGE_PIN Y4 [get_ports FMC0_LA_P0]
set_property PACKAGE_PIN Y3 [get_ports FMC0_LA_N0]
set_property IOSTANDARD LVDS [get_ports FMC0_LA_P0]
set_property DIFF_TERM_ADV TERM_100 [get_ports FMC0_LA_P0]

# LMK04828 SDCLKOUT3, DAC JESD204B core sysref clock: LMFC (fs/128 = 23.4375 MHz)
set_property PACKAGE_PIN AB4 [get_ports FMC0_LA_P1]
set_property PACKAGE_PIN AC4 [get_ports FMC0_LA_N1]
set_property IOSTANDARD LVDS [get_ports FMC0_LA_P1]
set_property DIFF_TERM_ADV TERM_100 [get_ports FMC0_LA_P1]

# ADC12DJ3200 NCOA0 
set_property PACKAGE_PIN V2 [get_ports FMC0_LA_P2]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_P2]

# ADC12DJ3200 NCOA1 
set_property PACKAGE_PIN V1 [get_ports FMC0_LA_N2]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_N2]

# ADC12DJ3200 NCOB0 
set_property PACKAGE_PIN Y2 [get_ports FMC0_LA_P3]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_P3]

# ADC12DJ3200 NCOB1 
set_property PACKAGE_PIN Y1 [get_ports FMC0_LA_N3]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_N3]

# ADC12DJ3200 TMSTP (OUT)
set_property PACKAGE_PIN AA2 [get_ports FMC0_LA_P4]
set_property PACKAGE_PIN AA1 [get_ports FMC0_LA_N4]
set_property IOSTANDARD LVDS [get_ports FMC0_LA_P4]

# ADC12DJ3200 ORA0 
set_property PACKAGE_PIN AB3 [get_ports FMC0_LA_P5]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_P5]

# ADC12DJ3200 ORA1 
set_property PACKAGE_PIN AC3 [get_ports FMC0_LA_N5]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_N5]

set_property PACKAGE_PIN AC2 [get_ports FMC0_LA_P6]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_P6]

set_property PACKAGE_PIN AC1 [get_ports FMC0_LA_N6]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_N6]

set_property PACKAGE_PIN U5 [get_ports FMC0_LA_P7]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_P7]

set_property PACKAGE_PIN U4 [get_ports FMC0_LA_N7]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_N7]

set_property PACKAGE_PIN V4 [get_ports FMC0_LA_P8]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_P8]

set_property PACKAGE_PIN V3 [get_ports FMC0_LA_N8]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_N8]

set_property PACKAGE_PIN W2 [get_ports FMC0_LA_P9]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_P9]

set_property PACKAGE_PIN W1 [get_ports FMC0_LA_N9]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_N9]

set_property PACKAGE_PIN W5 [get_ports FMC0_LA_P10]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_P10]

set_property PACKAGE_PIN W4 [get_ports FMC0_LA_N10]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_N10]

# HMC7043 RFSYNCIN (OUT)
set_property PACKAGE_PIN AB6 [get_ports FMC0_LA_P11]
set_property PACKAGE_PIN AB5 [get_ports FMC0_LA_N11]
set_property IOSTANDARD LVDS [get_ports FMC0_LA_P11]


set_property PACKAGE_PIN W7 [get_ports FMC0_LA_P12]
set_property PACKAGE_PIN W6 [get_ports FMC0_LA_N12]

set_property PACKAGE_PIN AB8 [get_ports FMC0_LA_P13]
set_property PACKAGE_PIN AC8 [get_ports FMC0_LA_N13]

set_property PACKAGE_PIN AC7 [get_ports FMC0_LA_P14]
set_property PACKAGE_PIN AC6 [get_ports FMC0_LA_N14]

set_property PACKAGE_PIN Y10 [get_ports FMC0_LA_P15]
set_property PACKAGE_PIN Y9 [get_ports FMC0_LA_N15]

set_property PACKAGE_PIN Y12 [get_ports FMC0_LA_P16]
set_property PACKAGE_PIN AA12 [get_ports FMC0_LA_N16]

# AD9162 SYNCOUT (IN)
set_property PACKAGE_PIN P11 [get_ports FMC0_LA_P17]
set_property PACKAGE_PIN N11 [get_ports FMC0_LA_N17]
set_property IOSTANDARD LVDS [get_ports FMC0_LA_P17]
set_property DIFF_TERM_ADV TERM_100 [get_ports FMC0_LA_P17]

# Front Panel TRIG IN
set_property PACKAGE_PIN N9 [get_ports FMC0_LA_P18]
set_property PACKAGE_PIN N8 [get_ports FMC0_LA_N18]
set_property IOSTANDARD LVDS [get_ports FMC0_LA_P18]
set_property DIFF_TERM_ADV TERM_100 [get_ports FMC0_LA_P18]


set_property PACKAGE_PIN L13 [get_ports FMC0_LA_P19]
set_property PACKAGE_PIN K13 [get_ports FMC0_LA_N19]

set_property PACKAGE_PIN N13 [get_ports FMC0_LA_P20]
set_property PACKAGE_PIN M13 [get_ports FMC0_LA_N20]

set_property PACKAGE_PIN P12 [get_ports FMC0_LA_P21]
set_property PACKAGE_PIN N12 [get_ports FMC0_LA_N21]

set_property PACKAGE_PIN M15 [get_ports FMC0_LA_P22]
# set_property PACKAGE_PIN A31 [get_ports FMC0_LA_N22]

# Front Panel TRIG OUT
set_property PACKAGE_PIN L16 [get_ports FMC0_LA_P23]
set_property PACKAGE_PIN K16 [get_ports FMC0_LA_N23]
set_property IOSTANDARD LVDS [get_ports FMC0_LA_P23]


set_property PACKAGE_PIN L12 [get_ports FMC0_LA_P24]
set_property PACKAGE_PIN K12 [get_ports FMC0_LA_N24]

set_property PACKAGE_PIN M11 [get_ports FMC0_LA_P25]
set_property PACKAGE_PIN L11 [get_ports FMC0_LA_N25]

set_property PACKAGE_PIN L15 [get_ports FMC0_LA_P26]
set_property PACKAGE_PIN K15 [get_ports FMC0_LA_N26]

set_property PACKAGE_PIN M10 [get_ports FMC0_LA_P27]
set_property PACKAGE_PIN L10 [get_ports FMC0_LA_N27]

set_property PACKAGE_PIN T7 [get_ports FMC0_LA_P28]
set_property PACKAGE_PIN T6 [get_ports FMC0_LA_N28]

set_property PACKAGE_PIN U9 [get_ports FMC0_LA_P29]
set_property PACKAGE_PIN U8 [get_ports FMC0_LA_N29]

# set_property PACKAGE_PIN A25 [get_ports FMC0_LA_P30]
set_property PACKAGE_PIN U6 [get_ports FMC0_LA_N30]

set_property PACKAGE_PIN V8 [get_ports FMC0_LA_P31]
set_property PACKAGE_PIN V7 [get_ports FMC0_LA_N31]

set_property PACKAGE_PIN U11 [get_ports FMC0_LA_P32]
set_property PACKAGE_PIN T11 [get_ports FMC0_LA_N32]

set_property PACKAGE_PIN V11 [get_ports FMC0_LA_P33]
set_property PACKAGE_PIN V12 [get_ports FMC0_LA_N33]


## ------ FMC0 GT
# NB: see UG912 (V2015.4), p.228: IOSTANDARDS cannot be aplied to GTs
# NB: see UG576 (V1.6), p.326: MGTREFCLKP/N internally terminated with 100? differential impedance
set_property PACKAGE_PIN G8 [get_ports FMC0_GBTCLK0_P]
set_property PACKAGE_PIN G7 [get_ports FMC0_GBTCLK0_N]

set_property PACKAGE_PIN L8 [get_ports FMC0_GBTCLK1_P]
set_property PACKAGE_PIN L7  [get_ports FMC0_GBTCLK1_N]



## ----- setting GTH channels for FMC ADC 
set_property PACKAGE_PIN H2 [get_ports FMC0_RX0_P]
set_property PACKAGE_PIN H1 [get_ports FMC0_RX0_N]

set_property PACKAGE_PIN J4 [get_ports FMC0_RX1_P]
set_property PACKAGE_PIN J3 [get_ports FMC0_RX1_N]

set_property PACKAGE_PIN F2 [get_ports FMC0_RX2_P]
set_property PACKAGE_PIN F1 [get_ports FMC0_RX2_N]

set_property PACKAGE_PIN K2 [get_ports FMC0_RX3_P]
set_property PACKAGE_PIN K1 [get_ports FMC0_RX3_N]

set_property PACKAGE_PIN L4 [get_ports FMC0_RX4_P]
set_property PACKAGE_PIN L3 [get_ports FMC0_RX4_N]

set_property PACKAGE_PIN P2 [get_ports FMC0_RX5_P]
set_property PACKAGE_PIN P1 [get_ports FMC0_RX5_N]

set_property PACKAGE_PIN T2 [get_ports FMC0_RX6_P]
set_property PACKAGE_PIN T1 [get_ports FMC0_RX6_N]

set_property PACKAGE_PIN M2 [get_ports FMC0_RX7_P]
set_property PACKAGE_PIN M1 [get_ports FMC0_RX7_N]


## ----- setting GTH channes for FMC DAC
set_property PACKAGE_PIN G4 [get_ports FMC0_TX0_P]
set_property PACKAGE_PIN G3 [get_ports FMC0_TX0_N]

set_property PACKAGE_PIN H6 [get_ports FMC0_TX1_P]
set_property PACKAGE_PIN H5 [get_ports FMC0_TX1_N]

set_property PACKAGE_PIN F6 [get_ports FMC0_TX2_P]
set_property PACKAGE_PIN F5 [get_ports FMC0_TX2_N]

set_property PACKAGE_PIN K6 [get_ports FMC0_TX3_P]
set_property PACKAGE_PIN K5 [get_ports FMC0_TX3_N]

set_property PACKAGE_PIN M6 [get_ports FMC0_TX4_P]
set_property PACKAGE_PIN M5 [get_ports FMC0_TX4_N]

set_property PACKAGE_PIN P6 [get_ports FMC0_TX5_P]
set_property PACKAGE_PIN P5 [get_ports FMC0_TX5_N]

set_property PACKAGE_PIN R4 [get_ports FMC0_TX6_P]
set_property PACKAGE_PIN R3 [get_ports FMC0_TX6_N]

set_property PACKAGE_PIN N4 [get_ports FMC0_TX7_P]
set_property PACKAGE_PIN N3 [get_ports FMC0_TX7_N]


## ====== FMC0 IOSTANDARD

## ------ FMC0 LA
# set_property IOSTANDARD LVCMOS18	[get_ports {FMC0_LA_P11}	]
# set_property IOSTANDARD LVCMOS18	[get_ports {FMC0_LA_N11}	]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_P12]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_N12]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_P13]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_N13]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_P14]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_N14]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_P15]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_N15]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_P16]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_N16]
# set_property IOSTANDARD LVCMOS18	[get_ports {FMC0_LA_P17}	]
# set_property IOSTANDARD LVCMOS18	[get_ports {FMC0_LA_N17}	]
# set_property IOSTANDARD LVCMOS18	[get_ports {FMC0_LA_P18}	]
# set_property IOSTANDARD LVCMOS18	[get_ports {FMC0_LA_N18}	]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_P19]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_N19]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_P20]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_N20]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_P21]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_N21]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_P22]
# set_property IOSTANDARD LVCMOS18	[get_ports {FMC0_LA_N22}	]
# set_property IOSTANDARD LVCMOS18	[get_ports {FMC0_LA_P23}	]
# set_property IOSTANDARD LVCMOS18	[get_ports {FMC0_LA_N23}	]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_P24]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_N24]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_P25]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_N25]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_P26]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_N26]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_P27]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_N27]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_P28]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_N28]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_P29]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_N29]
# set_property IOSTANDARD LVCMOS18	[get_ports {FMC0_LA_P30}	]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_N30]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_P31]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_N31]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_P32]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_N32]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_P33]
set_property IOSTANDARD LVCMOS18 [get_ports FMC0_LA_N33]





## ======== Bitstream settings ========
#set_property CFGBVS VCCO [current_design]
#set_property CONFIG_VOLTAGE 3.3 [current_design]
set_property BITSTREAM.GENERAL.COMPRESS TRUE [current_design]
set_property BITSTREAM.CONFIG.UNUSEDPIN PULLNONE [current_design]
#set_property BITSTREAM.CONFIG.SPI_32BIT_ADDR YES [current_design]
#set_property BITSTREAM.CONFIG.SPI_BUSWIDTH 4 [current_design]
#set_property BITSTREAM.CONFIG.SPI_FALL_EDGE YES [current_design]
#set_property BITSTREAM.CONFIG.EXTMASTERCCLK_EN DIV-2 [current_design]
