----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/07/2023 09:00:09 AM
-- Design Name: 
-- Module Name: interpolator20x16 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity interpolator20x16 is
    Port ( write_clk : in STD_LOGIC;
           read_clk : in STD_LOGIC;
           rst : in STD_LOGIC;
           I0 : in STD_LOGIC_VECTOR (11 downto 0);
           I1 : in STD_LOGIC_VECTOR (11 downto 0);
           I2 : in STD_LOGIC_VECTOR (11 downto 0);
           I3 : in STD_LOGIC_VECTOR (11 downto 0);
           I4 : in STD_LOGIC_VECTOR (11 downto 0);
           I5 : in STD_LOGIC_VECTOR (11 downto 0);
           I6 : in STD_LOGIC_VECTOR (11 downto 0);
           I7 : in STD_LOGIC_VECTOR (11 downto 0);
           I8 : in STD_LOGIC_VECTOR (11 downto 0);
           I9 : in STD_LOGIC_VECTOR (11 downto 0);
           I10 : in STD_LOGIC_VECTOR (11 downto 0);
           I11 : in STD_LOGIC_VECTOR (11 downto 0);
           I12 : in STD_LOGIC_VECTOR (11 downto 0);
           I13 : in STD_LOGIC_VECTOR (11 downto 0);
           I14 : in STD_LOGIC_VECTOR (11 downto 0);
           I15 : in STD_LOGIC_VECTOR (11 downto 0);
           I16 : in STD_LOGIC_VECTOR (11 downto 0);
           I17 : in STD_LOGIC_VECTOR (11 downto 0);
           I18 : in STD_LOGIC_VECTOR (11 downto 0);
           I19 : in STD_LOGIC_VECTOR (11 downto 0);
           O0 : out STD_LOGIC_VECTOR (15 downto 0);
           O1 : out STD_LOGIC_VECTOR (15 downto 0);
           O2 : out STD_LOGIC_VECTOR (15 downto 0);
           O3 : out STD_LOGIC_VECTOR (15 downto 0);
           O4 : out STD_LOGIC_VECTOR (15 downto 0);
           O5 : out STD_LOGIC_VECTOR (15 downto 0);
           O6 : out STD_LOGIC_VECTOR (15 downto 0);
           O7 : out STD_LOGIC_VECTOR (15 downto 0);
           O8 : out STD_LOGIC_VECTOR (15 downto 0);
           O9 : out STD_LOGIC_VECTOR (15 downto 0);
           O10 : out STD_LOGIC_VECTOR (15 downto 0);
           O11 : out STD_LOGIC_VECTOR (15 downto 0);
           O12 : out STD_LOGIC_VECTOR (15 downto 0);
           O13 : out STD_LOGIC_VECTOR (15 downto 0);
           O14 : out STD_LOGIC_VECTOR (15 downto 0);
           O15 : out STD_LOGIC_VECTOR (15 downto 0));
end interpolator20x16;

architecture Behavioral of interpolator20x16 is

component interpolator_block5x4 is
    Port ( I0 : in STD_LOGIC_VECTOR (11 downto 0);
           I1 : in STD_LOGIC_VECTOR (11 downto 0);
           I2 : in STD_LOGIC_VECTOR (11 downto 0);
           I3 : in STD_LOGIC_VECTOR (11 downto 0);
           I4 : in STD_LOGIC_VECTOR (11 downto 0);
           O0 : out STD_LOGIC_VECTOR (15 downto 0);
           O1 : out STD_LOGIC_VECTOR (15 downto 0);
           O2 : out STD_LOGIC_VECTOR (15 downto 0);
           O3 : out STD_LOGIC_VECTOR (15 downto 0));
end component;


signal           I0_s :  STD_LOGIC_VECTOR (11 downto 0);
signal           I1_s :  STD_LOGIC_VECTOR (11 downto 0);
signal           I2_s :  STD_LOGIC_VECTOR (11 downto 0);
signal           I3_s :  STD_LOGIC_VECTOR (11 downto 0);
signal           I4_s :  STD_LOGIC_VECTOR (11 downto 0);
signal           I5_s :  STD_LOGIC_VECTOR (11 downto 0);
signal           I6_s :  STD_LOGIC_VECTOR (11 downto 0);
signal           I7_s :  STD_LOGIC_VECTOR (11 downto 0);
signal           I8_s :  STD_LOGIC_VECTOR (11 downto 0);
signal           I9_s :  STD_LOGIC_VECTOR (11 downto 0);
signal           I10_s :  STD_LOGIC_VECTOR (11 downto 0);
signal           I11_s :  STD_LOGIC_VECTOR (11 downto 0);
signal           I12_s :  STD_LOGIC_VECTOR (11 downto 0);
signal           I13_s :  STD_LOGIC_VECTOR (11 downto 0);
signal           I14_s :  STD_LOGIC_VECTOR (11 downto 0);
signal           I15_s :  STD_LOGIC_VECTOR (11 downto 0);
signal           I16_s :  STD_LOGIC_VECTOR (11 downto 0);
signal           I17_s :  STD_LOGIC_VECTOR (11 downto 0);
signal           I18_s :  STD_LOGIC_VECTOR (11 downto 0);
signal           I19_s :  STD_LOGIC_VECTOR (11 downto 0);


signal O0_s : std_logic_vector(15 downto 0);
signal O1_s : std_logic_vector(15 downto 0);
signal O2_s : std_logic_vector(15 downto 0);
signal O3_s : std_logic_vector(15 downto 0);
signal O4_s : std_logic_vector(15 downto 0);
signal O5_s : std_logic_vector(15 downto 0);
signal O6_s : std_logic_vector(15 downto 0);
signal O7_s : std_logic_vector(15 downto 0);
signal O8_s : std_logic_vector(15 downto 0);
signal O9_s : std_logic_vector(15 downto 0);
signal O10_s : std_logic_vector(15 downto 0);
signal O11_s : std_logic_vector(15 downto 0);
signal O12_s : std_logic_vector(15 downto 0);
signal O13_s : std_logic_vector(15 downto 0);
signal O14_s : std_logic_vector(15 downto 0);
signal O15_s : std_logic_vector(15 downto 0);

signal fifo_in : std_logic_vector(255 downto 0);
signal fifo_out : std_logic_vector(255 downto 0);
signal fifo_wr_en : std_logic;
signal fifo_rd_en : std_logic;
signal fifo_full : std_logic;
signal fifo_empty : std_logic;
signal fifo_wr_rst_busy : std_logic;
signal fifo_rd_rst_busy : std_logic;
signal rstn : std_logic;

component fifo_generator_0 IS
  PORT (
    rst : IN STD_LOGIC;
    wr_clk : IN STD_LOGIC;
    rd_clk : IN STD_LOGIC;
    din : IN STD_LOGIC_VECTOR(255 DOWNTO 0);
    wr_en : IN STD_LOGIC;
    rd_en : IN STD_LOGIC;
    dout : OUT STD_LOGIC_VECTOR(255 DOWNTO 0);
    full : OUT STD_LOGIC;
    empty : OUT STD_LOGIC;
    wr_rst_busy : OUT STD_LOGIC;
    rd_rst_busy : OUT STD_LOGIC
  );
END component;

begin

rstn<= not rst;
fifo_wr_en <=rst;
fifo_rd_en <=rst;

interpolator0 : interpolator_block5x4
port map(I0=>I0_s,I1=>I1_s,I2=>I2_s,I3=>I3_s,I4=>I4_s,O0=>O0_s,O1=>O1_s,O2=>O2_s,O3=>O3_s);

interpolator1 : interpolator_block5x4
port map(I0=>I5_s,I1=>I6_s,I2=>I7_s,I3=>I8_s,I4=>I9_s,O0=>O4_s,O1=>O5_s,O2=>O6_s,O3=>O7_s);

interpolator2 : interpolator_block5x4
port map(I0=>I10_s,I1=>I11_s,I2=>I12_s,I3=>I13_s,I4=>I14_s,O0=>O8_s,O1=>O9_s,O2=>O10_s,O3=>O11_s);

interpolator3 : interpolator_block5x4
port map(I0=>I15_s,I1=>I16_s,I2=>I17_s,I3=>I18_s,I4=>I19_s,O0=>O12_s,O1=>O13_s,O2=>O14_s,O3=>O15_s);


fifo_in<= O0_s & O1_s & O2_s & O3_s &O4_s &O5_s &O6_s &O7_s &O8_s &O9_s &O10_s &O11_s &O12_s &O13_s &O14_s &O15_s ;

O0<=fifo_out(255 downto 240);
O1<=fifo_out(239 downto 224);
O2<=fifo_out(223 downto 208);
O3<=fifo_out(207 downto 192);
O4<=fifo_out(191 downto 176);
O5<=fifo_out(175 downto 160);
O6<=fifo_out(159 downto 144);
O7<=fifo_out(143 downto 128);
O8<=fifo_out(127 downto 112);
O9<=fifo_out(111 downto 96);
O10<=fifo_out(95 downto 80);
O11<=fifo_out(79 downto 64);
O12<=fifo_out(63 downto 48);
O13<=fifo_out(47 downto 32);
O14<=fifo_out(31 downto 16);
O15<=fifo_out(15 downto 0);

fifo : fifo_generator_0
  port map (
    rst=>rstn,
    wr_clk=>write_clk,
    rd_clk=>read_clk,
    din=>fifo_in,
    wr_en=>fifo_wr_en,
    rd_en =>fifo_rd_en,
    dout=>fifo_out,
    full=>fifo_full,
    empty=>fifo_full,
    wr_rst_busy=>fifo_wr_rst_busy,
    rd_rst_busy=>fifo_rd_rst_busy
  );

inputProcess: process(write_clk) 
begin
if rising_edge(write_clk) then
    I0_s<=I0;
    I1_s<=I1;
    I2_s<=I2;
    I3_s<=I3;
    I4_s<=I4;
    I5_s<=I5;
    I6_s<=I6;
    I7_s<=I7;
    I8_s<=I8;
    I9_s<=I9;
    I10_s<=I10;
    I11_s<=I11;
    I12_s<=I12;
    I13_s<=I13;
    I14_s<=I14;
    I15_s<=I15;
    I16_s<=I16;
    I17_s<=I17;
    I18_s<=I18;
    I19_s<=I19;
end if;

end process;




end Behavioral;

