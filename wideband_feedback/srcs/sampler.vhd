----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/06/2023 06:21:59 PM
-- Design Name: 
-- Module Name: sampler - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity sampler is
    Port ( clk : in STD_LOGIC;
           tdata : in STD_LOGIC_VECTOR (15 downto 0);
           out0 : out STD_LOGIC_VECTOR (11 downto 0);
           out1 : out STD_LOGIC_VECTOR (11 downto 0);
           out2 : out STD_LOGIC_VECTOR (11 downto 0);
           out3 : out STD_LOGIC_VECTOR (11 downto 0);
           out4 : out STD_LOGIC_VECTOR (11 downto 0));
end sampler;

architecture Behavioral of sampler is
type State is (state0,state1,state2,state3,state4);

signal currentState : State :=state0; 
signal out0_s : std_logic_vector(11 downto 0);
signal out1_s : std_logic_vector(11 downto 0);
signal out2_s : std_logic_vector(11 downto 0);
signal out3_s : std_logic_vector(11 downto 0);
begin

process(clk) is 

begin
if rising_edge(clk) then
case currentState is
    when state0=>
        out0_s<=tdata(15 downto 4);
        currentState<=state1;
    when state1=>
        out1_s<=tdata(15 downto 4);
        currentState<=state2;
    when state2=>
        out2_s<=tdata(15 downto 4);
        currentState<=state3;
    when state3=>
        out3_s<=tdata(15 downto 4);
        currentState<=state4;
    when state4=>
        out0<=out0_s;
        out1<=out1_s;
        out2<=out2_s;
        out3<=out3_s;
        out4<=tdata(15 downto 4);
        currentState<=state0;
end case;

end if;

end process;


end Behavioral;
