--Copyright 1986-2019 Xilinx, Inc. All Rights Reserved.
----------------------------------------------------------------------------------
--Tool Version: Vivado v.2019.2_AR72614 (lin64) Build 2708876 Wed Nov  6 21:39:14 MST 2019
--Date        : Thu Mar 16 15:39:02 2023
--Host        : PCBE16571 running 64-bit Ubuntu 22.04.1 LTS
--Command     : generate_target design_1_wrapper.bd
--Design      : design_1_wrapper
--Purpose     : IP block netlist
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
library UNISIM;
use UNISIM.VCOMPONENTS.ALL;
entity design_1_wrapper is
  port (
    FMC0_GBTCLK0_N : in STD_LOGIC;
    FMC0_GBTCLK0_P : in STD_LOGIC;
    FMC0_GBTCLK1_N : in STD_LOGIC;
    FMC0_GBTCLK1_P : in STD_LOGIC;
    FMC0_LA_N0 : in STD_LOGIC;
    FMC0_LA_N1 : in STD_LOGIC;
    FMC0_LA_N10 : out STD_LOGIC;
    FMC0_LA_N11 : out STD_LOGIC;
    FMC0_LA_N12 : out STD_LOGIC;
    FMC0_LA_N13 : out STD_LOGIC;
    FMC0_LA_N14 : in STD_LOGIC;
    FMC0_LA_N15 : out STD_LOGIC;
    FMC0_LA_N16 : out STD_LOGIC;
    FMC0_LA_N17 : in STD_LOGIC;
    FMC0_LA_N18 : in STD_LOGIC;
    FMC0_LA_N19 : in STD_LOGIC;
    FMC0_LA_N2 : out STD_LOGIC;
    FMC0_LA_N20 : out STD_LOGIC;
    FMC0_LA_N21 : out STD_LOGIC;
    FMC0_LA_N23 : out STD_LOGIC;
    FMC0_LA_N24 : out STD_LOGIC;
    FMC0_LA_N25 : out STD_LOGIC;
    FMC0_LA_N26 : out STD_LOGIC;
    FMC0_LA_N27 : out STD_LOGIC;
    FMC0_LA_N28 : in STD_LOGIC;
    FMC0_LA_N29 : out STD_LOGIC;
    FMC0_LA_N3 : out STD_LOGIC;
    FMC0_LA_N30 : inout STD_LOGIC;
    FMC0_LA_N31 : out STD_LOGIC;
    FMC0_LA_N32 : out STD_LOGIC;
    FMC0_LA_N33 : out STD_LOGIC;
    FMC0_LA_N4 : out STD_LOGIC;
    FMC0_LA_N5 : in STD_LOGIC;
    FMC0_LA_N6 : in STD_LOGIC;
    FMC0_LA_N7 : in STD_LOGIC;
    FMC0_LA_N8 : out STD_LOGIC;
    FMC0_LA_N9 : out STD_LOGIC;
    FMC0_LA_P0 : in STD_LOGIC;
    FMC0_LA_P1 : in STD_LOGIC;
    FMC0_LA_P10 : out STD_LOGIC;
    FMC0_LA_P11 : out STD_LOGIC;
    FMC0_LA_P12 : out STD_LOGIC;
    FMC0_LA_P13 : out STD_LOGIC;
    FMC0_LA_P14 : in STD_LOGIC;
    FMC0_LA_P15 : out STD_LOGIC;
    FMC0_LA_P16 : out STD_LOGIC;
    FMC0_LA_P17 : in STD_LOGIC;
    FMC0_LA_P18 : in STD_LOGIC;
    FMC0_LA_P19 : out STD_LOGIC;
    FMC0_LA_P2 : out STD_LOGIC;
    FMC0_LA_P20 : out STD_LOGIC;
    FMC0_LA_P21 : in STD_LOGIC;
    FMC0_LA_P22 : out STD_LOGIC;
    FMC0_LA_P23 : out STD_LOGIC;
    FMC0_LA_P24 : out STD_LOGIC;
    FMC0_LA_P25 : inout STD_LOGIC;
    FMC0_LA_P26 : out STD_LOGIC;
    FMC0_LA_P27 : out STD_LOGIC;
    FMC0_LA_P28 : in STD_LOGIC;
    FMC0_LA_P29 : out STD_LOGIC;
    FMC0_LA_P3 : out STD_LOGIC;
    FMC0_LA_P31 : out STD_LOGIC;
    FMC0_LA_P32 : out STD_LOGIC;
    FMC0_LA_P33 : out STD_LOGIC;
    FMC0_LA_P4 : out STD_LOGIC;
    FMC0_LA_P5 : in STD_LOGIC;
    FMC0_LA_P6 : in STD_LOGIC;
    FMC0_LA_P7 : in STD_LOGIC;
    FMC0_LA_P8 : out STD_LOGIC;
    FMC0_LA_P9 : out STD_LOGIC;
    FMC0_RX0_N : in STD_LOGIC;
    FMC0_RX0_P : in STD_LOGIC;
    FMC0_RX1_N : in STD_LOGIC;
    FMC0_RX1_P : in STD_LOGIC;
    FMC0_RX2_N : in STD_LOGIC;
    FMC0_RX2_P : in STD_LOGIC;
    FMC0_RX3_N : in STD_LOGIC;
    FMC0_RX3_P : in STD_LOGIC;
    FMC0_RX4_N : in STD_LOGIC;
    FMC0_RX4_P : in STD_LOGIC;
    FMC0_RX5_N : in STD_LOGIC;
    FMC0_RX5_P : in STD_LOGIC;
    FMC0_RX6_N : in STD_LOGIC;
    FMC0_RX6_P : in STD_LOGIC;
    FMC0_RX7_N : in STD_LOGIC;
    FMC0_RX7_P : in STD_LOGIC;
    FMC0_TX0_N : out STD_LOGIC;
    FMC0_TX0_P : out STD_LOGIC;
    FMC0_TX1_N : out STD_LOGIC;
    FMC0_TX1_P : out STD_LOGIC;
    FMC0_TX2_N : out STD_LOGIC;
    FMC0_TX2_P : out STD_LOGIC;
    FMC0_TX3_N : out STD_LOGIC;
    FMC0_TX3_P : out STD_LOGIC;
    FMC0_TX4_N : out STD_LOGIC;
    FMC0_TX4_P : out STD_LOGIC;
    FMC0_TX5_N : out STD_LOGIC;
    FMC0_TX5_P : out STD_LOGIC;
    FMC0_TX6_N : out STD_LOGIC;
    FMC0_TX6_P : out STD_LOGIC;
    FMC0_TX7_N : out STD_LOGIC;
    FMC0_TX7_P : out STD_LOGIC;
    dip_switches_8bits_tri_i : in STD_LOGIC_VECTOR ( 7 downto 0 );
    led_8bits_tri_o : out STD_LOGIC_VECTOR ( 7 downto 0 )
  );
end design_1_wrapper;

architecture STRUCTURE of design_1_wrapper is
  component design_1 is
  port (
    FMC0_LA_P2 : out STD_LOGIC;
    FMC0_LA_N2 : out STD_LOGIC;
    FMC0_LA_P3 : out STD_LOGIC;
    FMC0_LA_N3 : out STD_LOGIC;
    FMC0_LA_P4 : out STD_LOGIC;
    FMC0_LA_N4 : out STD_LOGIC;
    FMC0_LA_P8 : out STD_LOGIC;
    FMC0_LA_N8 : out STD_LOGIC;
    FMC0_LA_P9 : out STD_LOGIC;
    FMC0_LA_N9 : out STD_LOGIC;
    FMC0_LA_P10 : out STD_LOGIC;
    FMC0_LA_N10 : out STD_LOGIC;
    FMC0_LA_P11 : out STD_LOGIC;
    FMC0_LA_N11 : out STD_LOGIC;
    FMC0_LA_P12 : out STD_LOGIC;
    FMC0_LA_N12 : out STD_LOGIC;
    FMC0_LA_P13 : out STD_LOGIC;
    FMC0_LA_N13 : out STD_LOGIC;
    FMC0_LA_P15 : out STD_LOGIC;
    FMC0_LA_N15 : out STD_LOGIC;
    FMC0_LA_P16 : out STD_LOGIC;
    FMC0_LA_N16 : out STD_LOGIC;
    FMC0_LA_P20 : out STD_LOGIC;
    FMC0_LA_P19 : out STD_LOGIC;
    FMC0_LA_N20 : out STD_LOGIC;
    FMC0_LA_N21 : out STD_LOGIC;
    FMC0_LA_P22 : out STD_LOGIC;
    FMC0_LA_P23 : out STD_LOGIC;
    FMC0_LA_N23 : out STD_LOGIC;
    FMC0_LA_P24 : out STD_LOGIC;
    FMC0_LA_N24 : out STD_LOGIC;
    FMC0_LA_P25 : inout STD_LOGIC;
    FMC0_LA_N25 : out STD_LOGIC;
    FMC0_LA_P26 : out STD_LOGIC;
    FMC0_LA_N26 : out STD_LOGIC;
    FMC0_LA_P27 : out STD_LOGIC;
    FMC0_LA_N27 : out STD_LOGIC;
    FMC0_LA_P29 : out STD_LOGIC;
    FMC0_LA_N29 : out STD_LOGIC;
    FMC0_LA_N30 : inout STD_LOGIC;
    FMC0_LA_P31 : out STD_LOGIC;
    FMC0_LA_N31 : out STD_LOGIC;
    FMC0_LA_P32 : out STD_LOGIC;
    FMC0_LA_N32 : out STD_LOGIC;
    FMC0_LA_P33 : out STD_LOGIC;
    FMC0_LA_N33 : out STD_LOGIC;
    FMC0_TX0_P : out STD_LOGIC;
    FMC0_TX0_N : out STD_LOGIC;
    FMC0_TX1_P : out STD_LOGIC;
    FMC0_TX1_N : out STD_LOGIC;
    FMC0_TX2_P : out STD_LOGIC;
    FMC0_TX2_N : out STD_LOGIC;
    FMC0_TX3_P : out STD_LOGIC;
    FMC0_TX3_N : out STD_LOGIC;
    FMC0_TX4_P : out STD_LOGIC;
    FMC0_TX4_N : out STD_LOGIC;
    FMC0_TX5_P : out STD_LOGIC;
    FMC0_TX5_N : out STD_LOGIC;
    FMC0_TX6_P : out STD_LOGIC;
    FMC0_TX6_N : out STD_LOGIC;
    FMC0_TX7_P : out STD_LOGIC;
    FMC0_TX7_N : out STD_LOGIC;
    FMC0_LA_P0 : in STD_LOGIC;
    FMC0_LA_N0 : in STD_LOGIC;
    FMC0_LA_P1 : in STD_LOGIC;
    FMC0_LA_N1 : in STD_LOGIC;
    FMC0_LA_P5 : in STD_LOGIC;
    FMC0_LA_N5 : in STD_LOGIC;
    FMC0_LA_P6 : in STD_LOGIC;
    FMC0_LA_N6 : in STD_LOGIC;
    FMC0_LA_P7 : in STD_LOGIC;
    FMC0_LA_N7 : in STD_LOGIC;
    FMC0_LA_P14 : in STD_LOGIC;
    FMC0_LA_N14 : in STD_LOGIC;
    FMC0_LA_P17 : in STD_LOGIC;
    FMC0_LA_N17 : in STD_LOGIC;
    FMC0_LA_P18 : in STD_LOGIC;
    FMC0_LA_N18 : in STD_LOGIC;
    FMC0_LA_N19 : in STD_LOGIC;
    FMC0_LA_P21 : in STD_LOGIC;
    FMC0_LA_P28 : in STD_LOGIC;
    FMC0_LA_N28 : in STD_LOGIC;
    FMC0_RX0_P : in STD_LOGIC;
    FMC0_RX0_N : in STD_LOGIC;
    FMC0_RX1_P : in STD_LOGIC;
    FMC0_RX1_N : in STD_LOGIC;
    FMC0_RX2_P : in STD_LOGIC;
    FMC0_RX2_N : in STD_LOGIC;
    FMC0_RX3_P : in STD_LOGIC;
    FMC0_RX3_N : in STD_LOGIC;
    FMC0_RX4_P : in STD_LOGIC;
    FMC0_RX4_N : in STD_LOGIC;
    FMC0_RX5_P : in STD_LOGIC;
    FMC0_RX5_N : in STD_LOGIC;
    FMC0_RX6_P : in STD_LOGIC;
    FMC0_RX6_N : in STD_LOGIC;
    FMC0_RX7_P : in STD_LOGIC;
    FMC0_RX7_N : in STD_LOGIC;
    FMC0_GBTCLK0_P : in STD_LOGIC;
    FMC0_GBTCLK0_N : in STD_LOGIC;
    FMC0_GBTCLK1_P : in STD_LOGIC;
    FMC0_GBTCLK1_N : in STD_LOGIC;
    led_8bits_tri_o : out STD_LOGIC_VECTOR ( 7 downto 0 );
    dip_switches_8bits_tri_i : in STD_LOGIC_VECTOR ( 7 downto 0 )
  );
  end component design_1;
begin
design_1_i: component design_1
     port map (
      FMC0_GBTCLK0_N => FMC0_GBTCLK0_N,
      FMC0_GBTCLK0_P => FMC0_GBTCLK0_P,
      FMC0_GBTCLK1_N => FMC0_GBTCLK1_N,
      FMC0_GBTCLK1_P => FMC0_GBTCLK1_P,
      FMC0_LA_N0 => FMC0_LA_N0,
      FMC0_LA_N1 => FMC0_LA_N1,
      FMC0_LA_N10 => FMC0_LA_N10,
      FMC0_LA_N11 => FMC0_LA_N11,
      FMC0_LA_N12 => FMC0_LA_N12,
      FMC0_LA_N13 => FMC0_LA_N13,
      FMC0_LA_N14 => FMC0_LA_N14,
      FMC0_LA_N15 => FMC0_LA_N15,
      FMC0_LA_N16 => FMC0_LA_N16,
      FMC0_LA_N17 => FMC0_LA_N17,
      FMC0_LA_N18 => FMC0_LA_N18,
      FMC0_LA_N19 => FMC0_LA_N19,
      FMC0_LA_N2 => FMC0_LA_N2,
      FMC0_LA_N20 => FMC0_LA_N20,
      FMC0_LA_N21 => FMC0_LA_N21,
      FMC0_LA_N23 => FMC0_LA_N23,
      FMC0_LA_N24 => FMC0_LA_N24,
      FMC0_LA_N25 => FMC0_LA_N25,
      FMC0_LA_N26 => FMC0_LA_N26,
      FMC0_LA_N27 => FMC0_LA_N27,
      FMC0_LA_N28 => FMC0_LA_N28,
      FMC0_LA_N29 => FMC0_LA_N29,
      FMC0_LA_N3 => FMC0_LA_N3,
      FMC0_LA_N30 => FMC0_LA_N30,
      FMC0_LA_N31 => FMC0_LA_N31,
      FMC0_LA_N32 => FMC0_LA_N32,
      FMC0_LA_N33 => FMC0_LA_N33,
      FMC0_LA_N4 => FMC0_LA_N4,
      FMC0_LA_N5 => FMC0_LA_N5,
      FMC0_LA_N6 => FMC0_LA_N6,
      FMC0_LA_N7 => FMC0_LA_N7,
      FMC0_LA_N8 => FMC0_LA_N8,
      FMC0_LA_N9 => FMC0_LA_N9,
      FMC0_LA_P0 => FMC0_LA_P0,
      FMC0_LA_P1 => FMC0_LA_P1,
      FMC0_LA_P10 => FMC0_LA_P10,
      FMC0_LA_P11 => FMC0_LA_P11,
      FMC0_LA_P12 => FMC0_LA_P12,
      FMC0_LA_P13 => FMC0_LA_P13,
      FMC0_LA_P14 => FMC0_LA_P14,
      FMC0_LA_P15 => FMC0_LA_P15,
      FMC0_LA_P16 => FMC0_LA_P16,
      FMC0_LA_P17 => FMC0_LA_P17,
      FMC0_LA_P18 => FMC0_LA_P18,
      FMC0_LA_P19 => FMC0_LA_P19,
      FMC0_LA_P2 => FMC0_LA_P2,
      FMC0_LA_P20 => FMC0_LA_P20,
      FMC0_LA_P21 => FMC0_LA_P21,
      FMC0_LA_P22 => FMC0_LA_P22,
      FMC0_LA_P23 => FMC0_LA_P23,
      FMC0_LA_P24 => FMC0_LA_P24,
      FMC0_LA_P25 => FMC0_LA_P25,
      FMC0_LA_P26 => FMC0_LA_P26,
      FMC0_LA_P27 => FMC0_LA_P27,
      FMC0_LA_P28 => FMC0_LA_P28,
      FMC0_LA_P29 => FMC0_LA_P29,
      FMC0_LA_P3 => FMC0_LA_P3,
      FMC0_LA_P31 => FMC0_LA_P31,
      FMC0_LA_P32 => FMC0_LA_P32,
      FMC0_LA_P33 => FMC0_LA_P33,
      FMC0_LA_P4 => FMC0_LA_P4,
      FMC0_LA_P5 => FMC0_LA_P5,
      FMC0_LA_P6 => FMC0_LA_P6,
      FMC0_LA_P7 => FMC0_LA_P7,
      FMC0_LA_P8 => FMC0_LA_P8,
      FMC0_LA_P9 => FMC0_LA_P9,
      FMC0_RX0_N => FMC0_RX0_N,
      FMC0_RX0_P => FMC0_RX0_P,
      FMC0_RX1_N => FMC0_RX1_N,
      FMC0_RX1_P => FMC0_RX1_P,
      FMC0_RX2_N => FMC0_RX2_N,
      FMC0_RX2_P => FMC0_RX2_P,
      FMC0_RX3_N => FMC0_RX3_N,
      FMC0_RX3_P => FMC0_RX3_P,
      FMC0_RX4_N => FMC0_RX4_N,
      FMC0_RX4_P => FMC0_RX4_P,
      FMC0_RX5_N => FMC0_RX5_N,
      FMC0_RX5_P => FMC0_RX5_P,
      FMC0_RX6_N => FMC0_RX6_N,
      FMC0_RX6_P => FMC0_RX6_P,
      FMC0_RX7_N => FMC0_RX7_N,
      FMC0_RX7_P => FMC0_RX7_P,
      FMC0_TX0_N => FMC0_TX0_N,
      FMC0_TX0_P => FMC0_TX0_P,
      FMC0_TX1_N => FMC0_TX1_N,
      FMC0_TX1_P => FMC0_TX1_P,
      FMC0_TX2_N => FMC0_TX2_N,
      FMC0_TX2_P => FMC0_TX2_P,
      FMC0_TX3_N => FMC0_TX3_N,
      FMC0_TX3_P => FMC0_TX3_P,
      FMC0_TX4_N => FMC0_TX4_N,
      FMC0_TX4_P => FMC0_TX4_P,
      FMC0_TX5_N => FMC0_TX5_N,
      FMC0_TX5_P => FMC0_TX5_P,
      FMC0_TX6_N => FMC0_TX6_N,
      FMC0_TX6_P => FMC0_TX6_P,
      FMC0_TX7_N => FMC0_TX7_N,
      FMC0_TX7_P => FMC0_TX7_P,
      dip_switches_8bits_tri_i(7 downto 0) => dip_switches_8bits_tri_i(7 downto 0),
      led_8bits_tri_o(7 downto 0) => led_8bits_tri_o(7 downto 0)
    );
end STRUCTURE;
