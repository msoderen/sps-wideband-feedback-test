----------------------------------------------------------------------------------
-- Company: 
-- Engineer: 
-- 
-- Create Date: 03/06/2023 05:36:02 PM
-- Design Name: 
-- Module Name: interpolator_block5x4 - Behavioral
-- Project Name: 
-- Target Devices: 
-- Tool Versions: 
-- Description: 
-- 
-- Dependencies: 
-- 
-- Revision:
-- Revision 0.01 - File Created
-- Additional Comments:
-- 
----------------------------------------------------------------------------------


library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx leaf cells in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity interpolator_block5x4 is
    Port ( I0 : in STD_LOGIC_VECTOR (11 downto 0);
           I1 : in STD_LOGIC_VECTOR (11 downto 0);
           I2 : in STD_LOGIC_VECTOR (11 downto 0);
           I3 : in STD_LOGIC_VECTOR (11 downto 0);
           I4 : in STD_LOGIC_VECTOR (11 downto 0);
           O0 : out STD_LOGIC_VECTOR (15 downto 0);
           O1 : out STD_LOGIC_VECTOR (15 downto 0);
           O2 : out STD_LOGIC_VECTOR (15 downto 0);
           O3 : out STD_LOGIC_VECTOR (15 downto 0));
end interpolator_block5x4;

architecture Behavioral of interpolator_block5x4 is

signal I0_0 : signed(15 downto 0);
signal I1_0 : signed(15 downto 0);
signal I2_0 : signed(15 downto 0);
signal I3_0 : signed(15 downto 0);
signal I4_0 : signed(15 downto 0);

signal I0_1 : signed(15 downto 0);
signal I1_1 : signed(15 downto 0);
signal I2_1 : signed(15 downto 0);
signal I3_1 : signed(15 downto 0);
signal I4_1 : signed(15 downto 0);

signal I1_2 : signed(15 downto 0);
signal I2_2 : signed(15 downto 0);
signal I3_2 : signed(15 downto 0);
signal I4_2 : signed(15 downto 0);

begin

--extend to 16 bit
I0_0<=resize(signed(I0), I0_0'length);
I1_0<=resize(signed(I1), I1_0'length);
I2_0<=resize(signed(I2), I2_0'length);
I3_0<=resize(signed(I3), I3_0'length);
I4_0<=resize(signed(I4), I4_0'length);


--div by 4
--I1_1<=I1_0/4;
--I2_1<=I2_0/4;
--I3_1<=I3_0/4;
--I4_1<=I4_0/4;
--for maximum dynamic range we left shift by 4 and then right shift by 2 to divide by 4 -> shift left 2
I0_1<=shift_left(I0_0,4);
I1_1<=shift_left(I1_0,2);
I2_1<=shift_left(I2_0,2);
I3_1<=shift_left(I3_0,2);
I4_1<=shift_left(I4_0,2);

--x3
I1_2<= I1_1+I1_1+I1_1;
--x2
I2_2<=I2_1+ I2_1;
--x2
I3_2<=I3_1+ I3_1;

--x3
I4_2<= I4_1+I4_1+I4_1;
O0<=std_logic_vector(I0_1);

O1<=std_logic_vector(I1_2+I2_1);

O2<=std_logic_vector(I2_2+I3_2);

O3<=std_logic_vector(I3_1+I4_2);


end Behavioral;
